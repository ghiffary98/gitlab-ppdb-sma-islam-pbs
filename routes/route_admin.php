<?php

if (isset($_GET['menu'])) {
	switch ($_GET['menu']) {

		case 'home':
			include "views/admin/home/view_home.php";
			break;

		case 'data_siswa_master':
			include "views/admin/data_siswa/data_siswa/view_data_siswa_master.php";
			break;

		case 'export_data_siswa_format_8355':
			include "views/admin/data_siswa/export_data/view_export_data_siswa_format_8335.php";
			break;

		case 'export_data_siswa_format_raport':
			include "views/admin/data_siswa/export_data/view_export_data_siswa_format_rapor.php";
			break;

		case 'export_data_siswa_format_all':
			include "views/admin/data_siswa/export_data/view_export_data_siswa_format_all.php";
			break;
			
		case 'pengumuman_umum':
			include "views/admin/pengumuman/pengumuman/view_pengumuman_umum.php";
			break;

		case 'pengumuman_siswa':
			include "views/admin/pengumuman/pengumuman/view_pengumuman_siswa.php";
			break;

		case 'data_pengguna':
			include "views/admin/pengaturan/data_pengguna/view_data_pengguna.php";
			break;

		case 'data_hak_akses':
			include "views/admin/pengaturan/data_hak_akses/view_data_hak_akses.php";
			break;

		case 'pengaturan_sekolah':
			include "views/admin/pengaturan/pengaturan_sekolah/view_pengaturan_sekolah.php";
			break;

		case 'pengaturan_data_pekerjaan_orang_tua':
			include "views/admin/pengaturan/data_orang_tua/data_pekerjaan_orang_tua/view_data_pekerjaan_orang_tua.php";
			break;

		case 'pengaturan_data_penghasilan_orang_tua':
			include "views/admin/pengaturan/data_orang_tua/data_penghasilan_orang_tua/view_data_penghasilan_orang_tua.php";
			break;

		case 'data_ppdb':
			include "views/admin/pengaturan/data_ppdb/view_data_ppdb.php";
			break;

		case 'pendaftar_verifikasi_pendaftar':
			include "views/admin/pendaftaran/verifikasi/view_verifikasi_pendaftar.php";
			break;

		case 'pendaftar_verifikasi_pembelian_formulir':
			include "views/admin/pendaftaran/verifikasi/view_verifikasi_pembelian_formulir.php";
			break;

		case 'pendaftar_verifikasi_data_diri':
			include "views/admin/pendaftaran/verifikasi/view_verifikasi_data_diri.php";
			break;

		case 'pendaftar_verifikasi_raport':
			include "views/admin/pendaftaran/verifikasi/view_verifikasi_raport.php";
			break;

		case 'pendaftar_verifikasi_layanan':
			include "views/admin/pendaftaran/verifikasi/view_verifikasi_layanan.php";
			break;

		case 'pendaftar_verifikasi_berkas':
			include "views/admin/pendaftaran/verifikasi/view_verifikasi_berkas.php";
			break;

		case 'pendaftar_verifikasi_pembayaran_ppdb':
			include "views/admin/pendaftaran/verifikasi/view_verifikasi_pembayaran_ppdb.php";
			break;

		case 'pendaftar_verifikasi_kelulusan':
			include "views/admin/pendaftaran/verifikasi/view_verifikasi_kelulusan.php";
			break;

		case 'laporan_pendaftar':
			include "views/admin/pendaftaran/laporan/view_laporan_pendaftar.php";
			break;

		case 'laporan_pembelian_formulir':
			include "views/admin/pendaftaran/laporan/view_laporan_pembelian_formulir.php";
			break;

		case 'laporan_lolos_berkas':
			include "views/admin/pendaftaran/laporan/view_laporan_lolos_berkas.php";
			break;

		case 'laporan_lunas_ppdb':
			include "views/admin/pendaftaran/laporan/view_laporan_lunas_ppdb.php";
			break;

		case 'laporan_pendaftar_diterima':
			include "views/admin/pendaftaran/laporan/view_laporan_pendaftar_diterima.php";
			break;

		case 'pengaturan_data_asal_sekolah':
			include "views/admin/pengaturan/data_asal_sekolah/view_pengaturan_data_asal_sekolah.php";
			break;

		case 'pengaturan_data_kendaraan_yang_dipakai_kesekolah':
			include "views/admin/pengaturan/data_kendaraan_yang_dipakai_kesekolah/view_pengaturan_kendaraan_yang_dipakai_kesekolah.php";
			break;

		case 'reset_password':
			include "views/admin/profile/view_reset_password.php";
			break;

		default:
			break;
	}
} else {
	include "views/admin/home/view_home.php";
}
