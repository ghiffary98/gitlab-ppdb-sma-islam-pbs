<?php

if (isset($_GET['menu'])) {
	switch ($_GET['menu']) {

		case 'home':
			include "views/siswa/home/view_home.php";
			break;

		case 'pembelian_formulir':
			include "views/siswa/pendaftaran/pembelian_formulir/view_pembelian_formulir.php";
			break;

		case 'pembelian_formulir_cara_bayar':
			include "views/siswa/pendaftaran/pembelian_formulir/view_pembelian_formulir_cara_bayar.php";
			break;

		case 'verifikasi_data_diri':
			include "views/siswa/pendaftaran/verifikasi_data_diri/view_verifikasi_data_diri.php";
			break;

		case 'verifikasi_raport':
			include "views/siswa/pendaftaran/verifikasi_raport/view_verifikasi_raport.php";
			break;

		case 'pilih_layanan':
			include "views/siswa/pendaftaran/layanan/view_pilih_layanan.php";
			break;

		case 'verifikasi_berkas':
			include "views/siswa/pendaftaran/verifikasi_berkas/view_verifikasi_berkas.php";
			break;

		case 'pembayaran_ppdb':
			include "views/siswa/pendaftaran/pembayaran_ppdb/view_pembayaran_ppdb.php";
			break;

		case 'pembayaran_ppdb_cara_bayar':
			include "views/siswa/pendaftaran/pembayaran_ppdb/view_pembayaran_ppdb_cara_bayar.php";
			break;

		case 'pengumuman_kelulusan':
			include "views/siswa/pengumuman/view_pengumuman_kelulusan.php";
			break;

		case 'pengumuman_pendaftar':
			include "views/siswa/pengumuman_pendaftar/view_pengumuman_pendaftar.php";
			break;

		case 'reset_password':
			include "views/siswa/profile/view_reset_password.php";
			break;

		default:
			break;
	}
} else {
	include "views/siswa/home/view_home.php";
}
