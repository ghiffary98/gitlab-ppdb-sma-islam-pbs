<?php
include "init.php";
include "models/global/data_ppdb_syarat_dan_ketentuan/model_data_ppdb_syarat_dan_ketentuan.php";
include "controllers/siswa/register/controller_persyaratan.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>REGISTER PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></title>
    <meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" />
    <link rel="icon" href="assets/images/logo/logo_sma_soedirman.png" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
        WebFont.load({
            google: {
                families: ["Lato:300,400,700,900"]
            },
            custom: {
                families: [
                    "Flaticon",
                    "Font Awesome 5 Solid",
                    "Font Awesome 5 Regular",
                    "Font Awesome 5 Brands",
                    "simple-line-icons",
                ],
                urls: ["assets/css/fonts.min.css"],
            },
            active: function() {
                sessionStorage.fonts = true;
            },
        });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/atlantis.css" />
</head>

<body class="login">

    <!-- MENGAMBIL DARI HALAMAN LOGIN ADMIN-->
    <?php include "views/siswa/register/view_persyaratan.php"; ?>

    <!-- Include Bootstrap and other scripts -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
</body>

</html>