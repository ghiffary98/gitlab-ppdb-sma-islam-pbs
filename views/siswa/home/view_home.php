<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/data_pengumuman/model_data_pengumuman.php";
include "models/global/data_pendaftar_pembayaran_ppdb/model_data_pendaftar_pembayaran_ppdb.php";
include "models/global/data_pendaftar_pembelian_formulir/model_data_pendaftar_pembelian_formulir.php";
include "models/global/data_pendaftar_verifikasi_berkas/model_data_pendaftar_verifikasi_berkas.php";
include "models/global/data_pendaftar_nilai_rapor/model_data_pendaftar_nilai_rapor.php";
include "models/global/data_pendaftar_program_layanan/model_data_pendaftar_program_layanan.php";
include "controllers/siswa/home/controller_home.php";
?>
<div class="content">
    <div class="panel-header bg-primary-gradient">
        <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                <div>
                    <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                    <h5 class="text-white op-7 mb-2"><?php echo $data_ppdb_saat_ini['Judul'] ?></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="page-inner mt--5">
        <div class="row">
            <div class="col-md-4">
                <a href="?menu=pembelian_formulir" style="text-decoration:none">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title text-center fw-bold">Pembelian Formulir</div>
                        </div>
                        <div class="card-body pb-0">
                            <div class="d-flex">
                                <div class="">
                                    <!-- <img src="assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle"> -->
                                    <div style="padding: 12px;" class="rounded-circle bg-info"><i class="fas fa-money-bill-wave text-white" style="font-size:20px;"></i></div>
                                </div>
                                <div class="flex-1 pt-1 ml-3">
                                    <h4 class="fw-bold mb-1">Pembelian Formulir</h4>
                                    <?php if($Keterangan_Pembelian_Formulir == "Sudah Bayar"){ ?>
                                        <small class="text-muted text-success"><?php echo $Keterangan_Pembelian_Formulir?></small>
                                    <?php }else{ ?>
                                        <small class="text-muted text-danger"><?php echo $Keterangan_Pembelian_Formulir?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="separator-dashed"></div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a <?php if($Keterangan_Pembelian_Formulir == "Sudah Bayar"){ echo 'href="?menu=verifikasi_data_diri"'; }elseif($edit_data_pendaftar['Status_Kelulusan'] == "Gugur"){echo 'href="dashboard.php?menu=pengumuman_kelulusan" onclick="alert('."'Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir'".')"';}else{echo 'href="#" onclick="alert('."'Harap Lakukan Pembelian Formulir Terlebih Dahulu !'".')"';} ?> style="text-decoration:none">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title text-center fw-bold">Verifikasi Data Diri</div>
                        </div>
                        <div class="card-body pb-0">
                            <div class="d-flex">
                                <div class="">
                                    <!-- <img src="assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle"> -->
                                    <div style="padding: 12px;" class="rounded-circle bg-info"><i class="fas fa-user-check text-white" style="font-size:20px;"></i></div>
                                </div>
                                <div class="flex-1 pt-1 ml-3">
                                    <h4 class="fw-bold mb-1">Verifikasi Data Diri</h4>
                                    <?php if($Keterangan_Verifikasi_Data_Diri == "Belum Lengkap"){ ?>
                                        <small class="text-muted text-danger"><?php echo $Keterangan_Verifikasi_Data_Diri ?></small>
                                    <?php }else if($Keterangan_Verifikasi_Data_Diri == "Sudah Lengkap dan Sudah Diverifikasi"){ ?>
                                        <small class="text-muted text-success"><?php echo $Keterangan_Verifikasi_Data_Diri ?></small>
                                    <?php }else{ ?>
                                        <small class="text-muted text-warning"><?php echo $Keterangan_Verifikasi_Data_Diri ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="separator-dashed"></div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a <?php if($Keterangan_Pembelian_Formulir == "Sudah Bayar"){ echo 'href="?menu=verifikasi_raport"'; }elseif($edit_data_pendaftar['Status_Kelulusan'] == "Gugur"){echo 'href="dashboard.php?menu=pengumuman_kelulusan" onclick="alert('."'Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir'".')"';}else{echo 'href="#" onclick="alert('."'Harap Lakukan Pembelian Formulir Terlebih Dahulu !'".')"';} ?> style="text-decoration:none">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title text-center fw-bold">Nilai Rapor</div>
                        </div>
                        <div class="card-body pb-0">
                            <div class="d-flex">
                                <div class="">
                                    <!-- <img src="assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle"> -->
                                    <div style="padding: 12px;" class="rounded-circle bg-info"><i class="fas fa-book text-white" style="font-size:20px;"></i></div>
                                </div>
                                <div class="flex-1 pt-1 ml-3">
                                    <h4 class="fw-bold mb-1">Nilai Rapor</h4>
                                    <?php if($Keterangan_Nilai_Rapor == "Belum Lengkap"){ ?>
                                        <small class="text-muted text-danger"><?php echo $Keterangan_Nilai_Rapor ?></small>
                                    <?php }else if($Keterangan_Nilai_Rapor == "Sudah Lengkap dan Sudah Diverifikasi"){ ?>
                                        <small class="text-muted text-success"><?php echo $Keterangan_Nilai_Rapor ?></small>
                                    <?php }else{ ?>
                                        <small class="text-muted text-warning"><?php echo $Keterangan_Nilai_Rapor ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="separator-dashed"></div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a <?php if($Keterangan_Pembelian_Formulir == "Sudah Bayar"){ echo 'href="?menu=pilih_layanan"'; }elseif($edit_data_pendaftar['Status_Kelulusan'] == "Gugur"){echo 'href="dashboard.php?menu=pengumuman_kelulusan" onclick="alert('."'Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir'".')"';}else{echo 'href="#" onclick="alert('."'Harap Lakukan Pembelian Formulir Terlebih Dahulu !'".')"';} ?> style="text-decoration:none">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title text-center fw-bold">Pilih Layanan</div>
                        </div>
                        <div class="card-body pb-0">
                            <div class="d-flex">
                                <div class="">
                                    <!-- <img src="assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle"> -->
                                    <div style="padding: 12px;" class="rounded-circle bg-info"><i class="fas fa-tasks text-white" style="font-size:20px;"></i></div>
                                </div>
                                <div class="flex-1 pt-1 ml-3">
                                    <h4 class="fw-bold mb-1">Pilih Layanan</h4>
                                    <?php if($Keterangan_Pilih_Layanan == "Belum Lengkap"){ ?>
                                        <small class="text-muted text-danger"><?php echo $Keterangan_Pilih_Layanan ?></small>
                                    <?php }else if($Keterangan_Pilih_Layanan == "Sudah Lengkap dan Sudah Diverifikasi"){ ?>
                                        <small class="text-muted text-success"><?php echo $Keterangan_Pilih_Layanan ?></small>
                                    <?php }else{ ?>
                                        <small class="text-muted text-warning"><?php echo $Keterangan_Pilih_Layanan ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="separator-dashed"></div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a <?php if($Keterangan_Pembelian_Formulir == "Sudah Bayar"){ echo 'href="?menu=verifikasi_berkas"'; }elseif($edit_data_pendaftar['Status_Kelulusan'] == "Gugur"){echo 'href="dashboard.php?menu=pengumuman_kelulusan" onclick="alert('."'Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir'".')"';}else{echo 'href="#" onclick="alert('."'Harap Lakukan Pembelian Formulir Terlebih Dahulu !'".')"';} ?> style="text-decoration:none">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title text-center fw-bold">Verifikasi Berkas</div>
                        </div>
                        <div class="card-body pb-0">
                            <div class="d-flex">
                                <div class="">
                                    <!-- <img src="assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle"> -->
                                    <div style="padding: 12px;" class="rounded-circle bg-info"><i class="fas fa-id-card-alt text-white" style="font-size:20px;"></i></div>
                                </div>
                                <div class="flex-1 pt-1 ml-3">
                                    <h4 class="fw-bold mb-1">Verifikasi Berkas</h4>
                                    <?php if($Keterangan_Verifikasi_Berkas == "Belum Lengkap"){ ?>
                                        <small class="text-muted text-danger"><?php echo $Keterangan_Verifikasi_Berkas ?></small>
                                    <?php }else if($Keterangan_Verifikasi_Berkas == "Sudah Lengkap dan Sudah Diverifikasi"){ ?>
                                        <small class="text-muted text-success"><?php echo $Keterangan_Verifikasi_Berkas ?></small>
                                    <?php }else{ ?>
                                        <small class="text-muted text-warning"><?php echo $Keterangan_Verifikasi_Berkas ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="separator-dashed"></div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a <?php if($Keterangan_Pembelian_Formulir == "Sudah Bayar"){ echo 'href="?menu=pembayaran_ppdb"'; }elseif($edit_data_pendaftar['Status_Kelulusan'] == "Gugur"){echo 'href="dashboard.php?menu=pengumuman_kelulusan" onclick="alert('."'Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir'".')"';}else{echo 'href="#" onclick="alert('."'Harap Lakukan Pembelian Formulir Terlebih Dahulu !'".')"';} ?> style="text-decoration:none">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title text-center fw-bold">Pembayaran PPDB</div>
                        </div>
                        <div class="card-body pb-0">
                            <div class="d-flex">
                                <div class="">
                                    <!-- <img src="assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle"> -->
                                    <div style="padding: 12px;" class="rounded-circle bg-info"><i class="fas fa-money-check text-white" style="font-size:20px;"></i></div>
                                </div>
                                <div class="flex-1 pt-1 ml-3">
                                    <h4 class="fw-bold mb-1">Pembayaran PPDB</h4>
                                    <?php if($Keterangan_Pembayaran_PPDB == "Sudah Bayar"){ ?>
                                        <small class="text-muted text-success"><?php echo $Keterangan_Pembayaran_PPDB?></small>
                                    <?php }else{ ?>
                                        <small class="text-muted text-danger"><?php echo $Keterangan_Pembayaran_PPDB?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="separator-dashed"></div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card full-height">
                    <div class="card-header">
                        <div class="card-title fw-bold">Riwayat Aktivitas Akun</div>
                    </div>
                    <div class="card-body" style="overflow-x: scroll; height: 500px">
                        <ol class="activity-feed">
                            <?php
                            if ((isset($list_data_aktivitas_pendaftar))) {
                                foreach ($list_data_aktivitas_pendaftar as $data) {
                                $nomor++; ?>
                                <li class="feed-item feed-item-info">
                                    <time class="date"><?php echo tanggal_dan_waktu_24_jam_indonesia($data['Waktu_Simpan_Data'])?></time>
                                    <span class="text"><b><?php echo $data['Judul']?></b></span>
                                    <br>
                                    <span class="text"><?php echo $data['Deskripsi']?></span>
                                </li>
                            <?php 
                                }
                            }
                            ?>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card full-height">
                    <div class="card-header">
                        <div class="card-head-row">
                            <div class="card-title text-center fw-bold">Pengumuman</div>
                            <div class="card-tools">
                                <ul class="nav nav-pills nav-secondary nav-pills-no-bd nav-sm" id="pills-tab" role="tablist">
                                    <li class="nav-item d-none">
                                        <a class="nav-link" id="pills-today" data-toggle="pill" href="#pills-today" role="tab" aria-selected="true">Today</a>
                                    </li>
                                    <li class="nav-item d-none">
                                        <a class="nav-link active" id="pills-week" data-toggle="pill" href="#pills-week" role="tab" aria-selected="false">Week</a>
                                    </li>
                                    <li class="nav-item d-none">
                                        <a class="nav-link" id="pills-month" data-toggle="pill" href="#pills-month" role="tab" aria-selected="false">Month</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="overflow-x: scroll; height: 500px">
                        <?php
                        if ((isset($list_data_pengumuman))) {
                            foreach ($list_data_pengumuman as $data) {
                            $nomor++; ?>
                                <div class="d-flex">
                                    <div class="p-2">
                                        <i class="fas fa-info text-info"></i>
                                    </div>
                                    <div class="flex-1 ml-3 pt-1">
                                        <h4 class="text-uppercase fw-bold mb-1"><?php echo $data['Judul_Pengumuman']?></h4>
                                        <span class="text-muted"><?php echo $data['Isi_Pengumuman']?></span>
                                    </div>
                                    <div class="float-right pt-1">
                                        <small class="text-muted"><?php echo tanggal_dan_waktu_24_jam_indonesia($data['Waktu_Simpan_Data'])?></small>
                                    </div>
                                </div>
                                <div class="separator-dashed"></div>
                            <?php 
                            }
                        } 
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>