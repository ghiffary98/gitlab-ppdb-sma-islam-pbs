<?php
include "models/global/pengumuman_pendaftar/model_pengumuman_pendaftar.php";
include "controllers/siswa/pengumuman_pendaftar/controller_pengumuman_pendaftar.php";
?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Pengumuman Pendaftar</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pendaftaran</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pengumuman Pendaftar</a>
                </li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <h2 class=""> <b> Pengumuman Pendaftar </b> </h2>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div style="overflow-x: scroll; height: 500px">
                                <ol class="activity-feed">
                                    <?php
                                    if ((isset($list_pengumuman_pendaftar))) {
                                        foreach ($list_pengumuman_pendaftar as $data) {
                                        $nomor++; ?>
                                        <li class="feed-item feed-item-info">
                                            <time class="date"><?php echo tanggal_dan_waktu_24_jam_indonesia($data['Waktu_Simpan_Data'])?></time>
                                            <span class="text"><b><?php echo $data['Isi_Pengumuman']?></b></span>
                                        </li>
                                    <?php 
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>