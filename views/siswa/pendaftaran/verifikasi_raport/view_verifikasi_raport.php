<?php
include "models/global/data_pendaftar_nilai_rapor/model_data_pendaftar_nilai_rapor.php";
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/data_pendaftar_pembelian_formulir/model_data_pendaftar_pembelian_formulir.php";
include "controllers/siswa/pendaftaran/controller_rapor.php";
?>
<style>
    .form-control[readonly] {
        background: #fdfdfd;
        opacity: 1;
    }
</style>


<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Raport</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pendaftaran</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Raport</a>
                </li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 text-left">
                                <h2> <b> Verifikasi Raport </b> </h2>
                            </div>
                            <div class="col-lg-6 text-right">
                                <?php if (isset($edit_data_pendaftar_nilai_rapor)) { ?>
                                    <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Belum Diverifikasi") { ?>
                                        <span class="badge badge-danger"><b> Belum diverifikasi </b> </span>
                                    <?php } elseif ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") { ?>
                                        <span class="badge badge-success"><b> Sudah Diverifikasi </b> </span>
                                    <?php } elseif ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sedang Diverifikasi") { ?>
                                        <span class="badge badge-secondary"><b> Sedang Diverifikasi </b> </span>
                                    <?php } elseif ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Verifikasi Ditolak") { ?>
                                        <span class="badge badge-danger"><b> Verifikasi Ditolak </b> </span>
                                    <?php } else { ?>
                                        <span class="badge badge-danger"><b> Belum diverifikasi </b> </span>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <form method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th class="bg-light">
                                                                <h4><b>Nilai Rapor</b></h4>
                                                            </th>
                                                            <th class="bg-light text-center">
                                                                <h4><b>Semester 1</b></h4>
                                                            </th>
                                                            <th class="bg-light text-center">
                                                                <h4><b>Semester 2</b></h4>
                                                            </th>
                                                            <th class="bg-light text-center">
                                                                <h4><b>Semester 3</b></h4>
                                                            </th>
                                                            <th class="bg-light text-center">
                                                                <h4><b>Semester 4</b></h4>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td> Pendidikan Agama Islam dan Budi Pekerti </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> Pendidikan Pancasila dan Kewarganegaraan </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> Bahasa Indonesia </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_Bahasa_Indonesia"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Bahasa_Indonesia'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Bahasa_Indonesia'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_Bahasa_Indonesia"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Bahasa_Indonesia'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Bahasa_Indonesia'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_Bahasa_Indonesia"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Bahasa_Indonesia'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Bahasa_Indonesia'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_Bahasa_Indonesia"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Bahasa_Indonesia'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Bahasa_Indonesia'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> Bahasa Inggris </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_Bahasa_Inggris"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Bahasa_Inggris'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Bahasa_Inggris'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_Bahasa_Inggris"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Bahasa_Inggris'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Bahasa_Inggris'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_Bahasa_Inggris"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Bahasa_Inggris'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Bahasa_Inggris'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_Bahasa_Inggris"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Bahasa_Inggris'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Bahasa_Inggris'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> Matematika </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_Matematika" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Matematika'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Matematika'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_Matematika" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Matematika'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Matematika'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_Matematika" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Matematika'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Matematika'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_Matematika" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Matematika'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Matematika'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> IPA (Ilmu Pengetahuan Alam) </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_IPA" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_IPA'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_IPA'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_IPA" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_IPA'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_IPA'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_IPA" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_IPA'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_IPA'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_IPA" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_IPA'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_IPA'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>


                                                        <tr>
                                                            <td> IPS (Ilmu Pengetahuan Sosial) </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_IPS" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_IPS'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_IPS'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_IPS" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_IPS'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_IPS'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_IPS" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_IPS'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_IPS'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_IPS" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_IPS'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_IPS'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> Seni dan Budaya </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_Seni_Dan_Budaya"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Seni_Dan_Budaya'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Seni_Dan_Budaya'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_Seni_Dan_Budaya"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Seni_Dan_Budaya'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Seni_Dan_Budaya'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_Seni_Dan_Budaya"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Seni_Dan_Budaya'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Seni_Dan_Budaya'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_Seni_Dan_Budaya"
                                                                        type="number" min="1" max="100"
                                                                        class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Seni_Dan_Budaya'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Seni_Dan_Budaya'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> PJOK </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_PJOK" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_PJOK'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_PJOK'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_PJOK" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_PJOK'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_PJOK'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_PJOK" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_PJOK'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_PJOK'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_PJOK" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_PJOK'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_PJOK'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> Prakarya </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_1_Prakarya" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Prakarya'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_1_Prakarya'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_2_Prakarya" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Prakarya'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_2_Prakarya'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_3_Prakarya" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Prakarya'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_3_Prakarya'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_Semester_4_Prakarya" type="number"
                                                                        min="1" max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Prakarya'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_Semester_4_Prakarya'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr class="bg-light">
                                                            <th colspan="5">
                                                                <h2><b>Nilai SKL / Ijazah</b></h2>
                                                            </th>
                                                        </tr>

                                                        <tr class="">
                                                            <td class="">
                                                                Nilai SKL / Ijazah
                                                            </td>
                                                            <td>
                                                                <div class="form-group"> <input <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                                    echo "readonly";
                                                                } ?>
                                                                        name="Nilai_SKL_Ijazah" type="number" min="1"
                                                                        max="100" class="form-control"
                                                                        value="<?php if ((isset($edit_data_pendaftar_nilai_rapor)) and ($edit_data_pendaftar_nilai_rapor['Nilai_SKL_Ijazah'] <> "")) {
                                                                            echo $edit_data_pendaftar_nilai_rapor['Nilai_SKL_Ijazah'];
                                                                        } ?>">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr>
                                        </div>

                                    </div>

                                    <div class="alert alert-danger my-4 text-center" role="alert">
                                        <font class="text-danger"> <b> PERHATIAN! </b> </font> Harap pastikan bahwa data
                                        yang anda masukkan adalah <font class="text-success"> <b> BENAR </b> </font>,
                                        Karena <font class="text-danger"> DATA YANG SUDAH DIVERIFIKASI <b> TIDAK DAPAT
                                                DIUBAH </b> </font> <br>
                                        <!-- Anda dapat menghubuni admin jika ingin mengubah data yang sudah diverifikasi -->
                                    </div>


                                    <div class="form-group text-center">
                                        <?php if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] != "Sudah Diverifikasi") { ?>
                                            <button class="btn btn-primary" type="submit" name="submit_update"> <i
                                                    class="fa fa-check"></i> &nbsp; Simpan </button>
                                            <a href="?menu=<?php echo $_GET['menu'] ?>" class="btn btn-danger"> <i
                                                    class="fa fa-times"></i> &nbsp; Batal </a>
                                        <?php } ?>
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>