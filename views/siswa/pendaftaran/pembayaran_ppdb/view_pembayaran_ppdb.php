<?php
include "models/global/data_pendaftar_pembayaran_ppdb/model_data_pendaftar_pembayaran_ppdb.php";
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/data_pendaftar_pembelian_formulir/model_data_pendaftar_pembelian_formulir.php";
include "controllers/siswa/pendaftaran/controller_pembayaran_ppdb.php";
?>
<?php
$status_pembayaran = "Sudah Dibayar";
?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Pembayaran PPDB</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pendaftaran</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pembayaran PPDB</a>
                </li>
            </ul>
        </div>


        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6 text-left">
                        <h2 class=""> <b> Verifikasi Pembayaran PPDB </b> </h2>
                    </div>
                    <div class="col-lg-6 text-right">
                        <?php
                        if (isset($edit_data_pendaftar_pembayaran_ppdb)) {
                            $Status_Verifikasi_Pembayaran_PPDB = $edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'];
                        } else {
                            $Status_Verifikasi_Pembayaran_PPDB = "Belum Dibayar";
                        }
                        if (isset($edit_data_pendaftar_pembayaran_ppdb)) { ?>
                            <?php if ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Belum Diverifikasi") {
                                $badgeColor  = "badge badge-danger";
                            } elseif ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Sudah Diverifikasi") {
                                $badgeColor  = "badge badge-success";
                            } elseif ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Menunggu Verifikasi") {
                                $badgeColor  = "badge badge-warning";
                            } elseif ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Verifikasi Ditolak") {
                                $badgeColor  = "badge badge-danger";
                            } else {
                                $badgeColor  = "badge badge-info";
                            } ?>
                        <?php } ?>
                        <span class="<?php echo $badgeColor ?>"><b> <?php echo $edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] ?> </b> </span>
                    </div>
                </div>
            </div>
        </div>


        <?php
        if ((isset($edit_data_pendaftar_pembayaran_ppdb)) and ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] <> "Sudah Diverifikasi")) {
            if ($edit_data_pendaftar_pembayaran_ppdb['Bukti_Pembayaran_PPDB'] == "") { ?>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <h2>Pilih Metode Pembayaran</h2>
                            </div>
                            <div class="col-lg-3">
                                <select name="pilihan_pembayaran" id="pilihan_pembayaran" class="form-control" onchange="ubahMetodePembayaran();" style="cursor:pointer">
                                    <option value="Xendit"> Xendit </option>
                                    <option value="Transfer"> Transfer </option>
                                    <option value="Cash"> Cash </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

        <?php }
        } ?>

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-5">
                        <form method="POST" enctype="multipart/form-data">
                            <div class="card-header">
                                <div class="card-title">Informasi Formulir</div>
                            </div>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th style="width:40%"> No. Pendaftaran </th>
                                        <td style="width:60%"> : <?php echo $edit_data_pendaftar['Nomor_Pendaftaran'] ?> </td>
                                    </tr>
                                    <tr>
                                        <th style="width:40%"> Nama Siswa </th>
                                        <td style="width:60%"> : <?php echo $edit_data_pendaftar['Nama_Lengkap'] ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width:40%"> Biaya PPDB </th>
                                        <td style="width:60%"> : <?php echo rupiah($edit_data_ppdb['Biaya_PPDB']); ?>,-</td>
                                    </tr>
                                    <?php
                                    if ((isset($edit_data_pendaftar_pembayaran_ppdb)) AND ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] <> "Sudah Diverifikasi")) {
                                    ?>
                                    <tr id="div_biaya_admin">
                                        <th style="width:40%"> Biaya Admin </th>
                                        <td style="width:60%"> :
                                            <?php echo rupiah($edit_data_ppdb['Biaya_Admin_Xendit']); ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <th style="width:40%"> Status </th>
                                        <td style="width:60%"> :

                                            <?php
                                            if (isset($edit_data_pendaftar_pembayaran_ppdb)) {
                                                $Status_Verifikasi_Pembayaran_PPDB = $edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'];
                                            } else {
                                                $Status_Verifikasi_Pembayaran_PPDB = "Belum Dibayar";
                                            }

                                            if ($Status_Verifikasi_Pembayaran_PPDB == "Sudah Diverifikasi") {
                                                $badgeColor = "badge-success";
                                            } else if ($Status_Verifikasi_Pembayaran_PPDB == "Menunggu Verifikasi") {
                                                $badgeColor = "badge-warning";
                                            } else if ($Status_Verifikasi_Pembayaran_PPDB == "Verifikasi Ditolak") {
                                                $badgeColor = "badge-danger";
                                            } else {
                                                $badgeColor = "badge-info";
                                            } ?>

                                            <span class="badge <?php echo $badgeColor ?>">
                                                <b><?php echo $Status_Verifikasi_Pembayaran_PPDB ?></b>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php
                                    if ((isset($edit_data_pendaftar_pembayaran_ppdb)) and ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] <> "Sudah Diverifikasi")) {
                                        if ($edit_data_pendaftar_pembayaran_ppdb['Bukti_Pembayaran_PPDB'] <> "") { ?>
                                            <label class="col-form-label">File : <a href="<?php echo $folder_penyimpanan_file_bukti_pembayaran_pembelian_formulir ?><?php echo $edit_data_pendaftar_pembayaran_ppdb['Bukti_Pembayaran_PPDB'] ?>" target="_blank">Lihat Bukti Pembayaran</a> </label>
                                        <?php
                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="2" class="text-center">
                                                    <div id="div_bayar_sekarang">
                                                        <button class="btn btn-block btn-primary" type="submit" name="submit_bayar"> Bayar Sekarang </button>
                                                    </div>

                                                    <div id="div_transfer" style="display:none">
                                                        <br>
                                                        <h3>Silahkan Transfer ke : <br>
                                                            <b>
                                                                <font class="text-success"> <?php echo $data_ppdb_saat_ini['No_Rekening_Utama'] ?> </font>
                                                            </b>
                                                            <br>
                                                            dan Upload bukti pembayaran
                                                        </h3>
                                                    </div>

                                                    <div id="div_cash" style="display:none">
                                                        <br>
                                                        <h3>Silahkan tunggu verifikasi dari Admin</h3>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                    <tr class="d-none">
                                        <td colspan="2">
                                            <a href="?menu=pembayaran_ppdb_cara_bayar"><button class="btn btn-block btn-outline-dark" type="button"> Tata Cara Pembayaran </button></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <?php if (isset($edit_data_pendaftar_pembayaran_ppdb)) { ?>
                            <form method="POST" enctype="multipart/form-data">
                                <div class="full-height">

                                    <div class="card-header">
                                        <div class="card-title">Bukti Pembayaran</div>
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        if (isset($edit_data_pendaftar_pembayaran_ppdb)) {

                                            // TAMPILKAN LINK FILE JIKA ADA FILE BUKTI PEMBAYARAN PPDB 
                                            if ($edit_data_pendaftar_pembayaran_ppdb['Bukti_Pembayaran_PPDB'] <> "") { ?>
                                                <label class="col-form-label">File :
                                                    <a href="<?php echo $folder_penyimpanan_file_bukti_pembayaran_ppdb ?><?php echo $edit_data_pendaftar_pembayaran_ppdb['Bukti_Pembayaran_PPDB'] ?>" target="_blank">Lihat Bukti Pembayaran</a> </label>
                                            <?php
                                            }

                                            // -----------------------------------------------------------------
                                            // JIKA STATUS PEMBAYARAN PPDB SUDAH DIVERIFIKASI
                                            if ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Sudah Diverifikasi") {
                                            ?>
                                                <div class="alert alert-success" role="alert"> File Bukti Pembayaran anda sudah lolos verifikasi</div>
                                            <?php
                                            } else if ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Menunggu Verifikasi") {
                                                // JIKA STATUS PEMBAYARAN PPDB SEDANG DIVERIFIKASI
                                            ?>
                                                <div class="alert alert-warning" role="alert"> File Bukti Pembayaran anda sedang diverifikasi oleh Admin PPDB</div>
                                                <?php
                                            } else {
                                                // JIKA FILE PEMBAYARAN PPDB KOSONG ATAU DITOLAK
                                                if ($edit_data_pendaftar_pembayaran_ppdb['Bukti_Pembayaran_PPDB'] == "" or $edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Belum Diverifikasi" or $edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Verifikasi Ditolak") {
                                                    if ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Verifikasi Ditolak") {
                                                ?>
                                                        <div class="alert alert-danger" role="alert"> File Bukti Pembayaran anda ditolak oleh Admin PPDB, harap upload kembali bukti pembayaran yang sesuai</div>
                                                    <?php
                                                    }
                                                    ?>

                                                    <input type="file" required name="Bukti_Pembayaran_PPDB" class="form-control" accept=".pdf, image/*">
                                                    <font class="text-muted"><i> Klik 'Choose File' untuk memilih file </i></font>
                                                    <div class="form-group">
                                                        <button class="btn btn-sm btn-primary" name="submit_upload_bukti" type="submit"> <i class="fa fa-check"></i> &nbsp; Simpan </button>
                                                        <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-times"></i> &nbsp; Batal </button>
                                                    </div>
                                        <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </div>

                                </div>
                            </form>
                        <?php } ?>
                    </div>
                    <div class="col-lg-4 d-none">
                        <div class="card-header">
                            <div class="card-title">Riwayat Pembayaran PPDB</div>
                        </div>
                        <div class="card-body">
                            <ol class="activity-feed">
                                <li class="feed-item feed-item-secondary">
                                    <time class="date" datetime="9-25">Sep 25</time>
                                    <span class="text">Responded to need <a href="#">"Volunteer opportunity"</a></span>
                                </li>
                                <li class="feed-item feed-item-success">
                                    <time class="date" datetime="9-24">Sep 24</time>
                                    <span class="text">Added an interest <a href="#">"Volunteer Activities"</a></span>
                                </li>
                                <li class="feed-item feed-item-info">
                                    <time class="date" datetime="9-23">Sep 23</time>
                                    <span class="text">Joined the group <a href="single-group.php">"Boardsmanship Forum"</a></span>
                                </li>
                                <li class="feed-item feed-item-warning">
                                    <time class="date" datetime="9-21">Sep 21</time>
                                    <span class="text">Responded to need <a href="#">"In-Kind Opportunity"</a></span>
                                </li>
                                <li class="feed-item feed-item-danger">
                                    <time class="date" datetime="9-18">Sep 18</time>
                                    <span class="text">Created need <a href="#">"Volunteer Opportunity"</a></span>
                                </li>
                                <li class="feed-item">
                                    <time class="date" datetime="9-17">Sep 17</time>
                                    <span class="text">Attending the event <a href="single-event.php">"Some New Event"</a></span>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-header">
                            <div class="card-title">
                                <h1> Informasi Pembayaran </h1>
                            </div>
                        </div>
                        <div class="mt-2 form-group row">
                            <div class="col-lg-12">
                                <p> Pembayaran Formulir dapat dilakukan dengan 3 cara : </p>
                                <p> 1. Melalui Xendit (untuk pembayaran Virtual Account, Kartu Kredit dan Uang Digital)</p>
                                <p> 2. Melalui Transfer ke Rekening SMA Islam PB Soedirman</p>
                                <p> 3. Melalui Cash ke SMA Islam PB Soedirman</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card-header">
                            <div class="card-title">
                                <h1> Metode Pembayaran </h1>
                            </div>
                        </div>

                        <div class="accordion" id="accordionMetodePembayaran">
                            <div class="card mb-0">
                                <button class="btn bg-white btn-sm" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <div class="card-header" id="headingOne">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="card-title">
                                                    <h4 class="text-left"> Pembayaran melalui Virtual Account </h4>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 text-right">
                                                <i class="flaticon-down-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </button>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionMetodePembayaran">
                                    <div class="card-body">
                                        1. Klik tombol "Bayar Sekarang" <br>
                                        2. Anda akan diarahkan ke halaman Xendit untuk melakukan pembayaran. <br>
                                        3. Anda dapat memilih opsi Virtual Account Bank, seperti : BCA, BRI, BNI, Mandiri, Permata, CIMB Niaga dan Bank lainnya <br>
                                        4. Ikuti langkah selanjutnya di halaman Xendit
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-0">
                                <button class="btn bg-white btn-sm collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <div class="card-header" id="headingFour">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="card-title">
                                                    <h4 class="text-left"> Pembayaran E-Wallet </h4>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 text-right">
                                                <i class="flaticon-down-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </button>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionMetodePembayaran">
                                    <div class="card-body">
                                        1. Klik tombol "Bayar Sekarang" <br>
                                        2. Anda akan diarahkan ke halaman Xendit untuk melakukan pembayaran. <br>
                                        3. Anda dapat memilih opsi pembayaran, seperti : OVO, Dana, GoPay, Shopee Pay dan Link Aja <br>
                                        4. Ikuti langkah selanjutnya di halaman Xendit
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-0">
                                <button class="btn bg-white btn-sm collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <div class="card-header" id="headingFour">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="card-title">
                                                    <h4 class="text-left"> Pembayaran melalui Retail </h4>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 text-right">
                                                <i class="flaticon-down-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </button>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionMetodePembayaran">
                                    <div class="card-body">
                                        1. Klik tombol "Bayar Sekarang" <br>
                                        2. Anda akan diarahkan ke halaman Xendit untuk melakukan pembayaran. <br>
                                        3. Anda dapat memilih opsi pembayaran, seperti : Indomaret dan Alfamart <br>
                                        4. Ikuti langkah selanjutnya di halaman Xendit
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-0">
                                <button class="btn bg-white btn-sm collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <div class="card-header" id="headingTwo">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="card-title">
                                                    <h4 class="text-left"> Pembayaran melalui Transfer </h4>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 text-right">
                                                <i class="flaticon-down-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </button>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionMetodePembayaran">
                                    <div class="card-body">
                                        1. Silahkan Transfer ke : <b> <?php echo $data_ppdb_saat_ini['No_Rekening_Utama'] ?> </b> <br>
                                        2. Upload bukti pembayaran <br>
                                        3. File bukti pembayaran anda akan diverifikasi oleh Admin
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-0">
                                <button class="btn bg-white btn-sm collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <div class="card-header" id="headingThree">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="card-title">
                                                    <h4 class="text-left"> Pembayaran melalui Cash </h4>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 text-right">
                                                <i class="flaticon-down-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </button>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionMetodePembayaran">
                                    <div class="card-body">
                                        1. Silahkan data ke SMA Islam PB Soedirman Cijantung <br>
                                        2. Bayar via Cash
                                        3. Upload bukti pembayaran (Foto kwitansi) <br>
                                        4. File bukti pembayaran anda akan diverifikasi oleh Admin
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>


<script>
    function ubahMetodePembayaran() {
        var pilihan_pembayaran = document.getElementById('pilihan_pembayaran').value;

        if (pilihan_pembayaran == "Xendit") {
            document.getElementById('div_bayar_sekarang').style.display = "";
            document.getElementById('div_transfer').style.display = "none";
            document.getElementById('div_cash').style.display = "none";
            document.getElementById('div_biaya_admin').style.display = "";
        } else if (pilihan_pembayaran == "Transfer") {
            document.getElementById('div_bayar_sekarang').style.display = "none";
            document.getElementById('div_transfer').style.display = "";
            document.getElementById('div_cash').style.display = "none";
            document.getElementById('div_biaya_admin').style.display = "none";
        } else if (pilihan_pembayaran == "Cash") {
            document.getElementById('div_bayar_sekarang').style.display = "none";
            document.getElementById('div_transfer').style.display = "none";
            document.getElementById('div_cash').style.display = "";
            document.getElementById('div_biaya_admin').style.display = "none";
        } else {
            document.getElementById('div_bayar_sekarang').style.display = "none";
            document.getElementById('div_transfer').style.display = "none";
            document.getElementById('div_cash').style.display = "none";
            document.getElementById('div_biaya_admin').style.display = "none";
        }
    }
</script>