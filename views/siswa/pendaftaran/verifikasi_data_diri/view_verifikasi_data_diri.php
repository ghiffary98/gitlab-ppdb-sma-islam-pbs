<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/pengaturan_pekerjaan_orang_tua/model_pengaturan_pekerjaan_orang_tua.php";
include "models/global/pengaturan_penghasilan_orang_tua/model_pengaturan_penghasilan_orang_tua.php";
include "models/global/pengaturan_kendaraan_yang_dipakai_kesekolah/model_pengaturan_kendaraan_yang_dipakai_kesekolah.php";
include "models/global/pengaturan_asal_sekolah/model_pengaturan_asal_sekolah.php";
include "models/global/data_pendaftar_pembelian_formulir/model_data_pendaftar_pembelian_formulir.php";
include "controllers/siswa/pendaftaran/controller_data_diri.php";
?>

<style>
    .form-control[readonly] {
        background: #fdfdfd;
        opacity: 1;
    }
</style>


<script>
    function ajax_ambil_alamat_sekolah() {

        var asal_sekolah = document.getElementById('asal_sekolah').value;

        $.ajax({
            type: 'GET',
            url: 'controllers/admin/pengaturan_asal_sekolah/ajax_get_alamat_sekolah.php',
            dataType: 'json',
            data: 'nama_sekolah=' + asal_sekolah,
            success: function (response) {
                if (response.Alamat !== "") {
                    // Assuming that 'div_alamat_sekolah' is an input element, use val() to set its value
                    $('#div_alamat_sekolah').val(response.Alamat);
                } else {
                    // If Alamat is empty, you may want to handle it accordingly
                    $('#div_alamat_sekolah').val("No data available");
                }
            },
            error: function (xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    }
</script>


<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Data Diri</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pendaftaran</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Data Diri</a>
                </li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 text-left">
                                <h2 class=""> <b> Verifikasi Data Diri </b> </h2>
                            </div>
                            <div class="col-lg-6 text-right">
                                <?php if (isset($edit_data_pendaftar)) { ?>
                                    <?php if ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Belum Diverifikasi") { ?>
                                        <span class="badge badge-danger"><b> Belum diverifikasi </b> </span>
                                    <?php } elseif ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Sudah Diverifikasi") { ?>
                                        <span class="badge badge-success"><b> Sudah Diverifikasi </b> </span>
                                    <?php } elseif ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Sedang Diverifikasi") { ?>
                                        <span class="badge badge-secondary"><b> Sedang Diverifikasi </b> </span>
                                    <?php } elseif ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Verifikasi Ditolak") { ?>
                                        <span class="badge badge-danger"><b> Verifikasi Ditolak </b> </span>
                                    <?php } else { ?>
                                        <span class="badge badge-danger"><b> Belum diverifikasi </b> </span>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">

                        <form method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>

                                                <tr>
                                                    <th class="bg-light">
                                                        <h2><b>Identitas Siswa</b></h2>
                                                    </th>
                                                    <th class="bg-light">
                                                        <div class="text-right">
                                                            <?php if (!(isset($_GET['edit']))) { ?>
                                                                <?php if ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] != "Sudah Diverifikasi") { ?>
                                                                    <a href="?menu=<?php echo $_GET['menu'] ?>&edit"
                                                                        class="text text-info"><i class="fa fa-edit"> </i>
                                                                        &nbsp; Edit Data</a>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nama Siswa* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Nama_Lengkap"
                                                                id="nama_lengkap" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nama_Lengkap'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nama_Lengkap'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr style="display: none;">
                                                    <td style="width:30%"> Kelas </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Kelas" type="text"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kelas'] <> "")) {
                                                                    echo $edit_data_pendaftar['Kelas'];
                                                                } else {
                                                                    echo "X";
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tempat Lahir* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Tempat_Lahir"
                                                                id="tempat_lahir" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Tempat_Lahir'] <> "")) {
                                                                    echo $edit_data_pendaftar['Tempat_Lahir'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Tanggal Lahir* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;"
                                                                name="Tanggal_Lahir" type="date" class="form-control"
                                                                <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Tanggal_Lahir'] <> "")) {
                                                                    echo $edit_data_pendaftar['Tanggal_Lahir'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style="width:30%"> Jenis Kelamin* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Jenis_Kelamin'] == "Laki-laki")) {
                                                                echo "checked";
                                                            } ?> value="Laki-laki"
                                                                name="Jenis_Kelamin" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Laki-laki
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Jenis_Kelamin'] == "Perempuan")) {
                                                                echo "checked";
                                                            } ?> value="Perempuan"
                                                                name="Jenis_Kelamin" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Perempuan
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Agama* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select required name="Agama" id="" class="form-control"
                                                                <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Agama'] <> "")) { ?>
                                                                    <option
                                                                        value="<?php echo $edit_data_pendaftar['Agama']; ?> ">
                                                                        <?php echo $edit_data_pendaftar['Agama']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                                <option value="">- Pilih Agama - </option>
                                                                <option value="Islam">Islam</option>
                                                                <option value="Kristen Protestan">Kristen Protestan
                                                                </option>
                                                                <option value="Kristen Katolik">Kristen Katolik</option>
                                                                <option value="Hindu">Hindu</option>
                                                                <option value="Buddha">Buddha</option>
                                                                <option value="Khonghucu">Khonghucu</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr style="display: none">
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Pribadi</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Golongan Darah* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select required name="Golongan_Darah" id=""
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Golongan_Darah'] <> "")) { ?>
                                                                    <option
                                                                        value="<?php echo $edit_data_pendaftar['Golongan_Darah']; ?> ">
                                                                        <?php echo $edit_data_pendaftar['Golongan_Darah']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                                <option value="">- Pilih Golongan Darah - </option>
                                                                <option value="A">A</option>
                                                                <option value="AB">AB</option>
                                                                <option value="B">B</option>
                                                                <option value="O">O</option>
                                                            </select>

                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tinggi Badan* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Tinggi_Badan"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Tinggi_Badan'] <> "")) {
                                                                    echo $edit_data_pendaftar['Tinggi_Badan'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Berat Badan* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Berat_Badan"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Berat_Badan'] <> "")) {
                                                                    echo $edit_data_pendaftar['Berat_Badan'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Lingkar Kepala </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Lingkar_Kepala"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Lingkar_Kepala'] <> "")) {
                                                                    echo $edit_data_pendaftar['Lingkar_Kepala'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Kebutuhan Khusus </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kebutuhan_Khusus'] == "Ya")) {
                                                                echo "checked";
                                                            } ?> value="Ya" name="Kebutuhan_Khusus"
                                                                <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Ya
                                                            <input type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kebutuhan_Khusus'] == "Tidak")) {
                                                                echo "checked";
                                                            } ?> value="Tidak"
                                                                name="Kebutuhan_Khusus" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                            Tidak
                                                        </div>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style="width:30%"> Ukuran Seragam* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select required name="Ukuran_Seragam"
                                                                onchange="ubah_ukuran_seragam()" id="ukuran_seragam"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Ukuran_Seragam'] <> "")) { ?>
                                                                    <option
                                                                        value="<?php echo $edit_data_pendaftar['Ukuran_Seragam']; ?> ">
                                                                        <?php echo $edit_data_pendaftar['Ukuran_Seragam']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                                <option value="">- Pilih Ukuran Seragam - </option>
                                                                <option value="S">S</option>
                                                                <option value="M">M</option>
                                                                <option value="L">L</option>
                                                                <option value="XL">XL</option>
                                                                <option value="2XL">2XL</option>
                                                                <option value="3XL">3XL</option>
                                                                <option value="4XL">4XL</option>
                                                                <option value="Lainnya">Lainnya</option>
                                                            </select>

                                                            <div id="div_inputan_ukuran_seragam" style="display: none;">
                                                                <br>
                                                                <input type="text" class="form-control"
                                                                    name="Text_Inputan_Ukuran_Seragam"
                                                                    id="text_inputan_ukuran_seragam"
                                                                    placeholder="Masukkan ukuran seragam selain pilihan di atas">
                                                            </div>

                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Kontak</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No. HP* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;"
                                                                name="Nomor_Handphone" type="number"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_Handphone'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_Handphone'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> No. Telp </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Nomor_Telepon"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_Telepon'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_Telepon'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Email* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Email"
                                                                type="email" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Email'] <> "")) {
                                                                    echo $edit_data_pendaftar['Email'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Sekolah</b></h2>
                                                    </th>
                                                </tr>


                                                <tr>
                                                    <td style="width:30%"> NIS (Nomor Induk Siswa)* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="NIS"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['NIS'] <> "")) {
                                                                    echo $edit_data_pendaftar['NIS'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> NISN (Nomor Induk Siswa Nasional)* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="NISN"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['NISN'] <> "")) {
                                                                    echo $edit_data_pendaftar['NISN'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr style="display: none;">
                                                    <td style="width:30%"> NIPD* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="NIPD" type="number"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['NIPD'] <> "")) {
                                                                    echo $edit_data_pendaftar['NIPD'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Asal Sekolah* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <?php if (isset($_GET['edit'])) { ?>
                                                                <select required class="form-control" name="Asal_Sekolah"
                                                                    id="asal_sekolah"
                                                                    onchange="ajax_ambil_alamat_sekolah();">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_asal_sekolah_ini = $list_data_pengaturan_asal_sekolah;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit_data_pendaftar['Asal_Sekolah'] <> "") {
                                                                            $edit_data_sebelum_Asal_Sekolah['Id_Pengaturan_Asal_Sekolah'] = '0';
                                                                            $edit_data_sebelum_Asal_Sekolah['Nama_Sekolah'] = $edit_data_pendaftar['Asal_Sekolah'];
                                                                            array_unshift($list_data_pengaturan_asal_sekolah_ini, $edit_data_sebelum_Asal_Sekolah);
                                                                            $list_data_pengaturan_asal_sekolah_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_asal_sekolah_ini, "Nama_Sekolah");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_asal_sekolah_ini))) {
                                                                        foreach ($list_data_pengaturan_asal_sekolah_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Asal_Sekolah'] == $data['Nama_Sekolah']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit_data_pendaftar['Asal_Sekolah'] == $data['Nama_Sekolah']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Nama_Sekolah'] ?>">
                                                                                <?php echo $data['Nama_Sekolah'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            <?php } else { ?>
                                                                <input name="Asal_Sekolah" type="text" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Asal_Sekolah'] <> "")) {
                                                                        echo $edit_data_pendaftar['Asal_Sekolah'];
                                                                    } ?>">
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Alamat Sekolah* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;"
                                                                id="div_alamat_sekolah" name="Alamat_Sekolah"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Alamat_Sekolah'] <> "")) {
                                                                    echo $edit_data_pendaftar['Alamat_Sekolah'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No Peserta Ujian Sekolah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;"
                                                                name="Nomor_Peserta_Ujian_Nasional" type="number"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_Peserta_Ujian_Nasional'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_Peserta_Ujian_Nasional'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No Seri Ijazah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Nomor_Seri_Ijazah"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_Seri_Ijazah'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_Seri_Ijazah'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Keluarga</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> NIK (Nomor Induk Kependudukan)* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="NIK"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['NIK'] <> "")) {
                                                                    echo $edit_data_pendaftar['NIK'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No KK (Nomor Kartu Keluarga)* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Nomor_KK"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_KK'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_KK'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Anak Ke-berapa* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Anak_Ke"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Anak_Ke'] <> "")) {
                                                                    echo $edit_data_pendaftar['Anak_Ke'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Jumlah Saudara Kandung* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;"
                                                                name="Jumlah_Saudara" type="text" class="form-control"
                                                                <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Jumlah_Saudara'] <> "")) {
                                                                    echo $edit_data_pendaftar['Jumlah_Saudara'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penerima KPS* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Penerima_KPS'] == "Ya")) {
                                                                echo "checked";
                                                            } ?> value="Ya" name="Penerima_KPS" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Ya
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Penerima_KPS'] == "Tidak")) {
                                                                echo "checked";
                                                            } ?> value="Tidak" name="Penerima_KPS" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                            Tidak

                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No.KPS (Jika Ya) </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Nomor_KPS"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_KPS'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_KPS'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penerima KIP* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Penerima_KIP'] == "Ya")) {
                                                                echo "checked";
                                                            } ?> value="Ya" name="Penerima_KIP" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Ya
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Penerima_KIP'] == "Tidak")) {
                                                                echo "checked";
                                                            } ?> value="Tidak" name="Penerima_KIP" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                            Tidak


                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nomor KIP </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Nomor_KIP"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_KIP'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_KIP'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style="width:30%"> Penerima KJP DKI* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Penerima_KJP_DKI'] == "Ya")) {
                                                                echo "checked";
                                                            } ?> value="Ya" name="Penerima_KJP_DKI" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                            Ya
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Penerima_KJP_DKI'] == "Tidak")) {
                                                                echo "checked";
                                                            } ?> value="Tidak"
                                                                name="Penerima_KJP_DKI" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                            Tidak


                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nomor KJP DKI </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Nomor_KJP_DKI"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_KJP_DKI'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_KJP_DKI'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nomor KKS </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Nomor_KKS"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_KKS'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_KKS'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No Registrasi Akta Lahir </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;"
                                                                name="Nomor_Registrasi_Akta_Lahir" type="text"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nomor_Registrasi_Akta_Lahir'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nomor_Registrasi_Akta_Lahir'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Status Dalam Keluarga </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Status_Dalam_Keluarga"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Status_Dalam_Keluarga'] <> "")) {
                                                                    echo $edit_data_pendaftar['Status_Dalam_Keluarga'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Status Perwalian* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Status_Perwalian'] == "Kandung")) {
                                                                echo "checked";
                                                            } ?> value="Kandung"
                                                                name="Status_Perwalian" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                            Kandung
                                                            <input required type="radio" <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Status_Perwalian'] == "Wali")) {
                                                                echo "checked";
                                                            } ?> value="Wali"
                                                                name="Status_Perwalian" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Wali
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Orang Tua</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h4><b>Data Ayah</b></h4>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nama Ayah* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Nama_Ayah"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nama_Ayah'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nama_Ayah'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tempat Lahir Ayah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Tempat_Lahir_Ayah"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Tempat_Lahir_Ayah'] <> "")) {
                                                                    echo $edit_data_pendaftar['Tempat_Lahir_Ayah'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tanggal Lahir Ayah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Tanggal_Lahir_Ayah"
                                                                type="date" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Tanggal_Lahir_Ayah'] <> "")) {
                                                                    echo $edit_data_pendaftar['Tanggal_Lahir_Ayah'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Pendidikan Ayah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select name="Jenjang_Pendidikan_Ayah" id=""
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Jenjang_Pendidikan_Ayah'] <> "")) { ?>
                                                                    <option
                                                                        value="<?php echo $edit_data_pendaftar['Jenjang_Pendidikan_Ayah']; ?> ">
                                                                        <?php echo $edit_data_pendaftar['Jenjang_Pendidikan_Ayah']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                                <option value="">- Pilih Jenjang Pendidikan Ayah -
                                                                </option>
                                                                <option value="SD">SD</option>
                                                                <option value="SMP">SMP</option>
                                                                <option value="SMA">SMA</option>
                                                                <option value="D1">D1</option>
                                                                <option value="D3">D3</option>
                                                                <option value="D4">D4</option>
                                                                <option value="S1">S1</option>
                                                                <option value="S2">S2</option>
                                                                <option value="S3">S3</option>
                                                                <option value="Lainnya">Lainnya</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Pekerjaan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <?php if (isset($_GET['edit'])) { ?>
                                                                <select class="form-control" name="Pekerjaan_Ayah">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit_data_pendaftar['Pekerjaan_Ayah'] <> "") {
                                                                            $edit_data_sebelum_Pekerjaan_Ayah['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Pekerjaan_Ayah['Pekerjaan_Orang_Tua'] = $edit_data_pendaftar['Pekerjaan_Ayah'];
                                                                            array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Ayah);
                                                                            $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini, "Pekerjaan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Pekerjaan_Ayah'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit_data_pendaftar['Pekerjaan_Ayah'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Pekerjaan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            <?php } else { ?>
                                                                <input name="Pekerjaan_Ayah" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Pekerjaan_Ayah'] <> "")) {
                                                                        echo $edit_data_pendaftar['Pekerjaan_Ayah'];
                                                                    } ?>">
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penghasilan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <?php if (isset($_GET['edit'])) { ?>
                                                                <select class="form-control" name="Penghasilan_Ayah">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit_data_pendaftar['Penghasilan_Ayah'] <> "") {
                                                                            $edit_data_sebelum_Penghasilan_Ayah['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Penghasilan_Ayah['Penghasilan_Orang_Tua'] = $edit_data_pendaftar['Penghasilan_Ayah'];
                                                                            array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Ayah);
                                                                            $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini, "Penghasilan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Penghasilan_Ayah'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit_data_pendaftar['Penghasilan_Ayah'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Penghasilan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Penghasilan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            <?php } else { ?>
                                                                <input name="Penghasilan_Ayah" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Penghasilan_Ayah'] <> "")) {
                                                                        echo $edit_data_pendaftar['Penghasilan_Ayah'];
                                                                    } ?>">
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> NIK Ayah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="NIK_Ayah" type="text"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['NIK_Ayah'] <> "")) {
                                                                    echo $edit_data_pendaftar['NIK_Ayah'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No. HP Ayah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="No_HP_Ayah"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['No_HP_Ayah'] <> "")) {
                                                                    echo $edit_data_pendaftar['No_HP_Ayah'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h4><b>Data Ibu</b></h4>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nama Ibu* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Nama_Ibu"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nama_Ibu'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nama_Ibu'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tempat Lahir Ibu </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Tempat_Lahir_Ibu"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Tempat_Lahir_Ibu'] <> "")) {
                                                                    echo $edit_data_pendaftar['Tempat_Lahir_Ibu'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tanggal Lahir Ibu </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Tanggal_Lahir_Ibu"
                                                                type="date" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Tanggal_Lahir_Ibu'] <> "")) {
                                                                    echo $edit_data_pendaftar['Tanggal_Lahir_Ibu'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Pendidikan Ibu </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select name="Jenjang_Pendidikan_Ibu" id=""
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Jenjang_Pendidikan_Ibu'] <> "")) { ?>
                                                                    <option
                                                                        value="<?php echo $edit_data_pendaftar['Jenjang_Pendidikan_Ibu']; ?> ">
                                                                        <?php echo $edit_data_pendaftar['Jenjang_Pendidikan_Ibu']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                                <option value="">- Pilih Jenjang Pendidikan Ibu -
                                                                </option>
                                                                <option value="SD">SD</option>
                                                                <option value="SMP">SMP</option>
                                                                <option value="SMA">SMA</option>
                                                                <option value="D1">D1</option>
                                                                <option value="D3">D3</option>
                                                                <option value="D4">D4</option>
                                                                <option value="S1">S1</option>
                                                                <option value="S2">S2</option>
                                                                <option value="S3">S3</option>
                                                                <option value="Lainnya">Lainnya</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Pekerjaan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <?php if (isset($_GET['edit'])) { ?>
                                                                <select class="form-control" name="Pekerjaan_Ibu">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit_data_pendaftar['Pekerjaan_Ibu'] <> "") {
                                                                            $edit_data_sebelum_Pekerjaan_Ibu['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Pekerjaan_Ibu['Pekerjaan_Orang_Tua'] = $edit_data_pendaftar['Pekerjaan_Ibu'];
                                                                            array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Ibu);
                                                                            $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini, "Pekerjaan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Pekerjaan_Ibu'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit_data_pendaftar['Pekerjaan_Ibu'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Pekerjaan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            <?php } else { ?>
                                                                <input name="Pekerjaan_Ibu" type="text" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Pekerjaan_Ibu'] <> "")) {
                                                                        echo $edit_data_pendaftar['Pekerjaan_Ibu'];
                                                                    } ?>">
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penghasilan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <?php if (isset($_GET['edit'])) { ?>
                                                                <select class="form-control" name="Penghasilan_Ibu">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit_data_pendaftar['Penghasilan_Ibu'] <> "") {
                                                                            $edit_data_sebelum_Penghasilan_Ibu['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Penghasilan_Ibu['Penghasilan_Orang_Tua'] = $edit_data_pendaftar['Penghasilan_Ibu'];
                                                                            array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Ibu);
                                                                            $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini, "Penghasilan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Penghasilan_Ibu'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit_data_pendaftar['Penghasilan_Ibu'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Penghasilan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Penghasilan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            <?php } else { ?>
                                                                <input name="Penghasilan_Ibu" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Penghasilan_Ibu'] <> "")) {
                                                                        echo $edit_data_pendaftar['Penghasilan_Ibu'];
                                                                    } ?>">
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> NIK Ibu </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="NIK_Ibu" type="text"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['NIK_Ibu'] <> "")) {
                                                                    echo $edit_data_pendaftar['NIK_Ibu'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No. HP Ibu </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="No_HP_Ibu"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['No_HP_Ibu'] <> "")) {
                                                                    echo $edit_data_pendaftar['No_HP_Ibu'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Data Wali</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nama Wali </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Nama_Wali" type="text"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Nama_Wali'] <> "")) {
                                                                    echo $edit_data_pendaftar['Nama_Wali'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tempat Lahir Wali </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Tempat_Lahir_Wali"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Tempat_Lahir_Wali'] <> "")) {
                                                                    echo $edit_data_pendaftar['Tempat_Lahir_Wali'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tanggal Lahir Wali </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Tanggal_Lahir_Wali"
                                                                type="date" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Tanggal_Lahir_Wali'] <> "")) {
                                                                    echo $edit_data_pendaftar['Tanggal_Lahir_Wali'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Pendidikan Wali </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select name="Jenjang_Pendidikan_Wali" id=""
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Jenjang_Pendidikan_Wali'] <> "")) { ?>
                                                                    <option
                                                                        value="<?php echo $edit_data_pendaftar['Jenjang_Pendidikan_Wali']; ?> ">
                                                                        <?php echo $edit_data_pendaftar['Jenjang_Pendidikan_Wali']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                                <option value="">- Pilih Jenjang Pendidikan Wali -
                                                                </option>
                                                                <option value="SD">SD</option>
                                                                <option value="SMP">SMP</option>
                                                                <option value="SMA">SMA</option>
                                                                <option value="D1">D1</option>
                                                                <option value="D3">D3</option>
                                                                <option value="D4">D4</option>
                                                                <option value="S1">S1</option>
                                                                <option value="S2">S2</option>
                                                                <option value="S3">S3</option>
                                                                <option value="Lainnya">Lainnya</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Pekerjaan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <?php if (isset($_GET['edit'])) { ?>
                                                                <select class="form-control" name="Pekerjaan_Wali">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit_data_pendaftar['Pekerjaan_Wali'] <> "") {
                                                                            $edit_data_sebelum_Pekerjaan_Wali['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Pekerjaan_Wali['Pekerjaan_Orang_Tua'] = $edit_data_pendaftar['Pekerjaan_Wali'];
                                                                            array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Wali);
                                                                            $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini, "Pekerjaan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Pekerjaan_Wali'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit_data_pendaftar['Pekerjaan_Wali'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Pekerjaan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            <?php } else { ?>
                                                                <input name="Pekerjaan_Wali" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Pekerjaan_Wali'] <> "")) {
                                                                        echo $edit_data_pendaftar['Pekerjaan_Wali'];
                                                                    } ?>">
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penghasilan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <?php if (isset($_GET['edit'])) { ?>
                                                                <select class="form-control" name="Penghasilan_Wali">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit_data_pendaftar['Penghasilan_Wali'] <> "") {
                                                                            $edit_data_sebelum_Penghasilan_Wali['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Penghasilan_Wali['Penghasilan_Orang_Tua'] = $edit_data_pendaftar['Penghasilan_Wali'];
                                                                            array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Wali);
                                                                            $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini, "Penghasilan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Penghasilan_Wali'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit_data_pendaftar['Penghasilan_Wali'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Penghasilan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Penghasilan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            <?php } else { ?>
                                                                <input name="Penghasilan_Wali" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Penghasilan_Wali'] <> "")) {
                                                                        echo $edit_data_pendaftar['Penghasilan_Wali'];
                                                                    } ?>">
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> NIK Wali </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="NIK_Wali" type="text"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['NIK_Wali'] <> "")) {
                                                                    echo $edit_data_pendaftar['NIK_Wali'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No. HP Wali </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="No_HP_Wali"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['No_HP_Wali'] <> "")) {
                                                                    echo $edit_data_pendaftar['No_HP_Wali'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Alamat</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Provinsi* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">

                                                            <select style="<?php if (!isset($_GET['edit'])) {
                                                                echo 'display:none';
                                                            } ?>" id="Id_Provinsi" name="Id_Provinsi"
                                                                onclick="Cek_Validasi_Provinsi();"
                                                                onchange="Rubah_Provinsi();" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if (isset($_GET['edit'])) { ?>

                                                                    <option value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Id_Provinsi'] <> "")) {
                                                                        echo $edit_data_pendaftar['Id_Provinsi'];
                                                                    } ?>">
                                                                        <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Provinsi'] <> "")) {
                                                                            echo $edit_data_pendaftar['Provinsi'];
                                                                        } ?>
                                                                    </option>
                                                                <?php } else { ?>
                                                                    <option value="">Pilih Provinsi</option>
                                                                <?php } ?>
                                                            </select>
                                                            <input style="background:#fff; <?php if (isset($_GET['edit'])) {
                                                                echo 'display:none';
                                                            } ?>" name="Provinsi"
                                                                id="Provinsi" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Provinsi'] <> "")) {
                                                                    echo $edit_data_pendaftar['Provinsi'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Kota/Kab* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select style="<?php if (!isset($_GET['edit'])) {
                                                                echo 'display:none';
                                                            } ?>" id="Id_Kota" name="Id_Kota"
                                                                onclick="Cek_Validasi_Kota();" onchange="Rubah_Kota()"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if (isset($_GET['edit'])) { ?>
                                                                    <option value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Id_Kota'] <> "")) {
                                                                        echo $edit_data_pendaftar['Id_Kota'];
                                                                    } ?>">
                                                                        <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kota'] <> "")) {
                                                                            echo $edit_data_pendaftar['Kota'];
                                                                        } ?>
                                                                    </option>
                                                                <?php } else { ?>
                                                                    <option value="">Pilih Kota/Kab</option>
                                                                <?php } ?>
                                                            </select>
                                                            <input style="background:#fff; <?php if (isset($_GET['edit'])) {
                                                                echo 'display:none';
                                                            } ?>" name="Kota" id="Kota"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kota'] <> "")) {
                                                                    echo $edit_data_pendaftar['Kota'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Kecamatan* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select style="<?php if (!isset($_GET['edit'])) {
                                                                echo 'display:none';
                                                            } ?>" id="Id_Kecamatan" name="Id_Kecamatan"
                                                                onclick="Cek_Validasi_Kecamatan();"
                                                                onchange="Rubah_Kecamatan()" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if (isset($_GET['edit'])) { ?>
                                                                    <option value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Id_Kecamatan'] <> "")) {
                                                                        echo $edit_data_pendaftar['Id_Kecamatan'];
                                                                    } ?>">
                                                                        <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kecamatan'] <> "")) {
                                                                            echo $edit_data_pendaftar['Kecamatan'];
                                                                        } ?>
                                                                    </option>
                                                                <?php } else { ?>
                                                                    <option value="">Pilih Kecamatan</option>
                                                                <?php } ?>
                                                            </select>
                                                            <input style="background:#fff; <?php if (isset($_GET['edit'])) {
                                                                echo 'display:none';
                                                            } ?>" name="Kecamatan"
                                                                id="Kecamatan" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kecamatan'] <> "")) {
                                                                    echo $edit_data_pendaftar['Kecamatan'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Kelurahan* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select style="<?php if (!isset($_GET['edit'])) {
                                                                echo 'display:none';
                                                            } ?>" id="Id_Kelurahan" name="Id_Kelurahan"
                                                                onclick="Cek_Validasi_Kelurahan();"
                                                                onchange="Rubah_Kelurahan()" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                <?php if (isset($_GET['edit'])) { ?>
                                                                    <option value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Id_Kelurahan'] <> "")) {
                                                                        echo $edit_data_pendaftar['Id_Kelurahan'];
                                                                    } ?>">
                                                                        <?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kelurahan'] <> "")) {
                                                                            echo $edit_data_pendaftar['Kelurahan'];
                                                                        } ?>
                                                                    </option>
                                                                <?php } else { ?>
                                                                    <option value="">Pilih Kelurahan</option>
                                                                <?php } ?>
                                                            </select>
                                                            <input style="background:#fff; <?php if (isset($_GET['edit'])) {
                                                                echo 'display:none';
                                                            } ?>" name="Kelurahan"
                                                                id="Kelurahan" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kelurahan'] <> "")) {
                                                                    echo $edit_data_pendaftar['Kelurahan'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <script>
                                                    <?php if (isset($_GET['edit'])) { ?>
                                                        Ambil_Data_Provinsi('Edit');
                                                        Ambil_Data_Kota('Edit');
                                                        Ambil_Data_Kecamatan('Edit');
                                                        Ambil_Data_Kelurahan('Edit');
                                                    <?php } else { ?>
                                                        Ambil_Data_Provinsi();
                                                        Ambil_Data_Kota();
                                                        Ambil_Data_Kecamatan();
                                                        Ambil_Data_Kelurahan();
                                                    <?php } ?>

                                                    //AMBIL DATA PROVINSI DARI API
                                                    function Ambil_Data_Provinsi(Tipe = '') {
                                                        const selectElement = document.getElementById('Id_Provinsi');

                                                        fetch('<?php echo $Link_Website ?>vendor/api-wilayah-indonesia/api/provinces.json')
                                                            .then(response => response.json())
                                                            .then(data => {
                                                                <?php if (isset($_GET['edit'])) { ?>
                                                                    if (Tipe == "Edit") {
                                                                        selectElement.innerHTML = '<option value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Id_Provinsi'] <> "")) {
                                                                            echo $edit_data_pendaftar['Id_Provinsi'];
                                                                        } ?>"><?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Provinsi'] <> "")) {
                                                                             echo $edit_data_pendaftar['Provinsi'];
                                                                         } ?></option>';
                                                                    } else {
                                                                        selectElement.innerHTML = '<option value="">Pilih Provinsi</option>';
                                                                    }
                                                                <?php } else { ?>
                                                                    selectElement.innerHTML = '<option value="">Pilih Provinsi</option>';
                                                                <?php } ?>

                                                                data.forEach(district => {
                                                                    const option = document.createElement('option');
                                                                    option.value = district.id;
                                                                    option.textContent = district.name;
                                                                    selectElement.appendChild(option);
                                                                });
                                                            })
                                                            .catch(error => {
                                                                console.error('Error fetching data:', error);
                                                            });
                                                    }

                                                    //AMBIL DATA KOTA DARI API
                                                    function Ambil_Data_Kota(Tipe = '') {
                                                        const selectElement = document.getElementById('Id_Kota');
                                                        var Id_Provinsi = document.getElementById('Id_Provinsi').value;

                                                        fetch('<?php echo $Link_Website ?>vendor/api-wilayah-indonesia/api/regencies/' + Id_Provinsi + '.json')
                                                            .then(response => response.json())
                                                            .then(data => {
                                                                <?php if (isset($_GET['edit'])) { ?>
                                                                    if (Tipe == "Edit") {
                                                                        selectElement.innerHTML = '<option value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Id_Kota'] <> "")) {
                                                                            echo $edit_data_pendaftar['Id_Kota'];
                                                                        } ?>"><?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kota'] <> "")) {
                                                                             echo $edit_data_pendaftar['Kota'];
                                                                         } ?></option>';
                                                                    } else {
                                                                        selectElement.innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                                    }
                                                                <?php } else { ?>
                                                                    selectElement.innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                                <?php } ?>

                                                                data.forEach(district => {
                                                                    const option = document.createElement('option');
                                                                    option.value = district.id;
                                                                    option.textContent = district.name;
                                                                    selectElement.appendChild(option);
                                                                });
                                                            })
                                                            .catch(error => {
                                                                console.error('Error fetching data:', error);
                                                            });
                                                    }

                                                    //AMBIL DATA KECAMATAN DARI API
                                                    function Ambil_Data_Kecamatan(Tipe = '') {
                                                        const selectElement = document.getElementById('Id_Kecamatan');
                                                        var Id_Kota = document.getElementById('Id_Kota').value;

                                                        fetch('<?php echo $Link_Website ?>vendor/api-wilayah-indonesia/api/districts/' + Id_Kota + '.json')
                                                            .then(response => response.json())
                                                            .then(data => {
                                                                <?php if (isset($_GET['edit'])) { ?>
                                                                    if (Tipe == "Edit") {
                                                                        selectElement.innerHTML = '<option value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Id_Kecamatan'] <> "")) {
                                                                            echo $edit_data_pendaftar['Id_Kecamatan'];
                                                                        } ?>"><?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kecamatan'] <> "")) {
                                                                             echo $edit_data_pendaftar['Kecamatan'];
                                                                         } ?></option>';
                                                                    } else {
                                                                        selectElement.innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                                    }
                                                                <?php } else { ?>
                                                                    selectElement.innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                                <?php } ?>

                                                                data.forEach(district => {
                                                                    const option = document.createElement('option');
                                                                    option.value = district.id;
                                                                    option.textContent = district.name;
                                                                    selectElement.appendChild(option);
                                                                });
                                                            })
                                                            .catch(error => {
                                                                console.error('Error fetching data:', error);
                                                            });
                                                    }

                                                    //AMBIL DATA KELURAHAN DARI API
                                                    function Ambil_Data_Kelurahan(Tipe = '') {
                                                        const selectElement = document.getElementById('Id_Kelurahan');
                                                        var Id_Kecamatan = document.getElementById('Id_Kecamatan').value;

                                                        fetch('<?php echo $Link_Website ?>vendor/api-wilayah-indonesia/api/villages/' + Id_Kecamatan + '.json')
                                                            .then(response => response.json())
                                                            .then(data => {
                                                                <?php if (isset($_GET['edit'])) { ?>
                                                                    if (Tipe == "Edit") {
                                                                        selectElement.innerHTML = '<option value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Id_Kelurahan'] <> "")) {
                                                                            echo $edit_data_pendaftar['Id_Kelurahan'];
                                                                        } ?>"><?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kelurahan'] <> "")) {
                                                                             echo $edit_data_pendaftar['Kelurahan'];
                                                                         } ?></option>';
                                                                    } else {
                                                                        selectElement.innerHTML = '<option value="">Pilih Kelurahan</option>';
                                                                    }
                                                                <?php } else { ?>
                                                                    selectElement.innerHTML = '<option value="">Pilih Kelurahan</option>';
                                                                <?php } ?>

                                                                data.forEach(district => {
                                                                    const option = document.createElement('option');
                                                                    option.value = district.id;
                                                                    option.textContent = district.name;
                                                                    selectElement.appendChild(option);
                                                                });
                                                            })
                                                            .catch(error => {
                                                                console.error('Error fetching data:', error);
                                                            });
                                                    }

                                                    function Rubah_Provinsi() {
                                                        document.getElementById('Id_Kota').innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                        document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                        document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                        document.getElementById('Kota').value = '';
                                                        document.getElementById('Kecamatan').value = '';
                                                        document.getElementById('Kelurahan').value = '';
                                                        Ambil_Data_Kota();

                                                        var select = document.getElementById('Id_Provinsi');
                                                        document.getElementById('Provinsi').value = select.options[select.selectedIndex].innerHTML;
                                                    }

                                                    function Rubah_Kota() {
                                                        document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                        document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                        document.getElementById('Kecamatan').value = '';
                                                        document.getElementById('Kelurahan').value = '';
                                                        Ambil_Data_Kecamatan();

                                                        var select = document.getElementById('Id_Kota');
                                                        document.getElementById('Kota').value = select.options[select.selectedIndex].innerHTML;
                                                    }

                                                    function Rubah_Kecamatan() {
                                                        document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                        document.getElementById('Kelurahan').value = '';
                                                        Ambil_Data_Kelurahan();

                                                        var select = document.getElementById('Id_Kecamatan');
                                                        document.getElementById('Kecamatan').value = select.options[select.selectedIndex].innerHTML;
                                                    }

                                                    function Rubah_Kelurahan() {
                                                        var select = document.getElementById('Id_Kelurahan');
                                                        document.getElementById('Kelurahan').value = select.options[select.selectedIndex].innerHTML;
                                                    }

                                                    function Cek_Validasi_Provinsi() {

                                                    }

                                                    function Cek_Validasi_Kota() {
                                                        if (document.getElementById('Id_Kota').value == '0') {
                                                            alert('Mohon Maaf, Harap Pilih Dari Provinsi Terlebih Dahulu');
                                                            Ambil_Data_Provinsi();
                                                            document.getElementById('Id_Kota').innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                            document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                            document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                            document.getElementById('Id_Kota').value = '';
                                                            document.getElementById('Id_Kecamatan').value = '';
                                                            document.getElementById('Id_Kelurahan').value = '';
                                                            document.getElementById('Kota').value = '';
                                                            document.getElementById('Kecamatan').value = '';
                                                            document.getElementById('Kelurahan').value = '';

                                                            document.getElementById('Id_Provinsi').focus();
                                                        } else {

                                                        }
                                                    }

                                                    function Cek_Validasi_Kecamatan() {
                                                        if (document.getElementById('Id_Kecamatan').value == '0') {
                                                            alert('Mohon Maaf, Harap Pilih Dari Provinsi Terlebih Dahulu');
                                                            Ambil_Data_Provinsi();
                                                            document.getElementById('Id_Kota').innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                            document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                            document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                            document.getElementById('Id_Kota').value = '';
                                                            document.getElementById('Id_Kecamatan').value = '';
                                                            document.getElementById('Id_Kelurahan').value = '';
                                                            document.getElementById('Kota').value = '';
                                                            document.getElementById('Kecamatan').value = '';
                                                            document.getElementById('Kelurahan').value = '';

                                                            document.getElementById('Id_Provinsi').focus();
                                                        } else {

                                                        }
                                                    }

                                                    function Cek_Validasi_Kelurahan() {
                                                        if (document.getElementById('Id_Kelurahan').value == '0') {
                                                            alert('Mohon Maaf, Harap Pilih Dari Provinsi Terlebih Dahulu');
                                                            Ambil_Data_Provinsi();
                                                            document.getElementById('Id_Kota').innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                            document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                            document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                            document.getElementById('Id_Kota').value = '';
                                                            document.getElementById('Id_Kecamatan').value = '';
                                                            document.getElementById('Id_Kelurahan').value = '';
                                                            document.getElementById('Kota').value = '';
                                                            document.getElementById('Kecamatan').value = '';
                                                            document.getElementById('Kelurahan').value = '';

                                                            document.getElementById('Id_Provinsi').focus();
                                                        } else {

                                                        }
                                                    }
                                                </script>

                                                <tr>
                                                    <td style="width:30%"> Alamat Lengkap* </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input required style="background:#fff;" name="Jalan"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Jalan'] <> "")) {
                                                                    echo $edit_data_pendaftar['Jalan'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style="width:30%"> Kode Pos </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Kode_Pos"
                                                                type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kode_Pos'] <> "")) {
                                                                    echo $edit_data_pendaftar['Kode_Pos'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Lintang Koordinat </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Titik_Lintang_Alamat"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Titik_Lintang_Alamat'] <> "")) {
                                                                    echo $edit_data_pendaftar['Titik_Lintang_Alamat'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Bujur Koordinat </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;" name="Titik_Bujur_Alamat"
                                                                type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Titik_Bujur_Alamat'] <> "")) {
                                                                    echo $edit_data_pendaftar['Titik_Bujur_Alamat'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Jarak Rumah ke Sekolah (Km) </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <input style="background:#fff;"
                                                                name="Jarak_Rumah_Ke_Sekolah" type="number"
                                                                class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Jarak_Rumah_Ke_Sekolah'] <> "")) {
                                                                    echo $edit_data_pendaftar['Jarak_Rumah_Ke_Sekolah'];
                                                                } ?>">
                                                        </div>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td style="width:30%"> Alat Transportasi </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <?php if (isset($_GET['edit'])) { ?>
                                                                <select class="form-control"
                                                                    name="Kendaraan_Yang_Dipakai_Kesekolah">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini = $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit_data_pendaftar['Kendaraan_Yang_Dipakai_Kesekolah'] <> "") {
                                                                            $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah['Id_Pengaturan_Nama_Kendaraan'] = '0';
                                                                            $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah['Nama_Kendaraan'] = $edit_data_pendaftar['Kendaraan_Yang_Dipakai_Kesekolah'];
                                                                            array_unshift($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini, $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah);
                                                                            $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini, "Nama_Kendaraan");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini))) {
                                                                        foreach ($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Kendaraan_Yang_Dipakai_Kesekolah'] == $data['Nama_Kendaraan']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit_data_pendaftar['Kendaraan_Yang_Dipakai_Kesekolah'] == $data['Nama_Kendaraan']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Nama_Kendaraan'] ?>">
                                                                                <?php echo $data['Nama_Kendaraan'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            <?php } else { ?>
                                                                <input name="Kendaraan_Yang_Dipakai_Kesekolah" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit_data_pendaftar)) and ($edit_data_pendaftar['Kendaraan_Yang_Dipakai_Kesekolah'] <> "")) {
                                                                        echo $edit_data_pendaftar['Kendaraan_Yang_Dipakai_Kesekolah'];
                                                                    } ?>">
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <hr>

                                    <?php if (isset($_GET['edit'])) { ?>
                                        <div class="alert alert-danger my-4 text-center" role="alert">
                                            <font class="text-danger"> <b> PERHATIAN! </b> </font> Harap pastikan bahwa data
                                            yang anda masukkan adalah <font class="text-success"> <b> BENAR </b> </font>,
                                            Karena <font class="text-danger"> DATA YANG SUDAH DIVERIFIKASI <b> TIDAK DAPAT
                                                    DIUBAH </b> </font> <br>
                                            <!-- Anda dapat menghubuni admin jika ingin mengubah data yang sudah diverifikasi -->
                                        </div>
                                        <div class="form-group text-center">
                                            <button class="btn btn-primary" type="submit" name="submit_update"> <i
                                                    class="fa fa-check"></i> &nbsp; Simpan </button>
                                            <a href="?menu=<?php echo $_GET['menu'] ?>" class="btn btn-danger"> <i
                                                    class="fa fa-times"></i> &nbsp; Batal </a>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="display: none;">
            <div class="col-md-12">
                <div class="card">
                    <div class="full-height">
                        <div class="card-header">
                            <div class="card-title">Riwayat Perubahan Data</div>
                        </div>
                        <div class="card-body">
                            <ol class="activity-feed">
                                <li class="feed-item feed-item-secondary">
                                    <time class="date" datetime="9-25">Sep 25</time>
                                    <span class="text">Responded to need <a href="#">"Volunteer opportunity"</a></span>
                                </li>
                                <li class="feed-item feed-item-success">
                                    <time class="date" datetime="9-24">Sep 24</time>
                                    <span class="text">Added an interest <a href="#">"Volunteer Activities"</a></span>
                                </li>
                                <li class="feed-item feed-item-info">
                                    <time class="date" datetime="9-23">Sep 23</time>
                                    <span class="text">Joined the group <a href="single-group.php">"Boardsmanship
                                            Forum"</a></span>
                                </li>
                                <li class="feed-item feed-item-warning">
                                    <time class="date" datetime="9-21">Sep 21</time>
                                    <span class="text">Responded to need <a href="#">"In-Kind Opportunity"</a></span>
                                </li>
                                <li class="feed-item feed-item-danger">
                                    <time class="date" datetime="9-18">Sep 18</time>
                                    <span class="text">Created need <a href="#">"Volunteer Opportunity"</a></span>
                                </li>
                                <li class="feed-item">
                                    <time class="date" datetime="9-17">Sep 17</time>
                                    <span class="text">Attending the event <a href="single-event.php">"Some New
                                            Event"</a></span>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<script>
    const inputFieldNamaLengkap = document.getElementById("nama_lengkap");
    inputFieldNamaLengkap.addEventListener("input", function (event) {
        event.preventDefault();
        const inputValue = inputFieldNamaLengkap.value.replace(/[^a-zA-Z0-9\s-]/g, ''); // Allow alphabetic characters, numbers, spaces, and dashes
        const formattedValue = inputValue.toUpperCase().replace(/\s/g, ' '); // Convert to uppercase and replace spaces with underscores
        inputFieldNamaLengkap.value = formattedValue;
    });
</script>

<script>
    const inputFieldTempatLahir = document.getElementById("tempat_lahir");
    inputFieldTempatLahir.addEventListener("input", function (event) {
        event.preventDefault();
        const inputValue = inputFieldTempatLahir.value.replace(/[^a-zA-Z0-9\s-]/g, ''); // Allow alphabetic characters, numbers, spaces, and dashes
        const formattedValue = inputValue.toUpperCase().replace(/\s/g, ' '); // Convert to uppercase and replace spaces with underscores
        inputFieldTempatLahir.value = formattedValue;
    });
</script>

<script>
    const emailInput = document.getElementById("email");
    emailInput.addEventListener("input", function (event) {
        event.preventDefault();
        emailInput.value = emailInput.value.toLowerCase();
    });
</script>

<script>
    function ubah_ukuran_seragam() {
        var inputan_ukuran_seragam = document.getElementById('ukuran_seragam').value;
        var text_inputan_ukuran_seragam = document.getElementById('text_inputan_ukuran_seragam');
        var div_inputan_ukuran_seragam = document.getElementById('div_inputan_ukuran_seragam');

        if (inputan_ukuran_seragam === "Lainnya") {
            div_inputan_ukuran_seragam.style.display = "";
            text_inputan_ukuran_seragam.required = true;
        } else {
            text_inputan_ukuran_seragam.required = false;
            div_inputan_ukuran_seragam.style.display = "none";
        }
    }
</script>