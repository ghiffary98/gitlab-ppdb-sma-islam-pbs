<?php
include "models/global/data_pendaftar_verifikasi_berkas/model_data_pendaftar_verifikasi_berkas.php";
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/data_pendaftar_pembelian_formulir/model_data_pendaftar_pembelian_formulir.php";
include "controllers/siswa/pendaftaran/controller_verifikasi_berkas.php";
?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Verifikasi Berkas</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pendaftaran</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Verifikasi Berkas</a>
                </li>
            </ul>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6 text-left">
                        <h2 class=""> <b> Verifikasi Berkas </b> </h2>
                    </div>
                    <div class="col-lg-6 text-right">
                        <?php if (isset($edit_data_pendaftar_verifikasi_berkas)) { ?>
                            <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Belum Diverifikasi") { ?>
                                <span class="badge badge-danger"><b> Belum diverifikasi </b> </span>
                            <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Sudah Diverifikasi") { ?>
                                <span class="badge badge-success"><b> Sudah Diverifikasi </b> </span>
                            <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Menunggu Verifikasi") { ?>
                                <span class="badge badge-warning"><b> Menunggu Verifikasi </b> </span>
                            <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Verifikasi Ditolak") { ?>
                                <span class="badge badge-danger"><b> Verifikasi Ditolak </b> </span>
                            <?php } else { ?>
                                <span class="badge badge-danger"><b> Belum diverifikasi </b> </span>
                            <?php } ?>
                        <?php } else { ?>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if (isset($_GET['edit'])) { ?>
            <form method="POST" enctype="multipart/form-data">
                <?php if ($_GET['berkas'] == "akte_kelahiran") { ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <h3> Berkas File Akta Kelahiran</h3>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <h3>Lihat File</h3>
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Akta_Kelahiran'] <> "") { ?>
                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_akte_kelahiran ?><?php echo $edit_data_pendaftar_verifikasi_berkas['Akta_Kelahiran'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Akta_Kelahiran'] ?> </a>
                                        <?php } else { ?>
                                            File : <font class="text-danger"> Belum Ada File, Silahkan Upload Terlebih Dahulu &nbsp; </font>
                                        <?php } ?>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Akta_Kelahiran'] <> "") { ?>
                                            <h3>Ubah File</h3>
                                        <?php } else { ?>
                                            <h3>Upload File</h3>
                                        <?php } ?>
                                        <input type="file" accept=".pdf, .jpg, .jpeg, .png" class="form-control" name="Akta_Kelahiran">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Akta_Kelahiran'] <> "") { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk mengganti file </i></font>
                                        <?php } else { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk pilih file </i></font>
                                        <?php } ?>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" name="submit_update" class="btn btn-sm btn-primary"> <i class="fa fa-check"></i> &nbsp; Simpan </button>
                                        <a href="?menu=<?php echo $_GET['menu'] ?>"><button class="btn btn-sm btn-danger" type="button"><i class="fa fa-times"></i> &nbsp; Batal </button></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <font class="text-danger">Perhatian!!! Harap upload <b>File Hasil Scan</b> dengan tipe <b>pdf</b>. Selain file tersebut, maka file tidak diterima</font>
                    </div>
                <?php } else if ($_GET['berkas'] == "kartu_keluarga") { ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <h3> Berkas File Kartu Keluarga</h3>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <h3>Lihat File</h3>
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_Keluarga'] <> "") { ?>
                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_kartu_keluarga ?><?php echo $edit_data_pendaftar_verifikasi_berkas['Kartu_Keluarga'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Kartu_Keluarga'] ?> </a>
                                        <?php } else { ?>
                                            File : <font class="text-danger"> Belum Ada File, Silahkan Upload Terlebih Dahulu &nbsp; </font>
                                        <?php } ?>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_Keluarga'] <> "") { ?>
                                            <h3>Ubah File</h3>
                                        <?php } else { ?>
                                            <h3>Upload File</h3>
                                        <?php } ?>
                                        <input type="file" accept=".pdf, .jpg, .jpeg, .png" class="form-control" name="Kartu_Keluarga">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_Keluarga'] <> "") { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk mengganti file </i></font>
                                        <?php } else { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk pilih file </i></font>
                                        <?php } ?>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" name="submit_update" class="btn btn-sm btn-primary"> <i class="fa fa-check"></i> &nbsp; Simpan </button>
                                        <a href="?menu=<?php echo $_GET['menu'] ?>"><button class="btn btn-sm btn-danger" type="button"><i class="fa fa-times"></i> &nbsp; Batal </button></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <font class="text-danger">Perhatian!!! Harap upload <b>File Hasil Scan</b> dengan tipe <b>pdf</b>. Selain file tersebut, maka file tidak diterima</font>
                    </div>
                <?php } else if ($_GET['berkas'] == "rapor") { ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <h3> Berkas File Rapor</h3>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <h3>Lihat File</h3>
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Rapor'] <> "") { ?>
                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_rapor ?><?php echo $edit_data_pendaftar_verifikasi_berkas['Rapor'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Rapor'] ?> </a>
                                        <?php } else { ?>
                                            File : <font class="text-danger"> Belum Ada File, Silahkan Upload Terlebih Dahulu &nbsp; </font>
                                        <?php } ?>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Rapor'] <> "") { ?>
                                            <h3>Ubah File</h3>
                                        <?php } else { ?>
                                            <h3>Upload File</h3>
                                        <?php } ?>
                                        <input type="file" accept=".pdf, .jpg, .jpeg, .png" class="form-control" name="Rapor">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Rapor'] <> "") { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk mengganti file </i></font>
                                        <?php } else { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk pilih file </i></font>
                                        <?php } ?>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" name="submit_update" class="btn btn-sm btn-primary"> <i class="fa fa-check"></i> &nbsp; Simpan </button>
                                        <a href="?menu=<?php echo $_GET['menu'] ?>"><button class="btn btn-sm btn-danger" type="button"><i class="fa fa-times"></i> &nbsp; Batal </button></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <font class="text-danger">Perhatian!!! Harap upload <b>File Hasil Scan</b> dengan tipe <b>pdf</b>. Selain file tersebut, maka file tidak diterima</font>
                    </div>
                <?php } else if ($_GET['berkas'] == "kartu_nisn") { ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <h3> Berkas File Kartu NISN</h3>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <h3>Lihat File</h3>
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_NISN'] <> "") { ?>
                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_kartu_nisn ?><?php echo $edit_data_pendaftar_verifikasi_berkas['Kartu_NISN'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Kartu_NISN'] ?> </a>
                                        <?php } else { ?>
                                            File : <font class="text-danger"> Belum Ada File, Silahkan Upload Terlebih Dahulu &nbsp; </font>
                                        <?php } ?>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_NISN'] <> "") { ?>
                                            <h3>Ubah File</h3>
                                        <?php } else { ?>
                                            <h3>Upload File</h3>
                                        <?php } ?>
                                        <input type="file" accept=".pdf, .jpg, .jpeg, .png" class="form-control" name="Kartu_NISN">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_NISN'] <> "") { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk mengganti file </i></font>
                                        <?php } else { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk pilih file </i></font>
                                        <?php } ?>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" name="submit_update" class="btn btn-sm btn-primary"> <i class="fa fa-check"></i> &nbsp; Simpan </button>
                                        <a href="?menu=<?php echo $_GET['menu'] ?>"><button class="btn btn-sm btn-danger" type="button"><i class="fa fa-times"></i> &nbsp; Batal </button></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <font class="text-danger">Perhatian!!! Harap upload <b>File Hasil Scan</b> dengan tipe <b>pdf</b>. Selain file tersebut, maka file tidak diterima</font>
                    </div>
                <?php } else if ($_GET['berkas'] == "skl_ijazah") { ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <h3> Berkas File SKL Ijazah</h3>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <h3>Lihat File</h3>
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['SKL_Ijazah'] <> "") { ?>
                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_skl_ijazah ?><?php echo $edit_data_pendaftar_verifikasi_berkas['SKL_Ijazah'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['SKL_Ijazah'] ?> </a>
                                        <?php } else { ?>
                                            File : <font class="text-danger"> Belum Ada File, Silahkan Upload Terlebih Dahulu &nbsp; </font>
                                        <?php } ?>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['SKL_Ijazah'] <> "") { ?>
                                            <h3>Ubah File</h3>
                                        <?php } else { ?>
                                            <h3>Upload File</h3>
                                        <?php } ?>
                                        <input type="file" accept=".pdf, .jpg, .jpeg, .png" class="form-control" name="SKL_Ijazah">
                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['SKL_Ijazah'] <> "") { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk mengganti file </i></font>
                                        <?php } else { ?>
                                            <font class="text-muted"><i> Klik 'Choose File' untuk pilih file </i></font>
                                        <?php } ?>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" name="submit_update" class="btn btn-sm btn-primary"> <i class="fa fa-check"></i> &nbsp; Simpan </button>
                                        <a href="?menu=<?php echo $_GET['menu'] ?>"><button class="btn btn-sm btn-danger" type="button"><i class="fa fa-times"></i> &nbsp; Batal </button></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <font class="text-danger">Perhatian!!! Harap upload <b>File Hasil Scan</b> dengan tipe <b>pdf</b>. Selain file tersebut, maka file tidak diterima</font>
                    </div>
                <?php } ?>
            </form>
        <?php } else { ?>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">

                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="">
                                                        <i class="fas fa-address-book fa-8x text-primary"></i>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="">
                                                        <h3><b>Akta Kelahiran</b></h3>
                                                        <small> Silahkan upload Foto/Scan Akta Kelahiran dalam bentuk .pdf </small>
                                                    </div>
                                                    <div class="my-2">
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Akta_Kelahiran'] <> "") { ?>
                                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_akte_kelahiran ?><?php echo $edit_data_pendaftar_verifikasi_berkas['Akta_Kelahiran'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Akta_Kelahiran'] ?> </a>
                                                        <?php } else { ?>
                                                            File : <font class="text-danger"> Belum Ada File &nbsp; </font>
                                                        <?php } ?>
                                                        &nbsp;
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Akta_Kelahiran'] <> "") { ?>
                                                            <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Akta_Kelahiran'] <> "Sudah Diverifikasi") { ?>
                                                                <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=akte_kelahiran"><button class="btn btn-outline-warning btn-xs"><i class="fa fa-edit"></i></button></a>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=akte_kelahiran"><button class="btn btn-outline-primary btn-xs"><i class="fa fa-upload"></i></button></a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="">
                                                        Status :
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Akta_Kelahiran'] == "Sudah Diverifikasi") { ?>
                                                            <span class="badge badge-success"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Akta_Kelahiran'] ?> </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Akta_Kelahiran'] == "") { ?>
                                                            <span class="badge badge-danger"> Belum Diverifikasi </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Akta_Kelahiran'] == "Menunggu Verifikasi") { ?>
                                                            <span class="badge badge-warning"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Akta_Kelahiran'] ?> </span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Akta_Kelahiran'] ?> </span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="">
                                                        <i class="fas fa-address-book fa-8x text-primary"></i>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="">
                                                        <h3><b>KK</b></h3>
                                                        <small> Silahkan upload Foto/Scan KK (Kartu Keluarga) dalam bentuk .pdf </small>
                                                    </div>
                                                    <div class="my-2">
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_Keluarga'] <> "") { ?>
                                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_kartu_keluarga ?><?php echo $edit_data_pendaftar_verifikasi_berkas['Kartu_Keluarga'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Kartu_Keluarga'] ?> </a>
                                                        <?php } else { ?>
                                                            File : <font class="text-danger"> Belum Ada File &nbsp; </font>
                                                        <?php } ?>
                                                        &nbsp;
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_Keluarga'] <> "") { ?>
                                                            <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_Keluarga'] <> "Sudah Diverifikasi") { ?>
                                                                <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=kartu_keluarga"><button class="btn btn-outline-warning btn-xs"><i class="fa fa-edit"></i></button></a>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=kartu_keluarga"><button class="btn btn-outline-primary btn-xs"><i class="fa fa-upload"></i></button></a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="">
                                                        Status :
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_Keluarga'] == "Sudah Diverifikasi") { ?>
                                                            <span class="badge badge-success"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_Keluarga'] ?> </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_Keluarga'] == "") { ?>
                                                            <span class="badge badge-danger"> Belum Diverifikasi </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_Keluarga'] == "Menunggu Verifikasi") { ?>
                                                            <span class="badge badge-warning"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_Keluarga'] ?> </span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_Keluarga'] ?> </span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="">
                                                        <i class="fas fa-address-card fa-8x text-primary"></i>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="">
                                                        <h3><b>NISN</b></h3>
                                                        <small> Silahkan upload Foto/Scan NISN (Nomor Induk Siswa Nasional) dalam bentuk .pdf </small>
                                                    </div>
                                                    <div class="my-2">
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_NISN'] <> "") { ?>
                                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_kartu_nisn ?><?php echo $edit_data_pendaftar_verifikasi_berkas['Kartu_NISN'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Kartu_NISN'] ?> </a>
                                                        <?php } else { ?>
                                                            File : <font class="text-danger"> Belum Ada File &nbsp; </font>
                                                        <?php } ?>
                                                        &nbsp;
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Kartu_NISN'] <> "") { ?>
                                                            <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_NISN'] <> "Sudah Diverifikasi") { ?>
                                                                <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=kartu_nisn"><button class="btn btn-outline-warning btn-xs"><i class="fa fa-edit"></i></button></a>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=kartu_nisn"><button class="btn btn-outline-primary btn-xs"><i class="fa fa-upload"></i></button></a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="">
                                                        Status :
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_NISN'] == "Sudah Diverifikasi") { ?>
                                                            <span class="badge badge-success"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_NISN'] ?> </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_NISN'] == "") { ?>
                                                            <span class="badge badge-danger"> Belum Diverifikasi </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_NISN'] == "Menunggu Verifikasi") { ?>
                                                            <span class="badge badge-warning"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_NISN'] ?> </span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Kartu_NISN'] ?> </span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="">
                                                        <i class="fas fa-graduation-cap fa-8x text-primary"></i>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="">
                                                        <h3><b>Raport Semester 1-4</h3></b>
                                                        <small> Silahkan upload Raport Semester 1-4 digabungkan dan dikirim dalam bentuk .pdf </small>
                                                    </div>
                                                    <div class="my-2">
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Rapor'] <> "") { ?>
                                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_rapor ?><?php echo $edit_data_pendaftar_verifikasi_berkas['Rapor'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Rapor'] ?> </a>
                                                        <?php } else { ?>
                                                            File : <font class="text-danger"> Belum Ada File &nbsp; </font>
                                                        <?php } ?>
                                                        &nbsp;
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Rapor'] <> "") { ?>
                                                            <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Rapor'] <> "Sudah Diverifikasi") { ?>
                                                                <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=rapor"><button class="btn btn-outline-warning btn-xs"><i class="fa fa-edit"></i></button></a>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=rapor"><button class="btn btn-outline-primary btn-xs"><i class="fa fa-upload"></i></button></a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="">
                                                        Status :
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Rapor'] == "Sudah Diverifikasi") { ?>
                                                            <span class="badge badge-success"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Rapor'] ?> </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Rapor'] == "") { ?>
                                                            <span class="badge badge-danger"> Belum Diverifikasi </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Rapor'] == "Menunggu Verifikasi") { ?>
                                                            <span class="badge badge-warning"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Rapor'] ?> </span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Rapor'] ?> </span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="">
                                                        <i class="fas fa-file-alt fa-8x text-primary"></i>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="">
                                                        <h3><b>SKL (Surat Keterangan Lulus) / Ijazah</b></h3>
                                                        <small> Silahkan upload Foto/Scan SKL / Ijazah dalam bentuk .pdf </small>
                                                    </div>
                                                    <div class="my-2">
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['SKL_Ijazah'] <> "") { ?>
                                                            Lihat File : <a href="<?php echo $folder_penyimpanan_file_berkas_skl_ijazah ?><?php echo $edit_data_pendaftar_verifikasi_berkas['SKL_Ijazah'] ?>" target="_blank"> <?php echo $edit_data_pendaftar_verifikasi_berkas['SKL_Ijazah'] ?> </a>
                                                        <?php } else { ?>
                                                            File : <font class="text-danger"> Belum Ada File &nbsp; </font>
                                                        <?php } ?>
                                                        &nbsp;
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['SKL_Ijazah'] <> "") { ?>
                                                            <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_SKL_Ijazah'] <> "Sudah Diverifikasi") { ?>
                                                                <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=skl_ijazah"><button class="btn btn-outline-warning btn-xs"><i class="fa fa-edit"></i></button></a>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <a href="?menu=<?php echo $_GET['menu'] ?>&edit&berkas=skl_ijazah"><button class="btn btn-outline-primary btn-xs"><i class="fa fa-upload"></i></button></a>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="">
                                                        Status :
                                                        <?php if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_SKL_Ijazah'] == "Sudah Diverifikasi") { ?>
                                                            <span class="badge badge-success"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_SKL_Ijazah'] ?> </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_SKL_Ijazah'] == "") { ?>
                                                            <span class="badge badge-danger"> Belum Diverifikasi </span>
                                                        <?php } elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_SKL_Ijazah'] == "Menunggu Verifikasi") { ?>
                                                            <span class="badge badge-warning"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_SKL_Ijazah'] ?> </span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger"> <?php echo $edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_SKL_Ijazah'] ?> </span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="">
                                    <h2> Keterangan Status : </h2>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th> Status </th>
                                                <th> Keterangan </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> <span class="badge badge-danger"> Belum Diverifikasi</span> </td>
                                                <td> Berkas belum diupload</td>
                                            </tr>
                                            <tr>
                                                <td> <span class="badge badge-warning"> Menunggu Verifikasi</span> </td>
                                                <td> Berkas Menunggu Verifikasi</td>
                                            </tr>
                                            <tr>
                                                <td> <span class="badge badge-success"> Sudah Diverifikasi</span> </td>
                                                <td> Berkas sudah diverifikasi dan sudah disetujui</td>
                                            </tr>
                                            <tr>
                                                <td> <span class="badge badge-danger">Verifikasi Ditolak</span> </td>
                                                <td> Berkas tidak disetujui</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php
        }
        ?>

    </div>
</div>

<!-- <div class="">
    <div class="">
        <div class="alert alert-solid alert-danger" role="alert">
            <span> Belum Ada File !</span>
        </div>
    </div>
</div> -->