<?php
include "models/global/data_pendaftar_program_layanan/model_data_pendaftar_program_layanan.php";
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/data_pendaftar_pembelian_formulir/model_data_pendaftar_pembelian_formulir.php";
include "controllers/siswa/pendaftaran/controller_pilih_layanan.php";
?>
<style>
    .form-control[readonly] {
        background: #fdfdfd;
        opacity: 1;
    }
</style>


<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Pilih Layanan</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pendaftaran</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pilih Layanan</a>
                </li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 text-left">
                                <h2> <b> Pilih Layanan </b> </h2>
                            </div>
                            <div class="col-lg-6 text-right">
                                <?php if (isset($edit_data_pendaftar_program_layanan)) { ?>
                                    <?php if ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Belum Diverifikasi") { ?>
                                        <span class="badge badge-danger"><b> Belum diverifikasi </b> </span>
                                    <?php } elseif ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Sudah Diverifikasi") { ?>
                                        <span class="badge badge-success"><b> Sudah Diverifikasi </b> </span>
                                    <?php } elseif ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Sedang Diverifikasi") { ?>
                                        <span class="badge badge-secondary"><b> Sedang Diverifikasi </b> </span>
                                    <?php } elseif ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Verifikasi Ditolak") { ?>
                                        <span class="badge badge-danger"><b> Verifikasi Ditolak </b> </span>
                                    <?php } else { ?>
                                        <span class="badge badge-danger"><b> Belum diverifikasi </b> </span>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <script type="text/javascript">
                                    function cek_duplikat_data_pada_array(arr) {
                                        let set = new Set(arr);
                                        return set.size !== arr.length;
                                    }

                                    function cek_duplikat_layanan() {
                                        var layanan_terpilih = [];
                                        layanan_terpilih.push(document.getElementById("layanan_1").value);
                                        layanan_terpilih.push(document.getElementById("layanan_2").value);
                                        layanan_terpilih.push(document.getElementById("layanan_3").value);
                                        layanan_terpilih.push(document.getElementById("layanan_4").value);

                                        if (cek_duplikat_data_pada_array(layanan_terpilih)) {
                                            alert("Pilihan Layanan Tidak Boleh Sama Dengan Layanan Lainnya");
                                            return false;
                                        } else {
                                            return true;
                                        }
                                    }

                                    function rubah_list_option_layanan(program, layanan_ke) {
                                        var selectElement = document.getElementById("layanan_" + layanan_ke);

                                        selectElement.innerHTML = "";
                                        if (program == "MIPA") {
                                            var options = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                        } else {
                                            var options = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                        }
                                        for (var i = 0; i < options.length; i++) {
                                            var option = document.createElement("option");
                                            option.value = options[i];
                                            option.text = options[i];
                                            selectElement.appendChild(option);
                                        }
                                    }

                                    function pilih_program_mipa() {
                                        rubah_list_option_layanan("MIPA", "1");
                                        rubah_list_option_layanan("MIPA", "2");
                                        rubah_list_option_layanan("MIPA", "3");
                                        rubah_list_option_layanan("MIPA", "4");

                                        document.getElementById("kolom_layanan_4").style.display = "";
                                        document.getElementById("layanan_4").required = true;
                                    }

                                    function pilih_program_ips() {
                                        rubah_list_option_layanan("IPS", "1");
                                        rubah_list_option_layanan("IPS", "2");
                                        rubah_list_option_layanan("IPS", "3");
                                        rubah_list_option_layanan("IPS", "4");

                                        document.getElementById("kolom_layanan_4").style.display = "none";
                                        document.getElementById("layanan_4").required = false;
                                    }
                                </script>
                                <form method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <?php if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") { ?>
                                                <!-- DIHAPUS UNTUK PROGRAM -->
                                            <?php } else { ?>
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr class="bg-light">
                                                            <th colspan="3">
                                                                <h4><b>Program* (Pilih salah satu)</b></h4>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th style="width:5%;">1. </th>
                                                            <th style="width:15%;"> <input <?php if ((isset($edit_data_pendaftar_program_layanan)) and ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Sudah Diverifikasi")) {
                                                                echo "disabled";
                                                            } ?>
                                                                    onclick="pilih_program_mipa()" type="radio" value="MIPA"
                                                                    name="Program" required <?php if ((isset($edit_data_pendaftar_program_layanan)) and ($edit_data_pendaftar_program_layanan['Program'] == "MIPA")) {
                                                                        echo "checked";
                                                                    } ?>> MIPA </th>
                                                            <th> Program MIPA (PCE, Biologi, Teknologi, Tahfidz)</th>
                                                        </tr>
                                                        <tr>
                                                            <th style="width:5%;">2. </th>
                                                            <th style="width:15%;"> <input <?php if ((isset($edit_data_pendaftar_program_layanan)) and ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Sudah Diverifikasi")) {
                                                                echo "disabled";
                                                            } ?>
                                                                    onclick="pilih_program_ips()" type="radio" value="IPS"
                                                                    name="Program" required <?php if ((isset($edit_data_pendaftar_program_layanan)) and ($edit_data_pendaftar_program_layanan['Program'] == "IPS")) {
                                                                        echo "checked";
                                                                    } ?>> IPS </th>
                                                            <th> Program IPS (PCE, Ekonomi/Akuntantsi, Bahasa)</th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            <?php } ?>

                                            <br>

                                            <table class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <th class="bg-light" colspan="3">
                                                            <h4><b>Pilih Layanan</b></h4>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <th class="bg bg-danger" colspan="3">
                                                            <div class="text-white"> Pemilihan Kelas layanan awal*
                                                                (Dapat berubah setelah <i>Placement Test</i>) </div>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:15%"> Layanan 1 </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <select required id="layanan_1" required
                                                                    name="Layanan_1" class="form-control">
                                                                    <?php
                                                                    if (isset($edit_data_pendaftar_program_layanan)) {
                                                                        if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") {
                                                                            $array_option = ["", "Biologi", "Teknologi/IT", "Tahfidz", "Bahasa", "Ekonomi/Akuntantsi"];
                                                                        } else {
                                                                            if ($edit_data_pendaftar_program_layanan['Program'] == "MIPA") {
                                                                                $array_option = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                                                            } else {
                                                                                $array_option = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $array_option = [""];
                                                                    }
                                                                    $array_option_terpilih = [];
                                                                    if (isset($edit_data_pendaftar_program_layanan)) {
                                                                        if ($edit_data_pendaftar_program_layanan['Layanan_1'] <> "") {
                                                                            $array_option_terpilih[] = $edit_data_pendaftar_program_layanan['Layanan_1'];
                                                                        } else {
                                                                            $array_option_terpilih[] = "";
                                                                        }
                                                                    }
                                                                    foreach ($array_option as $option) {
                                                                        ?>
                                                                        <option <?php
                                                                        if (isset($edit_data_pendaftar_program_layanan)) {
                                                                            foreach ($array_option_terpilih as $option_terpilih) {
                                                                                if ($option_terpilih == $option) {
                                                                                    echo "selected";
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                        ?> value="<?php echo $option ?>">
                                                                            <?php echo $option ?>
                                                                        </option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:15%"> Layanan 2 </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <select required id="layanan_2" required
                                                                    name="Layanan_2" class="form-control">
                                                                    <?php
                                                                    if (isset($edit_data_pendaftar_program_layanan)) {
                                                                        if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") {
                                                                            $array_option = ["", "Biologi", "Teknologi/IT", "Tahfidz", "Bahasa", "Ekonomi/Akuntantsi"];
                                                                        } else {
                                                                            if ($edit_data_pendaftar_program_layanan['Program'] == "MIPA") {
                                                                                $array_option = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                                                            } else {
                                                                                $array_option = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $array_option = [""];
                                                                    }
                                                                    $array_option_terpilih = [];
                                                                    if (isset($edit_data_pendaftar_program_layanan)) {
                                                                        if ($edit_data_pendaftar_program_layanan['Layanan_2'] <> "") {
                                                                            $array_option_terpilih[] = $edit_data_pendaftar_program_layanan['Layanan_2'];
                                                                        } else {
                                                                            $array_option_terpilih[] = "";
                                                                        }
                                                                    }
                                                                    foreach ($array_option as $option) {
                                                                        ?>
                                                                        <option <?php
                                                                        if (isset($edit_data_pendaftar_program_layanan)) {
                                                                            foreach ($array_option_terpilih as $option_terpilih) {
                                                                                if ($option_terpilih == $option) {
                                                                                    echo "selected";
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                        ?> value="<?php echo $option ?>">
                                                                            <?php echo $option ?>
                                                                        </option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:15%"> Layanan 3 </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <select required id="layanan_3" required
                                                                    name="Layanan_3" class="form-control">
                                                                    <?php
                                                                    if (isset($edit_data_pendaftar_program_layanan)) {
                                                                        if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") {
                                                                            $array_option = ["", "Biologi", "Teknologi/IT", "Tahfidz", "Bahasa", "Ekonomi/Akuntantsi"];
                                                                        } else {
                                                                            if ($edit_data_pendaftar_program_layanan['Program'] == "MIPA") {
                                                                                $array_option = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                                                            } else {
                                                                                $array_option = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $array_option = [""];
                                                                    }
                                                                    $array_option_terpilih = [];
                                                                    if (isset($edit_data_pendaftar_program_layanan)) {
                                                                        if ($edit_data_pendaftar_program_layanan['Layanan_3'] <> "") {
                                                                            $array_option_terpilih[] = $edit_data_pendaftar_program_layanan['Layanan_3'];
                                                                        } else {
                                                                            $array_option_terpilih[] = "";
                                                                        }
                                                                    }
                                                                    foreach ($array_option as $option) {
                                                                        ?>
                                                                        <option <?php
                                                                        if (isset($edit_data_pendaftar_program_layanan)) {
                                                                            foreach ($array_option_terpilih as $option_terpilih) {
                                                                                if ($option_terpilih == $option) {
                                                                                    echo "selected";
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                        ?> value="<?php echo $option ?>">
                                                                            <?php echo $option ?>
                                                                        </option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr id="kolom_layanan_4" style="<?php if ((isset($edit_data_pendaftar_program_layanan)) and (($edit_data_pendaftar_program_layanan['Program'] == "IPS"))) {
                                                        echo "display:none";
                                                    } ?>">
                                                        <td style="width:15%"> Layanan 4 </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <select <?php if ((isset($edit_data_pendaftar_program_layanan)) and (($edit_data_pendaftar_program_layanan['Program'] == "IPS"))) {
                                                                    echo "";
                                                                } else {
                                                                    echo "required";
                                                                } ?>   id="layanan_4" name="Layanan_4"
                                                                    class="form-control">
                                                                    <?php
                                                                    if (isset($edit_data_pendaftar_program_layanan)) {
                                                                        if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") {
                                                                            $array_option = ["", "Biologi", "Teknologi/IT", "Tahfidz", "Bahasa", "Ekonomi/Akuntantsi"];
                                                                        } else {
                                                                            if ($edit_data_pendaftar_program_layanan['Program'] == "MIPA") {
                                                                                $array_option = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                                                            } else {
                                                                                $array_option = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $array_option = [""];
                                                                    }
                                                                    $array_option_terpilih = [];
                                                                    if (isset($edit_data_pendaftar_program_layanan)) {
                                                                        if ($edit_data_pendaftar_program_layanan['Layanan_4'] <> "") {
                                                                            $array_option_terpilih[] = $edit_data_pendaftar_program_layanan['Layanan_4'];
                                                                        } else {
                                                                            $array_option_terpilih[] = "";
                                                                        }
                                                                    }
                                                                    foreach ($array_option as $option) {
                                                                        ?>
                                                                        <option <?php
                                                                        if (isset($edit_data_pendaftar_program_layanan)) {
                                                                            foreach ($array_option_terpilih as $option_terpilih) {
                                                                                if ($option_terpilih == $option) {
                                                                                    echo "selected";
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                        ?> value="<?php echo $option ?>">
                                                                            <?php echo $option ?>
                                                                        </option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="3">
                                                            <font class="text-muted"><i> Note : Setiap layanan harus
                                                                    memilih kelas yang berbeda </i></font>
                                                        </th>
                                                    </tr>

                                                </tbody>
                                            </table>

                                            <br>

                                            <div class="alert alert-danger my-4 text-center" role="alert">
                                                <font class="text-danger"> <b> PERHATIAN! </b> </font> Harap pastikan
                                                bahwa data yang anda masukkan adalah <font class="text-success"> <b>
                                                        BENAR </b> </font>, Karena <font class="text-danger"> DATA YANG
                                                    SUDAH DIVERIFIKASI <b> TIDAK DAPAT DIUBAH </b> </font> <br>
                                                <!-- Anda dapat menghubuni admin jika ingin mengubah data yang sudah diverifikasi -->
                                            </div>


                                            <div class="form-group text-center">
                                                <?php if ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] != "Sudah Diverifikasi") { ?>
                                                    <button onclick="return cek_duplikat_layanan()" class="btn btn-primary"
                                                        type="submit" name="submit_update"> <i class="fa fa-check"></i>
                                                        &nbsp; Simpan </button>
                                                    <a href="?menu=<?php echo $_GET['menu'] ?>" class="btn btn-danger"> <i
                                                            class="fa fa-times"></i> &nbsp; Batal </a>
                                                <?php } ?>
                                            </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>