<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Cara Pembayaran Formulir</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pendaftaran</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pembayaran Formulir</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Cara Bayar</a>
                </li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>Pembayaran Formulir</h2>
                                    </div>
                                </div>
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td style="width:30%"> No. Pendaftaran </td>
                                            <td style="width:70%"> <?php echo "Muhammad Ardan Rahadian Syarif" ?> </td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%"> Nama Siswa </td>
                                            <td style="width:70%"> <?php echo "value" ?> </td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%"> NISN </td>
                                            <td style="width:70%"> <?php echo "value" ?> </td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%"> Pembayaran Formulir </td>
                                            <td style="width:70%"> <?php echo "value" ?> </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">

                        <div class="full-height">
                            <div class="accordion" id="accordionTataCaraPembayaran">
                                <div class="card mb-0">
                                    <button class="btn bg-white btn-sm" type="button" data-toggle="collapse" data-target="#collapseTataCaraPembayaran" aria-expanded="true" aria-controls="collapseTataCaraPembayaran">
                                        <div class="card-header" id="headingTataCaraPembayaran">
                                            <div class="row">
                                                <div class="col-lg-10">
                                                    <div class="card-title text-left">
                                                        Tata Cara Pembayaran
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 text-right">
                                                    <i class="flaticon-down-arrow"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </button>

                                    <div id="collapseTataCaraPembayaran" class="collapse show" aria-labelledby="headingTataCaraPembayaran" data-parent="#accordionTataCaraPembayaran">
                                        <div class="card-body bg-white" style="border:0px">
                                            <p>Untuk melakukan pembayaran melalui Xendit, berikut adalah tata cara umumnya:</p>
                                            <p>Pilih Metode Pembayaran: Dalam proses pembayaran melalui Xendit, pertama-tama Anda perlu memilih metode pembayaran yang ingin digunakan. Xendit mendukung berbagai metode pembayaran, seperti kartu kredit/debit, transfer bank, dan e-wallet.</p>
                                            <p>Isi Informasi Pembayaran: Setelah memilih metode pembayaran, Anda perlu mengisi informasi pembayaran yang diminta. Informasi ini mungkin mencakup jumlah pembayaran, nomor kartu kredit/debit, atau detail bank yang akan digunakan untuk transfer.</p>
                                            <p>Verifikasi Informasi: Pastikan untuk memeriksa kembali informasi yang Anda isi sebelum melanjutkan. Pastikan semua data yang Anda masukkan benar.</p>
                                            <p>Konfirmasi Pembayaran: Setelah mengisi informasi dengan benar, Anda biasanya akan diminta untuk mengkonfirmasi pembayaran. Ini bisa berupa mengklik tombol "Bayar" atau langkah serupa, tergantung pada metode pembayaran yang Anda pilih.</p>
                                            <p>Otorisasi Pembayaran: Jika Anda menggunakan kartu kredit/debit, Anda mungkin akan diminta untuk memasukkan kode keamanan kartu (CVV) atau kode verifikasi lainnya untuk mengotorisasi pembayaran.</p>
                                            <p>Tunggu Konfirmasi: Setelah mengkonfirmasi pembayaran, tunggu hingga Anda menerima konfirmasi pembayaran berhasil. Ini bisa berupa halaman konfirmasi online atau pemberitahuan email.</p>
                                            <p>Cetak atau Simpan Bukti Pembayaran: Selalu disarankan untuk mencetak atau menyimpan bukti pembayaran Anda, seperti tanda terima atau email konfirmasi. Ini akan berguna jika Anda memerlukan bukti pembayaran di masa depan.</p>
                                            <p>Selesai: Setelah Anda menerima konfirmasi pembayaran berhasil, proses pembayaran melalui Xendit telah selesa/i.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="full-height">
                                    <div class="card-header">
                                        <div class="card-title">
                                            Metode Pembayaran
                                        </div>
                                    </div>

                                    <div class="accordion" id="accordionMetodePembayaran">
                                        <div class="card mb-0">
                                            <button class="btn bg-white btn-sm" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                <div class="card-header" id="headingOne">
                                                    <div class="row">
                                                        <div class="col-lg-10">
                                                            <div class="card-title">
                                                                <h4 class="text-left"> Pembayaran melalui ATM </h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 text-right">
                                                            <i class="flaticon-down-arrow"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </button>

                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionMetodePembayaran">
                                                <div class="card-body">
                                                    1. Masukkan kartu ATM Anda ke mesin ATM.<br>
                                                    1. Pilih menu "Pembayaran Tagihan."<br>
                                                    1. Masukkan kode pembayaran dan nominal yang sesuai.<br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card mb-0">
                                            <button class="btn bg-white btn-sm collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                <div class="card-header" id="headingTwo">
                                                    <div class="row">
                                                        <div class="col-lg-10">
                                                            <div class="card-title">
                                                                <h4 class="text-left"> Pembayaran melalui Mobile Banking </h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 text-right">
                                                            <i class="flaticon-down-arrow"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </button>
                                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionMetodePembayaran">
                                                <div class="card-body">
                                                    1. Buka aplikasi mobile banking Anda.<br>
                                                    1. Pilih menu "Transfer/Pembayaran."<br>
                                                    1. Pilih tujuan pembayaran dan masukkan kode pembayaran.<br>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card mb-0">
                                            <button class="btn bg-white btn-sm collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                <div class="card-header" id="headingThree">
                                                    <div class="row">
                                                        <div class="col-lg-10">
                                                            <div class="card-title">
                                                                <h4 class="text-left"> Pembayaran melalui E-Wallet </h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 text-right">
                                                            <i class="flaticon-down-arrow"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </button>
                                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionMetodePembayaran">
                                                <div class="card-body">
                                                    1. Buka aplikasi E-Wallet (contoh: GoPay, OVO, Dana).<br>
                                                    1. Pilih menu "Bayar Tagihan."<br>
                                                    1. Masukkan kode pembayaran dan nominal yang sesuai.<br>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card mb-0">
                                            <button class="btn bg-white btn-sm collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                <div class="card-header" id="headingFour">
                                                    <div class="row">
                                                        <div class="col-lg-10">
                                                            <div class="card-title">
                                                                <h4 class="text-left"> Pembayaran melalui Retail </h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 text-right">
                                                            <i class="flaticon-down-arrow"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </button>
                                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionMetodePembayaran">
                                                <div class="card-body">
                                                    1. Datang ke gerai Indomaret atau Alfamart terdekat.<br>
                                                    1. Kasir akan membantu Anda untuk melakukan pembayaran.<br>
                                                    1. Ingatkan kasir untuk menggunakan kode pembayaran.<br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary"> Bayar Sekarang </button>
                                <a href="?menu=pembayaran_ppdb"><button class="btn btn-danger"> Kembali </button></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

</div>