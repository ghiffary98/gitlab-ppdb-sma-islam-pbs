<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "controllers/siswa/profile/controller_reset_password.php";
?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Reset Password</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pendaftaran</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Reset Password</a>
                </li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <h2 class=""> <b> Reset Password </b> </h2>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <form method="POST">
                                <div class="form-group row">
                                    <label class="col-lg-3">
                                        Password Lama
                                    </label>
                                    <div class="col-lg-9">
                                        <input required type="password" name="Password_Lama" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3">
                                        Password Baru
                                    </label>
                                    <div class="col-lg-9">
                                        <input required type="password" name="Password_Baru" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3">
                                        Konfirmasi Password
                                    </label>
                                    <div class="col-lg-9">
                                        <input required type="password" name="Konfirmasi_Password_Baru" class="form-control">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="text-center">
                                        <button class="btn btn-primary" type="submit" name="submit_update"><i class="fa fa-check"></i> Save </button>
                                        <button class="btn btn-danger" type="submit"><i class="fa fa-times"></i> Cancel </button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>