<div class="container">
    <div class="row justify-content-center align-items-center mt-4">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">

                        <center>
                            <img src="assets/images/logo/logo_sma_soedirman.png" alt="SMA-Islam-PBS-Cijantung" style="height: 120px; width: auto" />
                        </center>
                        <h2 class="text-center mt-4">
                            <b>PENDAFTARAN PPDB <br> <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></b>
                            <?php if (isset($data_ppdb)) { ?>
                                <br>
                                <b>
                                    <?php echo $data_ppdb['Tahun_Ajaran'] ?>
                                </b>
                            <?php } ?>
                        </h2>

                    </div>
                    <hr>
                    <?php if (isset($data_ppdb)) { ?>
                        <form method="POST">
                            <div class="form-group row">
                                <div class="col-12">
                                    <label>Email*</label>
                                    <input name="Email" id="email" type="email" class="form-control" placeholder="Masukkan Email" required />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label>No. HP*</label>
                                    <input name="Nomor_Handphone" type="number" class="form-control" placeholder="Masukkan Nomor HP" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="password">Password</label>
                                    <div class="input-group">
                                        <input name="Password" type="password" placeholder="Masukkan Password" class="form-control" style="background-color:white" required />
                                        <div class="input-group-append">
                                            <button type="button" id="togglePassword" class="btn btn-sm btn-secondary">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <label id="password_notice" class="text-danger" style="display: none;">Password minimal 8 karakter</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <label>Konfirmasi Password*</label>
                                    <div class="input-group">
                                        <input name="Konfirmasi_Password" type="password" class="form-control" placeholder="Masukkan Kembali Password" required />
                                        <div class="input-group-append">
                                            <button type="button" id="toggleKonfirmasiPassword" class="btn btn-sm btn-secondary">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <label>Nama Lengkap*</label>
                                    <input name="Nama_Lengkap" id="nama_lengkap" type="text" class="form-control" placeholder="Masukkan Nama Depan" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="">
                                        <label>Tempat Lahir*</label>
                                        <input name="Tempat_Lahir" id="tempat_lahir" type="text" class="form-control" placeholder="Masukkan " required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label>Tanggal Lahir*</label>
                                    <input onchange="Cek_Umur_Maksimal_Pendaftar()" id="tanggal_lahir"  name="Tanggal_Lahir" type="date" class="form-control" placeholder="Masukkan " required />
                                    <?php if((isset($data_ppdb['Umur_Maksimal_Pendaftar'])) AND ($data_ppdb['Umur_Maksimal_Pendaftar'] <> 0)){ ?>
                                    <label id="tanggal_lahir_notice" class="text-danger" style="display: none;">Maksimal umur pendaftar adalah <?php echo $data_ppdb['Umur_Maksimal_Pendaftar']?> Tahun<br> dari tanggal pendaftaran terakhir PPDB ini (<?php echo tanggal_indonesia($data_ppdb['Tanggal_Akhir_Pendaftaran'])?>)</label>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Alamat Lengkap</label>
                                <textarea name="Alamat_Lengkap" class="form-control" placeholder="Masukkan " rows="5"></textarea>
                            </div>

                            <hr>

                            <div class="text-center mt-4">
                                <button type="submit" id="submitDaftar" name="Submit_Daftar" class="btn btn-block btn-primary fw-bold" disabled>
                                    Daftar
                                </button>
                            </div>
                        </form>
                        <div class="login-account text-center mt-4">
                            <span class="mt-2">Sudah punya akun?</span>
                            <a href="login.php" id="show-signup" class="link">Login</a>
                        </div>
                    <?php } else { ?>
                        <br>
                        <center>
                            <b>Mohon Maaf, Untuk Saat Ini, PPDB Sedang Tidak Tersedia. Silahkan dicoba kembali di lain waktu</b>
                        </center>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (isset($data_ppdb)) { ?>
    <script>
        const inputFieldNamaLengkap = document.getElementById("nama_lengkap");
        inputFieldNamaLengkap.addEventListener("input", function(event) {
            event.preventDefault();
            const inputValue = inputFieldNamaLengkap.value.replace(/[^a-zA-Z0-9\s-]/g, ''); // Allow alphabetic characters, numbers, spaces, and dashes
            const formattedValue = inputValue.toUpperCase().replace(/\s/g, ' '); // Convert to uppercase and replace spaces with underscores
            inputFieldNamaLengkap.value = formattedValue;
        });
    </script>

    <script>
        const inputFieldTempatLahir = document.getElementById("tempat_lahir");
        inputFieldTempatLahir.addEventListener("input", function(event) {
            event.preventDefault();
            const inputValue = inputFieldTempatLahir.value.replace(/[^a-zA-Z0-9\s-]/g, ''); // Allow alphabetic characters, numbers, spaces, and dashes
            const formattedValue = inputValue.toUpperCase().replace(/\s/g, ' '); // Convert to uppercase and replace spaces with underscores
            inputFieldTempatLahir.value = formattedValue;
        });
    </script>

    <script>
        const emailInput = document.getElementById("email");
        emailInput.addEventListener("input", function(event) {
            event.preventDefault();
            emailInput.value = emailInput.value.toLowerCase();
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const passwordInput = document.querySelector('input[name="Password"]');
            const togglePasswordButton = document.getElementById("togglePassword");
            const passwordNotice = document.getElementById("password_notice");
            const submitDaftar = document.getElementById("submitDaftar");

            const minLength = 8;

            passwordInput.addEventListener("keyup", function() {
                const passwordValue = passwordInput.value;

                if (passwordValue.length >= minLength) {
                    passwordNotice.style.display = "none";
                    submitDaftar.disabled = false;
                } else {
                    passwordNotice.style.display = "block"; // Or set to an appropriate display value
                    submitDaftar.disabled = true;
                }
            });

            togglePasswordButton.addEventListener("click", function() {
                if (passwordInput.type === "password") {
                    passwordInput.type = "text";
                    togglePasswordButton.innerHTML = '<i class="fa fa-eye-slash"></i>';
                } else {
                    passwordInput.type = "password";
                    togglePasswordButton.innerHTML = '<i class="fa fa-eye"></i>';
                }
            });
        });
    </script>



    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const passwordInput = document.querySelector('input[name="Konfirmasi_Password"]');
            const togglePasswordButton = document.getElementById("toggleKonfirmasiPassword");

            togglePasswordButton.addEventListener("click", function() {
                if (passwordInput.type === "password") {
                    passwordInput.type = "text";
                    togglePasswordButton.innerHTML = '<i class="fa fa-eye-slash"></i>';
                } else {
                    passwordInput.type = "password";
                    togglePasswordButton.innerHTML = '<i class="fa fa-eye"></i>';
                }
            });
        });
    </script>

    <script>
        function Cek_Umur_Maksimal_Pendaftar(){
            <?php if((isset($data_ppdb['Umur_Maksimal_Pendaftar'])) AND ($data_ppdb['Umur_Maksimal_Pendaftar'] <> 0)){ ?>
                var Tanggal_Lahir_Dipilih = document.getElementById("tanggal_lahir").value;
                var Tanggal_Lahir = new Date(Tanggal_Lahir_Dipilih);
                var Tanggal_Batas = new Date('<?php echo $Tanggal_Batas_Maksimal_Umur_Pendaftaran ?>');
                const tanggalLahirNotice = document.getElementById("tanggal_lahir_notice");

                if (Tanggal_Lahir < Tanggal_Batas) {
                    tanggalLahirNotice.style.display = "block";
                    submitDaftar.disabled = true;
                }else{
                    tanggalLahirNotice.style.display = "none";
                    submitDaftar.disabled = false;
                }
            <?php } ?>

        }
    </script>
<?php } ?>