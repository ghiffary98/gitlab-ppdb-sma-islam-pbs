<div class="container">
    <div class="row justify-content-center align-items-center mt-4">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <center><img src="assets/images/logo/logo_sma_soedirman.png" alt="SMA-Islam-PBS-Cijantung" style="height: 95px; width: auto" /></center>
                        </div>
                        <div class="col-lg-10">
                            <h1 class="mt-1">
                                <b>KETENTUAN PENDAFTARAN <br> PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?> <?php if (isset($data_ppdb)) { ?><?php echo $data_ppdb['Tahun_Ajaran'] ?><?php } ?></b>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th colspan="2" style="font-size: 1.3em;" class="text-left"> Ketentuan Umum : </th>
                                    </tr>
                                    <?php
                                    $nomor = 0;
                                    if ((isset($list_data_syarat_dan_ketentuan_data_ketentuan))) {
                                        foreach ($list_data_syarat_dan_ketentuan_data_ketentuan as $data) {
                                            $nomor++; ?>
                                            <tr>
                                                <th class="bg-black text-white" style="width:5%; font-size: 1.2em;">
                                                    <div class="p-2 text-right"><?php echo $nomor?></div>
                                                </th>
                                                <th class="" style="width:95%; border:2px solid black; font-size: 1em;"><?php echo $data['Syarat_Dan_Ketentuan'] ?></th>
                                            </tr>
                                        <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>

                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th colspan="2" style="font-size: 1.3em;" class="text-left"> Catatan : </th>
                                    </tr>
                                    <?php
                                    $nomor = 0;
                                    if ((isset($list_data_syarat_dan_ketentuan_data_catatan))) {
                                        foreach ($list_data_syarat_dan_ketentuan_data_catatan as $data) {
                                            $nomor++; ?>
                                            <tr>
                                                <th class="bg-black text-white" style="width:5%; font-size: 1.2em;">
                                                    <div class="p-2 text-right"><?php echo $nomor?></div>
                                                </th>
                                                <th class="" style="width:95%; border:2px solid black; font-size: 1em;"><?php echo $data['Syarat_Dan_Ketentuan'] ?></th>
                                            </tr>
                                        <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- < ?php print_r($data_sekolah_saat_ini) ?> -->

                    <form action="register.php" method="POST">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <hr>
                                Apakah anda setuju dengan ketentuan di atas? <br><br>
                            </div>
                            <div class="col-lg-12">
                                <span class="p-2 text-white  bg-success">
                                    <input class="radio" required type="checkbox" name="radio" style="cursor: pointer">
                                    <font style="font: size 1.1rem;">&nbsp; Ya, Saya Setuju</font>
                                </span>
                            </div>
                            <div class="col-lg-12 text-right">
                                <hr>
                                <a href="login.php"><button type="button" class="btn btn-danger"> <b>Kembali</b> </button></a>
                                <button type="submit" class="btn btn-primary"> <b>Lanjutkan</b> </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>