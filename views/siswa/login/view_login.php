<div class="container">
  <div class="row justify-content-center align-items-center" style="height: 100vh">
    <div class="col-lg-5">
      <div class="card">
        <div class="card-body">
          <?php if (isset($data_ppdb)) { ?>
            <div class="text-center align-items-center mt-2" style="align-self:center; align-content:center">
              <img src="assets/images/logo/logo_sma_soedirman.png" alt="SMA-Islam-PBS-Cijantung" style="height: 150px; width: auto" />
              <h3 class="mt-2"> <b>LOGIN SISWA</b></h3>
              <h5 class="text-center">
                <b>PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></b>
                <br>
                <b><?php echo $data_ppdb['Tahun_Ajaran'] ?></b>
              </h5>
            </div>

            

            <form method="POST">
              <div class="form-group form-floating-label">
                <input id="emailFloatingLabel" name="Username" type="text" class="form-control input-border-bottom" required>
                <label for="emailFloatingLabel" class="placeholder">Email/No.HP</label>
              </div>
              <div class="form-group form-floating-label">
                <input id="passwordFloatingLabel" name="Password" type="password" class="form-control input-border-bottom" required>
                <label for="passwordFloatingLabel" class="placeholder">Password</label>
              </div>
              <div class="form-group form-check d-none">
                <input type="checkbox" class="form-check-input" id="rememberme" />
                <label class="form-check-label" for="rememberme">Remember Me</label>
              </div>
              <div class="text-center mt-4">
                <button type="submit" name="Submit_Login" class="btn btn-block btn-primary fw-bold">Masuk</button></a>
              </div>
            </form>
            <div class="login-account text-center mt-4">
              <span class="mt-2">
                <hr>
                <h1><b class="text-danger">Belum punya akun?</b></h1>
              </span>
              <h1><b>Registrasi </b><a href="persyaratan.php" id="show-signup" class="link"><b class="text-primary"><u>di sini</u></b></a></h1>
            </div>
          <?php } else { ?>
            <div class="text-center align-items-center mt-2" style="align-self:center; align-content:center">
              <img src="assets/images/logo/logo_sma_soedirman.png" alt="SMA-Islam-PBS-Cijantung" style="height: 200px; width: auto" />
              <h3 class="mt-4"> <b>LOGIN SISWA</b></h3>
              <h5 class="text-center">
                <b>PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></b>
                <br>
                <br>
                <br>
                <b>Mohon Maaf, Untuk Saat Ini, PPDB Sedang Tidak Tersedia. Silahkan dicoba kembali di lain waktu</b>
              </h5>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>