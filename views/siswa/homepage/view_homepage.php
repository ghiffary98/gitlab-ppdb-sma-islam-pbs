<style type="text/css">
    .logo-mobile {
        display: none;
    }

    .logo-desktop {
        display: block;
    }

    #div_login_mobile {
        display: none;
    }

    .text-info-akun {
        text-align: left;
    }

    .tombol-info-akun {
        text-align: right;
    }

    @media screen and (max-width: 768px) {

        .logo-mobile {
            display: block;
        }

        .logo-desktop {
            display: none;
        }

        #div_login_mobile {
            display: block;
        }

        .text-info-akun {
            text-align: center;
        }

        .tombol-info-akun {
            text-align: center;
        }

    }
</style>

<div>
    <div class="main-header">
        <!-- Logo Header -->
        <div class="logo-header" data-background-color="blue">
            <a href="#" class="logo text-white text-center">
                <b><?php echo $data_ppdb['Judul'] ?></b>
            </a>
        </div>

        <!-- Navbar Header -->
        <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

            <div class="container-fluid">
                <img src="assets/images/logo/logo-pb-soedirman-light2.png" alt="navbar brand" class="navbar-brand" style="height: 50px;">
                <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                    <li class="nav-item "> <a href="register.php?id_ppdb=<?php echo $a_hash->encode($data_ppdb['Id_PPDB']) ?>" class="btn btn-danger btn-sm"><b><i class="fas fa-user"></i> &nbsp; Daftar</b></a> </li>
                    <li class="nav-item"> <a href="login.php?id_ppdb=<?php echo $a_hash->encode($data_ppdb['Id_PPDB']) ?>" class="btn btn-warning btn-sm"><b> <i class="fas fa-sign-in-alt"></i> &nbsp; Login</b></a> </li>
                </ul>
            </div>
        </nav>
        <!-- End Navbar -->

    </div>
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner p-5">
                <div class="text-center mt-5">
                    <div class="logo-mobile">
                        <img src="assets/images/logo/logosmaislampbscijantung.png" style="height: 150px;">
                    </div>
                    <div class="logo-desktop">
                    <br>
                        <img src="assets/images/logo/logo-pb-soedirman-light2.png" alt="navbar brand" class="navbar-brand">
                    </div>
                    <br><br>
                    <h1 class="text-white text-center" style="font-size:xx-large">SELAMAT DATANG DI <br> <b>WEB PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></b></h1><br>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row mt--2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="alert <?php if ($data_ppdb['Status_PPDB'] == "Aktif") { ?> alert-success <?php } else { ?> alert-danger <?php } ?> alert-dismissible" role="alert">
                                <form action="" method="post">
                                    <div class="row form-group">
                                        <div class="col-lg-8 text-left">
                                            <h3>
                                                <?php if ($data_ppdb['Status_PPDB'] == "Aktif") { ?>
                                                    <font class="text-success"><strong>Status Pendaftaran "<?php echo $data_ppdb['Judul'] ?>" MASIH DIBUKA</strong></font>
                                                <?php } else { ?>
                                                    <font class="text-danger"><strong>Status Pendaftaran "<?php echo $data_ppdb['Judul'] ?>" SUDAH DITUTUP</strong></font>
                                                <?php } ?>
                                            </h3>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <div style="display: none" class="card-category">Informasi PPDB <?php echo $data_ppdb['Tahun_Ajaran'] ?></div>
                            <div style="display: none" class="form-group row">

                                <div class="col-lg-4">
                                    <div class="card text-center">
                                        <div class="card-body" id="">
                                            <h3 class="fw-bold mt-3 mb-0">Pendaftar</h3>
                                            <div class="mt-3">
                                                <h1 class="card-ttile text-info"> <b> <i class="fas fa-user"></i> &nbsp; <?php echo $hitung_Total_Pendaftar ?> </b> </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card text-center">
                                        <div class="card-body" id="">
                                            <h3 class="fw-bold mt-3 mb-0">Siswa Diterima</h3>
                                            <div class="mt-3">
                                                <h1 class="card-ttile text-success"> <b> <i class="fas fa-user-check"></i> &nbsp; <?php echo $hitung_Total_Pendaftar_Diterima ?> </b> </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card text-center">
                                        <div class="card-body" id="">
                                            <h3 class="fw-bold mt-3 mb-0">Tidak Diterima</h3>
                                            <div class="mt-3">
                                                <h1 class="card-ttile text-danger"> <b> <i class="fas fa-times-circle"></i> &nbsp; <?php echo $hitung_Total_Pendaftar_Tidak_Diterima ?> </b> </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="col-lg-3">
                                    <div class="card text-center">
                                        <div class="card-body" id="">
                                            <h3 class="fw-bold mt-3 mb-0">Sisa Kuota</h3>
                                            <div class="mt-3">
                                                <h1 class="card-ttile text-warning"> <b> <i class="fas fa-clock"></i> &nbsp; ?? </b> </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($data_ppdb['Status_PPDB'] == "Aktif") { ?>
                <div class="row mb-4">
                    <div class="col-lg-12">
                        <div class="">
                            <div class="">
                                <div class="alert alert-danger">
                                    <div class="row form-group">
                                        <div class="col-lg-9 text-info-akun">
                                            <h1>
                                                <font class="text-danger"><strong>BELUM PUNYA AKUN? </strong></font>Silahkan Daftar terlebih dahulu!
                                            </h1>
                                        </div>
                                        <div class="col-lg-3 tombol-info-akun">
                                            <a href="register.php?id_ppdb=<?php echo $a_hash->encode($data_ppdb['Id_PPDB']) ?>" class="btn btn-danger"> <b> <i class="fas fa-user"></i> &nbsp; DAFTAR DI SINI</b></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row mb-4" id="div_login_mobile">
                    <div class="col-lg-12">
                        <div class="">
                            <div class="">
                                <div class="alert alert-warning">
                                    <div class="row form-group">
                                        <div class="col-lg-9 text-info-akun">
                                            <h1>
                                                <font class="text-warning"><strong>SUDAH PUNYA AKUN? </strong></font>Silahkan Login!
                                            </h1>
                                        </div>
                                        <div class="col-lg-3 tombol-info-akun">
                                            <a href="login.php?id_ppdb=<?php echo $a_hash->encode($data_ppdb['Id_PPDB']) ?>" class="btn btn-warning"> <b> <i class="fas fa-user"></i> &nbsp; LOGIN</b></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="row">
                <div class="col-md-4">
                    <div class="card full-height">
                        <div class="card-header">
                            <div class="card-head-row">
                                <div class="card-title">Alur PPDB</div>
                            </div>
                        </div>
                        <div class="card-body">
                            <img src="<?php echo $folder_penyimpanan_file_foto_alur_pendaftaran ?><?php echo $data_ppdb['Foto_Alur_Pendaftaran'] ?>" alt="" class="" style="width:100%; height:auto">
                            <br>
                        </div>
                        <div class="card-footer">
                            <div class="text-center">Silahkan klik <a href="<?php echo $folder_penyimpanan_file_foto_alur_pendaftaran ?><?php echo $data_ppdb['Foto_Alur_Pendaftaran'] ?>">link</a> untuk mendownload alur <?php echo $data_ppdb['Judul'] ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card full-height">
                        <!-- <div class="card-header">
                            <div class="card-head-row">
                                <div class="card-title">Pengumuman</div>
                            </div>
                        </div>
                        <div class="card-body" style="overflow-x: scroll; ">
                            <?php
                            // if ((isset($list_datatable_pengumuman))) {
                            // foreach ($list_datatable_pengumuman as $data) {
                            // $nomor++; 
                            ?>
                                    <div class="d-flex">
                                        <div class="p-2">
                                            <i class="fas fa-info text-info"></i>
                                        </div>
                                        <div class="flex-1 ml-3 pt-1">
                                            <h4 class="text-uppercase fw-bold mb-1"><?php echo $data['Judul_Pengumuman'] ?></h4>
                                            <span class="text-muted"><?php echo $data['Isi_Pengumuman'] ?></span>
                                        </div>
                                        <div class="float-right pt-1">
                                            <small class="text-muted"><?php echo tanggal_dan_waktu_24_jam_indonesia($data['Waktu_Simpan_Data']) ?></small>
                                        </div>
                                    </div>
                                    <div class="separator-dashed"></div>
                                <?php
                                // }
                                // }else{
                                ?>
                            <i class=""> Tidak ada Pengumuman</i>
                        <?php //} 
                        ?>
                        </div> -->
                        <div class="card-header">
                            <div class="card-head-row">
                                <div class="card-title">KETENTUAN PENDAFTARAN <br> PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?> <?php if (isset($data_ppdb)) { ?><?php echo $data_ppdb['Tahun_Ajaran'] ?><?php } ?></div>
                            </div>
                        </div>
                        <div class="card-body" style="overflow-x: scroll; ">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th colspan="2" style="font-size: 1.3em;" class="text-left"> Ketentuan Umum : </th>
                                    </tr>
                                    <?php
                                    $nomor = 0;
                                    if ((isset($list_data_syarat_dan_ketentuan_data_ketentuan))) {
                                        foreach ($list_data_syarat_dan_ketentuan_data_ketentuan as $data) {
                                            $nomor++; ?>
                                            <tr>
                                                <th class="bg-black text-white" style="width:5%; font-size: 1.2em;">
                                                    <div class="p-2 text-right"><?php echo $nomor?></div>
                                                </th>
                                                <th class="" style="width:95%; border:2px solid black; font-size: 1em;"><?php echo $data['Syarat_Dan_Ketentuan'] ?></th>
                                            </tr>
                                        <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>

                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th colspan="2" style="font-size: 1.3em;" class="text-left"> Catatan : </th>
                                    </tr>
                                    <?php
                                    $nomor = 0;
                                    if ((isset($list_data_syarat_dan_ketentuan_data_catatan))) {
                                        foreach ($list_data_syarat_dan_ketentuan_data_catatan as $data) {
                                            $nomor++; ?>
                                            <tr>
                                                <th class="bg-black text-white" style="width:5%; font-size: 1.2em;">
                                                    <div class="p-2 text-right"><?php echo $nomor?></div>
                                                </th>
                                                <th class="" style="width:95%; border:2px solid black; font-size: 1em;"><?php echo $data['Syarat_Dan_Ketentuan'] ?></th>
                                            </tr>
                                        <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-header">
        <div class="page-inner bg-black-gradient">
            <div class="form-group row">
                <div class="col-lg-4 text-white">
                    <h1>SMA Islam PB Soedirman</h1>
                    <?php echo $data_sekolah_saat_ini['Alamat_Lengkap'] ?>
                </div>

                <div class="col-lg-4 text-white">
                    <h2>Informasi Kontak</h2>
                    <i class="mb-3 fas fa-phone-square"></i> &nbsp; <?php echo $data_sekolah_saat_ini['Nomor_Telpon'] ?> <br>
                    <i class="mb-3 fas fa-mobile-alt"></i> &nbsp; <?php echo $data_sekolah_saat_ini['Nomor_Handphone'] ?> <br>
                    <i class="mb-3 fas fa-envelope"></i> &nbsp;<?php echo $data_sekolah_saat_ini['Email_Customer_Service'] ?>
                </div>

                <div class="col-lg-4 text-white">
                    <h2>Peta Lokasi</h2>
                    <iframe style="width:100%; height:auto" src="https://www.google.com/maps/embed?pb=<?php echo $data_sekolah_saat_ini['Embed_Google_Maps'] ?>" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>

        <div class="px-4 py-2 bg-black">
            <div class="form-group row">
                <div class="col-lg-6 text-white text-left px-2">
                    WEB PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?>
                </div>
                <div class="col-lg-6 text-white text-right px-2">
                    Copyright &copy; 2023. All Right Reserved</a>
                </div>
            </div>
        </div>
    </div>
</div>