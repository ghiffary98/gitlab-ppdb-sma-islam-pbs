<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "controllers/siswa/pengumuman/controller_pengumuman_kelulusan.php";
?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Pengumuman Kelulusan</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="#">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pendaftaran</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Pengumuman Kelulusan</a>
                </li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <h2 class=""> <b> Pengumuman Kelulusan </b> </h2>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="width:30%"> No. Pendaftaran </td>
                                        <td style="width:70%"> <?php echo $edit_data_pendaftar['Nomor_Pendaftaran'] ?> </td>
                                    </tr>
                                    <tr>
                                        <td style="width:30%"> Nama Siswa </td>
                                        <td style="width:70%"> <?php echo $edit_data_pendaftar['Nama_Lengkap'] ?> </td>
                                    </tr>
                                    <tr>
                                        <td style="width:30%"> NISN </td>
                                        <td style="width:70%"> <?php echo $edit_data_pendaftar['NISN'] ?> </td>
                                    </tr>
                                    <tr>
                                        <td style="width:30%"> Tempat / Tanggal Lahir </td>
                                        <td style="width:70%"> <?php echo $edit_data_pendaftar['Tempat_Lahir'] . "," . $edit_data_pendaftar['Tanggal_Lahir'] ?> </td>
                                    </tr>

                                    <tr>
                                        <td style="width:30%"> Alamat </td>
                                        <td style="width:70%"> <?php echo $edit_data_pendaftar['Jalan'] ?> </td>
                                    </tr>
                                    <tr>
                                        <td style="width:30%"> Asal Sekolah </td>
                                        <td style="width:70%"> <?php echo $edit_data_pendaftar['Asal_Sekolah'] ?> </td>
                                    </tr>
                                </tbody>
                            </table>

                            <?php if ($edit_data_pendaftar['Status_Kelulusan'] == "Lulus") { ?>

                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th colspan="2" class="text-center">
                                                <h3> S T A T U S </h3>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2" class="text-center bg-success text-white">
                                                <h1 style="font-weight:bold"> LULUS </h1>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th colspan="2" class="text-center"> Selamat! Anda dinyatakan Lulus menjadi Siswa SMA Islam PB Soedirman Jakarta Timur</th>
                                        </tr>
                                    </tbody>
                                </table>

                            <?php } elseif ($edit_data_pendaftar['Status_Kelulusan'] == "Tidak Lulus") { ?>

                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th colspan="2" class="text-center">
                                                <h3> S T A T U S </h3>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2" class="text-center bg-danger text-white">
                                                <h1 style="font-weight:bold"> TIDAK LULUS </h1>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th colspan="2" class="text-center"> Mohon maaf, anda belum diterima di <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></th>
                                        </tr>
                                    </tbody>
                                </table>


                                <?php } elseif ($edit_data_pendaftar['Status_Kelulusan'] == "Gugur") { ?>

                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th colspan="2" class="text-center">
                                                    <h3> S T A T U S </h3>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="2" class="text-center bg-danger text-white">
                                                    <h1 style="font-weight:bold"> GUGUR </h1>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th colspan="2" class="text-center"> Mohon maaf, anda belum diterima di <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></th>
                                            </tr>
                                        </tbody>
                                    </table>

                            <?php } else { ?>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th colspan="2" class="text-center">
                                                <h3> S T A T U S </h3>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2" class="text-center bg-warning text-white">
                                                <h1 style="font-weight:bold"> - </h1>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th colspan="2" class="text-center"> Kami masih memproses data Anda, silahkan buka kembali dilain waktu untuk melihat hasil Lulus / Tidak Lulus nya</th>
                                        </tr>
                                    </tbody>
                                </table>
                            <?php } ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>