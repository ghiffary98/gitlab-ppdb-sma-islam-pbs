<?php
include "../../../../../init.php";
include "../../../../../models/admin/login/model_login.php";
include "../../../../../models/admin/dashboard/model_dashboard.php";
include "../../../../../models/global/data_siswa/model_data_siswa.php";
include "../../../../../models/global/data_siswa_nilai_rapor/model_data_siswa_nilai_rapor.php";
include "../../../../../models/global/pengaturan_pekerjaan_orang_tua/model_pengaturan_pekerjaan_orang_tua.php";
include "../../../../../models/global/pengaturan_penghasilan_orang_tua/model_pengaturan_penghasilan_orang_tua.php";
include "../../../../../models/global/pengaturan_kendaraan_yang_dipakai_kesekolah/model_pengaturan_kendaraan_yang_dipakai_kesekolah.php";
include "../../../../../models/global/pengaturan_asal_sekolah/model_pengaturan_asal_sekolah.php";
include "../../../../../controllers/admin/dashboard/controller_dashboard.php";
include "../../../../../controllers/admin/data_siswa/controller_data_siswa.php";

function replaceSymbolsWithUnderscore($inputString)
{
    $symbols = array("~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "=", "+", "[", "]", "{", "}", "|", "\\", ";", ":", "'", "\"", "<", ">", ",", ".", "/", "?", " ");
    $outputString = str_replace($symbols, "_", $inputString);
    return $outputString;
}

$Nama_File = "Data_Siswa_Format_8355" . replaceSymbolsWithUnderscore("") . ".xls";

?>
<!DOCTYPE html>
<html>

<head>
    <title>Export Data Format Excel</title>
</head>

<body>
    <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=$Nama_File");
    ?>
    <table>
        <tr>
            <td colspan="16" align="center">
                <h2>DAFTAR NAMA SISWA SMA NEGERI/SWASTA</h2>
            </td>
        </tr>
        <tr>
            <td colspan="16" align="center">
                <h2>DI LINGKUNGAN DINAS PENDIDIKAN PROVINSI DKI JAKARTA</h2>
            </td>
        </tr>
        <tr>
            <td colspan="16" align="center">
                <h2>PROVINSI DKI JAKARTA TAHUN PELAJARAN <?php echo $data_ppdb_saat_ini['Tahun_Ajaran'] ?></h2>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                NAMA SEKOLAH
            </td>
            <td style='mso-number-format:\@'>: <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></td>
        </tr>
        <tr>
            <td colspan="3">
                NPSN
            </td>
            <td style='mso-number-format:\@'>: 20103230</td>
        </tr>
        <tr>
            <td colspan="3">
                ALAMAT
            </td>
            <td style='mso-number-format:\@'>: Jl. Raya Bogor Km. 24 RT. 003 RW. 004</td>
        </tr>
        <tr>
            <td colspan="3">
                KECAMATAN
            </td>
            <td style='mso-number-format:\@'>: Pasar Rebo</td>
        </tr>
        <tr>
            <td colspan="3">
                KOTA ADMINISTRASI
            </td>
            <td style='mso-number-format:\@'>: Jakarta Timur</td>
        </tr>
    </table>
    <table border="1">
        <thead class="bg-light">
            <tr>
                <th>NO</th>
                <th>INDUK</th>
                <th>NISN</th>
                <th>NAMA</th>
                <th>L/P</th>
                <th>TMP_LHR</th>
                <th>TANGGAL LAHIR</th>
                <th>AGAMA</th>
                <th>NAMA ORANG TUA</th>
                <th>ALAMAT</th>
                <th>KEL</th>
                <th>KEC</th>
                <th>KOTA</th>
                <th>KD POS</th>
                <th>NO. IJAZAH</th>
                <th>TAHUN</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ((isset($list_datatable_master))) {
                foreach ($list_datatable_master as $data) {
                    $nomor++; ?>
                    <tr>
                        <td style='mso-number-format:\@'><?php echo $nomor ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NISN'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Lengkap'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenis_Kelamin'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tempat_Lahir'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tanggal_Lahir'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Agama'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Ayah'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jalan'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kelurahan']
                            ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kecamatan'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kota'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kode_Pos'] ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Seri_Ijazah'] ?></td>
                        <td style='mso-number-format:\@'><?php //echo $data['Tahun_Masuk']
                                ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>