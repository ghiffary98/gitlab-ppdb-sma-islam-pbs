<?php
include "../../../../../init.php";
include "../../../../../models/admin/login/model_login.php";
include "../../../../../models/admin/dashboard/model_dashboard.php";
include "../../../../../models/global/data_siswa/model_data_siswa.php";
include "../../../../../models/global/data_siswa_nilai_rapor/model_data_siswa_nilai_rapor.php";
include "../../../../../models/global/pengaturan_pekerjaan_orang_tua/model_pengaturan_pekerjaan_orang_tua.php";
include "../../../../../models/global/pengaturan_penghasilan_orang_tua/model_pengaturan_penghasilan_orang_tua.php";
include "../../../../../models/global/pengaturan_kendaraan_yang_dipakai_kesekolah/model_pengaturan_kendaraan_yang_dipakai_kesekolah.php";
include "../../../../../models/global/pengaturan_asal_sekolah/model_pengaturan_asal_sekolah.php";
include "../../../../../controllers/admin/dashboard/controller_dashboard.php";
include "../../../../../controllers/admin/data_siswa/controller_data_siswa.php";

function replaceSymbolsWithUnderscore($inputString)
{
    $symbols = array("~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "=", "+", "[", "]", "{", "}", "|", "\\", ";", ":", "'", "\"", "<", ">", ",", ".", "/", "?", " ");
    $outputString = str_replace($symbols, "_", $inputString);
    return $outputString;
}

$Nama_File = "Data_Siswa_Format_All" . replaceSymbolsWithUnderscore("") . ".xls";

?>
<!DOCTYPE html>
<html>

<head>
    <title>Export Data Format Excel</title>
</head>

<body>
    <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=$Nama_File");
    ?>
    <table>
        <tr>
            <td colspan="16">
                <h2>DAFTAR PESERTA DIDIK</h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                <h2><?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                Kecamatan Kec. Pasar Rebo, Kabupaten Kota Jakarta Timur, Provinsi Prov. D.K.I. Jakarta
            </td>
        </tr>
        <tr>
            <td colspan="16">
                Tanggal Unduh : <?php echo $Waktu_Sekarang?>
            </td>
        </tr>
    </table>
    <table border="1">
        <thead class="bg-light">
            <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Nama</th>
                <th rowspan="2">Kelas</th>
                <th rowspan="2">NIPD</th>
                <th rowspan="2">JK</th>
                <th rowspan="2">NISN IJAZAH</th>
                <th rowspan="2">NISN</th>
                <th rowspan="2">Tempat Lahir</th>
                <th rowspan="2">Tanggal Lahir</th>
                <th rowspan="2">Alat Transportasi</th>
                <th rowspan="2">Telepon</th>
                <th rowspan="2">HP</th>
                <th rowspan="2">E-Mail</th>
                <th rowspan="2">SKHUN</th>
                <th rowspan="2">Penerima KPS</th>
                <th rowspan="2">No. KPS</th>
                <th colspan="6">Data Ayah</th>
                <th colspan="6">Data Ibu</th>
                <th colspan="6">Data Wali</th>
                <th rowspan="2">Rombel Saat Ini</th>
                <th rowspan="2">No Peserta Ujian Nasional</th>
                <th rowspan="2">No Seri Ijazah</th>
                <th rowspan="2">Penerima KIP</th>
                <th rowspan="2">Nomor KIP</th>
                <th rowspan="2">Nama di KIP</th>
                <th rowspan="2">Nomor KKS</th>
                <th rowspan="2">No Registrasi Akta Lahir</th>
                <th rowspan="2">Bank</th>
                <th rowspan="2">Nomor Rekening Bank</th>
                <th rowspan="2">Rekening Atas Nama</th>
                <th rowspan="2">Layak PIP (usulan dari sekolah)</th>
                <th rowspan="2">Alasan Layak PIP</th>
                <th rowspan="2">Kebutuhan Khusus</th>
                <th rowspan="2">Sekolah Asal</th>
                <th rowspan="2">Anak ke-berapa</th>
                <th rowspan="2">Lintang</th>
                <th rowspan="2">Bujur</th>
                <th rowspan="2">No KK</th>
                <th rowspan="2">Berat Badan</th>
                <th rowspan="2">Tinggi Badan</th>
                <th rowspan="2">Lingkar Kepala</th>
                <th rowspan="2">Jml. Saudara Kandung</th>
                <th rowspan="2">Jarak Rumah ke Sekolah (KM)</th>
            </tr>
            <tr>
                <th>Nama</th>
                <th>Tanggal Lahir</th>
                <th>Jenjang Pendidikan</th>
                <th>Pekerjaan</th>
                <th>Penghasilan</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Tanggal Lahir</th>
                <th>Jenjang Pendidikan</th>
                <th>Pekerjaan</th>
                <th>Penghasilan</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Tanggal Lahir</th>
                <th>Jenjang Pendidikan</th>
                <th>Pekerjaan</th>
                <th>Penghasilan</th>
                <th>NIK</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ((isset($list_datatable_master))) {
                foreach ($list_datatable_master as $data) {
                    $nomor++; ?>
                    <tr>
                        <td style='mso-number-format:\@'><?php echo $nomor ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Lengkap'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kelas'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIPD'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenis_Kelamin'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NISN'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NISN'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tempat_Lahir'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tanggal_Lahir'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kendaraan_Yang_Dipakai_Kesekolah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Telepon'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Handphone'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Email'];?></td>
                        <td style='mso-number-format:\@'><?php //echo $data['SKHUN'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penerima_KPS'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_KPS'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Ayah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tanggal_Lahir_Ayah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenjang_Pendidikan_Ayah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Pekerjaan_Ayah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penghasilan_Ayah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK_Ayah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Ibu'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tanggal_Lahir_Ibu'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenjang_Pendidikan_Ibu'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Pekerjaan_Ibu'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penghasilan_Ibu'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK_Wali'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Wali'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tanggal_Lahir_Wali'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenjang_Pendidikan_Wali'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Pekerjaan_Wali'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penghasilan_Wali'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK_Wali'];?></td>
                        <td style='mso-number-format:\@'><?php //echo $data['Rombel_Saat_Ini'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Peserta_Ujian_Nasional'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Seri_Ijazah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penerima_KIP'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_KIP'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Lengkap'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_KKS'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Registrasi_Akta_Lahir'];?></td>
                        <td style='mso-number-format:\@'><?php //echo $data['Bank'];?></td>
                        <td style='mso-number-format:\@'><?php //echo $data['Nomor_Rekening_Bank'];?></td>
                        <td style='mso-number-format:\@'><?php //echo $data['Rekening_Atas_Nama'];?></td>
                        <td style='mso-number-format:\@'><?php //echo $data['Layak_PIP'];?></td>
                        <td style='mso-number-format:\@'><?php //echo $data['Alasan_Layak_PIP'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kebutuhan_Khusus'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Asal_Sekolah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Anak_Ke'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Titik_Lintang_Alamat'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Titik_Bujur_Alamat'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_KK'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Berat_Badan'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tinggi_Badan'];?></td>
                        <td style='mso-number-format:\@'><?php //echo $data['Lingkar_Kepala'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jumlah_Saudara'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jarak_Rumah_Ke_Sekolah'];?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>