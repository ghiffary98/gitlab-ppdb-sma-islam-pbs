<?php
include "../../../../../init.php";
include "../../../../../models/admin/login/model_login.php";
include "../../../../../models/admin/dashboard/model_dashboard.php";
include "../../../../../models/global/data_siswa/model_data_siswa.php";
include "../../../../../models/global/data_siswa_nilai_rapor/model_data_siswa_nilai_rapor.php";
include "../../../../../models/global/pengaturan_pekerjaan_orang_tua/model_pengaturan_pekerjaan_orang_tua.php";
include "../../../../../models/global/pengaturan_penghasilan_orang_tua/model_pengaturan_penghasilan_orang_tua.php";
include "../../../../../models/global/pengaturan_kendaraan_yang_dipakai_kesekolah/model_pengaturan_kendaraan_yang_dipakai_kesekolah.php";
include "../../../../../models/global/pengaturan_asal_sekolah/model_pengaturan_asal_sekolah.php";
include "../../../../../controllers/admin/dashboard/controller_dashboard.php";
include "../../../../../controllers/admin/data_siswa/controller_data_siswa.php";

function replaceSymbolsWithUnderscore($inputString)
{
    $symbols = array("~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "=", "+", "[", "]", "{", "}", "|", "\\", ";", ":", "'", "\"", "<", ">", ",", ".", "/", "?", " ");
    $outputString = str_replace($symbols, "_", $inputString);
    return $outputString;
}

$Nama_File = "Data_Siswa_Format_Rapor" . replaceSymbolsWithUnderscore("") . ".xls";

?>
<!DOCTYPE html>
<html>

<head>
    <title>Export Data Format Excel</title>
</head>

<body>
    <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=$Nama_File");
    ?>
    <table>
        <tr>
            <td colspan="16">
                <h2>DATA SISWA <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                <h2>FORMAT RAPORT</h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                <h2>Tanggal : <?php echo $Tanggal_Sekarang ?></h2>
            </td>
        </tr>
    </table>
    <table border="1">
        <thead class="bg-light">
            <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Nama Lengkap</th>
                <th rowspan="2">Nomor Induk</th>
                <th rowspan="2">NISN</th>
                <th rowspan="2">Tempat, Tanggal Lahir</th>
                <th rowspan="2">Jenis Kelamin</th>
                <th rowspan="2">Agama</th>
                <th rowspan="2">Status dalam Keluarga</th>
                <th rowspan="2">Anak ke</th>
                <th rowspan="2">Alamat Siswa</th>
                <th rowspan="2">Nomor Hanpdhone</th>
                <th rowspan="2">Sekolah Asal</th>
                <th colspan="2">Diterima di sekolah ini </th>

                <th colspan="3">Data Orang Tua</th>

                <th colspan="2">Pekerjaan Orang Tua</th>
                <th colspan="2">No. Hanpdhone Orang Tua</th>

                <th colspan="4">Wali Siswa</th>
                <th colspan="10">Semester 1</th>
                <th colspan="10">Semester 2</th>
                <th colspan="10">Semester 3</th>
                <th colspan="10">Semester 4</th>

                <th rowspan="2">Nilai SKL / Ijazah</th>
            </tr>
            <tr>
                <th>Di kelas</th>
                <th>Pada tanggal</th>

                <th>Ayah</th>
                <th>Ibu</th>
                <th>Alamat</th>

                <th>Ayah</th>
                <th>Ibu</th>

                <th>Ayah</th>
                <th>Ibu</th>

                <th>Nama Wali</th>
                <th>Nomor Telp / HP</th>
                <th>Alamat</th>
                <th>Pekerjaan</th>

                <th>Pendidikan Agama Islam dan Budi Pekerti</th>
                <th>Pendidikan Pancasila dan Kewarganegaraan</th>
                <th>Bahasa Indonesia</th>
                <th>Bahasa Inggris</th>
                <th>Matematika</th>
                <th>IPA (Ilmu Pengetahuan Alam)</th>
                <th>IPS (Ilmu Pengetahuan Sosial)</th>
                <th>Seni dan Budaya</th>
                <th>PJOK</th>
                <th>Prakarya</th>

                <th>Pendidikan Agama Islam dan Budi Pekerti</th>
                <th>Pendidikan Pancasila dan Kewarganegaraan</th>
                <th>Bahasa Indonesia</th>
                <th>Bahasa Inggris</th>
                <th>Matematika</th>
                <th>IPA (Ilmu Pengetahuan Alam)</th>
                <th>IPS (Ilmu Pengetahuan Sosial)</th>
                <th>Seni dan Budaya</th>
                <th>PJOK</th>
                <th>Prakarya</th>

                <th>Pendidikan Agama Islam dan Budi Pekerti</th>
                <th>Pendidikan Pancasila dan Kewarganegaraan</th>
                <th>Bahasa Indonesia</th>
                <th>Bahasa Inggris</th>
                <th>Matematika</th>
                <th>IPA (Ilmu Pengetahuan Alam)</th>
                <th>IPS (Ilmu Pengetahuan Sosial)</th>
                <th>Seni dan Budaya</th>
                <th>PJOK</th>
                <th>Prakarya</th>

                <th>Pendidikan Agama Islam dan Budi Pekerti</th>
                <th>Pendidikan Pancasila dan Kewarganegaraan</th>
                <th>Bahasa Indonesia</th>
                <th>Bahasa Inggris</th>
                <th>Matematika</th>
                <th>IPA (Ilmu Pengetahuan Alam)</th>
                <th>IPS (Ilmu Pengetahuan Sosial)</th>
                <th>Seni dan Budaya</th>
                <th>PJOK</th>
                <th>Prakarya</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ((isset($list_datatable_master))) {
                foreach ($list_datatable_master as $data) {
                    $nomor++; ?>
                    <?php 
                    $result = $a_data_siswa_nilai_rapor->baca_data_id("Id_Siswa", $data['Id_Siswa']);

                    if ($result['Status'] == "Sukses") {
                        $data_nilai_rapor = $result['Hasil'];
                    } else {
                        $data_nilai_rapor = null;
                    }
                    ?>
                    <tr>
                        <td style='mso-number-format:\@'><?php echo $nomor ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Lengkap']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIS']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NISN']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tempat_Lahir'] . ", " . $data['Tanggal_Lahir']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenis_Kelamin']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Agama']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Status_Dalam_Keluarga']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Anak_Ke']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jalan']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Handphone']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Asal_Sekolah']; ?></td>
                        <td style='mso-number-format:\@'><?php  ?></td>
                        <td style='mso-number-format:\@'><?php  ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jalan']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Pekerjaan_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Pekerjaan_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['No_HP_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['No_HP_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['No_HP_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jalan']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Pekerjaan_Wali']; ?></td>


                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_Bahasa_Indonesia'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_Bahasa_Inggris'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_Matematika'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_IPA'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_IPS'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_Seni_Dan_Budaya'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_PJOK'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_1_Prakarya'];} ?></td>

                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_Bahasa_Indonesia'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_Bahasa_Inggris'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_Matematika'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_IPA'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_IPS'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_Seni_Dan_Budaya'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_PJOK'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_2_Prakarya'];} ?></td>

                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_Bahasa_Indonesia'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_Bahasa_Inggris'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_Matematika'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_IPA'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_IPS'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_Seni_Dan_Budaya'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_PJOK'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_3_Prakarya'];} ?></td>

                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_Bahasa_Indonesia'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_Bahasa_Inggris'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_Matematika'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_IPA'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_IPS'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_Seni_Dan_Budaya'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_PJOK'];} ?></td>
                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_Semester_4_Prakarya'];} ?></td>

                        <td style='mso-number-format:\@'><?php if(isset($data_nilai_rapor)){echo $data_nilai_rapor['Nilai_SKL_Ijazah'];} ?></td>



                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>