<?php
include "models/global/data_siswa/model_data_siswa.php";
include "models/global/data_siswa_nilai_rapor/model_data_siswa_nilai_rapor.php";

include "models/global/pengaturan_pekerjaan_orang_tua/model_pengaturan_pekerjaan_orang_tua.php";
include "models/global/pengaturan_penghasilan_orang_tua/model_pengaturan_penghasilan_orang_tua.php";
include "models/global/pengaturan_kendaraan_yang_dipakai_kesekolah/model_pengaturan_kendaraan_yang_dipakai_kesekolah.php";
include "models/global/pengaturan_asal_sekolah/model_pengaturan_asal_sekolah.php";
include "controllers/admin/data_siswa/controller_data_siswa.php";
?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Export Data Siswa Format Raport</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="?menu=home">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="<?php echo "?menu=" . $_GET['menu'] ?>">Export Data Siswa Format Raport</a>
                </li>

                <?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <?php if (isset($_GET['tambah'])) { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
                        <?php } else { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
                        <?php } ?>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php
        //FORM TAMBAH/EDIT DATA
        if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <?php if (isset($_GET['tambah'])) { ?>
                                        <div class="card-title">Tambah Data</div>
                                    <?php } else { ?>
                                        <div class="card-title">Edit Data</div>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
                                    <?php if (isset($_GET["edit"])) { ?>
                                        <script type="text/javascript">
                                            function konfirmasi_hapus_data_permanen() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_hapus_data_ke_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_arsip_data() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_arsip() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }
                                        </script>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Aktif") { ?>
                                                    <a href="#" onclick="konfirmasi_arsip_data()"><i class="fa fa-archive fa-md"></i> ARSIPKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terarsip") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i class="fa fa-archive fa-md"></i> AKTIFKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i class="fa fa-archive fa-md"></i> RESTORE </a>
                                                <?php } ?>

                                            </li>
                                            <li class="list-inline-item"> | </li>
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_permanen()"><i class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Identitas Siswa</b></h2>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> NIS (Nomor Induk Siswa)</td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="NIS_Saat_Ini" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['NIS_Saat_Ini'] <> "")) {
                                                                                                                                                                                        echo $edit['NIS_Saat_Ini'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Nama Siswa </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nama_Lengkap" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nama_Lengkap'] <> "")) {
                                                                                                                                                                                        echo $edit['Nama_Lengkap'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Kelas </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Kelas" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Kelas'] <> "")) {
                                                                                                                                                                                echo $edit['Kelas'];
                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Jenis Kelamin </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Jenis_Kelamin" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Jenis_Kelamin'] <> "")) {
                                                                                                                                                                                        echo $edit['Jenis_Kelamin'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tempat Lahir </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Tempat_Lahir" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Tempat_Lahir'] <> "")) {
                                                                                                                                                                                        echo $edit['Tempat_Lahir'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Tanggal Lahir </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Tanggal_Lahir" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Tanggal_Lahir'] <> "")) {
                                                                                                                                                                                        echo $edit['Tanggal_Lahir'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Agama </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Agama" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Agama'] <> "")) {
                                                                                                                                                                                echo $edit['Agama'];
                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> NIK </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="NIK" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['NIK'] <> "")) {
                                                                                                                                                                                echo $edit['NIK'];
                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> PPDB Tahun </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select class="form-control" name="Id_PPDB">
                                                                <option value=""></option>
                                                                <?php
                                                                $list_data_ppdb_ini = $list_data_ppdb;
                                                                if (isset($_GET['edit'])) {
                                                                    if (($edit['Id_PPDB'] <> "") and ($edit['Id_PPDB'] <> "0")) {
                                                                        $result = Baca_Data_PPDB_Saat_Ini_Berdasarkan_ID($edit['Id_PPDB']);
                                                                        var_dump($result);
                                                                        if ($result['Status'] == 'Sukses') {
                                                                            $edit_data_sebelum_Id_PPDB['Id_PPDB'] = $edit['Id_PPDB'];
                                                                            $data_option_saat_ini = $result['Hasil'];
                                                                            $edit_data_sebelum_Id_PPDB['Tahun_Ajaran'] = $data_option_saat_ini['Tahun_Ajaran'];
                                                                            array_unshift($list_data_ppdb_ini, $edit_data_sebelum_Id_PPDB);
                                                                            $list_data_ppdb_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_ppdb_ini, "Id_PPDB");
                                                                        }
                                                                    }
                                                                }
                                                                if ((isset($list_data_ppdb_ini))) {
                                                                    foreach ($list_data_ppdb_ini as $data) {
                                                                ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Id_PPDB'] == $data['Id_PPDB']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Id_PPDB'] == $data['Id_PPDB']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Id_PPDB'] ?>"><?php echo $data['Tahun_Ajaran'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Kontak</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No. HP </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nomor_Handphone" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nomor_Handphone'] <> "")) {
                                                                                                                                                                                            echo $edit['Nomor_Handphone'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> No. Telp </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nomor_Telepon" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nomor_Telepon'] <> "")) {
                                                                                                                                                                                        echo $edit['Nomor_Telepon'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Email </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Email" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Email'] <> "")) {
                                                                                                                                                                                echo $edit['Email'];
                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Sekolah Asal</b></h2>
                                                    </th>
                                                </tr>


                                                <tr>
                                                    <td style="width:30%"> NIS (Nomor Induk Siswa) </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="NIS" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['NIS'] <> "")) {
                                                                                                                                                                                echo $edit['NIS'];
                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> NIPD </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="NIPD" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['NIPD'] <> "")) {
                                                                                                                                                                                echo $edit['NIPD'];
                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> NISN (Nomor Induk Siswa Nasional) </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="NISN" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['NISN'] <> "")) {
                                                                                                                                                                                echo $edit['NISN'];
                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Asal Sekolah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select class="form-control" name="Asal_Sekolah">
                                                                <option value=""></option>
                                                                <?php
                                                                $list_data_pengaturan_asal_sekolah_ini = $list_data_pengaturan_asal_sekolah;
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Asal_Sekolah'] <> "") {
                                                                        $edit_data_sebelum_Asal_Sekolah['Id_Pengaturan_Asal_Sekolah'] = '0';
                                                                        $edit_data_sebelum_Asal_Sekolah['Nama_Sekolah'] = $edit['Asal_Sekolah'];
                                                                        array_unshift($list_data_pengaturan_asal_sekolah_ini, $edit_data_sebelum_Asal_Sekolah);
                                                                        $list_data_pengaturan_asal_sekolah_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_asal_sekolah_ini, "Nama_Sekolah");
                                                                    }
                                                                }
                                                                if ((isset($list_data_pengaturan_asal_sekolah_ini))) {
                                                                    foreach ($list_data_pengaturan_asal_sekolah_ini as $data) {
                                                                ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Asal_Sekolah'] == $data['Nama_Sekolah']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Asal_Sekolah'] == $data['Nama_Sekolah']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Nama_Sekolah'] ?>"><?php echo $data['Nama_Sekolah'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Alamat Sekolah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Alamat_Sekolah" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Alamat_Sekolah'] <> "")) {
                                                                                                                                                                                            echo $edit['Alamat_Sekolah'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No Peserta Ujian Nasional </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nomor_Peserta_Ujian_Nasional" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nomor_Peserta_Ujian_Nasional'] <> "")) {
                                                                                                                                                                                                        echo $edit['Nomor_Peserta_Ujian_Nasional'];
                                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No Seri Ijazah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nomor_Seri_Ijazah" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nomor_Seri_Ijazah'] <> "")) {
                                                                                                                                                                                            echo $edit['Nomor_Seri_Ijazah'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Keluarga</b></h2>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> No KK </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nomor_KK" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nomor_KK'] <> "")) {
                                                                                                                                                                                    echo $edit['Nomor_KK'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Anak Ke-berapa </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Anak_Ke" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Anak_Ke'] <> "")) {
                                                                                                                                                                                    echo $edit['Anak_Ke'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Jumlah Saudara Kandung </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Jumlah_Saudara" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Jumlah_Saudara'] <> "")) {
                                                                                                                                                                                            echo $edit['Jumlah_Saudara'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penerima KPS </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Penerima_KPS" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Penerima_KPS'] <> "")) {
                                                                                                                                                                                        echo $edit['Penerima_KPS'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No.KPS (Jika Ya) </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nomor_KPS" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nomor_KPS'] <> "")) {
                                                                                                                                                                                    echo $edit['Nomor_KPS'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penerima KIP </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Penerima_KIP" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Penerima_KIP'] <> "")) {
                                                                                                                                                                                        echo $edit['Penerima_KIP'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nomor KIP </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nomor_KIP" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nomor_KIP'] <> "")) {
                                                                                                                                                                                    echo $edit['Nomor_KIP'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nomor KKS </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nomor_KKS" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nomor_KKS'] <> "")) {
                                                                                                                                                                                    echo $edit['Nomor_KKS'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> No Registrasi Akta Lahir </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nomor_Registrasi_Akta_Lahir" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nomor_Registrasi_Akta_Lahir'] <> "")) {
                                                                                                                                                                                                        echo $edit['Nomor_Registrasi_Akta_Lahir'];
                                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Status Dalam Keluarga </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Status_Dalam_Keluarga" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Status_Dalam_Keluarga'] <> "")) {
                                                                                                                                                                                                echo $edit['Status_Dalam_Keluarga'];
                                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Status Perwalian </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Status_Perwalian" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Status_Perwalian'] <> "")) {
                                                                                                                                                                                            echo $edit['Status_Perwalian'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Pribadi</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Golongan Darah </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Golongan_Darah" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Golongan_Darah'] <> "")) {
                                                                                                                                                                                            echo $edit['Golongan_Darah'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tinggi Badan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Tinggi_Badan" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Tinggi_Badan'] <> "")) {
                                                                                                                                                                                        echo $edit['Tinggi_Badan'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Berat Badan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Berat_Badan" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Berat_Badan'] <> "")) {
                                                                                                                                                                                        echo $edit['Berat_Badan'];
                                                                                                                                                                                    } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Lingkar Kepala </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Lingkar_Kepala" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Lingkar_Kepala'] <> "")) {
                                                                                                                                                                                            echo $edit['Lingkar_Kepala'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Kebutuhan Khusus </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Kebutuhan_Khusus" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Kebutuhan_Khusus'] <> "")) {
                                                                                                                                                                                            echo $edit['Kebutuhan_Khusus'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Orang Tua</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h4><b>Data Ayah</b></h4>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nama </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nama_Ayah" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nama_Ayah'] <> "")) {
                                                                                                                                                                                    echo $edit['Nama_Ayah'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tahun Lahir </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Tanggal_Lahir_Ayah" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Tanggal_Lahir_Ayah'] <> "")) {
                                                                                                                                                                                                echo $edit['Tanggal_Lahir_Ayah'];
                                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Jenjang Pendidikan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Jenjang_Pendidikan_Ayah" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Jenjang_Pendidikan_Ayah'] <> "")) {
                                                                                                                                                                                                    echo $edit['Jenjang_Pendidikan_Ayah'];
                                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Pekerjaan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select class="form-control" name="Pekerjaan_Ayah">
                                                                <option value=""></option>
                                                                <?php
                                                                $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Pekerjaan_Ayah'] <> "") {
                                                                        $edit_data_sebelum_Pekerjaan_Ayah['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                        $edit_data_sebelum_Pekerjaan_Ayah['Pekerjaan_Orang_Tua'] = $edit['Pekerjaan_Ayah'];
                                                                        array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Ayah);
                                                                        $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini, "Pekerjaan_Orang_Tua");
                                                                    }
                                                                }
                                                                if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                    foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Pekerjaan_Ayah'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Pekerjaan_Ayah'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>"><?php echo $data['Pekerjaan_Orang_Tua'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penghasilan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select class="form-control" name="Penghasilan_Ayah">
                                                                <option value=""></option>
                                                                <?php
                                                                $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Penghasilan_Ayah'] <> "") {
                                                                        $edit_data_sebelum_Penghasilan_Ayah['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                        $edit_data_sebelum_Penghasilan_Ayah['Penghasilan_Orang_Tua'] = $edit['Penghasilan_Ayah'];
                                                                        array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Ayah);
                                                                        $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini, "Penghasilan_Orang_Tua");
                                                                    }
                                                                }
                                                                if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                    foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Penghasilan_Ayah'] == $data['Penghasilan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Penghasilan_Ayah'] == $data['Penghasilan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Penghasilan_Orang_Tua'] ?>"><?php echo $data['Penghasilan_Orang_Tua'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> NIK </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="NIK_Ayah" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['NIK_Ayah'] <> "")) {
                                                                                                                                                                                    echo $edit['NIK_Ayah'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h4><b>Data Ibu</b></h4>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Nama </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nama_Ibu" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nama_Ibu'] <> "")) {
                                                                                                                                                                                    echo $edit['Nama_Ibu'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tahun Lahir </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Tanggal_Lahir_Ibu" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Tanggal_Lahir_Ibu'] <> "")) {
                                                                                                                                                                                            echo $edit['Tanggal_Lahir_Ibu'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Jenjang Pendidikan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Jenjang_Pendidikan_Ibu" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Jenjang_Pendidikan_Ibu'] <> "")) {
                                                                                                                                                                                                    echo $edit['Jenjang_Pendidikan_Ibu'];
                                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Pekerjaan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select class="form-control" name="Pekerjaan_Ibu">
                                                                <option value=""></option>
                                                                <?php
                                                                $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Pekerjaan_Ibu'] <> "") {
                                                                        $edit_data_sebelum_Pekerjaan_Ibu['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                        $edit_data_sebelum_Pekerjaan_Ibu['Pekerjaan_Orang_Tua'] = $edit['Pekerjaan_Ibu'];
                                                                        array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Ibu);
                                                                        $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini, "Pekerjaan_Orang_Tua");
                                                                    }
                                                                }
                                                                if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                    foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Pekerjaan_Ibu'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Pekerjaan_Ibu'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>"><?php echo $data['Pekerjaan_Orang_Tua'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penghasilan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select class="form-control" name="Penghasilan_Ibu">
                                                                <option value=""></option>
                                                                <?php
                                                                $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Penghasilan_Ibu'] <> "") {
                                                                        $edit_data_sebelum_Penghasilan_Ibu['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                        $edit_data_sebelum_Penghasilan_Ibu['Penghasilan_Orang_Tua'] = $edit['Penghasilan_Ibu'];
                                                                        array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Ibu);
                                                                        $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini, "Penghasilan_Orang_Tua");
                                                                    }
                                                                }
                                                                if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                    foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Penghasilan_Ibu'] == $data['Penghasilan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Penghasilan_Ibu'] == $data['Penghasilan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Penghasilan_Orang_Tua'] ?>"><?php echo $data['Penghasilan_Orang_Tua'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> NIK </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="NIK_Ibu" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['NIK_Ibu'] <> "")) {
                                                                                                                                                                                    echo $edit['NIK_Ibu'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Data Wali</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Nama </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Nama_Wali" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Nama_Wali'] <> "")) {
                                                                                                                                                                                    echo $edit['Nama_Wali'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Tahun Lahir </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Tanggal_Lahir_Wali" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Tanggal_Lahir_Wali'] <> "")) {
                                                                                                                                                                                                echo $edit['Tanggal_Lahir_Wali'];
                                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Jenjang Pendidikan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Jenjang_Pendidikan_Wali" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Jenjang_Pendidikan_Wali'] <> "")) {
                                                                                                                                                                                                    echo $edit['Jenjang_Pendidikan_Wali'];
                                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Pekerjaan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Pekerjaan_Wali" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Pekerjaan_Wali'] <> "")) {
                                                                                                                                                                                            echo $edit['Pekerjaan_Wali'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Penghasilan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Penghasilan_Wali" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Penghasilan_Wali'] <> "")) {
                                                                                                                                                                                            echo $edit['Penghasilan_Wali'];
                                                                                                                                                                                        } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> NIK </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="NIK_Wali" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['NIK_Wali'] <> "")) {
                                                                                                                                                                                    echo $edit['NIK_Wali'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="bg-light">
                                                        <h2><b>Informasi Alamat</b></h2>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Alamat </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Jalan" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Jalan'] <> "")) {
                                                                                                                                                                                echo $edit['Jalan'];
                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Kecamatan </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Kecamatan" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Kecamatan'] <> "")) {
                                                                                                                                                                                    echo $edit['Kecamatan'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Kota </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Kota" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Kota'] <> "")) {
                                                                                                                                                                                echo $edit['Kota'];
                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Provinsi </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Provinsi" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Provinsi'] <> "")) {
                                                                                                                                                                                    echo $edit['Provinsi'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Kode Pos </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Kode_Pos" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Kode_Pos'] <> "")) {
                                                                                                                                                                                    echo $edit['Kode_Pos'];
                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Lintang </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Titik_Lintang_Alamat" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Titik_Lintang_Alamat'] <> "")) {
                                                                                                                                                                                                echo $edit['Titik_Lintang_Alamat'];
                                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Lintang </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Titik_Lintang_Alamat" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Titik_Lintang_Alamat'] <> "")) {
                                                                                                                                                                                                echo $edit['Titik_Lintang_Alamat'];
                                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Bujur </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Titik_Bujur_Alamat" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Titik_Bujur_Alamat'] <> "")) {
                                                                                                                                                                                                echo $edit['Titik_Bujur_Alamat'];
                                                                                                                                                                                            } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Jarak Rumah ke Sekolah (Km) </td>
                                                    <td style="width:70%">
                                                        <div class="form-group"> <input name="Jarak_Rumah_Ke_Sekolah" type="text" class="form-control" style="background-color:white" value="<?php if ((isset($edit)) and ($edit['Jarak_Rumah_Ke_Sekolah'] <> "")) {
                                                                                                                                                                                                    echo $edit['Jarak_Rumah_Ke_Sekolah'];
                                                                                                                                                                                                } ?>"> </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:30%"> Alat Transportasi </td>
                                                    <td style="width:70%">
                                                        <div class="form-group">
                                                            <select class="form-control" name="Kendaraan_Yang_Dipakai_Kesekolah">
                                                                <option value=""></option>
                                                                <?php
                                                                $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini = $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah;
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Kendaraan_Yang_Dipakai_Kesekolah'] <> "") {
                                                                        $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah['Id_Pengaturan_Nama_Kendaraan'] = '0';
                                                                        $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah['Nama_Kendaraan'] = $edit['Kendaraan_Yang_Dipakai_Kesekolah'];
                                                                        array_unshift($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini, $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah);
                                                                        $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini, "Nama_Kendaraan");
                                                                    }
                                                                }
                                                                if ((isset($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini))) {
                                                                    foreach ($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini as $data) {
                                                                ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Kendaraan_Yang_Dipakai_Kesekolah'] == $data['Nama_Kendaraan']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Kendaraan_Yang_Dipakai_Kesekolah'] == $data['Nama_Kendaraan']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Nama_Kendaraan'] ?>"><?php echo $data['Nama_Kendaraan'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div style="text-align: center;">
                                    <?php if (isset($_GET["tambah"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
                                    <?php } elseif (isset($_GET["edit"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE">
                                    <?php } ?>
                                    <input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'" class="btn btn-danger" value="BATAL">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

        <?php
        //FORM LIST DATA
        if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">List Export Data Siswa Format Raport
                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
                                    echo "(" . $_GET['filter_status'] . ")";
                                } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">

                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
                                    <a href="views/admin/data_siswa/export_data/export_excel/view_export_excel_data_siswa_format_rapor.php" target="_blank" class="btn btn-success">
                                        <i class="fas fa-file-excel"></i> &nbsp;
                                        Export to Excel</a>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table id="basic-datatables" class="table table-borderless" style="width:100%">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>No</th>
                                            <th>NIS</th>
                                            <th>Nama Lengkap</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Tempat Lahir</th>
                                            <th>Tanggal Lahir</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ((isset($list_datatable_master))) {
                                            foreach ($list_datatable_master as $data) {
                                                $nomor++; ?>
                                                <tr>
                                                    <td><?php echo $nomor ?></td>
                                                    <td>
                                                        <?php if ($data['NIS_Saat_Ini'] <> "") {
                                                            echo $data['NIS_Saat_Ini'];
                                                        } else {
                                                            echo "-";
                                                        } ?>
                                                    </td>
                                                    <td><?php echo $data['Nama_Lengkap'] ?></td>
                                                    <td><?php echo $data['Jenis_Kelamin'] ?></td>
                                                    <td><?php echo $data['Tempat_Lahir'] ?></td>
                                                    <td><?php echo $data['Tanggal_Lahir'] ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    function datatable_konfirmasi_update_status_verifikasi(id_primary, status_verifikasi) {
                                        var txt;
                                        if (status_verifikasi == "Sudah Diverifikasi") {
                                            var r = confirm("Apakah Anda Yakin Ingin Mengubah Status Verifikasi Menjadi 'Sudah Diverifikasi' Pada Data Ini ?");
                                        } else {
                                            var r = confirm("Apakah Anda Yakin Ingin Mengubah Status Verifikasi Menjadi 'Belum Diverifikasi' Pada Data Ini ?");
                                        }
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&update_status_verifikasi=' + status_verifikasi + '&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_permanen(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
                                        } else {

                                        }
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>