<?php
include "models/global/data_pendaftar_verifikasi_berkas/model_data_pendaftar_verifikasi_berkas.php";
include "models/global/data_pendaftar/model_data_pendaftar.php";

include "controllers/admin/pendaftaran/laporan/controller_laporan_lolos_berkas.php";
?>
<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Laporan Rekap Lolos Berkas</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="?menu=home">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo "?menu=" . $_GET['menu'] ?>">Laporan Rekap Lolos Berkas</a>
				</li>

				<?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<?php if (isset($_GET['tambah'])) { ?>
							<a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
						<?php } else { ?>
							<a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
						<?php } ?>
					</li>
				<?php } ?>

			</ul>
		</div>

		<?php
		//FORM LIST DATA
		if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
		?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="card-title">List Lolos Berkas
								<?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
									echo "(" . $_GET['filter_status'] . ")";
								} ?>
							</div>
						</div>
						<div class="card-body">
							<div class="row">

								<div class="col-lg-2 text-left">
									
								</div>
								<div class="col-lg-4 text-left">
									
								</div>
								<div class="col-lg-6 text-right">
									<a href="views/admin/pendaftaran/laporan/export_excel/view_export_excel_laporan_lolos_berkas.php" target="_blank" class="btn btn-success">
										<i class="fas fa-file-excel"></i> &nbsp;
										Export to Excel</a>
								</div>


								<div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right; display: none;">
									<ul class="list-inline">
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (
												<?php echo $hitung_Aktif ?>)
											</a></li>
										<li class="list-inline-item"> | </li>
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (
												<?php echo $hitung_Terarsip ?>)
											</a></li>
										<li class="list-inline-item"> | </li>
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (
												<?php echo $hitung_Terhapus ?>)
											</a></li>
									</ul>
								</div>
							</div>
							<br>
							<div class="table-responsive">
								<table id="basic-datatables" class="table table-borderless" style="width:100%">
									<thead class="bg-light">
										<tr>
											<th>No</th>
											<th>Nomor Pendaftaran</th>
											<th>Nama Lengkap</th>
											<th>Asal Sekolah</th>
											<th>NIS</th>
											<th>NISN</th>
											<th>NIK</th>
											<th>Akta Kelahiran</th>
											<th>Kartu Keluarga</th>
											<th>Raport</th>
											<th>Kartu NISN</th>
											<th>SKL Ijazah</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ((isset($list_datatable_master))) {
											foreach ($list_datatable_master as $data) {
												$nomor++; ?>
												<?php
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'])) {
														$data['Nomor_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'];
													} else {
														$data['Nomor_Pendaftaran'] = "";
													}
												} else {
													$data['Nomor_Pendaftaran'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'])) {
														$data['NISN'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'];
													} else {
														$data['NISN'] = "";
													}
												} else {
													$data['NISN'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'])) {
														$data['NIK'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'];
													} else {
														$data['NIK'] = "";
													}
												} else {
													$data['NIK'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'])) {
														$data['Nama_Lengkap'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'];
													} else {
														$data['Nama_Lengkap'] = "";
													}
												} else {
													$data['Nama_Lengkap'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'])) {
														$data['NIS'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'];
													} else {
														$data['NIS'] = "";
													}
												} else {
													$data['NIS'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Asal_Sekolah
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Asal_Sekolah'])) {
														$data['Asal_Sekolah'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Asal_Sekolah'];
													} else {
														$data['Asal_Sekolah'] = "";
													}
												} else {
													$data['Asal_Sekolah'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Asal_Sekolah
												?>
												<tr>
													<td><?php echo $nomor ?></td>
													<td><?php echo $data['Nomor_Pendaftaran'] ?></td>
													<td><?php echo $data['Nama_Lengkap'] ?></td>
													<td><?php echo $data['Asal_Sekolah'] ?></td>
													<td><?php echo $data['NIS'] ?></td>
													<td><?php echo $data['NISN'] ?></td>
													<td><?php echo $data['NIK'] ?></td>
													<td>
														<?php if ($data['Akta_Kelahiran'] <> "") { ?>
															<a href="<?php echo $folder_penyimpanan_file_berkas_akte_kelahiran ?><?php echo $data['Akta_Kelahiran'] ?>" target="_blank">
																Lihat File
															</a>
														<?php } ?>
													</td>
													<td>
														<?php if ($data['Kartu_Keluarga'] <> "") { ?>
															<a href="<?php echo $folder_penyimpanan_file_berkas_kartu_keluarga ?><?php echo $data['Kartu_Keluarga'] ?>" target="_blank">
																Lihat File
															</a>
														<?php } ?>
													</td>
													<td>
														<?php if ($data['Rapor'] <> "") { ?>
															<a href="<?php echo $folder_penyimpanan_file_berkas_rapor ?><?php echo $data['Rapor'] ?>" target="_blank">
																Lihat File
															</a>
														<?php } ?>
													</td>
													<td>
														<?php if ($data['Kartu_NISN'] <> "") { ?>
															<a href="<?php echo $folder_penyimpanan_file_berkas_kartu_nisn ?><?php echo $data['Kartu_NISN'] ?>" target="_blank">
																Lihat File
															</a>
														<?php } ?>
													</td>
													<td>
														<?php if ($data['SKL_Ijazah'] <> "") { ?>
															<a href="<?php echo $folder_penyimpanan_file_berkas_skl_ijazah ?><?php echo $data['SKL_Ijazah'] ?>" target="_blank">
																Lihat File
															</a>
														<?php } ?>
													</td>
												</tr>
											<?php } ?>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>