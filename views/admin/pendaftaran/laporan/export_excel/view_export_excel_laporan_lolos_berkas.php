<?php
include "../../../../../init.php";
include "../../../../../models/admin/login/model_login.php";
include "../../../../../models/admin/dashboard/model_dashboard.php";
include "../../../../../controllers/admin/dashboard/controller_dashboard.php";
include "../../../../../models/global/data_pendaftar_verifikasi_berkas/model_data_pendaftar_verifikasi_berkas.php";
include "../../../../../models/global/data_pendaftar/model_data_pendaftar.php";
include "../../../../../controllers/admin/pendaftaran/laporan/controller_laporan_lolos_berkas.php";


function replaceSymbolsWithUnderscore($inputString)
{
    $symbols = array("~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "=", "+", "[", "]", "{", "}", "|", "\\", ";", ":", "'", "\"", "<", ">", ",", ".", "/", "?", " ");
    $outputString = str_replace($symbols, "_", $inputString);
    return $outputString;
}

$Nama_File = "Rekap_Laporan_Lolos_Berkas" . replaceSymbolsWithUnderscore("") . ".xls";
?>
<!DOCTYPE html>
<html>

<head>
    <title>Export Data Format Excel</title>
</head>

<body>
    <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=$Nama_File");
    ?>
    <table>
        <tr>
            <td colspan="16">
                <h2>REKAP LAPORAN LOLOS BERKAS</h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                <h2>PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                <h2>TAHUN AJARAN : <?php echo $data_ppdb_saat_ini['Tahun_Ajaran']?></h2>
            </td>
        </tr>
    </table>
    <table border="1">
        <thead class="bg-light">
            <tr>
                <th>No</th>
                <th>Nomor Pendaftaran</th>
                <th>Nama Lengkap</th>
                <th>Asal Sekolah</th>
                <th>NIS</th>
                <th>NISN</th>
                <th>NIK</th>
                <th>File Akta Kelahiran</th>
                <th>File Kartu Keluarga</th>
                <th>File Raport</th>
                <th>File NISN</th>
                <th>File SKL/Ijazah</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ((isset($list_datatable_master))) {
                foreach ($list_datatable_master as $data) {
                    $nomor++; ?>
                    <?php
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
                    if (isset($array_result_relasi_data_pendaftar)) {
                        $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                        if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'])) {
                            $data['Nomor_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'];
                        } else {
                            $data['Nomor_Pendaftaran'] = "";
                        }
                    } else {
                        $data['Nomor_Pendaftaran'] = "";
                    }
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
            
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN
                    if (isset($array_result_relasi_data_pendaftar)) {
                        $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                        if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'])) {
                            $data['NISN'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'];
                        } else {
                            $data['NISN'] = "";
                        }
                    } else {
                        $data['NISN'] = "";
                    }
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN
            
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK
                    if (isset($array_result_relasi_data_pendaftar)) {
                        $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                        if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'])) {
                            $data['NIK'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'];
                        } else {
                            $data['NIK'] = "";
                        }
                    } else {
                        $data['NIK'] = "";
                    }
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK
            
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
                    if (isset($array_result_relasi_data_pendaftar)) {
                        $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                        if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'])) {
                            $data['Nama_Lengkap'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'];
                        } else {
                            $data['Nama_Lengkap'] = "";
                        }
                    } else {
                        $data['Nama_Lengkap'] = "";
                    }
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
            
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS
                    if (isset($array_result_relasi_data_pendaftar)) {
                        $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                        if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'])) {
                            $data['NIS'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'];
                        } else {
                            $data['NIS'] = "";
                        }
                    } else {
                        $data['NIS'] = "";
                    }
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS
            
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Asal_Sekolah
                    if (isset($array_result_relasi_data_pendaftar)) {
                        $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                        if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Asal_Sekolah'])) {
                            $data['Asal_Sekolah'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Asal_Sekolah'];
                        } else {
                            $data['Asal_Sekolah'] = "";
                        }
                    } else {
                        $data['Asal_Sekolah'] = "";
                    }
                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Asal_Sekolah
                    ?>
                    <tr>
                        <td style='mso-number-format:\@'>
                            <?php echo $nomor ?>
                        </td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Pendaftaran'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Lengkap'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Asal_Sekolah'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIS'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NISN'];?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK'];?></td>
                        <td style='mso-number-format:\@'>
                        <?php if ($data['Akta_Kelahiran'] <> "") { ?>
                            <?php echo $Link_Website.$folder_penyimpanan_file_berkas_akte_kelahiran ?><?php echo $data['Akta_Kelahiran'] ?>
                        <?php } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                        <?php if ($data['Kartu_Keluarga'] <> "") { ?>
                            <?php echo $Link_Website.$folder_penyimpanan_file_berkas_kartu_keluarga ?><?php echo $data['Kartu_Keluarga'] ?>
                        <?php } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                        <?php if ($data['Rapor'] <> "") { ?>
                            <?php echo $Link_Website.$folder_penyimpanan_file_berkas_rapor ?><?php echo $data['Rapor'] ?>
                        <?php } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                        <?php if ($data['Kartu_NISN'] <> "") { ?>
                            <?php echo $Link_Website.$folder_penyimpanan_file_berkas_kartu_nisn ?><?php echo $data['Kartu_NISN'] ?>
                        <?php } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                        <?php if ($data['SKL_Ijazah'] <> "") { ?>
                            <?php echo $Link_Website.$folder_penyimpanan_file_berkas_skl_ijazah ?><?php echo $data['SKL_Ijazah'] ?>
                        <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>