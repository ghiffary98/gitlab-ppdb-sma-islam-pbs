<?php
include "../../../../../init.php";
include "../../../../../models/admin/login/model_login.php";
include "../../../../../models/admin/dashboard/model_dashboard.php";
include "../../../../../controllers/admin/dashboard/controller_dashboard.php";
include "../../../../../models/global/data_pendaftar/model_data_pendaftar.php";
include "../../../../../models/global/data_pendaftar_nilai_rapor/model_data_pendaftar_nilai_rapor.php";
include "../../../../../models/global/data_pendaftar_program_layanan/model_data_pendaftar_program_layanan.php";
include "../../../../../controllers/admin/pendaftaran/laporan/controller_laporan_pendaftar_diterima.php";

$a_data_pendaftar_nilai_rapor = new a_data_pendaftar_nilai_rapor();
$a_data_pendaftar_program_layanan = new a_data_pendaftar_program_layanan();
function replaceSymbolsWithUnderscore($inputString)
{
    $symbols = array("~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "=", "+", "[", "]", "{", "}", "|", "\\", ";", ":", "'", "\"", "<", ">", ",", ".", "/", "?", " ");
    $outputString = str_replace($symbols, "_", $inputString);
    return $outputString;
}

$Nama_File = "Rekap_Laporan_Pendaftar_Diterima" . replaceSymbolsWithUnderscore("") . ".xls";

?>
<!DOCTYPE html>
<html>

<head>
    <title>Export Data Format Excel</title>
</head>

<body>
    <?php
    if (isset($_GET['tampilan_html'])) {

    } else {
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=$Nama_File");
    }
    ?>
    <table>
        <tr>
            <td colspan="16">
                <h2>REKAP LAPORAN PENDAFTAR DITERIMA</h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                <h2>PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                <h2>TAHUN AJARAN : <?php echo $data_ppdb_saat_ini['Tahun_Ajaran'] ?></h2>
            </td>
        </tr>
    </table>
    <table border="1">
        <thead class="bg-light">
            <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">NIK</th>
                <th rowspan="2">NIS</th>
                <th rowspan="2">NISN</th>
                <th rowspan="2">NIK</th>
                <th rowspan="2">NIPD</th>
                <th rowspan="2">Nama Lengkap</th>
                <th rowspan="2">Jenis Kelamin</th>
                <th rowspan="2">Tempat Lahir</th>
                <th rowspan="2">Tanggal Lahir</th>
                <th rowspan="2">Agama</th>
                <th rowspan="2">Status Dalam Keluarga</th>
                <th rowspan="2">Jalan</th>
                <th rowspan="2">Kelurahan</th>
                <th rowspan="2">Kecamatan</th>
                <th rowspan="2">Kota</th>
                <th rowspan="2">Provinsi</th>
                <th rowspan="2">Kode Pos</th>
                <th rowspan="2">Titik Lintang Alamat</th>
                <th rowspan="2">Titik Bujur Alamat</th>
                <th rowspan="2">Nomor Handphone</th>
                <th rowspan="2">Nomor Telepon</th>
                <th rowspan="2">Email</th>
                <th rowspan="2">Golongan Darah</th>
                <th rowspan="2">Kebutuhan Khusus</th>
                <th rowspan="2">Tinggi Badan</th>
                <th rowspan="2">Berat Badan</th>
                <th rowspan="2">Lingkar Kepala</th>
                <th rowspan="2">Asal Sekolah</th>
                <th rowspan="2">Alamat Sekolah</th>
                <th rowspan="2">Kelas</th>
                <th rowspan="2">Nomor Peserta Ujian Nasional</th>
                <th rowspan="2">Nomor Seri Ijazah</th>
                <th rowspan="2">Penerima KPS</th>
                <th rowspan="2">Penerima KIP</th>
                <th rowspan="2">Nomor KIP</th>
                <th rowspan="2">Penerima KJP DKI</th>
                <th rowspan="2">Nomor KJP DKI</th>
                <th rowspan="2">Nomor KPS</th>
                <th rowspan="2">Nomor KKS</th>
                <th rowspan="2">Nomor KK</th>
                <th rowspan="2">Nomor Registrasi Akta Lahir</th>
                <th rowspan="2">Anak Ke</th>
                <th rowspan="2">Jumlah Saudara</th>
                <th rowspan="2">Status Perwalian</th>
                <th rowspan="2">NIK Ayah</th>
                <th rowspan="2">Nama Ayah</th>
                <th rowspan="2">Tempat Lahir Ayah</th>
                <th rowspan="2">Tanggal Lahir Ayah</th>
                <th rowspan="2">Jenjang Pendidikan Ayah</th>
                <th rowspan="2">Pekerjaan Ayah</th>
                <th rowspan="2">Penghasilan Ayah</th>
                <th rowspan="2">No HP Ayah</th>
                <th rowspan="2">NIK Ibu</th>
                <th rowspan="2">Nama Ibu</th>
                <th rowspan="2">Tempat Lahir Ibu</th>
                <th rowspan="2">Tanggal Lahir Ibu</th>
                <th rowspan="2">Jenjang Pendidikan Ibu</th>
                <th rowspan="2">Pekerjaan Ibu</th>
                <th rowspan="2">Penghasilan Ibu</th>
                <th rowspan="2">No HP Ibu</th>
                <th rowspan="2">NIK Wali</th>
                <th rowspan="2">Nama Wali</th>
                <th rowspan="2">Tempat Lahir Wali</th>
                <th rowspan="2">Tanggal Lahir Wali</th>
                <th rowspan="2">Jenjang Pendidikan Wali</th>
                <th rowspan="2">Pekerjaan Wali</th>
                <th rowspan="2">Penghasilan Wali</th>
                <th rowspan="2">No HP Wali</th>
                <th rowspan="2">Kendaraan Yang Dipakai Kesekolah</th>
                <th rowspan="2">Jarak Rumah Ke Sekolah</th>
                <th rowspan="2">Ukuran Seragam</th>
                <th rowspan="2">Program</th>
                <th rowspan="2">Layanan 1</th>
                <th rowspan="2">Layanan 2</th>
                <th rowspan="2">Layanan 3</th>
                <th rowspan="2">Layanan 4</th>
                <th colspan="10">Semester 1</th>
                <th colspan="10">Semester 2</th>
                <th colspan="10">Semester 3</th>
                <th colspan="10">Semester 4</th>
                <th rowspan="2">Nilai SKL / Ijazah</th>
            </tr>
            <tr>
                <th>Pendidikan Agama Islam dan Budi Pekerti</th>
                <th>Pendidikan Pancasila dan Kewarganegaraan</th>
                <th>Bahasa Indonesia</th>
                <th>Bahasa Inggris</th>
                <th>Matematika</th>
                <th>IPA (Ilmu Pengetahuan Alam)</th>
                <th>IPS (Ilmu Pengetahuan Sosial)</th>
                <th>Seni dan Budaya</th>
                <th>PJOK</th>
                <th>Prakarya</th>

                <th>Pendidikan Agama Islam dan Budi Pekerti</th>
                <th>Pendidikan Pancasila dan Kewarganegaraan</th>
                <th>Bahasa Indonesia</th>
                <th>Bahasa Inggris</th>
                <th>Matematika</th>
                <th>IPA (Ilmu Pengetahuan Alam)</th>
                <th>IPS (Ilmu Pengetahuan Sosial)</th>
                <th>Seni dan Budaya</th>
                <th>PJOK</th>
                <th>Prakarya</th>

                <th>Pendidikan Agama Islam dan Budi Pekerti</th>
                <th>Pendidikan Pancasila dan Kewarganegaraan</th>
                <th>Bahasa Indonesia</th>
                <th>Bahasa Inggris</th>
                <th>Matematika</th>
                <th>IPA (Ilmu Pengetahuan Alam)</th>
                <th>IPS (Ilmu Pengetahuan Sosial)</th>
                <th>Seni dan Budaya</th>
                <th>PJOK</th>
                <th>Prakarya</th>

                <th>Pendidikan Agama Islam dan Budi Pekerti</th>
                <th>Pendidikan Pancasila dan Kewarganegaraan</th>
                <th>Bahasa Indonesia</th>
                <th>Bahasa Inggris</th>
                <th>Matematika</th>
                <th>IPA (Ilmu Pengetahuan Alam)</th>
                <th>IPS (Ilmu Pengetahuan Sosial)</th>
                <th>Seni dan Budaya</th>
                <th>PJOK</th>
                <th>Prakarya</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ((isset($list_datatable_master))) {
                foreach ($list_datatable_master as $data) {
                    $nomor++; ?>
                    <?php
                    $result = $a_data_pendaftar_nilai_rapor->baca_data_id("Id_Pendaftar", $data['Id_Pendaftar']);

                    if ($result['Status'] == "Sukses") {
                        $data_nilai_rapor = $result['Hasil'];
                    } else {
                        $data_nilai_rapor = null;
                    }

                    $result = $a_data_pendaftar_program_layanan->baca_data_id("Id_Pendaftar", $data['Id_Pendaftar']);

                    if ($result['Status'] == "Sukses") {
                        $data_program_layanan = $result['Hasil'];
                    } else {
                        $data_program_layanan = null;
                    }
                    ?>
                    <tr>
                        <td style='mso-number-format:\@'>
                            <?php echo $nomor ?>
                        </td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIS']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NISN']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIPD']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Lengkap']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenis_Kelamin']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tempat_Lahir']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tanggal_Lahir']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Agama']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Status_Dalam_Keluarga']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jalan']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kelurahan']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kecamatan']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kota']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Provinsi']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kode_Pos']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Titik_Lintang_Alamat']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Titik_Bujur_Alamat']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Handphone']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Telepon']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Email']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Golongan_Darah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kebutuhan_Khusus']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tinggi_Badan']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Berat_Badan']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Lingkar_Kepala']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Asal_Sekolah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Alamat_Sekolah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kelas']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Peserta_Ujian_Nasional']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Seri_Ijazah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penerima_KPS']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penerima_KIP']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_KIP']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penerima_KJP_DKI']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_KJP_DKI']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_KPS']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_KKS']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_KK']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Registrasi_Akta_Lahir']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Anak_Ke']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jumlah_Saudara']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Status_Perwalian']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tempat_Lahir_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tanggal_Lahir_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenjang_Pendidikan_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Pekerjaan_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penghasilan_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['No_HP_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tempat_Lahir_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tanggal_Lahir_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenjang_Pendidikan_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Pekerjaan_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penghasilan_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['No_HP_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tempat_Lahir_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tanggal_Lahir_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenjang_Pendidikan_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Pekerjaan_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Penghasilan_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['No_HP_Wali']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Kendaraan_Yang_Dipakai_Kesekolah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jarak_Rumah_Ke_Sekolah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Ukuran_Seragam']; ?></td>

                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_program_layanan)) {
                                echo $data_program_layanan['Program'];
                            } ?>
                        </td>

                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_program_layanan)) {
                                echo $data_program_layanan['Layanan_1'];
                            } ?>
                        </td>

                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_program_layanan)) {
                                echo $data_program_layanan['Layanan_2'];
                            } ?>
                        </td>

                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_program_layanan)) {
                                echo $data_program_layanan['Layanan_3'];
                            } ?>
                        </td>

                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_program_layanan)) {
                                echo $data_program_layanan['Layanan_4'];
                            } ?>
                        </td>


                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_Bahasa_Indonesia'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_Bahasa_Inggris'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_Matematika'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_IPA'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_IPS'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_Seni_Dan_Budaya'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_PJOK'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_1_Prakarya'];
                            } ?>
                        </td>

                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_Bahasa_Indonesia'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_Bahasa_Inggris'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_Matematika'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_IPA'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_IPS'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_Seni_Dan_Budaya'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_PJOK'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_2_Prakarya'];
                            } ?>
                        </td>

                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_Bahasa_Indonesia'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_Bahasa_Inggris'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_Matematika'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_IPA'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_IPS'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_Seni_Dan_Budaya'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_PJOK'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_3_Prakarya'];
                            } ?>
                        </td>

                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_Bahasa_Indonesia'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_Bahasa_Inggris'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_Matematika'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_IPA'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_IPS'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_Seni_Dan_Budaya'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_PJOK'];
                            } ?>
                        </td>
                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_Semester_4_Prakarya'];
                            } ?>
                        </td>

                        <td style='mso-number-format:\@'>
                            <?php if (isset($data_nilai_rapor)) {
                                echo $data_nilai_rapor['Nilai_SKL_Ijazah'];
                            } ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>