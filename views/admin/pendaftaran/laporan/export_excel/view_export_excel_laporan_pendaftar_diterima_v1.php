<?php
include "../../../../../init.php";
include "../../../../../models/admin/login/model_login.php";
include "../../../../../models/admin/dashboard/model_dashboard.php";
include "../../../../../controllers/admin/dashboard/controller_dashboard.php";
include "../../../../../models/global/data_pendaftar/model_data_pendaftar.php";
include "../../../../../controllers/admin/pendaftaran/laporan/controller_laporan_pendaftar_diterima.php";

function replaceSymbolsWithUnderscore($inputString)
{
    $symbols = array("~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "=", "+", "[", "]", "{", "}", "|", "\\", ";", ":", "'", "\"", "<", ">", ",", ".", "/", "?", " ");
    $outputString = str_replace($symbols, "_", $inputString);
    return $outputString;
}

$Nama_File = "Rekap_Laporan_Pendaftar_Diterima" . replaceSymbolsWithUnderscore("") . ".xls";

?>
<!DOCTYPE html>
<html>

<head>
    <title>Export Data Format Excel</title>
</head>

<body>
    <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=$Nama_File");
    ?>
    <table>
        <tr>
            <td colspan="16">
                <h2>REKAP LAPORAN PENDAFTAR DITERIMA</h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                <h2>PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></h2>
            </td>
        </tr>
        <tr>
            <td colspan="16">
                <h2>TAHUN AJARAN : <?php echo $data_ppdb_saat_ini['Tahun_Ajaran'] ?></h2>
            </td>
        </tr>
    </table>
    <table border="1">
        <thead class="bg-light">
            <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Nomor Pendaftaran</th>
                <th rowspan="2">Tanggal Pendaftaran</th>
                <th rowspan="2">Nama Lengkap</th>
                <th rowspan="2">Asal Sekolah</th>
                <th rowspan="2">NIS</th>
                <th rowspan="2">NISN</th>
                <th rowspan="2">NIK</th>
                <th rowspan="2">Jenis Kelamin</th>
                <th rowspan="2">Agama</th>
                <th rowspan="2">Tempat & Tgl Lahir</th>
                <th rowspan="2">Agama</th>
                <th rowspan="2">No. HP</th>
                <th rowspan="2">Email</th>
                <th colspan="3">Nama Orang Tua</th>
            </tr>
            <tr>
                <th>Ayah</th>
                <th>Ibu</th>
                <th>Wali</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ((isset($list_datatable_master))) {
                foreach ($list_datatable_master as $data) {
                    $nomor++; ?>
                    <tr>
                        <td style='mso-number-format:\@'>
                            <?php echo $nomor ?>
                        </td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Pendaftaran']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Waktu_Simpan_Data']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Lengkap']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Asal_Sekolah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIS']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NISN']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['NIK']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Jenis_Kelamin']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Agama']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Tempat_Lahir'] . " " . $data['Tanggal_Lahir']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Agama']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nomor_Handphone']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Email']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Ayah']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Ibu']; ?></td>
                        <td style='mso-number-format:\@'><?php echo $data['Nama_Wali']; ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>