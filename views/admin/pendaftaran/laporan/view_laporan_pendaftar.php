<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";

include "controllers/admin/pendaftaran/laporan/controller_laporan_pendaftar.php";
?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Laporan Rekap Pendaftar</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="?menu=home">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="<?php echo "?menu=" . $_GET['menu'] ?>">Laporan Rekap Pendaftar</a>
                </li>

                <?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <?php if (isset($_GET['tambah'])) { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
                        <?php } else { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
                        <?php } ?>
                    </li>
                <?php } ?>

            </ul>
        </div>

        <?php
        //FORM LIST DATA
        if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">List Pendaftar
                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
                                    echo "(" . $_GET['filter_status'] . ")";
                                } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">


                                <div class="col-lg-2 text-left">
                                    
                                </div>
                                <div class="col-lg-4 text-left">
                                    
                                </div>
                                <div class="col-lg-6 text-right">
                                    <a href="views/admin/pendaftaran/laporan/export_excel/view_export_excel_laporan_pendaftar.php" target="_blank" class="btn btn-success">
                                        <i class="fas fa-file-excel"></i> &nbsp;
                                        Export to Excel</a>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right; display: none;">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (<?php echo $hitung_Aktif ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (<?php echo $hitung_Terarsip ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (<?php echo $hitung_Terhapus ?>)
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table id="basic-datatables" class="table table-borderless" style="width:100%">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Pendaftaran</th>
                                            <th>Tanggal Pendaftaran</th>
                                            <th>Nama Lengkap</th>
                                            <th>Asal Sekolah</th>
                                            <th>NIS</th>
                                            <th>NISN</th>
                                            <th>NIK</th>
                                            <th>Jenis Kelamin</th>
                                            <th>No. HP</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ((isset($list_datatable_master))) {
                                            foreach ($list_datatable_master as $data) {
                                                $nomor++; ?>
                                                <tr>
                                                    <td><?php echo $nomor ?></td>
                                                    <td><?php echo $data['Nomor_Pendaftaran'] ?></td>
                                                    <td><?php echo $data['Waktu_Simpan_Data'] ?></td>
                                                    <td><?php echo $data['Nama_Lengkap'] ?></td>
                                                    <td><?php echo $data['Asal_Sekolah'] ?></td>
                                                    <td><?php echo $data['NIS'] ?></td>
                                                    <td><?php echo $data['NISN'] ?></td>
                                                    <td><?php echo $data['NIK'] ?></td>
                                                    <td><?php echo $data['Jenis_Kelamin'] ?></td>
                                                    <td><?php echo $data['Nomor_Handphone'] ?></td>
                                                    <td><?php echo $data['Email'] ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>