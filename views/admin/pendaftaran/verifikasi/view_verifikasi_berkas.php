<?php
include "models/global/data_pendaftar_verifikasi_berkas/model_data_pendaftar_verifikasi_berkas.php";
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/pengumuman_pendaftar/model_pengumuman_pendaftar.php";
include "controllers/global/pengumuman_pendaftar/controller_pengumuman_pendaftar.php";
include "controllers/admin/pendaftaran/verifikasi/controller_verifikasi_berkas.php";
?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Verifikasi Berkas</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="?menu=home">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="<?php echo "?menu=" . $_GET['menu'] ?>">Verifikasi Berkas</a>
                </li>

                <?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <?php if (isset($_GET['tambah'])) { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
                        <?php } else { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
                        <?php } ?>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php
        //FORM TAMBAH/EDIT DATA
        if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <?php if (isset($_GET['tambah'])) { ?>
                                        <div class="card-title">Tambah Data</div>
                                    <?php } else { ?>
                                        <div class="card-title">Edit Data</div>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right; display: none;">
                                    <?php if (isset($_GET["edit"])) { ?>
                                        <script type="text/javascript">
                                            function konfirmasi_hapus_data_permanen() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_hapus_data_ke_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_arsip_data() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_arsip() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }
                                        </script>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Aktif") { ?>
                                                    <a href="#" onclick="konfirmasi_arsip_data()"><i class="fa fa-archive fa-md">
                                                            ARSIPKAN</i></a>
                                                <?php } elseif ($edit['Status'] == "Terarsip") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i class="fa fa-archive fa-md"> AKTIFKAN</i></a>
                                                <?php } elseif ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i class="fa fa-archive fa-md"> RESTORE</i></a>
                                                <?php } ?>

                                            </li>
                                            <li class="list-inline-item"> | </li>
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_permanen()"><i class="fa fa-trash fa-md"> HAPUS </i></a>
                                                <?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i class="fa fa-trash fa-md"> HAPUS </i></a>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">PPDB Tahun Ajaran</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_ppdb['Tahun_Ajaran'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">Nomor Pendaftaran</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_pendaftar['Nomor_Pendaftaran'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">NIS</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_pendaftar['NIS'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">NIK</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_pendaftar['NIK'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">NISN</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_pendaftar['NISN'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">Username</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_pendaftar['Username'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">Nomor Handphone</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_pendaftar['Nomor_Handphone'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">Nama Lengkap</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_pendaftar['Nama_Lengkap'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">Email</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_pendaftar['Email'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-lg-3 control-label">Status Pendaftaran</label>
                                                <div class="col-lg-9">:
                                                    <?php if (isset($_GET['edit'])) {
                                                        echo $edit_data_pendaftar['Status_Pendaftaran'];
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <hr>
                                        <h2> Daftar Berkas</h2>
                                    </div>
                                    <div class="col-lg-12">
                                        <hr>
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Akta Kelahiran </label>
                                            <div class="col-lg-6" style="display:flex;">:&nbsp;
                                                <?php if (isset($_GET['edit'])) {
                                                    if ($edit['Akta_Kelahiran'] == "") { ?>
                                                        <font class="text-danger"> Tidak ada File! </font>
                                                    <?php } else { ?>
                                                        <script>
                                                            function konfirmasi_hapus_file_akta_kelahiran() {
                                                                var txt;
                                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus File Ini ?");
                                                                if (r == true) {
                                                                    document.getElementById("kotak_div_tombol_file_akta_kelahiran").style.display = "none";
                                                                    document.getElementById("hapus_file_akta_kelahiran").value = "Iya";
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }
                                                            }
                                                        </script>
                                                        <div id="kotak_div_tombol_file_akta_kelahiran">
                                                            <b>
                                                            <a href="<?php echo $folder_penyimpanan_file_berkas_akte_kelahiran ?><?php echo $edit['Akta_Kelahiran'] ?>" target="_blank">
                                                                    Lihat / Download File
                                                                </a>
                                                            </b>
                                                            
                                                            <b onclick="return konfirmasi_hapus_file_akta_kelahiran()" style="color:red;<?php if ($edit['Status_Verifikasi_Akta_Kelahiran'] == "Sudah Diverifikasi") { echo "display:none"; } ?>">
                                                                | Hapus File
                                                            </b>
                                                            <input type="text" id="hapus_file_akta_kelahiran" name="hapus_file_akta_kelahiran" style="display:none">
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($edit['Status_Verifikasi_Akta_Kelahiran'] == "Sudah Diverifikasi") {
                                                    ?>
                                                        &nbsp; &nbsp; <span class="badge badge-success" style="margin-left:0px !important;"> Sudah Diverifikasi </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_Akta_Kelahiran'] == "Verifikasi Ditolak") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Verifikasi Ditolak </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_Akta_Kelahiran'] == "Menunggu Verifikasi") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-warning" style="margin-left:0px !important;"> Menunggu Verifikasi </span>
                                                    <?php } else { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Belum Diverifikasi </span>
                                                    <?php } ?>
                                                <?php
                                                } ?>
                                            </div>
                                            <div class="col-lg-3 text-right">
                                                <?php
                                                if ($edit['Status_Verifikasi_Akta_Kelahiran'] == "Sudah Diverifikasi") {
                                                ?>
                                                    <button name="Status_Verifikasi_Akta_Kelahiran_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_Akta_Kelahiran'] == "Verifikasi Ditolak") { ?>
                                                    <button name="Status_Verifikasi_Akta_Kelahiran_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Akta_Kelahiran_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_Akta_Kelahiran'] == "Menunggu Verifikasi") { ?>
                                                    <button name="Status_Verifikasi_Akta_Kelahiran_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Akta_Kelahiran_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } else { ?>
                                                    <button name="Status_Verifikasi_Akta_Kelahiran_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Akta_Kelahiran_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } ?>


                                            </div>
                                            <hr>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-3"> &nbsp; </div>
                                            <div class="col-lg-9">
                                                <input type="file" accept=".pdf, .jpg, .jpeg, .png" name="Akta_Kelahiran" class="form-control">
                                                <i>click "Choose File" untuk mengganti file</i>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Kartu Keluarga </label>
                                            <div class="col-lg-6" style="display:flex;">:&nbsp;
                                                <?php if (isset($_GET['edit'])) {
                                                    if ($edit['Kartu_Keluarga'] == "") { ?>
                                                        <font class="text-danger"> Tidak ada File! </font>
                                                    <?php } else { ?>
                                                        <script>
                                                            function konfirmasi_hapus_file_kartu_keluarga() {
                                                                var txt;
                                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus File Ini ?");
                                                                if (r == true) {
                                                                    document.getElementById("kotak_div_tombol_file_kartu_keluarga").style.display = "none";
                                                                    document.getElementById("hapus_file_kartu_keluarga").value = "Iya";
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }
                                                            }
                                                        </script>
                                                        <div id="kotak_div_tombol_file_kartu_keluarga">
                                                            <b>
                                                            <a href="<?php echo $folder_penyimpanan_file_berkas_kartu_keluarga ?><?php echo $edit['Kartu_Keluarga'] ?>" target="_blank">
                                                                    Lihat / Download File
                                                                </a>
                                                            </b>
                                                            
                                                            <b onclick="return konfirmasi_hapus_file_kartu_keluarga()" style="color:red;<?php if ($edit['Status_Verifikasi_Kartu_Keluarga'] == "Sudah Diverifikasi") { echo "display:none"; } ?>">
                                                                | Hapus File
                                                            </b>
                                                            <input type="text" id="hapus_file_kartu_keluarga" name="hapus_file_kartu_keluarga" style="display:none">
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($edit['Status_Verifikasi_Kartu_Keluarga'] == "Sudah Diverifikasi") {
                                                    ?>
                                                        &nbsp; &nbsp; <span class="badge badge-success" style="margin-left:0px !important;"> Sudah Diverifikasi </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_Kartu_Keluarga'] == "Verifikasi Ditolak") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Verifikasi Ditolak </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_Kartu_Keluarga'] == "Menunggu Verifikasi") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-warning" style="margin-left:0px !important;"> Menunggu Verifikasi </span>
                                                    <?php } else { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Belum Diverifikasi </span>
                                                    <?php } ?>
                                                <?php
                                                } ?>
                                            </div>
                                            <div class="col-lg-3 text-right">
                                                <?php
                                                if ($edit['Status_Verifikasi_Kartu_Keluarga'] == "Sudah Diverifikasi") {
                                                ?>
                                                    <button name="Status_Verifikasi_Kartu_Keluarga_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_Kartu_Keluarga'] == "Verifikasi Ditolak") { ?>
                                                    <button name="Status_Verifikasi_Kartu_Keluarga_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Kartu_Keluarga_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_Kartu_Keluarga'] == "Menunggu Verifikasi") { ?>
                                                    <button name="Status_Verifikasi_Kartu_Keluarga_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Kartu_Keluarga_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } else { ?>
                                                    <button name="Status_Verifikasi_Kartu_Keluarga_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Kartu_Keluarga_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } ?>


                                            </div>
                                            <hr>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-3"> &nbsp; </div>
                                            <div class="col-lg-9">
                                                <input type="file" accept=".pdf, .jpg, .jpeg, .png" name="Kartu_Keluarga" class="form-control">
                                                <i>click "Choose File" untuk mengganti file</i>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Rapor </label>
                                            <div class="col-lg-6" style="display:flex;">:&nbsp;
                                                <?php if (isset($_GET['edit'])) {
                                                    if ($edit['Rapor'] == "") { ?>
                                                        <font class="text-danger"> Tidak ada File! </font>
                                                    <?php } else { ?>
                                                        <script>
                                                            function konfirmasi_hapus_file_rapor() {
                                                                var txt;
                                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus File Ini ?");
                                                                if (r == true) {
                                                                    document.getElementById("kotak_div_tombol_file_rapor").style.display = "none";
                                                                    document.getElementById("hapus_file_rapor").value = "Iya";
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }
                                                            }
                                                        </script>
                                                        <div id="kotak_div_tombol_file_rapor">
                                                            <b>
                                                            <a href="<?php echo $folder_penyimpanan_file_berkas_rapor ?><?php echo $edit['Rapor'] ?>" target="_blank">
                                                                    Lihat / Download File
                                                                </a>
                                                            </b>
                                                            
                                                            <b onclick="return konfirmasi_hapus_file_rapor()" style="color:red;<?php if ($edit['Status_Verifikasi_Rapor'] == "Sudah Diverifikasi") { echo "display:none"; } ?>">
                                                                | Hapus File
                                                            </b>
                                                            <input type="text" id="hapus_file_rapor" name="hapus_file_rapor" style="display:none">
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($edit['Status_Verifikasi_Rapor'] == "Sudah Diverifikasi") {
                                                    ?>
                                                        &nbsp; &nbsp; <span class="badge badge-success" style="margin-left:0px !important;"> Sudah Diverifikasi </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_Rapor'] == "Verifikasi Ditolak") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Verifikasi Ditolak </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_Rapor'] == "Menunggu Verifikasi") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-warning" style="margin-left:0px !important;"> Menunggu Verifikasi </span>
                                                    <?php } else { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Belum Diverifikasi </span>
                                                    <?php } ?>
                                                <?php
                                                } ?>
                                            </div>
                                            <div class="col-lg-3 text-right">
                                                <?php
                                                if ($edit['Status_Verifikasi_Rapor'] == "Sudah Diverifikasi") {
                                                ?>
                                                    <button name="Status_Verifikasi_Rapor_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_Rapor'] == "Verifikasi Ditolak") { ?>
                                                    <button name="Status_Verifikasi_Rapor_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Rapor_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_Rapor'] == "Menunggu Verifikasi") { ?>
                                                    <button name="Status_Verifikasi_Rapor_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Rapor_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } else { ?>
                                                    <button name="Status_Verifikasi_Rapor_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Rapor_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } ?>


                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-3"> &nbsp; </div>
                                            <div class="col-lg-9">
                                                <input type="file" accept=".pdf, .jpg, .jpeg, .png" name="Rapor" class="form-control">
                                                <i>click "Choose File" untuk mengganti file</i>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Kartu NISN </label>
                                            <div class="col-lg-6" style="display:flex;">:&nbsp;
                                                <?php if (isset($_GET['edit'])) {
                                                    if ($edit['Kartu_NISN'] == "") { ?>
                                                        <font class="text-danger"> Tidak ada File! </font>
                                                    <?php } else { ?>
                                                        <script>
                                                            function konfirmasi_hapus_file_kartu_nisn() {
                                                                var txt;
                                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus File Ini ?");
                                                                if (r == true) {
                                                                    document.getElementById("kotak_div_tombol_file_kartu_nisn").style.display = "none";
                                                                    document.getElementById("hapus_file_kartu_nisn").value = "Iya";
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }
                                                            }
                                                        </script>
                                                        <div id="kotak_div_tombol_file_kartu_nisn">
                                                            <b>
                                                            <a href="<?php echo $folder_penyimpanan_file_berkas_kartu_nisn ?><?php echo $edit['Kartu_NISN'] ?>" target="_blank">
                                                                    Lihat / Download File
                                                                </a>
                                                            </b>
                                                            
                                                            <b onclick="return konfirmasi_hapus_file_kartu_nisn()" style="color:red;<?php if ($edit['Status_Verifikasi_Kartu_NISN'] == "Sudah Diverifikasi") { echo "display:none"; } ?>">
                                                                | Hapus File
                                                            </b>
                                                            <input type="text" id="hapus_file_kartu_nisn" name="hapus_file_kartu_nisn" style="display:none">
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($edit['Status_Verifikasi_Kartu_NISN'] == "Sudah Diverifikasi") {
                                                    ?>
                                                        &nbsp; &nbsp; <span class="badge badge-success" style="margin-left:0px !important;"> Sudah Diverifikasi </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_Kartu_NISN'] == "Verifikasi Ditolak") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Verifikasi Ditolak </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_Kartu_NISN'] == "Menunggu Verifikasi") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-warning" style="margin-left:0px !important;"> Menunggu Verifikasi </span>
                                                    <?php } else { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Belum Diverifikasi </span>
                                                    <?php } ?>
                                                <?php
                                                } ?>
                                            </div>
                                            <div class="col-lg-3 text-right">
                                                <?php
                                                if ($edit['Status_Verifikasi_Kartu_NISN'] == "Sudah Diverifikasi") {
                                                ?>
                                                    <button name="Status_Verifikasi_Kartu_NISN_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_Kartu_NISN'] == "Verifikasi Ditolak") { ?>
                                                    <button name="Status_Verifikasi_Kartu_NISN_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Kartu_NISN_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_Kartu_NISN'] == "Menunggu Verifikasi") { ?>
                                                    <button name="Status_Verifikasi_Kartu_NISN_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Kartu_NISN_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } else { ?>
                                                    <button name="Status_Verifikasi_Kartu_NISN_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_Kartu_NISN_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } ?>


                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-3"> &nbsp; </div>
                                            <div class="col-lg-9">
                                                <input type="file" accept=".pdf, .jpg, .jpeg, .png" name="Kartu_NISN" class="form-control">
                                                <i>click "Choose File" untuk mengganti file</i>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">SKL Ijazah </label>
                                            <div class="col-lg-6" style="display:flex;">:&nbsp;
                                                <?php if (isset($_GET['edit'])) {
                                                    if ($edit['SKL_Ijazah'] == "") { ?>
                                                        <font class="text-danger"> Tidak ada File! </font>
                                                    <?php } else { ?>
                                                        <script>
                                                            function konfirmasi_hapus_file_skl_ijazah() {
                                                                var txt;
                                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus File Ini ?");
                                                                if (r == true) {
                                                                    document.getElementById("kotak_div_tombol_file_skl_ijazah").style.display = "none";
                                                                    document.getElementById("hapus_file_skl_ijazah").value = "Iya";
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }
                                                            }
                                                        </script>
                                                        <div id="kotak_div_tombol_file_skl_ijazah">
                                                            <b>
                                                            <a href="<?php echo $folder_penyimpanan_file_berkas_skl_ijazah ?><?php echo $edit['SKL_Ijazah'] ?>" target="_blank">
                                                                    Lihat / Download File
                                                                </a>
                                                            </b>
                                                            
                                                            <b onclick="return konfirmasi_hapus_file_skl_ijazah()" style="color:red;<?php if ($edit['Status_Verifikasi_SKL_Ijazah'] == "Sudah Diverifikasi") { echo "display:none"; } ?>">
                                                                | Hapus File
                                                            </b>
                                                            <input type="text" id="hapus_file_skl_ijazah" name="hapus_file_skl_ijazah" style="display:none">
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($edit['Status_Verifikasi_SKL_Ijazah'] == "Sudah Diverifikasi") {
                                                    ?>
                                                        &nbsp; &nbsp; <span class="badge badge-success" style="margin-left:0px !important;"> Sudah Diverifikasi </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_SKL_Ijazah'] == "Verifikasi Ditolak") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Verifikasi Ditolak </span>
                                                    <?php } elseif ($edit['Status_Verifikasi_SKL_Ijazah'] == "Menunggu Verifikasi") { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-warning" style="margin-left:0px !important;"> Menunggu Verifikasi </span>
                                                    <?php } else { ?>
                                                        &nbsp; &nbsp; <span class="badge badge-danger" style="margin-left:0px !important;"> Belum Diverifikasi </span>
                                                    <?php } ?>
                                                <?php
                                                } ?>
                                            </div>
                                            <div class="col-lg-3 text-right">
                                                <?php
                                                if ($edit['Status_Verifikasi_SKL_Ijazah'] == "Sudah Diverifikasi") {
                                                ?>
                                                    <button name="Status_Verifikasi_SKL_Ijazah_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_SKL_Ijazah'] == "Verifikasi Ditolak") { ?>
                                                    <button name="Status_Verifikasi_SKL_Ijazah_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_SKL_Ijazah_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } elseif ($edit['Status_Verifikasi_SKL_Ijazah'] == "Menunggu Verifikasi") { ?>
                                                    <button name="Status_Verifikasi_SKL_Ijazah_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_SKL_Ijazah_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } else { ?>
                                                    <button name="Status_Verifikasi_SKL_Ijazah_Terima" type="submit" class="btn btn-sm btn-outline-success"><i class="fas fa-check"></i> &nbsp; Terima </button> &nbsp;
                                                    <button name="Status_Verifikasi_SKL_Ijazah_Tolak" type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i> &nbsp; Tolak</button> &nbsp;
                                                <?php } ?>


                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-3"> &nbsp; </div>
                                            <div class="col-lg-9">
                                                <input type="file" accept=".pdf, .jpg, .jpeg, .png" name="SKL_Ijazah" class="form-control">
                                                <i>click "Choose File" untuk mengganti file</i>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Status Verifikasi Berkas </label>
                                            <div class="col-lg-3">
                                                <select required class="form-control" name="Status_Verifikasi_Berkas">
                                                    <?php
                                                    $array_option = ["", "Sudah Diverifikasi", "Verifikasi Ditolak", "Belum Diverifikasi"];
                                                    $array_option_terpilih = [];
                                                    if (isset($_GET['edit'])) {
                                                        if ($edit['Status_Verifikasi_Berkas'] <> "") {
                                                            $array_option_terpilih[] = $edit['Status_Verifikasi_Berkas'];
                                                        } else {
                                                            $array_option_terpilih[] = "";
                                                        }
                                                    }
                                                    foreach ($array_option as $option) {
                                                    ?>
                                                        <option <?php
                                                                if (isset($_GET['edit'])) {
                                                                    foreach ($array_option_terpilih as $option_terpilih) {
                                                                        if ($option_terpilih == $option) {
                                                                            echo "selected";
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                ?> value="<?php echo $option ?>">
                                                            <?php echo $option ?>
                                                        </option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="form-group row">
                                    <div class="col-lg-12" style="text-align: center;">
                                        <?php if (isset($_GET["tambah"])) { ?>
                                            <input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
                                        <?php } elseif (isset($_GET["edit"])) { ?>
                                            <input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE">
                                        <?php } ?>
                                        <input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'" class="btn btn-danger" value="BATAL">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

        <?php
        //FORM LIST DATA
        if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">List Verifikasi Berkas
                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
                                    echo "(" . $_GET['filter_status'] . ")";
                                } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="<?php echo $kehalaman ?>&tambah" class="btn btn-sm btn-primary" style="display: none;"><i class="fas fa-plus-circle"></i> &nbsp; Tambah Baru</a>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12 d-none" style="text-align: right;">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (<?php echo $hitung_Aktif ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (<?php echo $hitung_Terarsip ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (<?php echo $hitung_Terhapus ?>)
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table id="basic-datatables" class="table table-borderless">
                                    <thead class="bg-light">
                                        <tr>
                                            <th style="width:5% !important;">No</th>
                                            <th style="width:5% !important;">No. Pendaftaran</th>
                                            <th style="width:15% !important;">NISN</th>
                                            <th style="width:15% !important;">Nama Lengkap</th>
                                            <th style="width:5% !important;">Akta Kelahiran</th>
                                            <th style="width:5% !important;">Kartu Keluarga</th>
                                            <th style="width:5% !important;">Raport (1-4)</th>
                                            <th style="width:5% !important;">Kartu NISN</th>
                                            <th style="width:5% !important;">SKL/Ijazah</th>
                                            <th style="width:5% !important;">Status Verifikasi</th>
                                            <th style="width:5% !important;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ((isset($list_datatable_master))) {
                                            foreach ($list_datatable_master as $data) {
                                                $nomor++; ?>
                                                <?php
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'])) {
                                                        $data['Nomor_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'];
                                                    } else {
                                                        $data['Nomor_Pendaftaran'] = "";
                                                    }
                                                } else {
                                                    $data['Nomor_Pendaftaran'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'])) {
                                                        $data['NISN'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'];
                                                    } else {
                                                        $data['NISN'] = "";
                                                    }
                                                } else {
                                                    $data['NISN'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'])) {
                                                        $data['NIK'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'];
                                                    } else {
                                                        $data['NIK'] = "";
                                                    }
                                                } else {
                                                    $data['NIK'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'])) {
                                                        $data['NIS'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'];
                                                    } else {
                                                        $data['NIS'] = "";
                                                    }
                                                } else {
                                                    $data['NIS'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'])) {
                                                        $data['Nama_Lengkap'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'];
                                                    } else {
                                                        $data['Nama_Lengkap'] = "";
                                                    }
                                                } else {
                                                    $data['Nama_Lengkap'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Username
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Username'])) {
                                                        $data['Username'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Username'];
                                                    } else {
                                                        $data['Username'] = "";
                                                    }
                                                } else {
                                                    $data['Username'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Username

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Pendaftaran
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Pendaftaran'])) {
                                                        $data['Status_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Pendaftaran'];
                                                    } else {
                                                        $data['Status_Pendaftaran'] = "";
                                                    }
                                                } else {
                                                    $data['Status_Pendaftaran'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Pendaftaran
                                                ?>
                                                <tr>
                                                    <td><?php echo $nomor ?></td>
                                                    <td><a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Verifikasi_Berkas"], $_GET['menu']); ?>"><?php echo $data['Nomor_Pendaftaran'] ?></a>
                                                    </td>
                                                    <td><?php echo $data['NISN'] ?></td>
                                                    <td><?php echo $data['Nama_Lengkap'] ?></td>
                                                    <td>
                                                        <?php if ($data['Akta_Kelahiran'] <> "") { ?>
                                                            <a class="" href="<?php echo $folder_penyimpanan_file_berkas_akte_kelahiran ?><?php echo $data['Akta_Kelahiran'] ?>" target="_blank">
                                                                Lihat File
                                                            </a>
                                                        <?php } else { ?>
                                                            <i class="fas fa-times-circle text-danger"></i>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($data['Kartu_Keluarga'] <> "") { ?>
                                                            <a class="" href="<?php echo $folder_penyimpanan_file_berkas_kartu_keluarga ?><?php echo $data['Kartu_Keluarga'] ?>" target="_blank">
                                                                Lihat File
                                                            </a>
                                                        <?php } else { ?>
                                                            <i class="fas fa-times-circle text-danger"></i>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($data['Rapor'] <> "") { ?>
                                                            <a class="" href="<?php echo $folder_penyimpanan_file_berkas_rapor ?><?php echo $data['Rapor'] ?>" target="_blank">
                                                                Lihat File
                                                            </a>
                                                        <?php } else { ?>
                                                            <i class="fas fa-times-circle text-danger"></i>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($data['Kartu_NISN'] <> "") { ?>
                                                            <a class="" href="<?php echo $folder_penyimpanan_file_berkas_kartu_nisn ?><?php echo $data['Kartu_NISN'] ?>" target="_blank">
                                                                Lihat File
                                                            </a>
                                                        <?php } else { ?>
                                                            <i class="fas fa-times-circle text-danger"></i>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($data['SKL_Ijazah'] <> "") { ?>
                                                            <a href="<?php echo $folder_penyimpanan_file_berkas_skl_ijazah ?><?php echo $data['SKL_Ijazah'] ?>" target="_blank">
                                                                Lihat File
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($data['Status_Verifikasi_Berkas'] == "Sudah Diverifikasi") { ?>
                                                            <span class="badge badge-success"><?php echo $data['Status_Verifikasi_Berkas'] ?></span>
                                                        <?php } else if ($data['Status_Verifikasi_Berkas'] == "Menunggu Verifikasi") { ?>
                                                            <span class="badge badge-warning"><?php echo $data['Status_Verifikasi_Berkas'] ?></span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger"><?php echo $data['Status_Verifikasi_Berkas'] ?></span>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <?php if ($data['Status_Verifikasi_Berkas'] == "Sudah Diverifikasi") { ?>
                                                                <a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Verifikasi_Berkas"], $_GET['menu']); ?>','Belum Diverifikasi')"><button class="btn btn-xs btn-outline-danger"><i class="fas fa-undo"></i></button></a>
                                                            <?php } else { ?>
                                                                &nbsp;
                                                                <a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Verifikasi_Berkas"], $_GET['menu']); ?>','Sudah Diverifikasi')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-check"></i></button></a>
                                                            <?php } ?> &nbsp;

                                                            <a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Verifikasi_Berkas"], $_GET['menu']); ?>"><button class="btn btn-xs btn-outline-warning"><i class="fas fa-edit"></i></button></a>

                                                            <div class="d-none">
                                                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] == "Terhapus")) { ?>
                                                                    <a href="#" onclick="datatable_konfirmasi_restore_data_dari_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar_Verifikasi_Berkas"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-redo"></i></button></a>
                                                                    &nbsp;
                                                                    <a href="#" onclick="datatable_konfirmasi_hapus_data_permanen('<?php echo $a_hash->encode($data["Id_Pendaftar_Verifikasi_Berkas"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
                                                                <?php } else { ?>
                                                                    &nbsp;
                                                                    <a href="#" onclick="datatable_konfirmasi_hapus_data_ke_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar_Verifikasi_Berkas"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    function datatable_konfirmasi_update_status_verifikasi(id_primary, status_verifikasi) {
                                        var txt;
                                        if (status_verifikasi == "Sudah Diverifikasi") {
                                            var r = confirm("Apakah Anda Yakin Ingin Mengubah Status Verifikasi Menjadi 'Sudah Diverifikasi' Pada Data Ini ?");
                                        } else {
                                            var r = confirm("Apakah Anda Yakin Ingin Mengubah Status Verifikasi Menjadi 'Belum Diverifikasi' Pada Data Ini ?");
                                        }
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&update_status_verifikasi=' + status_verifikasi + '&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_permanen(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
                                        } else {

                                        }
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>