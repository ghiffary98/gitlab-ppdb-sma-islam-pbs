<?php
include "models/global/data_pendaftar_pembelian_formulir/model_data_pendaftar_pembelian_formulir.php";
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/pengumuman_pendaftar/model_pengumuman_pendaftar.php";
include "controllers/global/pengumuman_pendaftar/controller_pengumuman_pendaftar.php";
include "controllers/admin/pendaftaran/verifikasi/controller_verifikasi_pembelian_formulir.php";
?>
<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Verifikasi Pembelian Formulir</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="?menu=home">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo "?menu=" . $_GET['menu'] ?>">Verifikasi Pembelian Formulir</a>
				</li>

				<?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<?php if (isset($_GET['tambah'])) { ?>
							<a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
						<?php } else { ?>
							<a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
						<?php } ?>
					</li>
				<?php } ?>

			</ul>
		</div>
		<?php
		//FORM TAMBAH/EDIT DATA
		if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
		?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<?php if (isset($_GET['tambah'])) { ?>
										<div class="card-title">Tambah Data</div>
									<?php } else { ?>
										<div class="card-title">Edit Data</div>
									<?php } ?>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
									<?php if (isset($_GET["edit"])) { ?>
										<script type="text/javascript">
											function konfirmasi_hapus_data_permanen() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_hapus_data_ke_tong_sampah() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_arsip_data() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_restore_data_dari_arsip() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_restore_data_dari_tong_sampah() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}
										</script>
										<ul class="list-inline d-none">
											<li class="list-inline-item">
												<?php if ($edit['Status'] == "Aktif") { ?>
													<a href="#" onclick="konfirmasi_arsip_data()"><i class="fa fa-archive fa-md">
														</i> ARSIPKAN </a>
												<?php } elseif ($edit['Status'] == "Terarsip") { ?>
													<a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i class="fa fa-archive fa-md"></i> AKTIFKAN </a>
												<?php } elseif ($edit['Status'] == "Terhapus") { ?>
													<a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i class="fa fa-archive fa-md"></i> RESTORE </a>
												<?php } ?>

											</li>
											<li class="list-inline-item"> | </li>
											<li class="list-inline-item">
												<?php if ($edit['Status'] == "Terhapus") { ?>
													<a href="#" onclick="konfirmasi_hapus_data_permanen()"><i class="fa fa-trash fa-md"></i> HAPUS </a>
												<?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
													<a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i class="fa fa-trash fa-md"></i> HAPUS </a>
												<?php } ?>
											</li>
										</ul>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="card-body">
							<form method="POST" enctype="multipart/form-data">
								<div class="row">
									<div class="col-lg-6">
										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">PPDB Tahun Ajaran</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_ppdb['Tahun_Ajaran'];
													} ?>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">Nomor Pendaftaran</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_pendaftar['Nomor_Pendaftaran'];
													} ?>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">NIS</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_pendaftar['NIS'];
													} ?>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">NIK</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_pendaftar['NIK'];
													} ?>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">NISN</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_pendaftar['NISN'];
													} ?>
												</div>
											</div>
										</div>

									</div>
									<div class="col-lg-6">
										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">Username</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_pendaftar['Username'];
													} ?>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">Nomor Handphone</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_pendaftar['Nomor_Handphone'];
													} ?>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">Nama Lengkap</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_pendaftar['Nama_Lengkap'];
													} ?>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">Email</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_pendaftar['Email'];
													} ?>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group row">
												<label class="col-lg-3 control-label">Status Pendaftaran</label>
												<div class="col-lg-9">:
													<?php if (isset($_GET['edit'])) {
														echo $edit_data_pendaftar['Status_Pendaftaran'];
													} ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group row">
											<label class="col-lg-3 control-label">Bukti Pembayaran</label>
											<div class="col-lg-9">
												<?php if (isset($_GET['edit'])) {
													if ($edit['Bukti_Pembayaran_Pembelian_Formulir'] == "") { ?>
														Tidak Ada File
													<?php } else { ?>
														<script>
															function konfirmasi_hapus_file_bukti_pembayaran() {
																var txt;
																var r = confirm("Apakah Anda Yakin Ingin Menghapus File Ini ?");
																if (r == true) {
																	document.getElementById("kotak_div_tombol_file_bukti_pembayaran").style.display = "none";
																	document.getElementById("hapus_file_bukti_pembayaran").value = "Iya";
																	return true;
																} else {
																	return false;
																}
															}
														</script>
														<div id="kotak_div_tombol_file_bukti_pembayaran">
															<b>
																<a href="<?php echo $folder_penyimpanan_file_bukti_pembelian_formulir ?><?php echo $edit['Bukti_Pembayaran_Pembelian_Formulir'] ?>" target="_blank">
																	Lihat File
																</a>
															</b>
															|
															<b onclick="return konfirmasi_hapus_file_bukti_pembayaran()" style="color:red">
																Hapus File
															</b>
															<input type="text" id="hapus_file_bukti_pembayaran" name="hapus_file_bukti_pembayaran" style="display:none">
														</div>
												<?php
													}
												} ?>
												<input type="file" name="Bukti_Pembayaran_Pembelian_Formulir" class="form-control" accept=".pdf, image/*">
												<i>Click "Choose File" untuk mengganti file</i>
											</div>
										</div>
									</div>

									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-lg-3 control-label">Status Verifikasi</label>
											<div class="col-lg-4 d-flex">
												<select class="form-control" name="Status_Verifikasi_Pembelian_Formulir">
													<?php
													$array_option = ["", "Sudah Diverifikasi", "Verifikasi Ditolak", "Belum Diverifikasi"];
													$array_option_terpilih = [];
													if (isset($_GET['edit'])) {
														if ($edit['Status_Verifikasi_Pembelian_Formulir'] <> "") {
															$array_option_terpilih[] = $edit['Status_Verifikasi_Pembelian_Formulir'];
														} else {
															$array_option_terpilih[] = "";
														}
													}
													foreach ($array_option as $option) {
													?>
														<option <?php
																if (isset($_GET['edit'])) {
																	foreach ($array_option_terpilih as $option_terpilih) {
																		if ($option_terpilih == $option) {
																			echo "selected";
																			break;
																		}
																	}
																}
																?> value="<?php echo $option ?>">
															<?php echo $option ?>
														</option>
													<?php
													}
													?>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div style="text-align: center;">
									<hr>
									<?php if (isset($_GET["tambah"])) { ?>
										<input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
									<?php } elseif (isset($_GET["edit"])) { ?>
										<input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE">
									<?php } ?>
									<input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'" class="btn btn-danger" value="BATAL">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>

		<?php
		//FORM LIST DATA
		if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
		?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="card-title">List Verifikasi Pembelian Formulir
								<?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
									echo "(" . $_GET['filter_status'] . ")";
								} ?>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="d-none">
										<a href="<?php echo $kehalaman ?>&tambah" class="btn btn-xs btn-primary"><i class="fas fa-plus-circle"></i> &nbsp; Tambah Baru</a>
									</div>
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12 d-none" style="text-align: right;">
									<ul class="list-inline">
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (
												<?php echo $hitung_Aktif ?>)
											</a></li>
										<li class="list-inline-item"> | </li>
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (
												<?php echo $hitung_Terarsip ?>)
											</a></li>
										<li class="list-inline-item"> | </li>
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (
												<?php echo $hitung_Terhapus ?>)
											</a></li>
									</ul>
								</div>
							</div>
							<br>
							<div class="table-responsive">
								<table id="basic-datatables" class="table table-borderless" style="width:100%">
									<thead class="bg-light">
										<tr>
											<th>No</th>
											<th>Nomor Pendaftaran</th>
											<th>NISN</th>
											<th>Nama Lengkap</th>
											<th>Invoice (Xendit)</th>
											<th>Bukti Pembayaran</th>
											<th>Status Verifikasi</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ((isset($list_datatable_master))) {
											foreach ($list_datatable_master as $data) {
												$nomor++; ?>
												<?php
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'])) {
														$data['Nomor_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'];
													} else {
														$data['Nomor_Pendaftaran'] = "";
													}
												} else {
													$data['Nomor_Pendaftaran'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'])) {
														$data['NIS'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'];
													} else {
														$data['NIS'] = "";
													}
												} else {
													$data['NIS'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'])) {
														$data['NISN'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'];
													} else {
														$data['NISN'] = "";
													}
												} else {
													$data['NISN'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'])) {
														$data['NIK'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'];
													} else {
														$data['NIK'] = "";
													}
												} else {
													$data['NIK'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'])) {
														$data['Nama_Lengkap'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'];
													} else {
														$data['Nama_Lengkap'] = "";
													}
												} else {
													$data['Nama_Lengkap'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Username
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Username'])) {
														$data['Username'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Username'];
													} else {
														$data['Username'] = "";
													}
												} else {
													$data['Username'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Username

												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Pendaftaran
												if (isset($array_result_relasi_data_pendaftar)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Pendaftaran'])) {
														$data['Status_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Pendaftaran'];
													} else {
														$data['Status_Pendaftaran'] = "";
													}
												} else {
													$data['Status_Pendaftaran'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Pendaftaran
												?>
												<tr>
													<td>
														<?php echo $nomor ?>
													</td>
													<td><a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Pembelian_Formulir"], $_GET['menu']); ?>">
															<?php echo $data['Nomor_Pendaftaran'] ?>
														</a></td>
													<td><?php echo $data['NISN'] ?></td>
													<td><?php echo $data['Nama_Lengkap'] ?></td>
													<td>
														<?php if ($data['JSON_Response_Pembayaran_Xendit'] <> "") { ?>
															<?php
															$data_pembayaran_xendit = json_decode($data['JSON_Response_Pembayaran_Xendit'], TRUE);
															if ((isset($data_pembayaran_xendit['invoice_url'])) and ($data_pembayaran_xendit['invoice_url'] <> "")) {
															?>
																<a href="<?php echo $data_pembayaran_xendit['invoice_url'] ?>" target="_blank">
																	Lihat Invoice
																</a>
															<?php } ?>
														<?php } ?>
													</td>
													<td>
														<?php if ($data['Bukti_Pembayaran_Pembelian_Formulir'] <> "") { ?>
															<a href="<?php echo $folder_penyimpanan_file_bukti_pembayaran_pembelian_formulir ?><?php echo $data['Bukti_Pembayaran_Pembelian_Formulir'] ?>" target="_blank">
																Lihat File
															</a>
														<?php } ?>
													</td>
													<td>
														<?php if ($data['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi") {
															$badgeColor = "badge badge-success";
														} else if ($data['Status_Verifikasi_Pembelian_Formulir'] == "Menunggu Verifikasi") {
															$badgeColor = "badge badge-warning";
														} else {
															$badgeColor = "badge badge-danger";
														} ?>

														<span class="<?php echo $badgeColor; ?>">
															<?php echo $data['Status_Verifikasi_Pembelian_Formulir'] ?>
														</span>
													</td>
													<td>
														<div class="d-flex">

															<?php if ($data['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi") { ?>
																&nbsp;
																<a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Pembelian_Formulir"], $_GET['menu']); ?>','Menunggu Verifikasi')"><button class="btn btn-xs btn-outline-danger" alt="Undo verified"><i class="fas fa-undo"></i></button></a>
															<?php } else { ?>
																&nbsp;
																<a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Pembelian_Formulir"], $_GET['menu']); ?>','Sudah Diverifikasi')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-check"></i></button></a>
																&nbsp;
																<a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Pembelian_Formulir"], $_GET['menu']); ?>','Verifikasi Ditolak')"><button class="btn btn-xs btn-outline-danger"><i class="fas fa-times"></i></button></a>
															<?php } ?>
															&nbsp;
															<a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Pembelian_Formulir"], $_GET['menu']); ?>"><button class="btn btn-xs btn-outline-warning"><i class="fas fa-edit"></i></button></a>

															<div class="d-none">

																<?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] == "Terhapus")) { ?>
																	&nbsp;
																	<a href="#" onclick="datatable_konfirmasi_restore_data_dari_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar_Pembelian_Formulir"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-redo"></i></button></a>
																	&nbsp;
																	<a href="#" onclick="datatable_konfirmasi_hapus_data_permanen('<?php echo $a_hash->encode($data["Id_Pendaftar_Pembelian_Formulir"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
																<?php } else { ?> &nbsp;
																	<a href="#" onclick="datatable_konfirmasi_hapus_data_ke_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar_Pembelian_Formulir"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
																<?php } ?>
															</div>

														</div>

													</td>
												</tr>
											<?php } ?>
										<?php } ?>
									</tbody>
								</table>
								<script type="text/javascript">
									function datatable_konfirmasi_update_status_verifikasi(id_primary, status_verifikasi) {
										var txt;
										if (status_verifikasi == "Sudah Diverifikasi") {
											var r = confirm("Apakah Anda yakin ingin mengubah status data ini menjadi 'Sudah Diverifikasi' ?");
										} else if (status_verifikasi == "Verifikasi Ditolak") {
											var r = confirm("Apakah Anda yakin ingin mengembalikan status data inimMenjadi 'Belum Diverifikasi' ?");
										} else {
											var r = confirm("Apakah Anda yakin ingin mengembalikan status data inimMenjadi 'Menunggu Verifikasi' ?");
										}
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&update_status_verifikasi=' + status_verifikasi + '&id=' + id_primary
										}
									}

									function datatable_konfirmasi_hapus_data_permanen(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
										}
									}

									function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
										}
									}

									function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
										}
									}
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>