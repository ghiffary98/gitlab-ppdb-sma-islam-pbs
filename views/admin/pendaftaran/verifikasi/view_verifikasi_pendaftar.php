<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/pengaturan_pekerjaan_orang_tua/model_pengaturan_pekerjaan_orang_tua.php";
include "models/global/pengaturan_penghasilan_orang_tua/model_pengaturan_penghasilan_orang_tua.php";
include "models/global/pengaturan_kendaraan_yang_dipakai_kesekolah/model_pengaturan_kendaraan_yang_dipakai_kesekolah.php";
include "models/global/pengaturan_asal_sekolah/model_pengaturan_asal_sekolah.php";
include "models/global/pengumuman_pendaftar/model_pengumuman_pendaftar.php";
include "controllers/global/pengumuman_pendaftar/controller_pengumuman_pendaftar.php";
include "controllers/admin/pendaftaran/verifikasi/controller_verifikasi_pendaftar.php";

?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Pendaftar</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="?menu=home">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="<?php echo "?menu=" . $_GET['menu'] ?>">Pendaftar</a>
                </li>

                <?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <?php if (isset($_GET['tambah'])) { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
                        <?php } else { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
                        <?php } ?>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php
        //FORM TAMBAH/EDIT DATA
        if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <?php if (isset($_GET['tambah'])) { ?>
                                        <div class="card-title">Tambah Data</div>
                                    <?php } else { ?>
                                        <div class="card-title">Edit Data</div>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
                                    <?php if (isset($_GET["edit"])) { ?>
                                        <script type="text/javascript">
                                            function konfirmasi_hapus_data_permanen() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_hapus_data_ke_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_arsip_data() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_arsip() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }
                                        </script>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Aktif") { ?>
                                                    <a href="#" onclick="konfirmasi_arsip_data()"><i class="fa fa-archive fa-md"></i> ARSIPKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terarsip") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i class="fa fa-archive fa-md"></i> AKTIFKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i class="fa fa-archive fa-md"></i> RESTORE </a>
                                                <?php } ?>

                                            </li>
                                            <li class="list-inline-item"> | </li>
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_permanen()"><i class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Identitas Login</b></h2>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Nomor Pendaftaran </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_Pendaftaran" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_Pendaftaran'] <> "") ){echo $edit['Nomor_Pendaftaran'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Email </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Email" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Email'] <> "") ){echo $edit['Email'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Nomor Hanpdhone </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_Handphone" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_Handphone'] <> "") ){echo $edit['Nomor_Handphone'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Password </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> 
                                                                <div class="input-group">
                                                                <input <?php if((isset($edit)) AND (($edit['Password_Pendaftar'] == "") OR ($edit['Password_Pendaftar'] == null)) ){echo "placeholder='Harap Update Password Pendaftar Ini, Jika Ingin Melihat Password Pendaftar'";}?> name="Password" type="password" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Password_Pendaftar'] <> "") ){echo $a_hash->decode($edit['Password_Pendaftar']);}?>"> 
                                                                    <div class="input-group-append">
                                                                        <button type="button" id="togglePassword" class="btn btn-secondary">
                                                                            <i class="fa fa-eye"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <script>
                                                                document.addEventListener("DOMContentLoaded", function() {
                                                                    const passwordInput = document.querySelector('input[name="Password"]');
                                                                    const togglePasswordButton = document.getElementById("togglePassword");

                                                                    togglePasswordButton.addEventListener("click", function() {
                                                                        if (passwordInput.type === "password") {
                                                                            passwordInput.type = "text";
                                                                            togglePasswordButton.innerHTML = '<i class="fa fa-eye-slash"></i>';
                                                                        } else {
                                                                            passwordInput.type = "password";
                                                                            togglePasswordButton.innerHTML = '<i class="fa fa-eye"></i>';
                                                                        }
                                                                    });
                                                                });
                                                            </script>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Identitas Siswa</b></h2>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Nama Siswa </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nama_Lengkap" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nama_Lengkap'] <> "") ){echo $edit['Nama_Lengkap'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kelas </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Kelas" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Kelas'] <> "") ){echo $edit['Kelas'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jenis Kelamin </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Jenis_Kelamin" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Jenis_Kelamin'] <> "") ){echo $edit['Jenis_Kelamin'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tempat Lahir </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Tempat_Lahir" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Tempat_Lahir'] <> "") ){echo $edit['Tempat_Lahir'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Tanggal Lahir </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Tanggal_Lahir" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Tanggal_Lahir'] <> "") ){echo $edit['Tanggal_Lahir'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Agama </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Agama" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Agama'] <> "") ){echo $edit['Agama'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NIK </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="NIK" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['NIK'] <> "") ){echo $edit['NIK'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Kontak</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No. HP </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_Handphone" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_Handphone'] <> "") ){echo $edit['Nomor_Handphone'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> No. Telp </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_Telepon" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_Telepon'] <> "") ){echo $edit['Nomor_Telepon'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Email </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Email" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Email'] <> "") ){echo $edit['Email'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Sekolah</b></h2>
                                                        </th>
                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%"> NIS (Nomor Induk Siswa) </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="NIS" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['NIS'] <> "") ){echo $edit['NIS'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> NIPD </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="NIPD" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['NIPD'] <> "") ){echo $edit['NIPD'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> NISN (Nomor Induk Siswa Nasional) </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="NISN" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['NISN'] <> "") ){echo $edit['NISN'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Asal Sekolah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select disabled class="form-control" name="Asal_Sekolah">
                                                                    <option value=""></option>
                                                                        <?php
                                                                        $list_data_pengaturan_asal_sekolah_ini = $list_data_pengaturan_asal_sekolah;
                                                                        if(isset($_GET['edit'])){
                                                                            if($edit['Asal_Sekolah'] <> ""){
                                                                                $edit_data_sebelum_Asal_Sekolah['Id_Pengaturan_Asal_Sekolah'] = '0';
                                                                                $edit_data_sebelum_Asal_Sekolah['Nama_Sekolah'] = $edit['Asal_Sekolah'];
                                                                                array_unshift($list_data_pengaturan_asal_sekolah_ini, $edit_data_sebelum_Asal_Sekolah);
                                                                                $list_data_pengaturan_asal_sekolah_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_asal_sekolah_ini,"Nama_Sekolah");
                                                                            }
                                                                        }
                                                                        if ((isset($list_data_pengaturan_asal_sekolah_ini))) {
                                                                        foreach ($list_data_pengaturan_asal_sekolah_ini as $data) {
                                                                        ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                        if ($_POST['Asal_Sekolah'] == $data['Nama_Sekolah']) {
                                                                                            echo "selected";
                                                                                        };
                                                                                    } elseif (isset($_GET['edit'])) {
                                                                                        if ($edit['Asal_Sekolah'] == $data['Nama_Sekolah']) {
                                                                                            echo "selected";
                                                                                        };
                                                                                    } ?> value="<?php echo $data['Nama_Sekolah'] ?>"><?php echo $data['Nama_Sekolah'] ?></option>
                                                                        <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Alamat Sekolah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Alamat_Sekolah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Alamat_Sekolah'] <> "") ){echo $edit['Alamat_Sekolah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No Peserta Ujian Sekolah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_Peserta_Ujian_Nasional" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_Peserta_Ujian_Nasional'] <> "") ){echo $edit['Nomor_Peserta_Ujian_Nasional'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No Seri Ijazah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_Seri_Ijazah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_Seri_Ijazah'] <> "") ){echo $edit['Nomor_Seri_Ijazah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Keluarga</b></h2>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> No KK </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_KK" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_KK'] <> "") ){echo $edit['Nomor_KK'];}?>"> </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Anak Ke-berapa </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Anak_Ke" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Anak_Ke'] <> "") ){echo $edit['Anak_Ke'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jumlah Saudara Kandung </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Jumlah_Saudara" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Jumlah_Saudara'] <> "") ){echo $edit['Jumlah_Saudara'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penerima KPS </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Penerima_KPS" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Penerima_KPS'] <> "") ){echo $edit['Penerima_KPS'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No.KPS (Jika Ya) </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_KPS" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_KPS'] <> "") ){echo $edit['Nomor_KPS'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penerima KIP </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Penerima_KIP" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Penerima_KIP'] <> "") ){echo $edit['Penerima_KIP'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nomor KIP </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_KIP" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_KIP'] <> "") ){echo $edit['Nomor_KIP'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penerima KJP DKI </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Penerima_KJP_DKI" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Penerima_KJP_DKI'] <> "") ){echo $edit['Penerima_KJP_DKI'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nomor KJP DKI </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_KJP_DKI" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_KJP_DKI'] <> "") ){echo $edit['Nomor_KJP_DKI'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nomor KKS </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_KKS" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_KKS'] <> "") ){echo $edit['Nomor_KKS'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No Registrasi Akta Lahir </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nomor_Registrasi_Akta_Lahir" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nomor_Registrasi_Akta_Lahir'] <> "") ){echo $edit['Nomor_Registrasi_Akta_Lahir'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Status Dalam Keluarga </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Status_Dalam_Keluarga" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Status_Dalam_Keluarga'] <> "") ){echo $edit['Status_Dalam_Keluarga'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Status Perwalian </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Status_Perwalian" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Status_Perwalian'] <> "") ){echo $edit['Status_Perwalian'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Pribadi</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Golongan Darah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Golongan_Darah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Golongan_Darah'] <> "") ){echo $edit['Golongan_Darah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tinggi Badan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Tinggi_Badan" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Tinggi_Badan'] <> "") ){echo $edit['Tinggi_Badan'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Berat Badan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Berat_Badan" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Berat_Badan'] <> "") ){echo $edit['Berat_Badan'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Lingkar Kepala </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Lingkar_Kepala" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Lingkar_Kepala'] <> "") ){echo $edit['Lingkar_Kepala'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kebutuhan Khusus </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Kebutuhan_Khusus" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Kebutuhan_Khusus'] <> "") ){echo $edit['Kebutuhan_Khusus'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Ukuran Seragam </td>
                                                        <td style="width:70%">
                                                        <div class="form-group"> <input readonly name="Ukuran_Seragam" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Ukuran_Seragam'] <> "") ){echo $edit['Ukuran_Seragam'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Orang Tua</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h4><b>Data Ayah</b></h4>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nama </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nama_Ayah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nama_Ayah'] <> "") ){echo $edit['Nama_Ayah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tempat Lahir Ayah</td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Tempat_Lahir_Ayah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Tempat_Lahir_Ayah'] <> "") ){echo $edit['Tempat_Lahir_Ayah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tanggal Lahir Ayah</td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Tanggal_Lahir_Ayah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Tanggal_Lahir_Ayah'] <> "") ){echo $edit['Tanggal_Lahir_Ayah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jenjang Pendidikan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Jenjang_Pendidikan_Ayah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Jenjang_Pendidikan_Ayah'] <> "") ){echo $edit['Jenjang_Pendidikan_Ayah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Pekerjaan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> 
                                                                <select disabled class="form-control" name="Pekerjaan_Ayah">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                    if(isset($_GET['edit'])){
                                                                        if($edit['Pekerjaan_Ayah'] <> ""){
                                                                            $edit_data_sebelum_Pekerjaan_Ayah['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Pekerjaan_Ayah['Pekerjaan_Orang_Tua'] = $edit['Pekerjaan_Ayah'];
                                                                            array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Ayah);
                                                                            $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini,"Pekerjaan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                    foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                    ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Pekerjaan_Ayah'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Pekerjaan_Ayah'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>"><?php echo $data['Pekerjaan_Orang_Tua'] ?></option>
                                                                    <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penghasilan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                            <select disabled class="form-control" name="Penghasilan_Ayah">
                                                                <option value=""></option>
                                                                <?php
                                                                $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                if(isset($_GET['edit'])){
                                                                    if($edit['Penghasilan_Ayah'] <> ""){
                                                                        $edit_data_sebelum_Penghasilan_Ayah['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                        $edit_data_sebelum_Penghasilan_Ayah['Penghasilan_Orang_Tua'] = $edit['Penghasilan_Ayah'];
                                                                        array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Ayah);
                                                                        $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini,"Penghasilan_Orang_Tua");
                                                                    }
                                                                }
                                                                if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                ?>
                                                                    <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Penghasilan_Ayah'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                };
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit['Penghasilan_Ayah'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                };
                                                                            } ?> value="<?php echo $data['Penghasilan_Orang_Tua'] ?>"><?php echo $data['Penghasilan_Orang_Tua'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NIK </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="NIK_Ayah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['NIK_Ayah'] <> "") ){echo $edit['NIK_Ayah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No. HP Ayah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="No_HP_Ayah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['No_HP_Ayah'] <> "") ){echo $edit['No_HP_Ayah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h4><b>Data Ibu</b></h4>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Nama </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nama_Ibu" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nama_Ibu'] <> "") ){echo $edit['Nama_Ibu'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tempat Lahir Ibu </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Tempat_Lahir_Ibu" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Tempat_Lahir_Ibu'] <> "") ){echo $edit['Tempat_Lahir_Ibu'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tanggal Lahir Ibu </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Tanggal_Lahir_Ibu" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Tanggal_Lahir_Ibu'] <> "") ){echo $edit['Tanggal_Lahir_Ibu'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jenjang Pendidikan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Jenjang_Pendidikan_Ibu" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Jenjang_Pendidikan_Ibu'] <> "") ){echo $edit['Jenjang_Pendidikan_Ibu'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Pekerjaan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select disabled class="form-control" name="Pekerjaan_Ibu">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                    if(isset($_GET['edit'])){
                                                                        if($edit['Pekerjaan_Ibu'] <> ""){
                                                                            $edit_data_sebelum_Pekerjaan_Ibu['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Pekerjaan_Ibu['Pekerjaan_Orang_Tua'] = $edit['Pekerjaan_Ibu'];
                                                                            array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Ibu);
                                                                            $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini,"Pekerjaan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                    foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                    ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Pekerjaan_Ibu'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Pekerjaan_Ibu'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>"><?php echo $data['Pekerjaan_Orang_Tua'] ?></option>
                                                                    <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penghasilan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select disabled class="form-control" name="Penghasilan_Ibu">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                    if(isset($_GET['edit'])){
                                                                        if($edit['Penghasilan_Ibu'] <> ""){
                                                                            $edit_data_sebelum_Penghasilan_Ibu['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Penghasilan_Ibu['Penghasilan_Orang_Tua'] = $edit['Penghasilan_Ibu'];
                                                                            array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Ibu);
                                                                            $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini,"Penghasilan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                    foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                    ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Penghasilan_Ibu'] == $data['Penghasilan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Penghasilan_Ibu'] == $data['Penghasilan_Orang_Tua']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Penghasilan_Orang_Tua'] ?>"><?php echo $data['Penghasilan_Orang_Tua'] ?></option>
                                                                    <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NIK </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="NIK_Ibu" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['NIK_Ibu'] <> "") ){echo $edit['NIK_Ibu'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No. HP Ibu </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="No_HP_Ibu" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['No_HP_Ibu'] <> "") ){echo $edit['No_HP_Ibu'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Data Wali</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nama </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Nama_Wali" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Nama_Wali'] <> "") ){echo $edit['Nama_Wali'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tempat Lahir Wali </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Tempat_Lahir_Wali" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Tempat_Lahir_Wali'] <> "") ){echo $edit['Tempat_Lahir_Wali'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tanggal Lahir Wali </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Tanggal_Lahir_Wali" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Tanggal_Lahir_Wali'] <> "") ){echo $edit['Tanggal_Lahir_Wali'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jenjang Pendidikan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Jenjang_Pendidikan_Wali" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Jenjang_Pendidikan_Wali'] <> "") ){echo $edit['Jenjang_Pendidikan_Wali'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Pekerjaan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Pekerjaan_Wali" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Pekerjaan_Wali'] <> "") ){echo $edit['Pekerjaan_Wali'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penghasilan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Penghasilan_Wali" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Penghasilan_Wali'] <> "") ){echo $edit['Penghasilan_Wali'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NIK </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="NIK_Wali" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['NIK_Wali'] <> "") ){echo $edit['NIK_Wali'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No. HP Wali </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="No_HP_Wali" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['No_HP_Wali'] <> "") ){echo $edit['No_HP_Wali'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Alamat</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jalan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Jalan" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Jalan'] <> "") ){echo $edit['Jalan'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kelurahan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Kelurahan" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Kelurahan'] <> "") ){echo $edit['Kelurahan'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kecamatan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Kecamatan" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Kecamatan'] <> "") ){echo $edit['Kecamatan'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kota </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Kota" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Kota'] <> "") ){echo $edit['Kota'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Provinsi </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Provinsi" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Provinsi'] <> "") ){echo $edit['Provinsi'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kode Pos </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Kode_Pos" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Kode_Pos'] <> "") ){echo $edit['Kode_Pos'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Lintang </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Titik_Lintang_Alamat" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Titik_Lintang_Alamat'] <> "") ){echo $edit['Titik_Lintang_Alamat'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Lintang </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Titik_Lintang_Alamat" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Titik_Lintang_Alamat'] <> "") ){echo $edit['Titik_Lintang_Alamat'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Bujur </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Titik_Bujur_Alamat" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Titik_Bujur_Alamat'] <> "") ){echo $edit['Titik_Bujur_Alamat'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jarak Rumah ke Sekolah (Km) </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> <input readonly name="Jarak_Rumah_Ke_Sekolah" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> style="background-color:white" value="<?php if((isset($edit)) AND ($edit['Jarak_Rumah_Ke_Sekolah'] <> "") ){echo $edit['Jarak_Rumah_Ke_Sekolah'];}?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Alat Transportasi </td>
                                                        <td style="width:70%">
                                                            <div class="form-group"> 
                                                                <select disabled class="form-control" name="Kendaraan_Yang_Dipakai_Kesekolah">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini = $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah;
                                                                    if(isset($_GET['edit'])){
                                                                        if($edit['Kendaraan_Yang_Dipakai_Kesekolah'] <> ""){
                                                                            $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah['Id_Pengaturan_Nama_Kendaraan'] = '0';
                                                                            $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah['Nama_Kendaraan'] = $edit['Kendaraan_Yang_Dipakai_Kesekolah'];
                                                                            array_unshift($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini, $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah);
                                                                            $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini,"Nama_Kendaraan");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini))) {
                                                                    foreach ($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini as $data) {
                                                                    ?>
                                                                        <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                    if ($_POST['Kendaraan_Yang_Dipakai_Kesekolah'] == $data['Nama_Kendaraan']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } elseif (isset($_GET['edit'])) {
                                                                                    if ($edit['Kendaraan_Yang_Dipakai_Kesekolah'] == $data['Nama_Kendaraan']) {
                                                                                        echo "selected";
                                                                                    };
                                                                                } ?> value="<?php echo $data['Nama_Kendaraan'] ?>"><?php echo $data['Nama_Kendaraan'] ?></option>
                                                                    <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>   
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b><hr></b></h2>
                                                        </th>
                                                    </tr>
                                                    


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div style="text-align: center;">
                                    <?php if (isset($_GET["tambah"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
                                    <?php } elseif (isset($_GET["edit"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE">
                                    <?php } ?>
                                    <input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'" class="btn btn-danger" value="BATAL">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

        <?php
        //FORM LIST DATA
        if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">List Pendaftar
                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
                                    echo "(" . $_GET['filter_status'] . ")";
                                } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="<?php echo $kehalaman ?>&tambah" class="btn btn-sm btn-primary" style="display:none"><i class="fas fa-plus-circle"></i> &nbsp; Tambah Baru</a>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (<?php echo $hitung_Aktif ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (<?php echo $hitung_Terarsip ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (<?php echo $hitung_Terhapus ?>)
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table id="basic-datatables" class="table table-borderless" style="width:100%">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Pendaftaran</th>
                                            <th>Username</th>
                                            <th>Nama Lengkap</th>
                                            <th>NIS</th>
                                            <th>NISN</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ((isset($list_datatable_master))) {
                                            foreach ($list_datatable_master as $data) {
                                                $nomor++; ?>
                                                <tr>
                                                    <td><?php echo $nomor ?></td>
                                                    <td>
                                                        <a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>">
                                                            <?php echo $data['Nomor_Pendaftaran'] ?>
                                                        </a>
                                                    </td>
                                                    <td><?php echo $data['Username'] ?></td>
                                                    <td><?php echo $data['Nama_Lengkap'] ?></td>
                                                    <td><?php echo $data['NIS'] ?></td>
                                                    <td><?php echo $data['NISN'] ?></td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>"><button class="btn btn-xs btn-outline-warning"><i class="fas fa-edit fa-lg"></i></button></a>
                                                            <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] == "Terhapus")) { ?>
                                                                <a href="#" onclick="datatable_konfirmasi_restore_data_dari_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-redo fa-lg"></i></button></a>

                                                                <a href="#" onclick="datatable_konfirmasi_hapus_data_permanen('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash fa-lg"></i></button></a>
                                                            <?php } else { ?>
                                                                <a href="#" onclick="datatable_konfirmasi_hapus_data_ke_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash fa-lg"></i></button></a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
                                        } else {

                                        }
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>