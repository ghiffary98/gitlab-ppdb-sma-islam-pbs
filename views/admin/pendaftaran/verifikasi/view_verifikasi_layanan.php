<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/data_pendaftar_program_layanan/model_data_pendaftar_program_layanan.php";
include "models/global/pengumuman_pendaftar/model_pengumuman_pendaftar.php";
include "controllers/global/pengumuman_pendaftar/controller_pengumuman_pendaftar.php";
include "controllers/admin/pendaftaran/verifikasi/controller_verifikasi_layanan.php";
?>
<style>
    .form-control[readonly] {
        background: #fdfdfd;
        opacity: 1;
    }
</style>


<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Verifikasi Program Layanan</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="?menu=home">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="<?php echo "?menu=" . $_GET['menu'] ?>">Verifikasi Program Layanan</a>
                </li>

                <?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <?php if (isset($_GET['tambah'])) { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
                        <?php } else { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
                        <?php } ?>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php
        //FORM TAMBAH/EDIT DATA
        if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <?php if (isset($_GET['tambah'])) { ?>
                                        <div class="card-title">Tambah Data</div>
                                    <?php } else { ?>
                                        <div class="card-title">Edit Data</div>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
                                    <?php if (isset($_GET["edit"])) { ?>
                                        <script type="text/javascript">
                                            function konfirmasi_hapus_data_permanen() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_hapus_data_ke_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_arsip_data() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_arsip() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }
                                        </script>
                                        <ul class="list-inline d-none">
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Aktif") { ?>
                                                    <a href="#" onclick="konfirmasi_arsip_data()"><i
                                                            class="fa fa-archive fa-md"></i> ARSIPKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terarsip") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i
                                                            class="fa fa-archive fa-md"></i> AKTIFKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i
                                                            class="fa fa-archive fa-md"></i> RESTORE </a>
                                                <?php } ?>

                                            </li>
                                            <li class="list-inline-item"> | </li>
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_permanen()"><i
                                                            class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i
                                                            class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <script type="text/javascript">
                                function cek_duplikat_data_pada_array(arr) {
                                    let set = new Set(arr);
                                    return set.size !== arr.length;
                                }

                                function cek_duplikat_layanan() {
                                    var layanan_terpilih = [];
                                    layanan_terpilih.push(document.getElementById("layanan_1").value);
                                    layanan_terpilih.push(document.getElementById("layanan_2").value);
                                    layanan_terpilih.push(document.getElementById("layanan_3").value);
                                    layanan_terpilih.push(document.getElementById("layanan_4").value);

                                    if (cek_duplikat_data_pada_array(layanan_terpilih)) {
                                        alert("Pilihan Layanan Tidak Boleh Sama Dengan Layanan Lainnya");
                                        return false;
                                    } else {
                                        return true;
                                    }
                                }

                                function rubah_list_option_layanan(program, layanan_ke) {
                                    var selectElement = document.getElementById("layanan_" + layanan_ke);

                                    selectElement.innerHTML = "";
                                    if (program == "MIPA") {
                                        var options = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                    } else {
                                        var options = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                    }
                                    for (var i = 0; i < options.length; i++) {
                                        var option = document.createElement("option");
                                        option.value = options[i];
                                        option.text = options[i];
                                        selectElement.appendChild(option);
                                    }
                                }

                                function pilih_program_mipa() {
                                    rubah_list_option_layanan("MIPA", "1");
                                    rubah_list_option_layanan("MIPA", "2");
                                    rubah_list_option_layanan("MIPA", "3");
                                    rubah_list_option_layanan("MIPA", "4");

                                    document.getElementById("kolom_layanan_4").style.display = "";
                                    document.getElementById("layanan_4").required = true;
                                }

                                function pilih_program_ips() {
                                    rubah_list_option_layanan("IPS", "1");
                                    rubah_list_option_layanan("IPS", "2");
                                    rubah_list_option_layanan("IPS", "3");
                                    rubah_list_option_layanan("IPS", "4");

                                    document.getElementById("kolom_layanan_4").style.display = "none";
                                    document.getElementById("layanan_4").required = false;
                                }
                            </script>
                            <form method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") { ?>
                                            <!-- DIHAPUS UNTUK PROGRAM -->
                                        <?php } else { ?>
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr class="bg-light">
                                                        <th colspan="3">
                                                            <h4><b>Program* (Pilih salah satu)</b></h4>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width:5%;">1. </th>
                                                        <th style="width:15%;"> <input onclick="pilih_program_mipa()"
                                                                type="radio" value="MIPA" name="Program" required <?php if ((isset($_GET['edit'])) and (($edit['Program'] == "MIPA"))) {
                                                                    echo "checked";
                                                                } ?>> MIPA </th>
                                                        <th> Program MIPA (PCE, Biologi, Teknologi, Tahfidz)</th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width:5%;">2. </th>
                                                        <th style="width:15%;"> <input onclick="pilih_program_ips()"
                                                                type="radio" value="IPS" name="Program" required <?php if ((isset($_GET['edit'])) and (($edit['Program'] == "IPS"))) {
                                                                    echo "checked";
                                                                } ?>> IPS </th>
                                                        <th> Program IPS (PCE, Ekonomi/Akuntantsi, Bahasa)</th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <?php } ?>

                                        <br>

                                        <table class="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <th class="bg-light" colspan="3">
                                                        <h4><b>Pilih Layanan</b></h4>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <th class="bg bg-danger" colspan="3">
                                                        <div class="text-white"> Pemilihan Kelas layanan awal* (Dapat
                                                            berubah setelah <i>Placement Test</i>) </div>
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <td style="width:15%"> Layanan 1 </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select required id="layanan_1" required name="Layanan_1"
                                                                class="form-control">
                                                                <?php
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") {
                                                                        $array_option = ["", "Biologi", "Teknologi/IT", "Tahfidz", "Bahasa", "Ekonomi/Akuntantsi"];
                                                                    } else {
                                                                        if ($edit['Program'] == "MIPA") {
                                                                            $array_option = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                                                        } else {
                                                                            $array_option = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                                                        }
                                                                    }
                                                                } else {
                                                                    $array_option = [""];
                                                                }
                                                                $array_option_terpilih = [];
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Layanan_1'] <> "") {
                                                                        $array_option_terpilih[] = $edit['Layanan_1'];
                                                                    } else {
                                                                        $array_option_terpilih[] = "";
                                                                    }
                                                                }
                                                                foreach ($array_option as $option) {
                                                                    ?>
                                                                    <option <?php
                                                                    if (isset($_GET['edit'])) {
                                                                        foreach ($array_option_terpilih as $option_terpilih) {
                                                                            if ($option_terpilih == $option) {
                                                                                echo "selected";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?> value="<?php echo $option ?>">
                                                                        <?php echo $option ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:15%"> Layanan 2 </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select required id="layanan_2" required name="Layanan_2"
                                                                class="form-control">
                                                                <?php
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") {
                                                                        $array_option = ["", "Biologi", "Teknologi/IT", "Tahfidz", "Bahasa", "Ekonomi/Akuntantsi"];
                                                                    } else {
                                                                        if ($edit['Program'] == "MIPA") {
                                                                            $array_option = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                                                        } else {
                                                                            $array_option = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                                                        }
                                                                    }
                                                                } else {
                                                                    $array_option = [""];
                                                                }
                                                                $array_option_terpilih = [];
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Layanan_2'] <> "") {
                                                                        $array_option_terpilih[] = $edit['Layanan_2'];
                                                                    } else {
                                                                        $array_option_terpilih[] = "";
                                                                    }
                                                                }
                                                                foreach ($array_option as $option) {
                                                                    ?>
                                                                    <option <?php
                                                                    if (isset($_GET['edit'])) {
                                                                        foreach ($array_option_terpilih as $option_terpilih) {
                                                                            if ($option_terpilih == $option) {
                                                                                echo "selected";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?> value="<?php echo $option ?>">
                                                                        <?php echo $option ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width:15%"> Layanan 3 </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select required id="layanan_3" required name="Layanan_3"
                                                                class="form-control">
                                                                <?php
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") {
                                                                        $array_option = ["", "Biologi", "Teknologi/IT", "Tahfidz", "Bahasa", "Ekonomi/Akuntantsi"];
                                                                    } else {
                                                                        if ($edit['Program'] == "MIPA") {
                                                                            $array_option = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                                                        } else {
                                                                            $array_option = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                                                        }
                                                                    }
                                                                } else {
                                                                    $array_option = [""];
                                                                }
                                                                $array_option_terpilih = [];
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Layanan_3'] <> "") {
                                                                        $array_option_terpilih[] = $edit['Layanan_3'];
                                                                    } else {
                                                                        $array_option_terpilih[] = "";
                                                                    }
                                                                }
                                                                foreach ($array_option as $option) {
                                                                    ?>
                                                                    <option <?php
                                                                    if (isset($_GET['edit'])) {
                                                                        foreach ($array_option_terpilih as $option_terpilih) {
                                                                            if ($option_terpilih == $option) {
                                                                                echo "selected";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?> value="<?php echo $option ?>">
                                                                        <?php echo $option ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr id="kolom_layanan_4" style="<?php if ((isset($_GET['edit'])) and (($edit['Program'] == "IPS"))) {
                                                    echo "display:none";
                                                } ?>">
                                                    <td style="width:15%"> Layanan 4 </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select <?php if ((isset($_GET['edit'])) and (($edit['Program'] == "IPS"))) {
                                                                echo "";
                                                            } else {
                                                                echo "required";
                                                            } ?> id="layanan_4" name="Layanan_4"
                                                                class="form-control">
                                                                <?php
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit_data_pendaftar['Waktu_Simpan_Data'] >= "2023-01-01 00:00:00") {
                                                                        $array_option = ["", "Biologi", "Teknologi/IT", "Tahfidz", "Bahasa", "Ekonomi/Akuntantsi"];
                                                                    } else {
                                                                        if ($edit['Program'] == "MIPA") {
                                                                            $array_option = ["", "PCE", "Biologi", "Teknologi", "Tahfidz"];
                                                                        } else {
                                                                            $array_option = ["", "PCE", "Ekonomi/Akuntantsi", "Bahasa"];
                                                                        }
                                                                    }
                                                                } else {
                                                                    $array_option = [""];
                                                                }
                                                                $array_option_terpilih = [];
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Layanan_4'] <> "") {
                                                                        $array_option_terpilih[] = $edit['Layanan_4'];
                                                                    } else {
                                                                        $array_option_terpilih[] = "";
                                                                    }
                                                                }
                                                                foreach ($array_option as $option) {
                                                                    ?>
                                                                    <option <?php
                                                                    if (isset($_GET['edit'])) {
                                                                        foreach ($array_option_terpilih as $option_terpilih) {
                                                                            if ($option_terpilih == $option) {
                                                                                echo "selected";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?> value="<?php echo $option ?>">
                                                                        <?php echo $option ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th colspan="3">
                                                        <font class="text-muted"><i> Note : Setiap layanan harus memilih
                                                                kelas yang berbeda </i></font>
                                                    </th>
                                                </tr>

                                            </tbody>
                                        </table>

                                        <br>

                                        <table class="table table-borderless">
                                            <tbody>
                                                <tr class="bg-light">
                                                    <th colspan="5">
                                                        <h2><b>Status Verifikasi</b></h2>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%"> Status Verifikasi </td>
                                                    <td style="width:70%" colspan="4">
                                                        <div class="form-group">
                                                            <select required class="form-control"
                                                                name="Status_Verifikasi_Program_Layanan">
                                                                <?php
                                                                $array_option = ["", "Sudah Diverifikasi", "Verifikasi Ditolak", "Belum Diverifikasi"];
                                                                $array_option_terpilih = [];
                                                                if (isset($_GET['edit'])) {
                                                                    if ($edit['Status_Verifikasi_Program_Layanan'] <> "") {
                                                                        $array_option_terpilih[] = $edit['Status_Verifikasi_Program_Layanan'];
                                                                    } else {
                                                                        $array_option_terpilih[] = "";
                                                                    }
                                                                }
                                                                foreach ($array_option as $option) {
                                                                    ?>
                                                                    <option <?php
                                                                    if (isset($_GET['edit'])) {
                                                                        foreach ($array_option_terpilih as $option_terpilih) {
                                                                            if ($option_terpilih == $option) {
                                                                                echo "selected";
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?> value="<?php echo $option ?>">
                                                                        <?php echo $option ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div style="text-align: center;">
                                    <?php if (isset($_GET["tambah"])) { ?>
                                        <input onclick="return cek_duplikat_layanan()" type="submit" class="btn btn-primary"
                                            name="submit_simpan" value="SIMPAN">
                                    <?php } elseif (isset($_GET["edit"])) { ?>
                                        <input onclick="return cek_duplikat_layanan()" type="submit" class="btn btn-primary"
                                            name="submit_update" value="UPDATE">
                                    <?php } ?>
                                    <input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'"
                                        class="btn btn-danger" value="BATAL">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>

        <?php
        //FORM LIST DATA
        if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">List Verifikasi Program Layanan
                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
                                    echo "(" . $_GET['filter_status'] . ")";
                                } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="<?php echo $kehalaman ?>&tambah" class="btn btn-sm btn-primary"
                                        style="display:none"><i class="fas fa-plus-circle"></i> &nbsp; Tambah Baru</a>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12 d-none" style="text-align: right;">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a
                                                href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (
                                                <?php echo $hitung_Aktif ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a
                                                href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (
                                                <?php echo $hitung_Terarsip ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a
                                                href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (
                                                <?php echo $hitung_Terhapus ?>)
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table id="basic-datatables" class="table table-borderless" style="width:100%">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Pendaftaran</th>
                                            <th>NISN</th>
                                            <th>Nama Lengkap</th>
                                            <th>Program</th>
                                            <th>Layanan 1</th>
                                            <th>Layanan 2</th>
                                            <th>Layanan 3</th>
                                            <th>Layanan 4</th>
                                            <th>Status Verifikasi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ((isset($list_datatable_master))) {
                                            foreach ($list_datatable_master as $data) {
                                                $nomor++; ?>
                                                <?php
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'])) {
                                                        $data['Nomor_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'];
                                                    } else {
                                                        $data['Nomor_Pendaftaran'] = "";
                                                    }
                                                } else {
                                                    $data['Nomor_Pendaftaran'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
                                    
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'])) {
                                                        $data['NIS'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'];
                                                    } else {
                                                        $data['NIS'] = "";
                                                    }
                                                } else {
                                                    $data['NIS'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS
                                    
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'])) {
                                                        $data['NISN'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'];
                                                    } else {
                                                        $data['NISN'] = "";
                                                    }
                                                } else {
                                                    $data['NISN'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN
                                    
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'])) {
                                                        $data['NIK'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'];
                                                    } else {
                                                        $data['NIK'] = "";
                                                    }
                                                } else {
                                                    $data['NIK'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK
                                    
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'])) {
                                                        $data['Nama_Lengkap'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'];
                                                    } else {
                                                        $data['Nama_Lengkap'] = "";
                                                    }
                                                } else {
                                                    $data['Nama_Lengkap'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
                                    
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Username
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Username'])) {
                                                        $data['Username'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Username'];
                                                    } else {
                                                        $data['Username'] = "";
                                                    }
                                                } else {
                                                    $data['Username'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Username
                                    
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Pendaftaran
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Pendaftaran'])) {
                                                        $data['Status_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Pendaftaran'];
                                                    } else {
                                                        $data['Status_Pendaftaran'] = "";
                                                    }
                                                } else {
                                                    $data['Status_Pendaftaran'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Pendaftaran
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $nomor ?>
                                                    </td>
                                                    <td><a
                                                            href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Program_Layanan"], $_GET['menu']); ?>">
                                                            <?php echo $data['Nomor_Pendaftaran'] ?>
                                                        </a></td>
                                                    <td>
                                                        <?php echo $data['NISN'] ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['Nama_Lengkap'] ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['Program'] ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['Layanan_1'] ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['Layanan_2'] ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['Layanan_3'] ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['Layanan_4'] ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($data['Status_Verifikasi_Program_Layanan'] == "Sudah Diverifikasi") {
                                                            $badgeColor = "badge badge-success";
                                                        } else if ($data['Status_Verifikasi_Program_Layanan'] == "Menunggu Verifikasi") {
                                                            $badgeColor = "badge badge-warning";
                                                        } else {
                                                            $badgeColor = "badge badge-danger";
                                                        } ?>

                                                        <span class="<?php echo $badgeColor; ?>">
                                                            <?php echo $data['Status_Verifikasi_Program_Layanan'] ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">

                                                            <?php if ($data['Status_Verifikasi_Program_Layanan'] == "Sudah Diverifikasi") { ?>
                                                                &nbsp;
                                                                <a href="#"
                                                                    onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Program_Layanan"], $_GET['menu']); ?>','Menunggu Verifikasi')"><button
                                                                        class="btn btn-xs btn-outline-danger" alt="Undo verified"><i
                                                                            class="fas fa-undo"></i></button></a>
                                                            <?php } else { ?>
                                                                &nbsp;
                                                                <a href="#"
                                                                    onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Program_Layanan"], $_GET['menu']); ?>','Sudah Diverifikasi')"><button
                                                                        class="btn btn-xs btn-outline-success"><i
                                                                            class="fas fa-check"></i></button></a>
                                                                &nbsp;
                                                                <a href="#"
                                                                    onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Program_Layanan"], $_GET['menu']); ?>','Verifikasi Ditolak')"><button
                                                                        class="btn btn-xs btn-outline-danger"><i
                                                                            class="fas fa-times"></i></button></a>
                                                            <?php } ?>
                                                            &nbsp;
                                                            <a
                                                                href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Program_Layanan"], $_GET['menu']); ?>"><button
                                                                    class="btn btn-xs btn-outline-warning"><i
                                                                        class="fas fa-edit"></i></button></a>

                                                            <div class="d-none">

                                                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] == "Terhapus")) { ?>
                                                                    &nbsp;
                                                                    <a href="#"
                                                                        onclick="datatable_konfirmasi_restore_data_dari_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar_Program_Layanan"], $_GET['menu']); ?>')"><button
                                                                            class="btn btn-xs btn-outline-success"><i
                                                                                class="fas fa-redo"></i></button></a>
                                                                    &nbsp;
                                                                    <a href="#"
                                                                        onclick="datatable_konfirmasi_hapus_data_permanen('<?php echo $a_hash->encode($data["Id_Pendaftar_Program_Layanan"], $_GET['menu']); ?>')"><button
                                                                            class="btn btn-xs btn-outline-danger"><i
                                                                                class="fa fa-trash"></i></button></a>
                                                                <?php } else { ?> &nbsp;
                                                                    <a href="#"
                                                                        onclick="datatable_konfirmasi_hapus_data_ke_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar_Program_Layanan"], $_GET['menu']); ?>')"><button
                                                                            class="btn btn-xs btn-outline-danger"><i
                                                                                class="fa fa-trash"></i></button></a>
                                                                <?php } ?>
                                                            </div>

                                                        </div>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    function datatable_konfirmasi_update_status_verifikasi(id_primary, status_verifikasi) {
                                        var txt;
                                        if (status_verifikasi == "Sudah Diverifikasi") {
                                            var r = confirm("Apakah Anda yakin ingin mengubah status data ini menjadi 'Sudah Diverifikasi' ?");
                                        } else if (status_verifikasi == "Verifikasi Ditolak") {
                                            var r = confirm("Apakah Anda yakin ingin mengembalikan status data inimMenjadi 'Belum Diverifikasi' ?");
                                        } else {
                                            var r = confirm("Apakah Anda yakin ingin mengembalikan status data inimMenjadi 'Menunggu Verifikasi' ?");
                                        }
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&update_status_verifikasi=' + status_verifikasi + '&id=' + id_primary
                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_permanen(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
                                        }
                                    }

                                    function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
                                        }
                                    }
                                </script>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>