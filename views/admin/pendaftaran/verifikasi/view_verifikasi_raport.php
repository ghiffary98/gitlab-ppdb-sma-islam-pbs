<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/data_pendaftar_nilai_rapor/model_data_pendaftar_nilai_rapor.php";
include "models/global/pengumuman_pendaftar/model_pengumuman_pendaftar.php";
include "controllers/global/pengumuman_pendaftar/controller_pengumuman_pendaftar.php";
include "controllers/admin/pendaftaran/verifikasi/controller_verifikasi_rapor.php";
?>
<style>
    .form-control[readonly] {
        background: #fdfdfd;
        opacity: 1;
    }
</style>


<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Verifikasi Nilai Rapor</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="?menu=home">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="<?php echo "?menu=" . $_GET['menu'] ?>">Verifikasi Nilai Rapor</a>
                </li>

                <?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <?php if (isset($_GET['tambah'])) { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
                        <?php } else { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
                        <?php } ?>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php
        //FORM TAMBAH/EDIT DATA
        if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <?php if (isset($_GET['tambah'])) { ?>
                                        <div class="card-title">Tambah Data</div>
                                    <?php } else { ?>
                                        <div class="card-title">Edit Data</div>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
                                    <?php if (isset($_GET["edit"])) { ?>
                                        <script type="text/javascript">
                                            function konfirmasi_hapus_data_permanen() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_hapus_data_ke_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_arsip_data() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_arsip() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }
                                        </script>
                                        <ul class="list-inline d-none">
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Aktif") { ?>
                                                    <a href="#" onclick="konfirmasi_arsip_data()"><i class="fa fa-archive fa-md"></i> ARSIPKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terarsip") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i class="fa fa-archive fa-md"></i> AKTIFKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i class="fa fa-archive fa-md"></i> RESTORE </a>
                                                <?php } ?>

                                            </li>
                                            <li class="list-inline-item"> | </li>
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_permanen()"><i class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th class="bg-light">
                                                            <h4><b>Nilai Rapor</b></h4>
                                                        </th>
                                                        <th class="bg-light text-center">
                                                            <h4><b>Semester 1</b></h4>
                                                        </th>
                                                        <th class="bg-light text-center">
                                                            <h4><b>Semester 2</b></h4>
                                                        </th>
                                                        <th class="bg-light text-center">
                                                            <h4><b>Semester 3</b></h4>
                                                        </th>
                                                        <th class="bg-light text-center">
                                                            <h4><b>Semester 4</b></h4>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td> Pendidikan Agama Islam dan Budi Pekerti </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "")) {
                                                                                                                                                                                                                            echo $edit['Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                                                                                                                                                                                                                        } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "")) {
                                                                                                                                                                                                                            echo $edit['Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                                                                                                                                                                                                                        } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "")) {
                                                                                                                                                                                                                            echo $edit['Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                                                                                                                                                                                                                        } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "")) {
                                                                                                                                                                                                                            echo $edit['Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'];
                                                                                                                                                                                                                        } ?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td> Pendidikan Pancasila dan Kewarganegaraan </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "")) {
                                                                                                                                                                                                                                echo $edit['Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                                                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "")) {
                                                                                                                                                                                                                                echo $edit['Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                                                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "")) {
                                                                                                                                                                                                                                echo $edit['Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                                                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "")) {
                                                                                                                                                                                                                                echo $edit['Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan'];
                                                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td> Bahasa Indonesia </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_Bahasa_Indonesia" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_Bahasa_Indonesia'] <> "")) {
                                                                                                                                                                                                        echo $edit['Nilai_Semester_1_Bahasa_Indonesia'];
                                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_Bahasa_Indonesia" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_Bahasa_Indonesia'] <> "")) {
                                                                                                                                                                                                        echo $edit['Nilai_Semester_2_Bahasa_Indonesia'];
                                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_Bahasa_Indonesia" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_Bahasa_Indonesia'] <> "")) {
                                                                                                                                                                                                        echo $edit['Nilai_Semester_3_Bahasa_Indonesia'];
                                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_Bahasa_Indonesia" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_Bahasa_Indonesia'] <> "")) {
                                                                                                                                                                                                        echo $edit['Nilai_Semester_4_Bahasa_Indonesia'];
                                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td> Bahasa Inggris </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_Bahasa_Inggris" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_Bahasa_Inggris'] <> "")) {
                                                                                                                                                                                                    echo $edit['Nilai_Semester_1_Bahasa_Inggris'];
                                                                                                                                                                                                } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_Bahasa_Inggris" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_Bahasa_Inggris'] <> "")) {
                                                                                                                                                                                                    echo $edit['Nilai_Semester_2_Bahasa_Inggris'];
                                                                                                                                                                                                } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_Bahasa_Inggris" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_Bahasa_Inggris'] <> "")) {
                                                                                                                                                                                                    echo $edit['Nilai_Semester_3_Bahasa_Inggris'];
                                                                                                                                                                                                } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_Bahasa_Inggris" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_Bahasa_Inggris'] <> "")) {
                                                                                                                                                                                                    echo $edit['Nilai_Semester_4_Bahasa_Inggris'];
                                                                                                                                                                                                } ?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td> Matematika </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_Matematika" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_Matematika'] <> "")) {
                                                                                                                                                                                                echo $edit['Nilai_Semester_1_Matematika'];
                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_Matematika" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_Matematika'] <> "")) {
                                                                                                                                                                                                echo $edit['Nilai_Semester_2_Matematika'];
                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_Matematika" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_Matematika'] <> "")) {
                                                                                                                                                                                                echo $edit['Nilai_Semester_3_Matematika'];
                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_Matematika" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_Matematika'] <> "")) {
                                                                                                                                                                                                echo $edit['Nilai_Semester_4_Matematika'];
                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td> IPA (Ilmu Pengetahuan Alam) </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_IPA" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_IPA'] <> "")) {
                                                                                                                                                                                        echo $edit['Nilai_Semester_1_IPA'];
                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_IPA" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_IPA'] <> "")) {
                                                                                                                                                                                        echo $edit['Nilai_Semester_2_IPA'];
                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_IPA" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_IPA'] <> "")) {
                                                                                                                                                                                        echo $edit['Nilai_Semester_3_IPA'];
                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_IPA" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_IPA'] <> "")) {
                                                                                                                                                                                        echo $edit['Nilai_Semester_4_IPA'];
                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td> IPS (Ilmu Pengetahuan Sosial) </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_IPS" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_IPS'] <> "")) {
                                                                                                                                                                                        echo $edit['Nilai_Semester_1_IPS'];
                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_IPS" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_IPS'] <> "")) {
                                                                                                                                                                                        echo $edit['Nilai_Semester_2_IPS'];
                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_IPS" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_IPS'] <> "")) {
                                                                                                                                                                                        echo $edit['Nilai_Semester_3_IPS'];
                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_IPS" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_IPS'] <> "")) {
                                                                                                                                                                                        echo $edit['Nilai_Semester_4_IPS'];
                                                                                                                                                                                    } ?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td> Seni dan Budaya </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_Seni_Dan_Budaya" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_Seni_Dan_Budaya'] <> "")) {
                                                                                                                                                                                                    echo $edit['Nilai_Semester_1_Seni_Dan_Budaya'];
                                                                                                                                                                                                } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_Seni_Dan_Budaya" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_Seni_Dan_Budaya'] <> "")) {
                                                                                                                                                                                                    echo $edit['Nilai_Semester_2_Seni_Dan_Budaya'];
                                                                                                                                                                                                } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_Seni_Dan_Budaya" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_Seni_Dan_Budaya'] <> "")) {
                                                                                                                                                                                                    echo $edit['Nilai_Semester_3_Seni_Dan_Budaya'];
                                                                                                                                                                                                } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_Seni_Dan_Budaya" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_Seni_Dan_Budaya'] <> "")) {
                                                                                                                                                                                                    echo $edit['Nilai_Semester_4_Seni_Dan_Budaya'];
                                                                                                                                                                                                } ?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td> PJOK </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_PJOK" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_PJOK'] <> "")) {
                                                                                                                                                                                            echo $edit['Nilai_Semester_1_PJOK'];
                                                                                                                                                                                        } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_PJOK" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_PJOK'] <> "")) {
                                                                                                                                                                                            echo $edit['Nilai_Semester_2_PJOK'];
                                                                                                                                                                                        } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_PJOK" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_PJOK'] <> "")) {
                                                                                                                                                                                            echo $edit['Nilai_Semester_3_PJOK'];
                                                                                                                                                                                        } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_PJOK" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_PJOK'] <> "")) {
                                                                                                                                                                                            echo $edit['Nilai_Semester_4_PJOK'];
                                                                                                                                                                                        } ?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td> Prakarya </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_1_Prakarya" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_1_Prakarya'] <> "")) {
                                                                                                                                                                                                echo $edit['Nilai_Semester_1_Prakarya'];
                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_2_Prakarya" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_2_Prakarya'] <> "")) {
                                                                                                                                                                                                echo $edit['Nilai_Semester_2_Prakarya'];
                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_3_Prakarya" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_3_Prakarya'] <> "")) {
                                                                                                                                                                                                echo $edit['Nilai_Semester_3_Prakarya'];
                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_Semester_4_Prakarya" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_Semester_4_Prakarya'] <> "")) {
                                                                                                                                                                                                echo $edit['Nilai_Semester_4_Prakarya'];
                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                    </tr>
                                                                                                                                                                                        
                                                    <tr class="bg-light">
                                                        <th colspan="5">
                                                            <h2><b>Nilai SKL / Ijazah</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr class="">
                                                        <td class="">
                                                            Nilai SKL / Ijazah
                                                        </td>
                                                        <td>
                                                            <div class="form-group"> <input name="Nilai_SKL_Ijazah" type="number" min="1" max="100" class="form-control" value="<?php if ((isset($_GET['edit'])) and ($edit['Nilai_SKL_Ijazah'] <> "")) {
                                                                                                                                                                                                echo $edit['Nilai_SKL_Ijazah'];
                                                                                                                                                                                            } ?>"> </div>
                                                        </td>
                                                    </tr>

                                                    <tr class="bg-light">
                                                        <th colspan="5">
                                                            <h2><b>Status Verifikasi</b></h2>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Status Verifikasi </td>
                                                        <td style="width:70%" colspan="4">
                                                            <div class="form-group">
                                                                <select class="form-control" name="Status_Verifikasi_Nilai_Rapor">
                                                                    <?php
                                                                    $array_option = ["", "Sudah Diverifikasi", "Verifikasi Ditolak", "Belum Diverifikasi"];
                                                                    $array_option_terpilih = [];
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Status_Verifikasi_Nilai_Rapor'] <> "") {
                                                                            $array_option_terpilih[] = $edit['Status_Verifikasi_Nilai_Rapor'];
                                                                        } else {
                                                                            $array_option_terpilih[] = "";
                                                                        }
                                                                    }
                                                                    foreach ($array_option as $option) {
                                                                    ?>
                                                                        <option <?php
                                                                                if (isset($_GET['edit'])) {
                                                                                    foreach ($array_option_terpilih as $option_terpilih) {
                                                                                        if ($option_terpilih == $option) {
                                                                                            echo "selected";
                                                                                            break;
                                                                                        }
                                                                                    }
                                                                                }
                                                                                ?> value="<?php echo $option ?>">
                                                                            <?php echo $option ?>
                                                                        </option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div style="text-align: center;">
                                    <?php if (isset($_GET["tambah"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
                                    <?php } elseif (isset($_GET["edit"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE">
                                    <?php } ?>
                                    <input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'" class="btn btn-danger" value="BATAL">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

        <?php
        //FORM LIST DATA
        if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">List Verifikasi Nilai Rapor
                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
                                    echo "(" . $_GET['filter_status'] . ")";
                                } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="<?php echo $kehalaman ?>&tambah" class="btn btn-sm btn-primary" style="display:none"><i class="fas fa-plus-circle"></i> &nbsp; Tambah Baru</a>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12 d-none" style="text-align: right;">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (<?php echo $hitung_Aktif ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (<?php echo $hitung_Terarsip ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (<?php echo $hitung_Terhapus ?>)
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table id="basic-datatables" class="table table-borderless" style="width:100%">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Pendaftaran</th>
                                            <th>NISN</th>
                                            <th>Nama Lengkap</th>
                                            <th>Semester 1</th>
                                            <th>Semester 2</th>
                                            <th>Semester 3</th>
                                            <th>Semester 4</th>
                                            <th>Status Verifikasi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ((isset($list_datatable_master))) {
                                            foreach ($list_datatable_master as $data) {
                                                $nomor++; ?>
                                                <?php
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'])) {
                                                        $data['Nomor_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'];
                                                    } else {
                                                        $data['Nomor_Pendaftaran'] = "";
                                                    }
                                                } else {
                                                    $data['Nomor_Pendaftaran'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'])) {
                                                        $data['NIS'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIS'];
                                                    } else {
                                                        $data['NIS'] = "";
                                                    }
                                                } else {
                                                    $data['NIS'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIS

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'])) {
                                                        $data['NISN'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NISN'];
                                                    } else {
                                                        $data['NISN'] = "";
                                                    }
                                                } else {
                                                    $data['NISN'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NISN

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'])) {
                                                        $data['NIK'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['NIK'];
                                                    } else {
                                                        $data['NIK'] = "";
                                                    }
                                                } else {
                                                    $data['NIK'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => NIK

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'])) {
                                                        $data['Nama_Lengkap'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'];
                                                    } else {
                                                        $data['Nama_Lengkap'] = "";
                                                    }
                                                } else {
                                                    $data['Nama_Lengkap'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Username
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Username'])) {
                                                        $data['Username'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Username'];
                                                    } else {
                                                        $data['Username'] = "";
                                                    }
                                                } else {
                                                    $data['Username'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Username

                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Pendaftaran
                                                if (isset($array_result_relasi_data_pendaftar)) {
                                                    $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                    if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Pendaftaran'])) {
                                                        $data['Status_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Pendaftaran'];
                                                    } else {
                                                        $data['Status_Pendaftaran'] = "";
                                                    }
                                                } else {
                                                    $data['Status_Pendaftaran'] = "";
                                                }
                                                //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Pendaftaran


                                                $Keterangan_Semester_1 = "Belum Lengkap";
                                                $Keterangan_Semester_2 = "Belum Lengkap";
                                                $Keterangan_Semester_3 = "Belum Lengkap";
                                                $Keterangan_Semester_4 = "Belum Lengkap";

                                                if (
                                                    ($data['Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "") and
                                                    ($data['Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "") and
                                                    ($data['Nilai_Semester_1_Bahasa_Indonesia'] <> "") and
                                                    ($data['Nilai_Semester_1_Bahasa_Inggris'] <> "") and
                                                    ($data['Nilai_Semester_1_Matematika'] <> "") and
                                                    ($data['Nilai_Semester_1_IPA'] <> "") and
                                                    ($data['Nilai_Semester_1_IPS'] <> "") and
                                                    ($data['Nilai_Semester_1_Seni_Dan_Budaya'] <> "") and
                                                    ($data['Nilai_Semester_1_PJOK'] <> "") and
                                                    ($data['Nilai_Semester_1_Prakarya'] <> "")
                                                ) {
                                                    $Keterangan_Semester_1 = "Lengkap";
                                                }

                                                if (
                                                    ($data['Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "") and
                                                    ($data['Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "") and
                                                    ($data['Nilai_Semester_2_Bahasa_Indonesia'] <> "") and
                                                    ($data['Nilai_Semester_2_Bahasa_Inggris'] <> "") and
                                                    ($data['Nilai_Semester_2_Matematika'] <> "") and
                                                    ($data['Nilai_Semester_2_IPA'] <> "") and
                                                    ($data['Nilai_Semester_2_IPS'] <> "") and
                                                    ($data['Nilai_Semester_2_Seni_Dan_Budaya'] <> "") and
                                                    ($data['Nilai_Semester_2_PJOK'] <> "") and
                                                    ($data['Nilai_Semester_2_Prakarya'] <> "")
                                                ) {
                                                    $Keterangan_Semester_2 = "Lengkap";
                                                }

                                                if (
                                                    ($data['Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "") and
                                                    ($data['Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "") and
                                                    ($data['Nilai_Semester_3_Bahasa_Indonesia'] <> "") and
                                                    ($data['Nilai_Semester_3_Bahasa_Inggris'] <> "") and
                                                    ($data['Nilai_Semester_3_Matematika'] <> "") and
                                                    ($data['Nilai_Semester_3_IPA'] <> "") and
                                                    ($data['Nilai_Semester_3_IPS'] <> "") and
                                                    ($data['Nilai_Semester_3_Seni_Dan_Budaya'] <> "") and
                                                    ($data['Nilai_Semester_3_PJOK'] <> "") and
                                                    ($data['Nilai_Semester_3_Prakarya'] <> "")
                                                ) {
                                                    $Keterangan_Semester_3 = "Lengkap";
                                                }

                                                if (
                                                    ($data['Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti'] <> "") and
                                                    ($data['Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan'] <> "") and
                                                    ($data['Nilai_Semester_4_Bahasa_Indonesia'] <> "") and
                                                    ($data['Nilai_Semester_4_Bahasa_Inggris'] <> "") and
                                                    ($data['Nilai_Semester_4_Matematika'] <> "") and
                                                    ($data['Nilai_Semester_4_IPA'] <> "") and
                                                    ($data['Nilai_Semester_4_IPS'] <> "") and
                                                    ($data['Nilai_Semester_4_Seni_Dan_Budaya'] <> "") and
                                                    ($data['Nilai_Semester_4_PJOK'] <> "") and
                                                    ($data['Nilai_Semester_4_Prakarya'] <> "")
                                                ) {
                                                    $Keterangan_Semester_4 = "Lengkap";
                                                }

                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $nomor ?>
                                                    </td>
                                                    <td><a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Nilai_Rapor"], $_GET['menu']); ?>">
                                                            <?php echo $data['Nomor_Pendaftaran'] ?>
                                                        </a></td>
                                                    <td><?php echo $data['NISN'] ?></td>
                                                    <td><?php echo $data['Nama_Lengkap'] ?></td>
                                                    <td>
                                                        <?php
                                                        if ($Keterangan_Semester_1 == 'Lengkap') {
                                                        ?>
                                                            <i class="fas fa-check text-success"></i>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <i class="fas fa-times text-danger"></i>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($Keterangan_Semester_2 == 'Lengkap') {
                                                        ?>
                                                            <i class="fas fa-check text-success"></i>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <i class="fas fa-times text-danger"></i>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($Keterangan_Semester_3 == 'Lengkap') {
                                                        ?>
                                                            <i class="fas fa-check text-success"></i>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <i class="fas fa-times text-danger"></i>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($Keterangan_Semester_4 == 'Lengkap') {
                                                        ?>
                                                            <i class="fas fa-check text-success"></i>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <i class="fas fa-times text-danger"></i>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($data['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
                                                            $badgeColor = "badge badge-success";
                                                        } else if ($data['Status_Verifikasi_Nilai_Rapor'] == "Menunggu Verifikasi") {
                                                            $badgeColor = "badge badge-warning";
                                                        } else {
                                                            $badgeColor = "badge badge-danger";
                                                        } ?>

                                                        <span class="<?php echo $badgeColor; ?>">
                                                            <?php echo $data['Status_Verifikasi_Nilai_Rapor'] ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">

                                                            <?php if ($data['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") { ?>
                                                                &nbsp;
                                                                <a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Nilai_Rapor"], $_GET['menu']); ?>','Menunggu Verifikasi')"><button class="btn btn-xs btn-outline-danger" alt="Undo verified"><i class="fas fa-undo"></i></button></a>
                                                            <?php } else { ?>
                                                                &nbsp;
                                                                <a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Nilai_Rapor"], $_GET['menu']); ?>','Sudah Diverifikasi')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-check"></i></button></a>
                                                                &nbsp;
                                                                <a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar_Nilai_Rapor"], $_GET['menu']); ?>','Verifikasi Ditolak')"><button class="btn btn-xs btn-outline-danger"><i class="fas fa-times"></i></button></a>
                                                            <?php } ?>
                                                            &nbsp;
                                                            <a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Nilai_Rapor"], $_GET['menu']); ?>"><button class="btn btn-xs btn-outline-warning"><i class="fas fa-edit"></i></button></a>

                                                            <div class="d-none">

                                                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] == "Terhapus")) { ?>
                                                                    &nbsp;
                                                                    <a href="#" onclick="datatable_konfirmasi_restore_data_dari_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar_Nilai_Rapor"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-redo"></i></button></a>
                                                                    &nbsp;
                                                                    <a href="#" onclick="datatable_konfirmasi_hapus_data_permanen('<?php echo $a_hash->encode($data["Id_Pendaftar_Nilai_Rapor"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
                                                                <?php } else { ?> &nbsp;
                                                                    <a href="#" onclick="datatable_konfirmasi_hapus_data_ke_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar_Nilai_Rapor"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
                                                                <?php } ?>
                                                            </div>

                                                        </div>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    function datatable_konfirmasi_update_status_verifikasi(id_primary, status_verifikasi) {
                                        var txt;
                                        if (status_verifikasi == "Sudah Diverifikasi") {
                                            var r = confirm("Apakah Anda yakin ingin mengubah status data ini menjadi 'Sudah Diverifikasi' ?");
                                        } else if (status_verifikasi == "Verifikasi Ditolak") {
                                            var r = confirm("Apakah Anda yakin ingin mengembalikan status data inimMenjadi 'Belum Diverifikasi' ?");
                                        } else {
                                            var r = confirm("Apakah Anda yakin ingin mengembalikan status data inimMenjadi 'Menunggu Verifikasi' ?");
                                        }
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&update_status_verifikasi=' + status_verifikasi + '&id=' + id_primary
                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_permanen(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
                                        }
                                    }

                                    function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
                                        }
                                    }
                                </script>
                            </div>
                            <br>
                            <div class="form-group row mt-4">
                                <div class="col-lg-12">
                                    <hr>
                                    Keterangan : <br>
                                    <i class="fas fa-check text-success"></i> : Lengkap <br>
                                    <i class="fas fa-times text-danger"></i> : Belum Lengkap
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>