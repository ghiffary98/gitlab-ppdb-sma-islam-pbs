<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/pengaturan_pekerjaan_orang_tua/model_pengaturan_pekerjaan_orang_tua.php";
include "models/global/pengaturan_penghasilan_orang_tua/model_pengaturan_penghasilan_orang_tua.php";
include "models/global/pengaturan_kendaraan_yang_dipakai_kesekolah/model_pengaturan_kendaraan_yang_dipakai_kesekolah.php";
include "models/global/pengaturan_asal_sekolah/model_pengaturan_asal_sekolah.php";
include "models/global/pengumuman_pendaftar/model_pengumuman_pendaftar.php";
include "controllers/global/pengumuman_pendaftar/controller_pengumuman_pendaftar.php";
include "controllers/admin/pendaftaran/verifikasi/controller_verifikasi_data_diri.php";
?>

<script>
    function ajax_ambil_alamat_sekolah() {

        var asal_sekolah = document.getElementById('asal_sekolah').value;

        $.ajax({
            type: 'GET',
            url: 'controllers/admin/pengaturan_asal_sekolah/ajax_get_alamat_sekolah.php',
            dataType: 'json',
            data: 'nama_sekolah=' + asal_sekolah,
            success: function (response) {
                if (response.Alamat !== "") {
                    // Assuming that 'div_alamat_sekolah' is an input element, use val() to set its value
                    $('#div_alamat_sekolah').val(response.Alamat);
                } else {
                    // If Alamat is empty, you may want to handle it accordingly
                    $('#div_alamat_sekolah').val("No data available");
                }
            },
            error: function (xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    }
</script>

<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Verifikasi Data Diri</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="?menu=home">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="<?php echo "?menu=" . $_GET['menu'] ?>">Verifikasi Data Diri</a>
                </li>

                <?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <?php if (isset($_GET['tambah'])) { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
                        <?php } else { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
                        <?php } ?>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php
        //FORM TAMBAH/EDIT DATA
        if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <?php if (isset($_GET['tambah'])) { ?>
                                        <div class="card-title">Tambah Data</div>
                                    <?php } else { ?>
                                        <div class="card-title">Edit Data</div>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
                                    <?php if (isset($_GET["edit"])) { ?>
                                        <script type="text/javascript">
                                            function konfirmasi_hapus_data_permanen() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
                                                }
                                            }

                                            function konfirmasi_hapus_data_ke_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                }
                                            }

                                            function konfirmasi_arsip_data() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
                                                }
                                            }

                                            function konfirmasi_restore_data_dari_arsip() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
                                                }
                                            }

                                            function konfirmasi_restore_data_dari_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                }
                                            }
                                        </script>
                                        <ul class="list-inline d-none">
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Aktif") { ?>
                                                    <a href="#" onclick="konfirmasi_arsip_data()"><i
                                                            class="fa fa-archive fa-md"></i> ARSIPKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terarsip") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i
                                                            class="fa fa-archive fa-md"></i> AKTIFKAN </a>
                                                <?php } elseif ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i
                                                            class="fa fa-archive fa-md"></i> RESTORE </a>
                                                <?php } ?>

                                            </li>
                                            <li class="list-inline-item"> | </li>
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_permanen()"><i
                                                            class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i
                                                            class="fa fa-trash fa-md"></i> HAPUS </a>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Identitas Login</b></h2>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Nomor Pendaftaran </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input readonly name="Nomor_Pendaftaran" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_Pendaftaran'] <> "")) {
                                                                               echo $edit['Nomor_Pendaftaran'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Email </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input readonly name="Email" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Email'] <> "")) {
                                                                               echo $edit['Email'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Nomor Hanpdhone </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input readonly name="Nomor_Handphone" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_Handphone'] <> "")) {
                                                                               echo $edit['Nomor_Handphone'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Password </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <input readonly <?php if ((isset($edit)) and (($edit['Password_Pendaftar'] == "") or ($edit['Password_Pendaftar'] == null))) {
                                                                        echo "placeholder='Harap Update Password Pendaftar Ini, Jika Ingin Melihat Password Pendaftar'";
                                                                    } ?>
                                                                        name="Password" type="password" class="form-control"
                                                                        <?php if (!(isset($_GET['edit']))) { ?> readonly
                                                                        <?php } ?> value="<?php if ((isset($edit)) and ($edit['Password_Pendaftar'] <> "")) {
                                                                               echo $a_hash->decode($edit['Password_Pendaftar']);
                                                                           } ?>">
                                                                    <div class="input-group-append">
                                                                        <button type="button" id="togglePassword"
                                                                            class="btn btn-secondary">
                                                                            <i class="fa fa-eye"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <script>
                                                                document.addEventListener("DOMContentLoaded", function () {
                                                                    const passwordInput = document.querySelector('input[name="Password"]');
                                                                    const togglePasswordButton = document.getElementById("togglePassword");

                                                                    togglePasswordButton.addEventListener("click", function () {
                                                                        if (passwordInput.type === "password") {
                                                                            passwordInput.type = "text";
                                                                            togglePasswordButton.innerHTML = '<i class="fa fa-eye-slash"></i>';
                                                                        } else {
                                                                            passwordInput.type = "password";
                                                                            togglePasswordButton.innerHTML = '<i class="fa fa-eye"></i>';
                                                                        }
                                                                    });
                                                                });
                                                            </script>



                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Identitas Siswa</b></h2>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Nama Siswa* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Nama_Lengkap" id="nama_lengkap"
                                                                    type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit)) and ($edit['Nama_Lengkap'] <> "")) {
                                                                        echo $edit['Nama_Lengkap'];
                                                                    } ?>">


                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kelas </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select name="Kelas" id="" class="form-control">
                                                                    <?php if (($edit['Kelas'] <> "")) { ?>
                                                                        <option value="<?php echo $edit['Kelas']; ?> ">
                                                                            <?php echo $edit['Kelas']; ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                    <option value="">- Pilih Kelas - </option>
                                                                    <option value="X">X</option>
                                                                    <option value="XI">XI</option>
                                                                    <option value="XII">XII</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tempat Lahir* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Tempat_Lahir" id="tempat_lahir"
                                                                    type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit)) and ($edit['Tempat_Lahir'] <> "")) {
                                                                        echo $edit['Tempat_Lahir'];
                                                                    } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Tanggal Lahir* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Tanggal_Lahir" type="date"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Tanggal_Lahir'] <> "")) {
                                                                               echo $edit['Tanggal_Lahir'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jenis Kelamin* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required type="radio" <?php if (($edit['Jenis_Kelamin'] == "Laki-laki")) {
                                                                    echo "checked";
                                                                } ?> value="Laki-laki" name="Jenis_Kelamin">
                                                                Laki-laki
                                                                <input required type="radio" <?php if (($edit['Jenis_Kelamin'] == "Perempuan")) {
                                                                    echo "checked";
                                                                } ?> value="Perempuan" name="Jenis_Kelamin">
                                                                Perempuan
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Agama* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select required name="Agama" id="" class="form-control">
                                                                    <?php if (($edit['Agama'] <> "")) { ?>
                                                                        <option value="<?php echo $edit['Agama']; ?> ">
                                                                            <?php echo $edit['Agama']; ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                    <option value="">- Pilih Agama - </option>
                                                                    <option value="Islam">Islam</option>
                                                                    <option value="Kristen Protestan">Kristen Protestan
                                                                    </option>
                                                                    <option value="Kristen Katolik">Kristen Katolik</option>
                                                                    <option value="Hindu">Hindu</option>
                                                                    <option value="Buddha">Buddha</option>
                                                                    <option value="Khonghucu">Khonghucu</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Golongan Darah* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select required name="Golongan_Darah" id=""
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                    <?php if ((isset($edit)) and ($edit['Golongan_Darah'] <> "")) { ?>
                                                                        <option value="<?php echo $edit['Golongan_Darah']; ?> ">
                                                                            <?php echo $edit['Golongan_Darah']; ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                    <option value="">- Pilih Golongan Darah - </option>
                                                                    <option value="A">A</option>
                                                                    <option value="AB">AB</option>
                                                                    <option value="B">B</option>
                                                                    <option value="O">O</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tinggi Badan* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Tinggi_Badan" type="number"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Tinggi_Badan'] <> "")) {
                                                                               echo $edit['Tinggi_Badan'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Berat Badan* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Berat_Badan" type="number"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Berat_Badan'] <> "")) {
                                                                               echo $edit['Berat_Badan'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Lingkar Kepala </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Lingkar_Kepala" type="number"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Lingkar_Kepala'] <> "")) {
                                                                               echo $edit['Lingkar_Kepala'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kebutuhan Khusus </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Kebutuhan_Khusus" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Kebutuhan_Khusus'] <> "")) {
                                                                               echo $edit['Kebutuhan_Khusus'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Ukuran Seragam* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select required name="Ukuran_Seragam"
                                                                    onchange="ubah_ukuran_seragam()" id="ukuran_seragam"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                    <?php if ((isset($edit)) and ($edit['Ukuran_Seragam'] <> "")) { ?>
                                                                        <option value="<?php echo $edit['Ukuran_Seragam']; ?> ">
                                                                            <?php echo $edit['Ukuran_Seragam']; ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                    <option value="">- Pilih Ukuran Seragam - </option>
                                                                    <option value="S">S</option>
                                                                    <option value="M">M</option>
                                                                    <option value="L">L</option>
                                                                    <option value="XL">XL</option>
                                                                    <option value="2XL">2XL</option>
                                                                    <option value="3XL">3XL</option>
                                                                    <option value="4XL">4XL</option>
                                                                    <option value="Lainnya">Lainnya</option>
                                                                </select>
                                                                <div id="div_inputan_ukuran_seragam" style="display: none;">
                                                                    <br>
                                                                    <input type="text" class="form-control"
                                                                        name="Text_Inputan_Ukuran_Seragam"
                                                                        id="text_inputan_ukuran_seragam"
                                                                        placeholder="Masukkan ukuran seragam selain pilihan di atas">
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Kontak</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No. HP* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Nomor_Handphone" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_Handphone'] <> "")) {
                                                                               echo $edit['Nomor_Handphone'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> No. Telp </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Nomor_Telepon" type="text" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_Telepon'] <> "")) {
                                                                               echo $edit['Nomor_Telepon'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Email* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Email" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Email'] <> "")) {
                                                                               echo $edit['Email'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Sekolah</b></h2>
                                                        </th>
                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%"> NIS (Nomor Induk Siswa)* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="NIS" type="text" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['NIS'] <> "")) {
                                                                               echo $edit['NIS'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NISN (Nomor Induk Siswa Nasional)* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="NISN" type="text" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['NISN'] <> "")) {
                                                                               echo $edit['NISN'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NIPD </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="NIPD" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit)) and ($edit['NIPD'] <> "")) {
                                                                        echo $edit['NIPD'];
                                                                    } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Asal Sekolah* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select required class="form-control" name="Asal_Sekolah"
                                                                    id="asal_sekolah"
                                                                    onchange="ajax_ambil_alamat_sekolah();">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_asal_sekolah_ini = $list_data_pengaturan_asal_sekolah;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Asal_Sekolah'] <> "") {
                                                                            $edit_data_sebelum_Asal_Sekolah['Id_Pengaturan_Asal_Sekolah'] = '0';
                                                                            $edit_data_sebelum_Asal_Sekolah['Nama_Sekolah'] = $edit['Asal_Sekolah'];
                                                                            array_unshift($list_data_pengaturan_asal_sekolah_ini, $edit_data_sebelum_Asal_Sekolah);
                                                                            $list_data_pengaturan_asal_sekolah_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_asal_sekolah_ini, "Nama_Sekolah");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_asal_sekolah_ini))) {
                                                                        foreach ($list_data_pengaturan_asal_sekolah_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Asal_Sekolah'] == $data['Nama_Sekolah']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit['Asal_Sekolah'] == $data['Nama_Sekolah']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Nama_Sekolah'] ?>">
                                                                                <?php echo $data['Nama_Sekolah'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Alamat Sekolah* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required id="div_alamat_sekolah"
                                                                    name="Alamat_Sekolah" type="text" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Alamat_Sekolah'] <> "")) {
                                                                               echo $edit['Alamat_Sekolah'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No Peserta Ujian Sekolah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Nomor_Peserta_Ujian_Nasional" type="number"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_Peserta_Ujian_Nasional'] <> "")) {
                                                                               echo $edit['Nomor_Peserta_Ujian_Nasional'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No Seri Ijazah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Nomor_Seri_Ijazah" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_Seri_Ijazah'] <> "")) {
                                                                               echo $edit['Nomor_Seri_Ijazah'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Keluarga</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NIK (Nomor Induk Kependudukan)* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="NIK" type="number"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['NIK'] <> "")) {
                                                                               echo $edit['NIK'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%"> No KK (Kartu Keluarga)*</td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Nomor_KK" type="number"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_KK'] <> "")) {
                                                                               echo $edit['Nomor_KK'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Anak Ke-berapa </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Anak_Ke" type="number" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Anak_Ke'] <> "")) {
                                                                               echo $edit['Anak_Ke'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jumlah Saudara Kandung </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Jumlah_Saudara" type="number"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Jumlah_Saudara'] <> "")) {
                                                                               echo $edit['Jumlah_Saudara'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penerima KPS* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">

                                                                <input required type="radio" <?php if ((isset($edit)) and ($edit['Penerima_KPS'] == "Ya")) {
                                                                    echo "checked";
                                                                } ?>
                                                                    value="Ya" name="Penerima_KPS" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Ya
                                                                <input required type="radio" <?php if ((isset($edit)) and ($edit['Penerima_KPS'] == "Tidak")) {
                                                                    echo "checked";
                                                                } ?>
                                                                    value="Tidak" name="Penerima_KPS" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                Tidak

                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No.KPS (Jika Ya) </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Nomor_KPS" type="number" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_KPS'] <> "")) {
                                                                               echo $edit['Nomor_KPS'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penerima KIP* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required type="radio" <?php if ((isset($edit)) and ($edit['Penerima_KIP'] == "Ya")) {
                                                                    echo "checked";
                                                                } ?>
                                                                    value="Ya" name="Penerima_KIP" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Ya
                                                                <input required type="radio" <?php if ((isset($edit)) and ($edit['Penerima_KIP'] == "Tidak")) {
                                                                    echo "checked";
                                                                } ?>
                                                                    value="Tidak" name="Penerima_KIP" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                Tidak

                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nomor KIP </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Nomor_KIP" type="number" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_KIP'] <> "")) {
                                                                               echo $edit['Nomor_KIP'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penerima KJP DKI* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required type="radio" <?php if ((isset($edit)) and ($edit['Penerima_KJP_DKI'] == "Ya")) {
                                                                    echo "checked";
                                                                } ?> value="Ya" name="Penerima_KJP_DKI" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Ya
                                                                <input required type="radio" <?php if ((isset($edit)) and ($edit['Penerima_KJP_DKI'] == "Tidak")) {
                                                                    echo "checked";
                                                                } ?> value="Tidak" name="Penerima_KJP_DKI" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                Tidak


                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nomor KJP DKI </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input style="background:#fff;" name="Nomor_KJP_DKI"
                                                                    type="number" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit)) and ($edit['Nomor_KJP_DKI'] <> "")) {
                                                                        echo $edit['Nomor_KJP_DKI'];
                                                                    } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nomor KKS </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Nomor_KKS" type="number" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_KKS'] <> "")) {
                                                                               echo $edit['Nomor_KKS'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No Registrasi Akta Lahir </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Nomor_Registrasi_Akta_Lahir" type="number"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nomor_Registrasi_Akta_Lahir'] <> "")) {
                                                                               echo $edit['Nomor_Registrasi_Akta_Lahir'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Status Dalam Keluarga </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Status_Dalam_Keluarga" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Status_Dalam_Keluarga'] <> "")) {
                                                                               echo $edit['Status_Dalam_Keluarga'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Status Perwalian* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required type="radio" <?php if ((isset($edit)) and ($edit['Status_Perwalian'] == "Kandung")) {
                                                                    echo "checked";
                                                                } ?> value="Kandung" name="Status_Perwalian"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Kandung
                                                                <input required type="radio" <?php if ((isset($edit)) and ($edit['Status_Perwalian'] == "Wali")) {
                                                                    echo "checked";
                                                                } ?> value="Wali" name="Status_Perwalian" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>> Wali
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Orang Tua</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h4><b>Data Ayah</b></h4>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nama Ayah* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Nama_Ayah" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nama_Ayah'] <> "")) {
                                                                               echo $edit['Nama_Ayah'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tempat Lahir Ayah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Tempat_Lahir_Ayah" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Tempat_Lahir_Ayah'] <> "")) {
                                                                               echo $edit['Tempat_Lahir_Ayah'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tanggal Lahir Ayah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Tanggal_Lahir_Ayah" type="date"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Tanggal_Lahir_Ayah'] <> "")) {
                                                                               echo $edit['Tanggal_Lahir_Ayah'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Pendidikan Ayah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select name="Jenjang_Pendidikan_Ayah" id=""
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                    <?php if ((isset($edit)) and ($edit['Jenjang_Pendidikan_Ayah'] <> "")) { ?>
                                                                        <option
                                                                            value="<?php echo $edit['Jenjang_Pendidikan_Ayah']; ?> ">
                                                                            <?php echo $edit['Jenjang_Pendidikan_Ayah']; ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                    <option value="">- Pilih Jenjang Pendidikan Ayah -
                                                                    </option>
                                                                    <option value="SD">SD</option>
                                                                    <option value="SMP">SMP</option>
                                                                    <option value="SMA">SMA</option>
                                                                    <option value="D1">D1</option>
                                                                    <option value="D3">D3</option>
                                                                    <option value="D4">D4</option>
                                                                    <option value="S1">S1</option>
                                                                    <option value="S2">S2</option>
                                                                    <option value="S3">S3</option>
                                                                    <option value="Lainnya">Lainnya</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Pekerjaan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select class="form-control" name="Pekerjaan_Ayah">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Pekerjaan_Ayah'] <> "") {
                                                                            $edit_data_sebelum_Pekerjaan_Ayah['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Pekerjaan_Ayah['Pekerjaan_Orang_Tua'] = $edit['Pekerjaan_Ayah'];
                                                                            array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Ayah);
                                                                            $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini, "Pekerjaan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Pekerjaan_Ayah'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit['Pekerjaan_Ayah'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Pekerjaan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penghasilan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select class="form-control" name="Penghasilan_Ayah">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Penghasilan_Ayah'] <> "") {
                                                                            $edit_data_sebelum_Penghasilan_Ayah['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Penghasilan_Ayah['Penghasilan_Orang_Tua'] = $edit['Penghasilan_Ayah'];
                                                                            array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Ayah);
                                                                            $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini, "Penghasilan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Penghasilan_Ayah'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit['Penghasilan_Ayah'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Penghasilan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Penghasilan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NIK Ayah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="NIK_Ayah" type="text" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['NIK_Ayah'] <> "")) {
                                                                               echo $edit['NIK_Ayah'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No. HP Ayah </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="No_HP_Ayah" type="number" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['No_HP_Ayah'] <> "")) {
                                                                               echo $edit['No_HP_Ayah'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h4><b>Data Ibu</b></h4>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Nama Ibu* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required name="Nama_Ibu" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nama_Ibu'] <> "")) {
                                                                               echo $edit['Nama_Ibu'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tempat Lahir Ibu </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Tempat_Lahir_Ibu" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Tempat_Lahir_Ibu'] <> "")) {
                                                                               echo $edit['Tempat_Lahir_Ibu'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tanggal Lahir Ibu </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Tanggal_Lahir_Ibu" type="date"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Tanggal_Lahir_Ibu'] <> "")) {
                                                                               echo $edit['Tanggal_Lahir_Ibu'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%">Pendidikan Ibu </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select name="Jenjang_Pendidikan_Ibu" id=""
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                    <?php if ((isset($edit)) and ($edit['Jenjang_Pendidikan_Ibu'] <> "")) { ?>
                                                                        <option
                                                                            value="<?php echo $edit['Jenjang_Pendidikan_Ibu']; ?> ">
                                                                            <?php echo $edit['Jenjang_Pendidikan_Ibu']; ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                    <option value="">- Pilih Jenjang Pendidikan Ibu -
                                                                    </option>
                                                                    <option value="SD">SD</option>
                                                                    <option value="SMP">SMP</option>
                                                                    <option value="SMA">SMA</option>
                                                                    <option value="D1">D1</option>
                                                                    <option value="D3">D3</option>
                                                                    <option value="D4">D4</option>
                                                                    <option value="S1">S1</option>
                                                                    <option value="S2">S2</option>
                                                                    <option value="S3">S3</option>
                                                                    <option value="Lainnya">Lainnya</option>
                                                                </select>

                                                                <input style="background:#fff; display: none; "
                                                                    name="Jenjang_Pendidikan_Ibu" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Jenjang_Pendidikan_Ibu'] <> "")) {
                                                                               echo $edit['Jenjang_Pendidikan_Ibu'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Pekerjaan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select class="form-control" name="Pekerjaan_Ibu">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Pekerjaan_Ibu'] <> "") {
                                                                            $edit_data_sebelum_Pekerjaan_Ibu['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Pekerjaan_Ibu['Pekerjaan_Orang_Tua'] = $edit['Pekerjaan_Ibu'];
                                                                            array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Ibu);
                                                                            $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini, "Pekerjaan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Pekerjaan_Ibu'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit['Pekerjaan_Ibu'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Pekerjaan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penghasilan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select class="form-control" name="Penghasilan_Ibu">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Penghasilan_Ibu'] <> "") {
                                                                            $edit_data_sebelum_Penghasilan_Ibu['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Penghasilan_Ibu['Penghasilan_Orang_Tua'] = $edit['Penghasilan_Ibu'];
                                                                            array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Ibu);
                                                                            $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini, "Penghasilan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Penghasilan_Ibu'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit['Penghasilan_Ibu'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Penghasilan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Penghasilan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NIK Ibu </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="NIK_Ibu" type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit)) and ($edit['NIK_Ibu'] <> "")) {
                                                                        echo $edit['NIK_Ibu'];
                                                                    } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No. HP Ibu </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="No_HP_Ibu" type="number" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['No_HP_Ibu'] <> "")) {
                                                                               echo $edit['No_HP_Ibu'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Data Wali</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Nama Wali </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Nama_Wali" type="text" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Nama_Wali'] <> "")) {
                                                                               echo $edit['Nama_Wali'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tempat Lahir Wali </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Tempat_Lahir_Wali" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Tempat_Lahir_Wali'] <> "")) {
                                                                               echo $edit['Tempat_Lahir_Wali'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Tanggal Lahir Wali </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Tanggal_Lahir_Wali" type="date"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Tanggal_Lahir_Wali'] <> "")) {
                                                                               echo $edit['Tanggal_Lahir_Wali'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Pendidikan Wali </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select name="Jenjang_Pendidikan_Wali" id=""
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                    <?php if ((isset($edit)) and ($edit['Jenjang_Pendidikan_Wali'] <> "")) { ?>
                                                                        <option
                                                                            value="<?php echo $edit['Jenjang_Pendidikan_Wali']; ?> ">
                                                                            <?php echo $edit['Jenjang_Pendidikan_Wali']; ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                    <option value="">- Pilih Jenjang Pendidikan Wali -
                                                                    </option>
                                                                    <option value="SD">SD</option>
                                                                    <option value="SMP">SMP</option>
                                                                    <option value="SMA">SMA</option>
                                                                    <option value="D1">D1</option>
                                                                    <option value="D3">D3</option>
                                                                    <option value="D4">D4</option>
                                                                    <option value="S1">S1</option>
                                                                    <option value="S2">S2</option>
                                                                    <option value="S3">S3</option>
                                                                    <option value="Lainnya">Lainnya</option>
                                                                </select>

                                                                <input style="background:#fff; display: none; "
                                                                    name="Jenjang_Pendidikan_Wali" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Jenjang_Pendidikan_Wali'] <> "")) {
                                                                               echo $edit['Jenjang_Pendidikan_Wali'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Pekerjaan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select class="form-control" name="Pekerjaan_Wali">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_pekerjaan_orang_tua_ini = $list_data_pengaturan_pekerjaan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Pekerjaan_Wali'] <> "") {
                                                                            $edit_data_sebelum_Pekerjaan_Wali['Id_Pengaturan_Pekerjaan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Pekerjaan_Wali['Pekerjaan_Orang_Tua'] = $edit['Pekerjaan_Wali'];
                                                                            array_unshift($list_data_pengaturan_pekerjaan_orang_tua_ini, $edit_data_sebelum_Pekerjaan_Wali);
                                                                            $list_data_pengaturan_pekerjaan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_pekerjaan_orang_tua_ini, "Pekerjaan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_pekerjaan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_pekerjaan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Pekerjaan_Wali'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit['Pekerjaan_Wali'] == $data['Pekerjaan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Pekerjaan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Pekerjaan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Penghasilan </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select class="form-control" name="Penghasilan_Wali">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_penghasilan_orang_tua_ini = $list_data_pengaturan_penghasilan_orang_tua;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Penghasilan_Wali'] <> "") {
                                                                            $edit_data_sebelum_Penghasilan_Wali['Id_Pengaturan_Penghasilan_Orang_Tua'] = '0';
                                                                            $edit_data_sebelum_Penghasilan_Wali['Penghasilan_Orang_Tua'] = $edit['Penghasilan_Wali'];
                                                                            array_unshift($list_data_pengaturan_penghasilan_orang_tua_ini, $edit_data_sebelum_Penghasilan_Wali);
                                                                            $list_data_pengaturan_penghasilan_orang_tua_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_penghasilan_orang_tua_ini, "Penghasilan_Orang_Tua");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_penghasilan_orang_tua_ini))) {
                                                                        foreach ($list_data_pengaturan_penghasilan_orang_tua_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Penghasilan_Wali'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit['Penghasilan_Wali'] == $data['Penghasilan_Orang_Tua']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Penghasilan_Orang_Tua'] ?>">
                                                                                <?php echo $data['Penghasilan_Orang_Tua'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>

                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> NIK Wali </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="NIK_Wali" type="text" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['NIK_Wali'] <> "")) {
                                                                               echo $edit['NIK_Wali'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> No. HP Wali </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="No_HP_Wali" type="number" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['No_HP_Wali'] <> "")) {
                                                                               echo $edit['No_HP_Wali'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="2" class="bg-light">
                                                            <h2><b>Informasi Alamat</b></h2>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Provinsi* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select required style="<?php if (!isset($_GET['edit'])) {
                                                                    echo 'display:none';
                                                                } ?>" id="Id_Provinsi" name="Id_Provinsi"
                                                                    onclick="Cek_Validasi_Provinsi();"
                                                                    onchange="Rubah_Provinsi();" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                    <?php if (isset($_GET['edit'])) { ?>
                                                                        <option value="<?php if ((isset($edit)) and ($edit['Id_Provinsi'] <> "")) {
                                                                            echo $edit['Id_Provinsi'];
                                                                        } ?>">
                                                                            <?php if ((isset($edit)) and ($edit['Provinsi'] <> "")) {
                                                                                echo $edit['Provinsi'];
                                                                            } ?>
                                                                        </option>
                                                                    <?php } else { ?>
                                                                        <option value="">Pilih Provinsi</option>
                                                                    <?php } ?>
                                                                </select>
                                                                <input style="background:#fff; <?php if (isset($_GET['edit'])) {
                                                                    echo 'display:none';
                                                                } ?>" name="Provinsi" id="Provinsi" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Provinsi'] <> "")) {
                                                                               echo $edit['Provinsi'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kota/Kab* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select required style="<?php if (!isset($_GET['edit'])) {
                                                                    echo 'display:none';
                                                                } ?>" id="Id_Kota" name="Id_Kota"
                                                                    onclick="Cek_Validasi_Kota();" onchange="Rubah_Kota()"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                    <?php if (isset($_GET['edit'])) { ?>
                                                                        <option value="<?php if ((isset($edit)) and ($edit['Id_Kota'] <> "")) {
                                                                            echo $edit['Id_Kota'];
                                                                        } ?>">
                                                                            <?php if ((isset($edit)) and ($edit['Kota'] <> "")) {
                                                                                echo $edit['Kota'];
                                                                            } ?>
                                                                        </option>
                                                                    <?php } else { ?>
                                                                        <option value="">Pilih Kota/Kab</option>
                                                                    <?php } ?>
                                                                </select>
                                                                <input style="background:#fff; <?php if (isset($_GET['edit'])) {
                                                                    echo 'display:none';
                                                                } ?>" name="Kota" id="Kota" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Kota'] <> "")) {
                                                                               echo $edit['Kota'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kecamatan* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select required style="<?php if (!isset($_GET['edit'])) {
                                                                    echo 'display:none';
                                                                } ?>" id="Id_Kecamatan" name="Id_Kecamatan"
                                                                    onclick="Cek_Validasi_Kecamatan();"
                                                                    onchange="Rubah_Kecamatan()" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                    <?php if (isset($_GET['edit'])) { ?>
                                                                        <option value="<?php if ((isset($edit)) and ($edit['Id_Kecamatan'] <> "")) {
                                                                            echo $edit['Id_Kecamatan'];
                                                                        } ?>">
                                                                            <?php if ((isset($edit)) and ($edit['Kecamatan'] <> "")) {
                                                                                echo $edit['Kecamatan'];
                                                                            } ?>
                                                                        </option>
                                                                    <?php } else { ?>
                                                                        <option value="">Pilih Kecamatan</option>
                                                                    <?php } ?>
                                                                </select>
                                                                <input style="background:#fff; <?php if (isset($_GET['edit'])) {
                                                                    echo 'display:none';
                                                                } ?>" name="Kecamatan" id="Kecamatan" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Kecamatan'] <> "")) {
                                                                               echo $edit['Kecamatan'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kelurahan* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select required style="<?php if (!isset($_GET['edit'])) {
                                                                    echo 'display:none';
                                                                } ?>" id="Id_Kelurahan" name="Id_Kelurahan"
                                                                    onclick="Cek_Validasi_Kelurahan();"
                                                                    onchange="Rubah_Kelurahan()" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> disabled <?php } ?>>
                                                                    <?php if (isset($_GET['edit'])) { ?>
                                                                        <option value="<?php if ((isset($edit)) and ($edit['Id_Kelurahan'] <> "")) {
                                                                            echo $edit['Id_Kelurahan'];
                                                                        } ?>">
                                                                            <?php if ((isset($edit)) and ($edit['Kelurahan'] <> "")) {
                                                                                echo $edit['Kelurahan'];
                                                                            } ?>
                                                                        </option>
                                                                    <?php } else { ?>
                                                                        <option value="">Pilih Kelurahan</option>
                                                                    <?php } ?>
                                                                </select>
                                                                <input style="background:#fff; <?php if (isset($_GET['edit'])) {
                                                                    echo 'display:none';
                                                                } ?>" name="Kelurahan" id="Kelurahan" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Kelurahan'] <> "")) {
                                                                               echo $edit['Kelurahan'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <script>
                                                        <?php if (isset($_GET['edit'])) { ?>
                                                            Ambil_Data_Provinsi('Edit');
                                                            Ambil_Data_Kota('Edit');
                                                            Ambil_Data_Kecamatan('Edit');
                                                            Ambil_Data_Kelurahan('Edit');
                                                        <?php } else { ?>
                                                            Ambil_Data_Provinsi();
                                                            Ambil_Data_Kota();
                                                            Ambil_Data_Kecamatan();
                                                            Ambil_Data_Kelurahan();
                                                        <?php } ?>

                                                        //AMBIL DATA PROVINSI DARI API
                                                        function Ambil_Data_Provinsi(Tipe = '') {

                                                            console.log('<?php echo $Link_Website ?>vendor/api-wilayah-indonesia/api/villages/' + '.json');


                                                            const selectElement = document.getElementById('Id_Provinsi');

                                                            fetch('<?php echo $Link_Website ?>vendor/api-wilayah-indonesia/api/provinces.json')
                                                                .then(response => response.json())
                                                                .then(data => {
                                                                    <?php if (isset($_GET['edit'])) { ?>
                                                                        if (Tipe == "Edit") {
                                                                            selectElement.innerHTML = '<option value="<?php if ((isset($edit)) and ($edit['Id_Provinsi'] <> "")) {
                                                                                echo $edit['Id_Provinsi'];
                                                                            } ?>"><?php if ((isset($edit)) and ($edit['Provinsi'] <> "")) {
                                                                                 echo $edit['Provinsi'];
                                                                             } ?></option>';
                                                                        } else {
                                                                            selectElement.innerHTML = '<option value="">Pilih Provinsi</option>';
                                                                        }
                                                                    <?php } else { ?>
                                                                        selectElement.innerHTML = '<option value="">Pilih Provinsi</option>';
                                                                    <?php } ?>

                                                                    data.forEach(district => {
                                                                        const option = document.createElement('option');
                                                                        option.value = district.id;
                                                                        option.textContent = district.name;
                                                                        selectElement.appendChild(option);
                                                                    });
                                                                })
                                                                .catch(error => {
                                                                    console.error('Error fetching data:', error);
                                                                });
                                                        }

                                                        //AMBIL DATA KOTA DARI API
                                                        function Ambil_Data_Kota(Tipe = '') {
                                                            const selectElement = document.getElementById('Id_Kota');
                                                            var Id_Provinsi = document.getElementById('Id_Provinsi').value;

                                                            fetch('<?php echo $Link_Website ?>vendor/api-wilayah-indonesia/api/regencies/' + Id_Provinsi + '.json')
                                                                .then(response => response.json())
                                                                .then(data => {
                                                                    <?php if (isset($_GET['edit'])) { ?>
                                                                        if (Tipe == "Edit") {
                                                                            selectElement.innerHTML = '<option value="<?php if ((isset($edit)) and ($edit['Id_Kota'] <> "")) {
                                                                                echo $edit['Id_Kota'];
                                                                            } ?>"><?php if ((isset($edit)) and ($edit['Kota'] <> "")) {
                                                                                 echo $edit['Kota'];
                                                                             } ?></option>';
                                                                        } else {
                                                                            selectElement.innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                                        }
                                                                    <?php } else { ?>
                                                                        selectElement.innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                                    <?php } ?>

                                                                    data.forEach(district => {
                                                                        const option = document.createElement('option');
                                                                        option.value = district.id;
                                                                        option.textContent = district.name;
                                                                        selectElement.appendChild(option);
                                                                    });
                                                                })
                                                                .catch(error => {
                                                                    console.error('Error fetching data:', error);
                                                                });
                                                        }

                                                        //AMBIL DATA KECAMATAN DARI API
                                                        function Ambil_Data_Kecamatan(Tipe = '') {
                                                            const selectElement = document.getElementById('Id_Kecamatan');
                                                            var Id_Kota = document.getElementById('Id_Kota').value;

                                                            fetch('<?php echo $Link_Website ?>vendor/api-wilayah-indonesia/api/districts/' + Id_Kota + '.json')
                                                                .then(response => response.json())
                                                                .then(data => {
                                                                    <?php if (isset($_GET['edit'])) { ?>
                                                                        if (Tipe == "Edit") {
                                                                            selectElement.innerHTML = '<option value="<?php if ((isset($edit)) and ($edit['Id_Kecamatan'] <> "")) {
                                                                                echo $edit['Id_Kecamatan'];
                                                                            } ?>"><?php if ((isset($edit)) and ($edit['Kecamatan'] <> "")) {
                                                                                 echo $edit['Kecamatan'];
                                                                             } ?></option>';
                                                                        } else {
                                                                            selectElement.innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                                        }
                                                                    <?php } else { ?>
                                                                        selectElement.innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                                    <?php } ?>

                                                                    data.forEach(district => {
                                                                        const option = document.createElement('option');
                                                                        option.value = district.id;
                                                                        option.textContent = district.name;
                                                                        selectElement.appendChild(option);
                                                                    });
                                                                })
                                                                .catch(error => {
                                                                    console.error('Error fetching data:', error);
                                                                });
                                                        }

                                                        //AMBIL DATA KELURAHAN DARI API
                                                        function Ambil_Data_Kelurahan(Tipe = '') {
                                                            const selectElement = document.getElementById('Id_Kelurahan');
                                                            var Id_Kecamatan = document.getElementById('Id_Kecamatan').value;

                                                            fetch('<?php echo $Link_Website ?>vendor/api-wilayah-indonesia/api/villages/' + Id_Kecamatan + '.json')
                                                                .then(response => response.json())
                                                                .then(data => {
                                                                    <?php if (isset($_GET['edit'])) { ?>
                                                                        if (Tipe == "Edit") {
                                                                            selectElement.innerHTML = '<option value="<?php if ((isset($edit)) and ($edit['Id_Kelurahan'] <> "")) {
                                                                                echo $edit['Id_Kelurahan'];
                                                                            } ?>"><?php if ((isset($edit)) and ($edit['Kelurahan'] <> "")) {
                                                                                 echo $edit['Kelurahan'];
                                                                             } ?></option>';
                                                                        } else {
                                                                            selectElement.innerHTML = '<option value="">Pilih Kelurahan</option>';
                                                                        }
                                                                    <?php } else { ?>
                                                                        selectElement.innerHTML = '<option value="">Pilih Kelurahan</option>';
                                                                    <?php } ?>

                                                                    data.forEach(district => {
                                                                        const option = document.createElement('option');
                                                                        option.value = district.id;
                                                                        option.textContent = district.name;
                                                                        selectElement.appendChild(option);
                                                                    });
                                                                })
                                                                .catch(error => {
                                                                    console.error('Error fetching data:', error);
                                                                });
                                                        }

                                                        function Rubah_Provinsi() {
                                                            document.getElementById('Id_Kota').innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                            document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                            document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                            document.getElementById('Kota').value = '';
                                                            document.getElementById('Kecamatan').value = '';
                                                            document.getElementById('Kelurahan').value = '';
                                                            Ambil_Data_Kota();

                                                            var select = document.getElementById('Id_Provinsi');
                                                            document.getElementById('Provinsi').value = select.options[select.selectedIndex].innerHTML;
                                                        }

                                                        function Rubah_Kota() {
                                                            document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                            document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                            document.getElementById('Kecamatan').value = '';
                                                            document.getElementById('Kelurahan').value = '';
                                                            Ambil_Data_Kecamatan();

                                                            var select = document.getElementById('Id_Kota');
                                                            document.getElementById('Kota').value = select.options[select.selectedIndex].innerHTML;
                                                        }

                                                        function Rubah_Kecamatan() {
                                                            document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                            document.getElementById('Kelurahan').value = '';
                                                            Ambil_Data_Kelurahan();

                                                            var select = document.getElementById('Id_Kecamatan');
                                                            document.getElementById('Kecamatan').value = select.options[select.selectedIndex].innerHTML;
                                                        }

                                                        function Rubah_Kelurahan() {
                                                            var select = document.getElementById('Id_Kelurahan');
                                                            document.getElementById('Kelurahan').value = select.options[select.selectedIndex].innerHTML;
                                                        }

                                                        function Cek_Validasi_Provinsi() {

                                                        }

                                                        function Cek_Validasi_Kota() {
                                                            if (document.getElementById('Id_Kota').value == '0') {
                                                                alert('Mohon Maaf, Harap Pilih Dari Provinsi Terlebih Dahulu');
                                                                Ambil_Data_Provinsi();
                                                                document.getElementById('Id_Kota').innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                                document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                                document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                                document.getElementById('Id_Kota').value = '';
                                                                document.getElementById('Id_Kecamatan').value = '';
                                                                document.getElementById('Id_Kelurahan').value = '';
                                                                document.getElementById('Kota').value = '';
                                                                document.getElementById('Kecamatan').value = '';
                                                                document.getElementById('Kelurahan').value = '';

                                                                document.getElementById('Id_Provinsi').focus();
                                                            } else {

                                                            }
                                                        }

                                                        function Cek_Validasi_Kecamatan() {
                                                            if (document.getElementById('Id_Kecamatan').value == '0') {
                                                                alert('Mohon Maaf, Harap Pilih Dari Provinsi Terlebih Dahulu');
                                                                Ambil_Data_Provinsi();
                                                                document.getElementById('Id_Kota').innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                                document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                                document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                                document.getElementById('Id_Kota').value = '';
                                                                document.getElementById('Id_Kecamatan').value = '';
                                                                document.getElementById('Id_Kelurahan').value = '';
                                                                document.getElementById('Kota').value = '';
                                                                document.getElementById('Kecamatan').value = '';
                                                                document.getElementById('Kelurahan').value = '';

                                                                document.getElementById('Id_Provinsi').focus();
                                                            } else {

                                                            }
                                                        }

                                                        function Cek_Validasi_Kelurahan() {
                                                            if (document.getElementById('Id_Kelurahan').value == '0') {
                                                                alert('Mohon Maaf, Harap Pilih Dari Provinsi Terlebih Dahulu');
                                                                Ambil_Data_Provinsi();
                                                                document.getElementById('Id_Kota').innerHTML = '<option value="">Pilih Kota/Kab</option>';
                                                                document.getElementById('Id_Kecamatan').innerHTML = '<option value="">Pilih Kecamatan</option>';
                                                                document.getElementById('Id_Kelurahan').innerHTML = '<option value="">Pilih Kelurahan</option>';

                                                                document.getElementById('Id_Kota').value = '';
                                                                document.getElementById('Id_Kecamatan').value = '';
                                                                document.getElementById('Id_Kelurahan').value = '';
                                                                document.getElementById('Kota').value = '';
                                                                document.getElementById('Kecamatan').value = '';
                                                                document.getElementById('Kelurahan').value = '';

                                                                document.getElementById('Id_Provinsi').focus();
                                                            } else {

                                                            }
                                                        }
                                                    </script>

                                                    <tr>
                                                        <td style="width:30%"> Alamat Lengkap* </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input required style="background:#fff;" name="Jalan"
                                                                    type="text" class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?>
                                                                    value="<?php if ((isset($edit)) and ($edit['Jalan'] <> "")) {
                                                                        echo $edit['Jalan'];
                                                                    } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Kode Pos </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Kode_Pos" type="number" class="form-control"
                                                                    <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Kode_Pos'] <> "")) {
                                                                               echo $edit['Kode_Pos'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Lintang Koordinat </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Titik_Lintang_Alamat" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Titik_Lintang_Alamat'] <> "")) {
                                                                               echo $edit['Titik_Lintang_Alamat'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Lintang Koordinat </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Titik_Lintang_Alamat" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Titik_Lintang_Alamat'] <> "")) {
                                                                               echo $edit['Titik_Lintang_Alamat'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Bujur </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Titik_Bujur_Alamat" type="text"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Titik_Bujur_Alamat'] <> "")) {
                                                                               echo $edit['Titik_Bujur_Alamat'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Jarak Rumah ke Sekolah (Km) </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <input name="Jarak_Rumah_Ke_Sekolah" type="number"
                                                                    class="form-control" <?php if (!(isset($_GET['edit']))) { ?> readonly <?php } ?> value="<?php if ((isset($edit)) and ($edit['Jarak_Rumah_Ke_Sekolah'] <> "")) {
                                                                               echo $edit['Jarak_Rumah_Ke_Sekolah'];
                                                                           } ?>">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width:30%"> Alat Transportasi </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select class="form-control"
                                                                    name="Kendaraan_Yang_Dipakai_Kesekolah">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini = $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah;
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Kendaraan_Yang_Dipakai_Kesekolah'] <> "") {
                                                                            $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah['Id_Pengaturan_Nama_Kendaraan'] = '0';
                                                                            $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah['Nama_Kendaraan'] = $edit['Kendaraan_Yang_Dipakai_Kesekolah'];
                                                                            array_unshift($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini, $edit_data_sebelum_Kendaraan_Yang_Dipakai_Kesekolah);
                                                                            $list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini, "Nama_Kendaraan");
                                                                        }
                                                                    }
                                                                    if ((isset($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini))) {
                                                                        foreach ($list_data_pengaturan_kendaraan_yang_dipakai_kesekolah_ini as $data) {
                                                                            ?>
                                                                            <option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                                                if ($_POST['Kendaraan_Yang_Dipakai_Kesekolah'] == $data['Nama_Kendaraan']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } elseif (isset($_GET['edit'])) {
                                                                                if ($edit['Kendaraan_Yang_Dipakai_Kesekolah'] == $data['Nama_Kendaraan']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ;
                                                                            } ?>
                                                                                value="<?php echo $data['Nama_Kendaraan'] ?>">
                                                                                <?php echo $data['Nama_Kendaraan'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr class="bg-light">
                                                        <th colspan="2">
                                                            <h2><b>Status Verifikasi</b></h2>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%"> Status Verifikasi </td>
                                                        <td style="width:70%">
                                                            <div class="form-group">
                                                                <select required class="form-control"
                                                                    name="Status_Verifikasi_Data_Diri">
                                                                    <?php
                                                                    $array_option = ["", "Sudah Diverifikasi", "Verifikasi Ditolak", "Belum Diverifikasi"];
                                                                    $array_option_terpilih = [];
                                                                    if (isset($_GET['edit'])) {
                                                                        if ($edit['Status_Verifikasi_Data_Diri'] <> "") {
                                                                            $array_option_terpilih[] = $edit['Status_Verifikasi_Data_Diri'];
                                                                        } else {
                                                                            $array_option_terpilih[] = "";
                                                                        }
                                                                    }
                                                                    foreach ($array_option as $option) {
                                                                        ?>
                                                                        <option <?php
                                                                        if (isset($_GET['edit'])) {
                                                                            foreach ($array_option_terpilih as $option_terpilih) {
                                                                                if ($option_terpilih == $option) {
                                                                                    echo "selected";
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                        ?> value="<?php echo $option ?>">
                                                                            <?php echo $option ?>
                                                                        </option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>



                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div style="text-align: center;">
                                    <?php if (isset($_GET["tambah"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
                                    <?php } elseif (isset($_GET["edit"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE">
                                    <?php } ?>
                                    <input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'"
                                        class="btn btn-danger" value="BATAL">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>

        <?php
        //FORM LIST DATA
        if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">List Verifikasi Data Diri
                                <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
                                    echo "(" . $_GET['filter_status'] . ")";
                                } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="<?php echo $kehalaman ?>&tambah" class="btn btn-sm btn-primary"
                                        style="display:none"><i class="fas fa-plus-circle"></i> &nbsp; Tambah Baru</a>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12 d-none" style="text-align: right;">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a
                                                href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (
                                                <?php echo $hitung_Aktif ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a
                                                href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (
                                                <?php echo $hitung_Terarsip ?>)
                                            </a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a
                                                href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (
                                                <?php echo $hitung_Terhapus ?>)
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table id="basic-datatables" class="table table-borderless" style="width:100%">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Pendaftaran</th>
                                            <th>Username</th>
                                            <th>Nama Lengkap</th>
                                            <th>NIS</th>
                                            <th>NISN</th>
                                            <th>Status Verifikasi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ((isset($list_datatable_master))) {
                                            foreach ($list_datatable_master as $data) {
                                                $nomor++; ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $nomor ?>
                                                    </td>
                                                    <td>
                                                        <a
                                                            href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>">
                                                            <?php echo $data['Nomor_Pendaftaran'] ?>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['Username'] ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['Nama_Lengkap'] ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['NIS'] ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $data['NISN'] ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($data['Status_Verifikasi_Data_Diri'] == "Sudah Diverifikasi") { ?>
                                                            <span class="badge badge-success">
                                                                <?php echo $data['Status_Verifikasi_Data_Diri'] ?>
                                                            </span>
                                                        <?php } else if ($data['Status_Verifikasi_Data_Diri'] == "Sedang Diverifikasi") { ?>
                                                                <span class="badge badge-warning">
                                                                <?php echo $data['Status_Verifikasi_Data_Diri'] ?>
                                                                </span>
                                                        <?php } else { ?>
                                                                <span class="badge badge-danger">
                                                                <?php echo $data['Status_Verifikasi_Data_Diri'] ?>
                                                                </span>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <?php if ($data['Status_Verifikasi_Data_Diri'] == "Sudah Diverifikasi") { ?>
                                                                <a href="#"
                                                                    onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>','Belum Diverifikasi')"><button
                                                                        class="btn btn-xs btn-outline-danger"><i
                                                                            class="fas fa-undo"></i></button></a>
                                                            <?php } else { ?>
                                                                &nbsp;
                                                                <a href="#"
                                                                    onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>','Sudah Diverifikasi')"><button
                                                                        class="btn btn-xs btn-outline-success"><i
                                                                            class="fas fa-check"></i></button></a>
                                                            <?php } ?> &nbsp;

                                                            <a
                                                                href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>"><button
                                                                    class="btn btn-xs btn-outline-warning"><i
                                                                        class="fas fa-edit"></i></button></a>
                                                            <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] == "Terhapus")) { ?>
                                                                &nbsp;
                                                                <a href="#"
                                                                    onclick="datatable_konfirmasi_restore_data_dari_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>')"><button
                                                                        class="btn btn-xs btn-outline-success"><i
                                                                            class="fas fa-redo"></i></button></a>
                                                                &nbsp;
                                                                <a href="#"
                                                                    onclick="datatable_konfirmasi_hapus_data_permanen('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>')"><button
                                                                        class="btn btn-xs btn-outline-danger"><i
                                                                            class="fa fa-trash"></i></button></a>
                                                            <?php } else { ?>
                                                                &nbsp;
                                                                <a href="#"
                                                                    onclick="datatable_konfirmasi_hapus_data_ke_tong_sampah('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>')"><button
                                                                        class="btn btn-xs btn-outline-danger"><i
                                                                            class="fa fa-trash"></i></button></a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    function datatable_konfirmasi_update_status_verifikasi(id_primary, status_verifikasi) {
                                        var txt;
                                        if (status_verifikasi == "Sudah Diverifikasi") {
                                            var r = confirm("Apakah Anda Yakin Ingin Mengubah Status Verifikasi Menjadi 'Sudah Diverifikasi' Pada Data Ini ?");
                                        } else {
                                            var r = confirm("Apakah Anda Yakin Ingin Mengubah Status Verifikasi Menjadi 'Belum Diverifikasi' Pada Data Ini ?");
                                        }
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&update_status_verifikasi=' + status_verifikasi + '&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_permanen(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
                                        } else {

                                        }
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<script>
    const inputFieldNamaLengkap = document.getElementById("nama_lengkap");
    inputFieldNamaLengkap.addEventListener("input", function (event) {
        event.preventDefault();
        const inputValue = inputFieldNamaLengkap.value.replace(/[^a-zA-Z0-9\s-]/g, ''); // Allow alphabetic characters, numbers, spaces, and dashes
        const formattedValue = inputValue.toUpperCase().replace(/\s/g, ' '); // Convert to uppercase and replace spaces with underscores
        inputFieldNamaLengkap.value = formattedValue;
    });
</script>

<script>
    const inputFieldTempatLahir = document.getElementById("tempat_lahir");
    inputFieldTempatLahir.addEventListener("input", function (event) {
        event.preventDefault();
        const inputValue = inputFieldTempatLahir.value.replace(/[^a-zA-Z0-9\s-]/g, ''); // Allow alphabetic characters, numbers, spaces, and dashes
        const formattedValue = inputValue.toUpperCase().replace(/\s/g, ' '); // Convert to uppercase and replace spaces with underscores
        inputFieldTempatLahir.value = formattedValue;
    });
</script>

<script>
    const emailInput = document.getElementById("email");
    emailInput.addEventListener("input", function (event) {
        event.preventDefault();
        emailInput.value = emailInput.value.toLowerCase();
    });
</script>

<script>
    function ubah_ukuran_seragam() {
        var inputan_ukuran_seragam = document.getElementById('ukuran_seragam').value;
        var text_inputan_ukuran_seragam = document.getElementById('text_inputan_ukuran_seragam');
        var div_inputan_ukuran_seragam = document.getElementById('div_inputan_ukuran_seragam');

        if (inputan_ukuran_seragam === "Lainnya") {
            div_inputan_ukuran_seragam.style.display = "";
            text_inputan_ukuran_seragam.required = true;
        } else {
            text_inputan_ukuran_seragam.required = false;
            div_inputan_ukuran_seragam.style.display = "none";
        }
    }
</script>