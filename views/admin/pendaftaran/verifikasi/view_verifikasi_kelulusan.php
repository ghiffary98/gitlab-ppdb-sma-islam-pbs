<?php
include "models/global/data_pendaftar_verifikasi_berkas/model_data_pendaftar_verifikasi_berkas.php";
include "models/global/data_pendaftar_pembelian_formulir/model_data_pendaftar_pembelian_formulir.php";
include "models/global/data_pendaftar_pembayaran_ppdb/model_data_pendaftar_pembayaran_ppdb.php";
include "models/global/data_pendaftar_program_layanan/model_data_pendaftar_program_layanan.php";
include "models/global/data_pendaftar_nilai_rapor/model_data_pendaftar_nilai_rapor.php";
include "models/global/data_pendaftar/model_data_pendaftar.php";
include "models/global/data_siswa/model_data_siswa.php";
include "models/global/data_siswa_nilai_rapor/model_data_siswa_nilai_rapor.php";
include "models/global/pengumuman_pendaftar/model_pengumuman_pendaftar.php";
include "controllers/global/pengumuman_pendaftar/controller_pengumuman_pendaftar.php";
include "controllers/admin/pendaftaran/verifikasi/controller_verifikasi_kelulusan.php";
?>
<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Verifikasi Kelulusan</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="?menu=home">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo "?menu=" . $_GET['menu'] ?>">Verifikasi Kelulusan</a>
				</li>

				<?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<?php if (isset($_GET['tambah'])) { ?>
							<a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
						<?php } else { ?>
							<a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
						<?php } ?>
					</li>
				<?php } ?>

			</ul>
		</div>

		<?php
		//FORM LIST DATA
		if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
		?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="card-title">List Verifikasi Kelulusan
								<?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
									echo "(" . $_GET['filter_status'] . ")";
								} ?>
							</div>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="basic-datatables" class="table table-borderless" style="width:100%">
									<thead class="bg-light">
										<tr>
											<th>No</th>
											<th>Nomor Pendaftaran</th>
											<th>NISN</th>
											<th>Nama Lengkap</th>
											<th>Pembelian Formulir</th>
											<th>Verifikasi Data Diri</th>
											<th>Verifikasi Nilai Rapor</th>
											<th>Verifikasi Program Layanan</th>
											<th>Verifikasi Berkas</th>
											<th>Pembayaran PPDB</th>
											<th>Status Kelulusan</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ((isset($list_datatable_master))) {
											foreach ($list_datatable_master as $data) {
												$nomor++; ?>
												<?php
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_pembelian_formulir => Id_Pendaftar => Status_Verifikasi_Pembelian_Formulir
												if (isset($array_result_relasi_data_pendaftar_pembelian_formulir)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Pembelian_Formulir'])) {
														$data['Status_Verifikasi_Pembelian_Formulir'] = $array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Pembelian_Formulir'];
													} else {
														$data['Status_Verifikasi_Pembelian_Formulir'] = "";
													}
												} else {
													$data['Status_Verifikasi_Pembelian_Formulir'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_pembelian_formulir => Id_Pendaftar => Status_Verifikasi_Pembelian_Formulir

												//BACA DATA RELASI ARRAY TABLE data_pendaftar_pembelian_formulir => Id_Pendaftar => Id_Pendaftar_Pembelian_Formulir
												if (isset($array_result_relasi_data_pendaftar_pembelian_formulir)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Pembelian_Formulir'])) {
														$data['Id_Pendaftar_Pembelian_Formulir'] = $array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Pembelian_Formulir'];
													} else {
														$data['Id_Pendaftar_Pembelian_Formulir'] = "";
													}
												} else {
													$data['Id_Pendaftar_Pembelian_Formulir'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_pembelian_formulir => Id_Pendaftar => Id_Pendaftar_Pembelian_Formulir


												//BACA DATA RELASI ARRAY TABLE data_pendaftar_verifikasi_berkas => Id_Pendaftar => Status_Verifikasi_Berkas
												if (isset($array_result_relasi_data_pendaftar_verifikasi_berkas)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Berkas'])) {
														$data['Status_Verifikasi_Berkas'] = $array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Berkas'];
													} else {
														$data['Status_Verifikasi_Berkas'] = "";
													}
												} else {
													$data['Status_Verifikasi_Berkas'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_verifikasi_berkas => Id_Pendaftar => Status_Verifikasi_Berkas

												//BACA DATA RELASI ARRAY TABLE data_pendaftar_verifikasi_berkas => Id_Pendaftar => Id_Pendaftar_Verifikasi_Berkas
												if (isset($array_result_relasi_data_pendaftar_verifikasi_berkas)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Verifikasi_Berkas'])) {
														$data['Id_Pendaftar_Verifikasi_Berkas'] = $array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Verifikasi_Berkas'];
													} else {
														$data['Id_Pendaftar_Verifikasi_Berkas'] = "";
													}
												} else {
													$data['Id_Pendaftar_Verifikasi_Berkas'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_verifikasi_berkas => Id_Pendaftar => Id_Pendaftar_Verifikasi_Berkas

												//BACA DATA RELASI ARRAY TABLE data_pendaftar_pembayaran_ppdb => Id_Pendaftar => Status_Verifikasi_Pembayaran_PPDB
												if (isset($array_result_relasi_data_pendaftar_pembayaran_ppdb)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Pembayaran_PPDB'])) {
														$data['Status_Verifikasi_Pembayaran_PPDB'] = $array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Pembayaran_PPDB'];
													} else {
														$data['Status_Verifikasi_Pembayaran_PPDB'] = "";
													}
												} else {
													$data['Status_Verifikasi_Pembayaran_PPDB'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_pembayaran_ppdb => Id_Pendaftar => Status_Verifikasi_Pembayaran_PPDB

												//BACA DATA RELASI ARRAY TABLE data_pendaftar_pembayaran_ppdb => Id_Pendaftar => Id_Pendaftar_Pembayaran_PPDB
												if (isset($array_result_relasi_data_pendaftar_pembayaran_ppdb)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Pembayaran_PPDB'])) {
														$data['Id_Pendaftar_Pembayaran_PPDB'] = $array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Pembayaran_PPDB'];
													} else {
														$data['Id_Pendaftar_Pembayaran_PPDB'] = "";
													}
												} else {
													$data['Id_Pendaftar_Pembayaran_PPDB'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_pembayaran_ppdb => Id_Pendaftar => Id_Pendaftar_Pembayaran_PPDB

												//BACA DATA RELASI ARRAY TABLE data_pendaftar_nilai_rapor => Id_Pendaftar => Status_Verifikasi_Nilai_Rapor
												if (isset($array_result_relasi_data_pendaftar_nilai_rapor)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Nilai_Rapor'])) {
														$data['Status_Verifikasi_Nilai_Rapor'] = $array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Nilai_Rapor'];
													} else {
														$data['Status_Verifikasi_Nilai_Rapor'] = "";
													}
												} else {
													$data['Status_Verifikasi_Nilai_Rapor'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_nilai_rapor => Id_Pendaftar => Status_Verifikasi_Nilai_Rapor
												
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_nilai_rapor => Id_Pendaftar => Id_Pendaftar_Nilai_Rapor
												if (isset($array_result_relasi_data_pendaftar_nilai_rapor)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Nilai_Rapor'])) {
														$data['Id_Pendaftar_Nilai_Rapor'] = $array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Nilai_Rapor'];
													} else {
														$data['Id_Pendaftar_Nilai_Rapor'] = "";
													}
												} else {
													$data['Id_Pendaftar_Nilai_Rapor'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_nilai_rapor => Id_Pendaftar => Id_Pendaftar_Nilai_Rapor

												//BACA DATA RELASI ARRAY TABLE data_pendaftar_program_layanan => Id_Pendaftar => Status_Verifikasi_Program_Layanan
												if (isset($array_result_relasi_data_pendaftar_program_layanan)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Program_Layanan'])) {
														$data['Status_Verifikasi_Program_Layanan'] = $array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Program_Layanan'];
													} else {
														$data['Status_Verifikasi_Program_Layanan'] = "";
													}
												} else {
													$data['Status_Verifikasi_Program_Layanan'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_program_layanan => Id_Pendaftar => Status_Verifikasi_Program_Layanan

												//BACA DATA RELASI ARRAY TABLE data_pendaftar_program_layanan => Id_Pendaftar => Id_Pendaftar_Program_Layanan
												if (isset($array_result_relasi_data_pendaftar_program_layanan)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Program_Layanan'])) {
														$data['Id_Pendaftar_Program_Layanan'] = $array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Program_Layanan'];
													} else {
														$data['Id_Pendaftar_Program_Layanan'] = "";
													}
												} else {
													$data['Id_Pendaftar_Program_Layanan'] = "";
												}
												//BACA DATA RELASI ARRAY TABLE data_pendaftar_program_layanan => Id_Pendaftar => Id_Pendaftar_Program_Layanan

												//BACA DATA RELASI ARRAY TABLE data_siswa => Id_Pendaftar => Id_Pendaftar
												if (isset($array_result_relasi_data_siswa)) {
													$Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

													if (isset($array_hasil_relasi_data_siswa['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar'])) {
														$Status_Salin_Ke_Data_Siswa_Master = "Sudah";
													} else {
														$Status_Salin_Ke_Data_Siswa_Master = "Belum";
													}
												} else {
													$Status_Salin_Ke_Data_Siswa_Master = "Belum";
												}
												//BACA DATA RELASI ARRAY TABLE data_siswa => Id_Pendaftar => Id_Pendaftar
												?>
												<tr>
													<td>
														<?php echo $nomor ?>
													</td>
													<td><?php echo $data['Nomor_Pendaftaran'] ?></td>
													<td><?php echo $data['NISN'] ?></td>
													<td><?php echo $data['Nama_Lengkap'] ?></td>
													<td>
														<a href="?menu=pendaftar_verifikasi_pembelian_formulir&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Pembelian_Formulir"], "pendaftar_verifikasi_pembelian_formulir"); ?>&url_kembali=<?php echo $a_hash->encode_link_kembali($Link_Sekarang); ?>">
															<?php if ($data['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi") { ?>
																<span class="badge badge-success">
																	<?php echo $data['Status_Verifikasi_Pembelian_Formulir'] ?>
																</span>
															<?php } else if ($data['Status_Verifikasi_Pembelian_Formulir'] == "Sedang Diverifikasi") { ?>
																<span class="badge badge-warning">
																	<?php echo $data['Status_Verifikasi_Pembelian_Formulir'] ?>
																</span>
															<?php } else { ?>
																<span class="badge badge-danger">
																	Belum Diverifikasi
																</span>
															<?php } ?>
														</a>
													</td>
													<td>
														<a href="?menu=pendaftar_verifikasi_data_diri&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar"], "pendaftar_verifikasi_data_diri"); ?>&url_kembali=<?php echo $a_hash->encode_link_kembali($Link_Sekarang); ?>">
															<?php if ($data['Status_Verifikasi_Data_Diri'] == "Sudah Diverifikasi") { ?>
																<span class="badge badge-success">
																	<?php echo $data['Status_Verifikasi_Data_Diri'] ?>
																</span>
															<?php } else if ($data['Status_Verifikasi_Data_Diri'] == "Sedang Diverifikasi") { ?>
																<span class="badge badge-warning">
																	<?php echo $data['Status_Verifikasi_Data_Diri'] ?>
																</span>
															<?php } else { ?>
																<span class="badge badge-danger">
																	Belum Diverifikasi
																</span>
															<?php } ?>
														</a>
													</td>
													<td>
														<a href="?menu=pendaftar_verifikasi_raport&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Nilai_Rapor"], "pendaftar_verifikasi_raport"); ?>&url_kembali=<?php echo $a_hash->encode_link_kembali($Link_Sekarang); ?>">
															<?php if ($data['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") { ?>
																<span class="badge badge-success">
																	<?php echo $data['Status_Verifikasi_Nilai_Rapor'] ?>
																</span>
															<?php } else if ($data['Status_Verifikasi_Nilai_Rapor'] == "Sedang Diverifikasi") { ?>
																<span class="badge badge-warning">
																	<?php echo $data['Status_Verifikasi_Nilai_Rapor'] ?>
																</span>
															<?php } else { ?>
																<span class="badge badge-danger">
																	Belum Diverifikasi
																</span>
															<?php } ?>
														</a>
													</td>

													<td>
														<a href="?menu=pendaftar_verifikasi_layanan&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Program_Layanan"], "pendaftar_verifikasi_layanan"); ?>&url_kembali=<?php echo $a_hash->encode_link_kembali($Link_Sekarang); ?>">
															<?php if ($data['Status_Verifikasi_Program_Layanan'] == "Sudah Diverifikasi") { ?>
																<span class="badge badge-success">
																	<?php echo $data['Status_Verifikasi_Program_Layanan'] ?>
																</span>
															<?php } else if ($data['Status_Verifikasi_Program_Layanan'] == "Sedang Diverifikasi") { ?>
																<span class="badge badge-warning">
																	<?php echo $data['Status_Verifikasi_Program_Layanan'] ?>
																</span>
															<?php } else { ?>
																<span class="badge badge-danger">
																	Belum Diverifikasi
																</span>
															<?php } ?>
														</a>
													</td>

													<td>
														<a href="?menu=pendaftar_verifikasi_berkas&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Verifikasi_Berkas"], "pendaftar_verifikasi_berkas"); ?>&url_kembali=<?php echo $a_hash->encode_link_kembali($Link_Sekarang); ?>">
															<?php if ($data['Status_Verifikasi_Berkas'] == "Sudah Diverifikasi") { ?>
																<span class="badge badge-success">
																	<?php echo $data['Status_Verifikasi_Berkas'] ?>
																</span>
															<?php } else if ($data['Status_Verifikasi_Berkas'] == "Sedang Diverifikasi") { ?>
																<span class="badge badge-warning">
																	<?php echo $data['Status_Verifikasi_Berkas'] ?>
																</span>
															<?php } else { ?>
																<span class="badge badge-danger">
																	Belum Diverifikasi
																</span>
															<?php } ?>
														</a>
													</td>
													<td>
														<a href="?menu=pendaftar_verifikasi_pembayaran_ppdb&edit&id=<?php echo $a_hash->encode($data["Id_Pendaftar_Pembayaran_PPDB"], "pendaftar_verifikasi_pembayaran_ppdb"); ?>&url_kembali=<?php echo $a_hash->encode_link_kembali($Link_Sekarang); ?>">
															<?php if ($data['Status_Verifikasi_Pembayaran_PPDB'] == "Sudah Diverifikasi") { ?>
																<span class="badge badge-success">
																	<?php echo $data['Status_Verifikasi_Pembayaran_PPDB'] ?>
																</span>
															<?php } else if ($data['Status_Verifikasi_Pembayaran_PPDB'] == "Sedang Diverifikasi") { ?>
																<span class="badge badge-warning">
																	<?php echo $data['Status_Verifikasi_Pembayaran_PPDB'] ?>
																</span>
															<?php } else { ?>
																<span class="badge badge-danger">
																	Belum Diverifikasi
																</span>
															<?php } ?>
														</a>
													</td>

													<td>
														<?php if ($data['Status_Kelulusan'] == "Lulus") { ?>
															<span class="badge badge-success">
																<?php echo $data['Status_Kelulusan'] ?>
															</span>
														<?php } else if ($data['Status_Kelulusan'] == "Tidak Lulus") { ?>
															<span class="badge badge-danger">
																<?php echo $data['Status_Kelulusan'] ?>
															</span>
															<?php } else if ($data['Status_Kelulusan'] == "Gugur") { ?>
															<span class="badge badge-danger">
																<?php echo $data['Status_Kelulusan'] ?>
															</span>
														<?php } else { ?>
															<span class="badge badge-warning">
																Menunggu Hasil
															</span>
														<?php } ?>
													</td>


													<td>

														<div class="d-flex">
															<?php if (
																($data['Status_Verifikasi_Pembelian_Formulir'] <> "Sudah Diverifikasi") or
																($data['Status_Verifikasi_Data_Diri'] <> "Sudah Diverifikasi") or
																($data['Status_Verifikasi_Nilai_Rapor'] <> "Sudah Diverifikasi") or
																($data['Status_Verifikasi_Program_Layanan'] <> "Sudah Diverifikasi") or
																($data['Status_Verifikasi_Berkas'] <> "Sudah Diverifikasi") or
																($data['Status_Verifikasi_Pembayaran_PPDB'] <> "Sudah Diverifikasi")
															) {
															?>
																<?php if(($data['Status_Kelulusan'] == "Gugur")){?>
																	&nbsp;
																	<a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>','Menunggu Hasil Lulus')"><button class="btn btn-xs btn-outline-danger"><i class="fas fa-undo"></i></button></a>
																<?php }else{ ?>
																	&nbsp;
																	<button class="btn btn-xs btn-outline-success" type="button" onclick="alert('Harap Verifikasi Semua Data Terlebih Dahulu')"><i class="fas fa-check"></i></button>
																<?php } ?>
															<?php } else if (($data['Status_Kelulusan'] == "Lulus") OR ($data['Status_Kelulusan'] == "Tidak Lulus") OR ($data['Status_Kelulusan'] == "Gugur")) { ?>
																&nbsp;
																<a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>','Menunggu Hasil Lulus')"><button class="btn btn-xs btn-outline-danger"><i class="fas fa-undo"></i></button></a>
															<?php } else { ?>
																&nbsp;
																<a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>','Lulus')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-check"></i></button></a>
																&nbsp;
																<a href="#" onclick="datatable_konfirmasi_update_status_verifikasi('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>','Tidak Lulus')"><button class="btn btn-xs btn-outline-danger"><i class="fas fa-minus"></i></button></a>
															<?php } ?>

															<?php if ($data['Status_Kelulusan'] == "Lulus") { ?>
																&nbsp;
																<?php if ($Status_Salin_Ke_Data_Siswa_Master == "Sudah") { ?>
																	<button class="btn btn-xs btn-success" type="button">Data Ini Sudah di Salin Ke Data Siswa (Master)</button>
																<?php } else { ?>
																	<a href="#" onclick="datatable_konfirmasi_salin_ke_data_siswa_master('<?php echo $a_hash->encode($data["Id_Pendaftar"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-primary">Salin Data Ke Data Siswa (Master)</button></a>
																<?php } ?>
															<?php } ?>
														</div>

													</td>
												</tr>
											<?php } ?>
										<?php } ?>
									</tbody>
								</table>
								<script type="text/javascript">
									function datatable_konfirmasi_update_status_verifikasi(id_primary, status_verifikasi) {
										var txt;
										if (status_verifikasi == "Lulus") {
											var r = confirm("Apakah Anda Yakin Ingin Mengubah Status Kelulusan Menjadi 'Lulus' Pada Data Ini ?");
										} else if (status_verifikasi == "Tidak Lulus") {
											var r = confirm("Apakah Anda Yakin Ingin Mengubah Status Kelulusan Menjadi 'Tidak Lulus' Pada Data Ini ?");
										} else {
											var r = confirm("Apakah Anda Yakin Ingin Mengubah Status Kelulusan Menjadi 'Menunggu Hasil' Pada Data Ini ?");
										}
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&update_status_verifikasi=' + status_verifikasi + '&id=' + id_primary
										} else {

										}
									}

									function datatable_konfirmasi_salin_ke_data_siswa_master(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Salin Data Ini Ke Siswa Master ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&salin_ke_data_siswa_master&id=' + id_primary
										} else {

										}
									}

									function datatable_konfirmasi_hapus_data_permanen(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
										} else {

										}
									}

									function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
										} else {

										}
									}

									function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
										} else {

										}
									}
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>