<?php
include "models/global/data_pendaftar/model_data_pendaftar.php";

include "models/global/data_pengumuman/model_data_pengumuman.php";
include "models/global/data_pendaftar_pembayaran_ppdb/model_data_pendaftar_pembayaran_ppdb.php";
include "models/global/data_pendaftar_pembelian_formulir/model_data_pendaftar_pembelian_formulir.php";
include "models/global/data_pendaftar_verifikasi_berkas/model_data_pendaftar_verifikasi_berkas.php";
include "controllers/admin/home/controller_home.php";
?>
<div class="content">
    <div class="panel-header bg-primary-gradient">
        <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                <div>
                    <h1 class="text-white pb-2 fw-bold">Dashboard</h1>
                    <h1 class="text-white op-7 mb-2">PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="page-inner mt--5">
        <div class="row mt--2">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-body">
                        <div class="alert <?php if ($data_ppdb_saat_ini['Status_PPDB'] == "Aktif") { ?> alert-success <?php } else { ?> alert-danger <?php } ?> alert-dismissible"
                            role="alert">
                            <form action="" method="post">
                                <div class="row form-group">
                                    <div class="col-lg-8 text-left">
                                        <h3>
                                            <?php if ($data_ppdb_saat_ini['Status_PPDB'] == "Aktif") { ?>
                                                <font class="text-success"><strong>Status Pendaftaran
                                                        "<?php echo $data_ppdb_saat_ini['Judul'] ?>" MASIH DIBUKA</strong>
                                                </font>. <br>Terakhir diubah
                                                <?php echo tanggal_dan_waktu_24_jam_indonesia($data_ppdb_saat_ini['Waktu_Update_Data']) ?>
                                            <?php } else { ?>
                                                <font class="text-danger"><strong>Status Pendaftaran
                                                        "<?php echo $data_ppdb_saat_ini['Judul'] ?>" SUDAH DITUTUP</strong>
                                                </font>. <br>Terakhir diubah
                                                <?php echo tanggal_dan_waktu_24_jam_indonesia($data_ppdb_saat_ini['Waktu_Update_Data']) ?>
                                            <?php } ?>
                                        </h3>
                                    </div>
                                    <div class="col-lg-4 text-left">
                                        <?php if ($data_ppdb_saat_ini['Status_PPDB'] == "Aktif") { ?>
                                            <form method="POST">
                                                <button type="submit" name="Update_Status_PPDB_Menjadi_Tidak_Aktif"
                                                    class="btn btn-block btn-danger"
                                                    onclick="return confirm('Anda yakin ingin menutup PPDB ini?')"> <i
                                                        class="fas fa-times-circle"></i> &nbsp;&nbsp;&nbsp; <b> TUTUP
                                                        Pendaftaran PPDB Online! </b> </button>
                                            </form>
                                        <?php } else { ?>
                                            <form method="POST">
                                                <button type="submit" name="Update_Status_PPDB_Menjadi_Aktif"
                                                    class="btn btn-block btn-warning"
                                                    onclick="return confirm('Anda yakin ingin membuka kembali PPDB ini?')">
                                                    <i class="fas fa-check"></i> &nbsp;&nbsp;&nbsp; <b> BUKA Pendaftaran
                                                        PPDB Online! </b> </button>
                                            </form>
                                        <?php } ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-category">Informasi keseluruhan data PPDB</div>
                        <div class="form-group row">

                            <div class="col-lg-3">
                                <div class="card text-center">
                                    <div class="card-body" id="">
                                        <h3 class="fw-bold mt-3 mb-0">Target</h3>
                                        <div class="mt-3">
                                            <h1 class="card-ttile text-success"> <b> <i class="fas fa-user"></i> &nbsp;
                                                    <?php echo $Total_Target_Siswa ?> </b> </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="card text-center">
                                    <div class="card-body" id="">
                                        <h3 class="fw-bold mt-3 mb-0">Pendaftaran Formulir</h3>
                                        <div class="mt-3">
                                            <h1 class="card-ttile text-info"> <b> <i class="fas fa-user"></i> &nbsp;
                                                    <?php echo $hitung_Total_Pendaftar ?> </b> </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="card text-center">
                                    <div class="card-body" id="">
                                        <h3 class="fw-bold mt-3 mb-0">Lolos Berkas</h3>
                                        <div class="mt-3">
                                            <h1 class="card-ttile text-secondary"> <b> <i
                                                        class="fas fa-clipboard-check"></i> &nbsp;
                                                    <?php echo $hitung_Total_Lolos_Berkas ?> </b> </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="card text-center">
                                    <div class="card-body" id="">
                                        <h3 class="fw-bold mt-3 mb-0">Bayar PPDB</h3>
                                        <div class="mt-3">
                                            <h1 class="card-ttile text-warning"> <b> <i class="fas fa-credit-card"></i>
                                                    &nbsp; <?php echo $hitung_Total_Lunas_PPDB ?> </b> </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="card text-center">
                                    <div class="card-body" id="">
                                        <h3 class="fw-bold mt-3 mb-0">Kekurangan</h3>
                                        <div class="mt-3">
                                            <h1 class="card-ttile text-danger"> <b> <i class="fas fa-credit-card"></i>
                                                    &nbsp; <?php echo $Total_Target_Siswa - $hitung_Total_Lunas_PPDB ?>
                                                </b> </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="card text-center">
                                    <div class="card-body" id="">
                                        <h3 class="fw-bold mt-3 mb-0">Siswa Diterima</h3>
                                        <div class="mt-3">
                                            <h1 class="card-ttile text-success"> <b> <i class="fas fa-user-check"></i>
                                                    &nbsp; <?php echo $hitung_Total_Pendaftar_Diterima ?> </b> </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="card text-center">
                                    <div class="card-body" id="">
                                        <h3 class="fw-bold mt-3 mb-0">Tidak Diterima</h3>
                                        <div class="mt-3">
                                            <h1 class="card-ttile text-danger"> <b> <i class="fas fa-times-circle"></i>
                                                    &nbsp; <?php echo $hitung_Total_Pendaftar_Tidak_Diterima ?> </b>
                                            </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-none">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-head-row">
                            <div class="card-title">Pendaftar</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-container" style="min-height: 375px">
                            <canvas id="statisticsChart"></canvas>
                        </div>
                        <div id="myChartLegend"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-head-row">
                            <div class="card-title">Pembeli Formulir</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-container" style="min-height: 375px">
                            <canvas id="statisticsChart"></canvas>
                        </div>
                        <div id="myChartLegend"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-head-row">
                            <div class="card-title">Lolos Berkas</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-container" style="min-height: 375px">
                            <canvas id="statisticsChart"></canvas>
                        </div>
                        <div id="myChartLegend"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-head-row">
                            <div class="card-title">Lunas PPDB</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-container" style="min-height: 375px">
                            <canvas id="statisticsChart"></canvas>
                        </div>
                        <div id="myChartLegend"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card full-height">
                    <div class="card-header">
                        <div class="card-title">Riwayat Aktivitas Siswa</div>
                    </div>
                    <div class="card-body" style="overflow-x: scroll; height: 500px">
                        <ol class="activity-feed">
                            <?php
                            if ((isset($list_data_aktivitas_pendaftar))) {
                                foreach ($list_data_aktivitas_pendaftar as $data) {
                                    $nomor++; ?>
                                    <?php
                                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
                                    if (isset($array_result_relasi_data_pendaftar)) {
                                        $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                        if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'])) {
                                            $data['Nama_Lengkap'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'];
                                        } else {
                                            $data['Nama_Lengkap'] = "";
                                        }
                                    } else {
                                        $data['Nama_Lengkap'] = "";
                                    }
                                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
                                    ?>
                                    <li class="feed-item feed-item-info">
                                        <time
                                            class="date"><?php echo tanggal_dan_waktu_24_jam_indonesia($data['Waktu_Simpan_Data']) ?></time>
                                        <span
                                            class="text"><b><?php echo $data['Nama_Lengkap'] . " - " . $data['Judul'] ?></b></span>
                                        <br>
                                        <span class="text"><?php echo $data['Deskripsi'] ?></span>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ol>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card full-height">
                    <div class="card-header">
                        <div class="card-head-row">
                            <div class="card-title">Pengumuman</div>
                            <div class="card-tools">
                                <ul class="nav nav-pills nav-secondary nav-pills-no-bd nav-sm" id="pills-tab"
                                    role="tablist">
                                    <li class="nav-item d-none">
                                        <a class="nav-link" id="pills-today" data-toggle="pill" href="#pills-today"
                                            role="tab" aria-selected="true">Today</a>
                                    </li>
                                    <li class="nav-item d-none">
                                        <a class="nav-link active" id="pills-week" data-toggle="pill" href="#pills-week"
                                            role="tab" aria-selected="false">Week</a>
                                    </li>
                                    <li class="nav-item d-none">
                                        <a class="nav-link" id="pills-month" data-toggle="pill" href="#pills-month"
                                            role="tab" aria-selected="false">Month</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="overflow-x: scroll; height: 500px">
                        <?php
                        if ((isset($list_data_pengumuman))) {
                            foreach ($list_data_pengumuman as $data) {
                                $nomor++; ?>
                                <div class="d-flex">
                                    <div class="avatar">
                                        <span class="avatar-title rounded-circle border border-white bg-info"><i
                                                class="fas fa-info"></i></span>
                                    </div>
                                    <div class="flex-1 ml-3 pt-1">
                                        <h6 class="text-uppercase fw-bold mb-1"><?php echo $data['Judul_Pengumuman'] ?></h6>
                                        <span class="text-muted"><?php echo $data['Isi_Pengumuman'] ?></span>
                                    </div>
                                    <div class="float-right pt-1">
                                        <small
                                            class="text-muted"><?php echo tanggal_dan_waktu_24_jam_indonesia($data['Waktu_Simpan_Data']) ?></small>
                                    </div>
                                </div>
                                <div class="separator-dashed"></div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>