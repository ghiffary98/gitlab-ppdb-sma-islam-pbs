<?php
include "models/admin/data_pengguna/model_data_pengguna.php";
include "controllers/admin/pengaturan/data_pengguna/controller_data_pengguna.php";
?>
<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Admin</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="?menu=home">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo "?menu=" . $_GET['menu'] ?>">Admin</a>
				</li>

				<?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<?php if (isset($_GET['tambah'])) { ?>
							<a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
						<?php } else { ?>
							<a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
						<?php } ?>
					</li>
				<?php } ?>

			</ul>
		</div>
		<?php
		//FORM TAMBAH/EDIT DATA
		if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
		?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<?php if (isset($_GET['tambah'])) { ?>
										<div class="card-title">Tambah Data</div>
									<?php } else { ?>
										<div class="card-title">Edit Data</div>
									<?php } ?>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
									<?php if (isset($_GET["edit"])) { ?>
										<script type="text/javascript">
											function konfirmasi_hapus_data_permanen() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_hapus_data_ke_tong_sampah() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_arsip_data() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_restore_data_dari_arsip() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_restore_data_dari_tong_sampah() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}
										</script>
										<ul class="list-inline">
											<li class="list-inline-item">
												<?php if ($edit['Status'] == "Aktif") { ?>
													<a href="#" onclick="konfirmasi_arsip_data()"><i class="fa fa-archive fa-md">
															ARSIPKAN</i></a>
												<?php } elseif ($edit['Status'] == "Terarsip") { ?>
													<a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i class="fa fa-archive fa-md"> AKTIFKAN</i></a>
												<?php } elseif ($edit['Status'] == "Terhapus") { ?>
													<a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i class="fa fa-archive fa-md"> RESTORE</i></a>
												<?php } ?>

											</li>
											<li class="list-inline-item"> | </li>
											<li class="list-inline-item">
												<?php if ($edit['Status'] == "Terhapus") { ?>
													<a href="#" onclick="konfirmasi_hapus_data_permanen()"><i class="fa fa-trash fa-md"> HAPUS </i></a>
												<?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
													<a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i class="fa fa-trash fa-md"> HAPUS </i></a>
												<?php } ?>
											</li>
										</ul>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="card-body">
							<form method="POST" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group row">
											<label class="col-lg-3 control-label">Username</label>
											<div class="col-lg-9">
												<input <?php if (isset($_GET['edit'])) {
															echo "readonly";
														} ?> required type="text" class="form-control" name="Username" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																																	echo $_POST['Username'];
																																} elseif (isset($_GET['edit'])) {
																																	echo $edit['Username'];
																																} ?>" onkeyup="this.value=this.value.replace(/[^A-Za-z0-9]/g,'');">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Password</label>
											<div class="col-lg-9">
												<div class="input-group">
													<input type="password" class="form-control" name="Password" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																															echo $_POST['Password'];
																														} ?>" <?php if (isset($_GET['edit'])) { ?> placeholder="Biarkan kosong jika tidak ingin diganti" <?php } ?>>
													<div class="input-group-append">
														<button type="button" id="togglePassword" class="btn btn-secondary">
															<i class="fa fa-eye"></i>
														</button>
													</div>
												</div>
											</div>
										</div>
										<script>
											document.addEventListener("DOMContentLoaded", function() {
												const passwordInput = document.querySelector('input[name="Password"]');
												const togglePasswordButton = document.getElementById("togglePassword");

												togglePasswordButton.addEventListener("click", function() {
													if (passwordInput.type === "password") {
														passwordInput.type = "text";
														togglePasswordButton.innerHTML = '<i class="fa fa-eye-slash"></i>';
													} else {
														passwordInput.type = "password";
														togglePasswordButton.innerHTML = '<i class="fa fa-eye"></i>';
													}
												});
											});
										</script>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Hak Akses</label>
											<div class="col-lg-9">
												<div class="input-group">
													<select required class="form-control" name="Id_Hak_Akses">
														<option value=""></option>
														<?php
														$list_data_hak_akses_ini = $list_data_hak_akses;
														if (isset($_GET['edit'])) {
															if ($edit['Id_Hak_Akses'] <> "") {
																$list_data_hak_akses_ini = Hapus_Duplikat_Data_Pada_Select_Option($list_data_hak_akses_ini, "Hak_Akses");
															}
														}
														if ((isset($list_data_hak_akses_ini))) {
															foreach ($list_data_hak_akses_ini as $data) {
														?>
																<option <?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																			if ($_POST['Id_Hak_Akses'] == $data['Id_Hak_Akses']) {
																				echo "selected";
																			};
																		} elseif (isset($_GET['edit'])) {
																			if ($edit['Id_Hak_Akses'] == $data['Id_Hak_Akses']) {
																				echo "selected";
																			};
																		} ?> value="<?php echo $data['Id_Hak_Akses'] ?>"><?php echo $data['Hak_Akses'] ?></option>
														<?php
															}
														}
														?>
													</select>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group row">
											<label class="col-lg-3 control-label">Nama Lengkap</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nama_Lengkap" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Nama_Lengkap'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Nama_Lengkap'];
																													} ?>">
											</div>
										</div>

										<div class="form-group row">
											<label class="col-lg-3 control-label">Email</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Email" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																												echo $_POST['Email'];
																											} elseif (isset($_GET['edit'])) {
																												echo $edit['Email'];
																											} ?>" onkeyup="this.value=this.value.replace(/[^A-Za-z0-9@_.-]/g,'');">
											</div>
										</div>

										<div class="form-group row">
											<label class="col-lg-3 control-label">Nomor Handphone</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nomor_Handphone" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																															echo $_POST['Nomor_Handphone'];
																														} elseif (isset($_GET['edit'])) {
																															echo $edit['Nomor_Handphone'];
																														} ?>">
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group row">
											<label class="col-lg-3 control-label">Jenis Kelamin</label>
											<div class="col-lg-9">
												<select class="form-control" name="Jenis_Kelamin">
													<option value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																		echo $_POST['Jenis_Kelamin'];
																	} elseif (isset($_GET['edit'])) {
																		echo $edit['Jenis_Kelamin'];
																	} ?>">
														<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
															echo $_POST['Jenis_Kelamin'];
														} elseif (isset($_GET['edit'])) {
															echo $edit['Jenis_Kelamin'];
														} ?>
													</option>
													<option value="Laki-Laki">Laki-Laki</option>
													<option value="Perempuan">Perempuan</option>
												</select>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-lg-3 control-label">Tempat Lahir</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Tempat_Lahir" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Tempat_Lahir'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Tempat_Lahir'];
																													} ?>">
											</div>
										</div>

										<div class="form-group row">
											<label class="col-lg-3 control-label">Tanggal Lahir</label>
											<div class="col-lg-9">
												<input type="date" class="form-control" name="Tanggal_Lahir" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Tanggal_Lahir'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Tanggal_Lahir'];
																													} ?>">
											</div>
										</div>
									</div>

									<div class="col-lg-6">
										<div class="form-group row">
											<label class="col-lg-3 control-label">Alamat Lengkap</label>
											<div class="col-lg-9">
												<textarea rows="3" class="form-control" name="Alamat_Lengkap"><?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																													echo $_POST['Alamat_Lengkap'];
																												} elseif (isset($_GET['edit'])) {
																													echo $edit['Alamat_Lengkap'];
																												} ?></textarea>
											</div>
										</div>
									</div>

								</div>

								<div style="text-align: center;">
									<?php if (isset($_GET["tambah"])) { ?>
										<input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
									<?php } elseif (isset($_GET["edit"])) { ?>
										<input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE">
									<?php } ?>
									<input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'" class="btn btn-danger" value="BATAL">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>

		<?php
		//FORM LIST DATA
		if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
		?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="card-title">List Admin
								<?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
									echo "(" . $_GET['filter_status'] . ")";
								} ?>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<a href="<?php echo $kehalaman ?>&tambah" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i> &nbsp; Tambah Baru</a>
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
									<ul class="list-inline">
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (
												<?php echo $hitung_Aktif ?>)
											</a></li>
										<li class="list-inline-item"> | </li>
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (
												<?php echo $hitung_Terarsip ?>)
											</a></li>
										<li class="list-inline-item"> | </li>
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (
												<?php echo $hitung_Terhapus ?>)
											</a></li>
									</ul>
								</div>
							</div>
							<br>
							<div class="table-responsive">
								<table id="basic-datatables" class="table table-borderless" style="width:100%">
									<thead class="bg-light">
										<tr>
											<th>No</th>
											<th>Username</th>
											<th>Nama Lengkap</th>
											<th>Email</th>
											<th>Nomor Handphone</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ((isset($list_datatable_master))) {
											foreach ($list_datatable_master as $data) {
												$nomor++; ?>
												<tr>
													<td>
														<?php echo $nomor ?>
													</td>
													<td>
														<a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pengguna"], $_GET['menu']); ?>">
															<?php echo $data['Username'] ?>
														</a>
													</td>
													<td>
														<?php echo $data['Nama_Lengkap'] ?>
													</td>
													<td>
														<?php echo $data['Email'] ?>
													</td>
													<td>
														<?php echo $data['Nomor_Handphone'] ?>
													</td>
													<td>
														<a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Pengguna"], $_GET['menu']); ?>"><button class="btn btn-xs btn-outline-warning"><i class="fas fa-edit"></i></button></a>
														<?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] == "Terhapus")) { ?>
															<a href="#" onclick="datatable_konfirmasi_restore_data_dari_tong_sampah('<?php echo $a_hash->encode($data["Id_Pengguna"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-redo"></i></button></a>

															<a href="#" onclick="datatable_konfirmasi_hapus_data_permanen('<?php echo $a_hash->encode($data["Id_Pengguna"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
														<?php } else { ?>

															<a href="#" <?php if ($data['Nama_Lengkap'] == "Admin") {
																			echo "style='display:none'";
																		} ?> onclick="datatable_konfirmasi_hapus_data_ke_tong_sampah('<?php echo $a_hash->encode($data["Id_Pengguna"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
														<?php } ?>

													</td>
												</tr>
											<?php } ?>
										<?php } ?>
									</tbody>
								</table>
								<script type="text/javascript">
									function datatable_konfirmasi_hapus_data_permanen(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
										} else {

										}
									}

									function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
										} else {

										}
									}

									function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
										} else {

										}
									}
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>