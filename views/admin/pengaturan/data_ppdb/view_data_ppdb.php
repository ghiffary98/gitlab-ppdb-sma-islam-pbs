<?php
include "models/global/data_ppdb_syarat_dan_ketentuan/model_data_ppdb_syarat_dan_ketentuan.php";
include "controllers/admin/pengaturan/data_ppdb/controller_data_ppdb.php";
?>
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">PPDB</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="?menu=home">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="<?php echo "?menu=" . $_GET['menu'] ?>">PPDB</a>
                </li>

                <?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <?php if (isset($_GET['tambah'])) { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
                        <?php } else { ?>
                            <a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
                        <?php } ?>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php
        //FORM TAMBAH/EDIT DATA
        if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <?php if (isset($_GET['tambah'])) { ?>
                                        <div class="card-title">Tambah Data</div>
                                    <?php } else { ?>
                                        <div class="card-title">Edit Data</div>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
                                    <?php if (isset($_GET["edit"])) { ?>
                                        <script type="text/javascript">
                                            function konfirmasi_hapus_data_permanen() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_hapus_data_ke_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_arsip_data() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_arsip() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }

                                            function konfirmasi_restore_data_dari_tong_sampah() {
                                                var txt;
                                                var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                                if (r == true) {
                                                    document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
                                                } else {

                                                }
                                            }
                                        </script>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Aktif") { ?>
                                                    <a href="#" onclick="konfirmasi_arsip_data()"><i class="fa fa-archive fa-md">
                                                            ARSIPKAN</i></a>
                                                <?php } elseif ($edit['Status'] == "Terarsip") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i
                                                            class="fa fa-archive fa-md"> AKTIFKAN</i></a>
                                                <?php } elseif ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i
                                                            class="fa fa-archive fa-md"> RESTORE</i></a>
                                                <?php } ?>

                                            </li>
                                            <li class="list-inline-item"> | </li>
                                            <li class="list-inline-item">
                                                <?php if ($edit['Status'] == "Terhapus") { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_permanen()"><i
                                                            class="fa fa-trash fa-md"> HAPUS </i></a>
                                                <?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
                                                    <a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i
                                                            class="fa fa-trash fa-md"> HAPUS </i></a>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Tahun Ajaran</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="Tahun_Ajaran" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                    echo $_POST['Tahun_Ajaran'];
                                                } elseif (isset($_GET['edit'])) {
                                                    echo $edit['Tahun_Ajaran'];
                                                } ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Judul</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="Judul" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                    echo $_POST['Judul'];
                                                } elseif (isset($_GET['edit'])) {
                                                    echo $edit['Judul'];
                                                } ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Deskripsi</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="Deskripsi" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                    echo $_POST['Deskripsi'];
                                                } elseif (isset($_GET['edit'])) {
                                                    echo $edit['Deskripsi'];
                                                } ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Tanggal Mulai Pendaftaran</label>
                                            <div class="col-lg-9">
                                                <input type="date" class="form-control" name="Tanggal_Mulai_Pendaftaran"
                                                    value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                        echo $_POST['Tanggal_Mulai_Pendaftaran'];
                                                    } elseif (isset($_GET['edit'])) {
                                                        echo $edit['Tanggal_Mulai_Pendaftaran'];
                                                    } ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Tanggal Akhir Pendaftaran</label>
                                            <div class="col-lg-9">
                                                <input type="date" class="form-control" name="Tanggal_Akhir_Pendaftaran"
                                                    value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                        echo $_POST['Tanggal_Akhir_Pendaftaran'];
                                                    } elseif (isset($_GET['edit'])) {
                                                        echo $edit['Tanggal_Akhir_Pendaftaran'];
                                                    } ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">No Rekening Utama</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="No_Rekening_Utama" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                    echo $_POST['No_Rekening_Utama'];
                                                } elseif (isset($_GET['edit'])) {
                                                    echo $edit['No_Rekening_Utama'];
                                                } ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">No Rekening Cadangan</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" name="No_Rekening_Cadangan" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                    echo $_POST['No_Rekening_Cadangan'];
                                                } elseif (isset($_GET['edit'])) {
                                                    echo $edit['No_Rekening_Cadangan'];
                                                } ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Biaya Pendaftaran Formulir</label>
                                            <div class="col-lg-9">
                                                <input type="number" class="form-control" name="Biaya_Pendaftaran_Formulir"
                                                    value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                        echo $_POST['Biaya_Pendaftaran_Formulir'];
                                                    } elseif (isset($_GET['edit'])) {
                                                        echo $edit['Biaya_Pendaftaran_Formulir'];
                                                    } ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Biaya PPDB</label>
                                            <div class="col-lg-9">
                                                <input type="number" class="form-control" name="Biaya_PPDB" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                    echo $_POST['Biaya_PPDB'];
                                                } elseif (isset($_GET['edit'])) {
                                                    echo $edit['Biaya_PPDB'];
                                                } ?>">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Biaya Admin Xendit</label>
                                            <div class="col-lg-9">
                                                <input type="number" class="form-control" name="Biaya_Admin_Xendit" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                    echo $_POST['Biaya_Admin_Xendit'];
                                                } elseif (isset($_GET['edit'])) {
                                                    echo $edit['Biaya_Admin_Xendit'];
                                                } ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">
                                                Batas Waktu Pembayaran Formulir (Hari)
                                                <br>
                                                <p style="color:red">Input angka 0 / kosongkan jika tidak ada batasan</p>
                                            </label>
                                            <div class="col-lg-9">
                                                <input type="number" class="form-control"
                                                    name="Masa_Waktu_Pembayaran_Pembelian_Formulir" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                        echo $_POST['Masa_Waktu_Pembayaran_Pembelian_Formulir'];
                                                    } elseif (isset($_GET['edit'])) {
                                                        echo $edit['Masa_Waktu_Pembayaran_Pembelian_Formulir'];
                                                    } ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">
                                                Batas Waktu Pengisian Berkas (Hari)
                                                <br>
                                                <p style="color:red">Input angka 0 / kosongkan jika tidak ada batasan</p>
                                            </label>
                                            <div class="col-lg-9">
                                                <input type="number" class="form-control" name="Masa_Waktu_Pengisian_Berkas"
                                                    value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                        echo $_POST['Masa_Waktu_Pengisian_Berkas'];
                                                    } elseif (isset($_GET['edit'])) {
                                                        echo $edit['Masa_Waktu_Pengisian_Berkas'];
                                                    } ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">
                                                Masa Waktu Pembayaran PPDB <br>Setelah Semua Terverifikasi (Hari)
                                                <br>
                                                <p style="color:red">Input angka 0 / kosongkan jika tidak ada batasan</p>
                                            </label>
                                            <div class="col-lg-9">
                                                <input type="number" class="form-control"
                                                    name="Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                        echo $_POST['Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi'];
                                                    } elseif (isset($_GET['edit'])) {
                                                        echo $edit['Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi'];
                                                    } ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">
                                                Umur Maksimal Pendaftar (Tahun)
                                                <br>
                                                <p style="color:red">Input angka 0 / kosongkan jika tidak ada batasan</p>
                                            </label>
                                            <div class="col-lg-9">
                                                <input type="number" class="form-control" name="Umur_Maksimal_Pendaftar"
                                                    value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                        echo $_POST['Umur_Maksimal_Pendaftar'];
                                                    } elseif (isset($_GET['edit'])) {
                                                        echo $edit['Umur_Maksimal_Pendaftar'];
                                                    } ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">
                                                Total Target Siswa
                                                <br>
                                                <p style="color:red">Input angka 0 / kosongkan jika tidak ada target</p>
                                            </label>
                                            <div class="col-lg-9">
                                                <input type="number" class="form-control" name="Total_Target_Siswa" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                    echo $_POST['Total_Target_Siswa'];
                                                } elseif (isset($_GET['edit'])) {
                                                    echo $edit['Total_Target_Siswa'];
                                                } ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Informasi Tambahan</label>
                                            <div class="col-lg-9">
                                                <textarea class="form-control" name="Informasi_Tambahan"><?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                    echo $_POST['Informasi_Tambahan'];
                                                } elseif (isset($_GET['edit'])) {
                                                    echo $edit['Informasi_Tambahan'];
                                                } ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Foto Alur Pendaftaran</label>
                                            <div class="col-lg-9">
                                                <?php if (isset($_GET['edit'])) {
                                                    if ($edit['Foto_Alur_Pendaftaran'] == "") { ?>
                                                        Tidak Ada File
                                                    <?php } else { ?>
                                                        <a href="<?php echo $folder_penyimpanan_file_foto_alur_pendaftaran ?><?php echo $edit['Foto_Alur_Pendaftaran'] ?>"
                                                            target="_blank">
                                                            <img src="<?php echo $folder_penyimpanan_file_foto_alur_pendaftaran ?><?php echo $edit['Foto_Alur_Pendaftaran'] ?>"
                                                                width="300px" height="auto">
                                                        </a>
                                                        <?php
                                                    }
                                                } ?>
                                                <input type="file" name="Foto_Alur_Pendaftaran" class="form-control"
                                                    accept="image/*">
                                                <i>Click "Choose File" untuk mengganti file</i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 control-label">Status PPDB</label>
                                            <div class="col-lg-9">
                                                <select name="Status_PPDB" class="form-control">
                                                    <option value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                        echo $_POST['Status_PPDB'];
                                                    } elseif (isset($_GET['edit'])) {
                                                        echo $edit['Status_PPDB'];
                                                    } ?>"><?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
                                                         echo $_POST['Status_PPDB'];
                                                     } elseif (isset($_GET['edit'])) {
                                                         echo $edit['Status_PPDB'];
                                                     } ?></option>
                                                    <option value="Aktif">Aktif</option>
                                                    <option value="Tidak Aktif">Tidak Aktif</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                                <div style="text-align: center;">
                                    <?php if (isset($_GET["tambah"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
                                    <?php } elseif (isset($_GET["edit"])) { ?>
                                        <input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE">
                                    <?php } ?>
                                    <input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'"
                                        class="btn btn-danger" value="BATAL">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php if (isset($_GET["edit"])) { ?>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="card-title">Syarat dan Ketentuan PPDB</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <h2>Ketentuan Umum :</h2>
                                                    <button type="button" class="btn btn-primary mb-3"
                                                        onclick="Add_Row_Ketentuan_Umum()">+ TAMBAH</button>
                                                    <table class="table table-bordered" id="dynamic-table-Ketentuan_Umum">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Ketentuan</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $nomor = 0;
                                                            if ((isset($list_data_syarat_dan_ketentuan_data_ketentuan))) {
                                                                foreach ($list_data_syarat_dan_ketentuan_data_ketentuan as $data) {
                                                                    $nomor++; ?>
                                                                    <tr>
                                                                        <td><?php echo $nomor ?></td>
                                                                        <td><input
                                                                                style="margin-top:5px; margin-bottom:5px !important; padding-top:5px !important; padding-bottom:5px"
                                                                                class="form-control" type="text"
                                                                                name="Syarat_Dan_Ketentuan_Data_Ketentuan_Umum[]"
                                                                                value="<?php echo $data['Syarat_Dan_Ketentuan'] ?>">
                                                                        </td>
                                                                        <td>
                                                                            <button class="btn btn-danger btn-sm"
                                                                                onclick="Delete_Row_Ketentuan_Umum(this)">Hapus</button>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <hr>
                                                <div class="col-lg-12">
                                                    <h2>Catatan :</h2>
                                                    <button type="button" class="btn btn-primary mb-3"
                                                        onclick="Add_Row_Catatan()">+ TAMBAH</button>
                                                    <table class="table table-bordered" id="dynamic-table-Catatan">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Ketentuan</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $nomor = 0;
                                                            if ((isset($list_data_syarat_dan_ketentuan_data_catatan))) {
                                                                foreach ($list_data_syarat_dan_ketentuan_data_catatan as $data) {
                                                                    $nomor++; ?>
                                                                    <tr>
                                                                        <td><?php echo $nomor ?></td>
                                                                        <td><input
                                                                                style="margin-top:5px; margin-bottom:5px !important; padding-top:5px !important; padding-bottom:5px"
                                                                                class="form-control" type="text"
                                                                                name="Syarat_Dan_Ketentuan_Data_Catatan[]"
                                                                                value="<?php echo $data['Syarat_Dan_Ketentuan'] ?>">
                                                                        </td>
                                                                        <td>
                                                                            <button class="btn btn-danger btn-sm"
                                                                                onclick="Delete_Row_Catatan(this)">Hapus</button>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="text-align: center;">
                                        <input type="submit" class="btn btn-primary"
                                            name="submit_simpan_update_syarat_dan_ketentuan" value="SIMPAN">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <script>
                        function Add_Row_Ketentuan_Umum() {
                            var table = document.getElementById("dynamic-table-Ketentuan_Umum");
                            var newRow = table.insertRow(table.rows.length);

                            var cell1 = newRow.insertCell(0);
                            var cell2 = newRow.insertCell(1);
                            var cell3 = newRow.insertCell(2);

                            cell1.innerHTML = table.rows.length - 1;
                            cell2.innerHTML = `<input style="margin-top:5px; margin-bottom:5px !important; padding-top:5px !important; padding-bottom:5px" class="form-control" type="text" name="Syarat_Dan_Ketentuan_Data_Ketentuan_Umum[]" value="">`;
                            cell3.innerHTML = '<button class="btn btn-danger btn-sm" onclick="Delete_Row_Ketentuan_Umum(this)">Hapus</button>';
                        }

                        function Delete_Row_Ketentuan_Umum(button) {
                            var row = button.parentNode.parentNode;
                            row.parentNode.removeChild(row);
                            Update_Row_Numbers_Ketentuan_Umum();
                        }

                        function Update_Row_Numbers_Ketentuan_Umum() {
                            var table = document.getElementById("dynamic-table-Ketentuan_Umum");

                            for (var i = 1; i < table.rows.length; i++) {
                                table.rows[i].cells[0].innerHTML = i;
                            }
                        }

                        $(function () {
                            $("#dynamic-table-Ketentuan_Umum tbody").sortable({
                                helper: function (e, tr) {
                                    var $originals = tr.children();
                                    var $helper = tr.clone();
                                    $helper.children().each(function (index) {
                                        $(this).width($originals.eq(index).width());
                                    });
                                    return $helper;
                                },
                                stop: Update_Row_Numbers_Ketentuan_Umum
                            }).disableSelection();
                        });
                    </script>

                    <script>
                        function Add_Row_Catatan() {
                            var table = document.getElementById("dynamic-table-Catatan");
                            var newRow = table.insertRow(table.rows.length);

                            var cell1 = newRow.insertCell(0);
                            var cell2 = newRow.insertCell(1);
                            var cell3 = newRow.insertCell(2);

                            cell1.innerHTML = table.rows.length - 1;
                            cell2.innerHTML = `<input style="margin-top:5px; margin-bottom:5px !important; padding-top:5px !important; padding-bottom:5px" class="form-control" type="text" name="Syarat_Dan_Ketentuan_Data_Catatan[]" value="">`;
                            cell3.innerHTML = '<button class="btn btn-danger btn-sm" onclick="Delete_Row_Catatan(this)">Hapus</button>';
                        }

                        function Delete_Row_Catatan(button) {
                            var row = button.parentNode.parentNode;
                            row.parentNode.removeChild(row);
                            Update_Row_Numbers_Catatan();
                        }

                        function Update_Row_Numbers_Catatan() {
                            var table = document.getElementById("dynamic-table-Catatan");

                            for (var i = 1; i < table.rows.length; i++) {
                                table.rows[i].cells[0].innerHTML = i;
                            }
                        }

                        $(function () {
                            $("#dynamic-table-Catatan tbody").sortable({
                                helper: function (e, tr) {
                                    var $originals = tr.children();
                                    var $helper = tr.clone();
                                    $helper.children().each(function (index) {
                                        $(this).width($originals.eq(index).width());
                                    });
                                    return $helper;
                                },
                                stop: Update_Row_Numbers_Catatan
                            }).disableSelection();
                        });
                    </script>
                <?php } ?>
            </div>
            <?php
        }
        ?>

        <?php
        //FORM LIST DATA
        if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">List PPDB <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
                                echo "(" . $_GET['filter_status'] . ")";
                            } ?></div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="<?php echo $kehalaman ?>&tambah" class="btn btn-sm btn-primary"><i
                                            class="fas fa-plus-circle"></i> &nbsp; Tambah Baru</a>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a
                                                href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF
                                                (<?php echo $hitung_Aktif ?>)</a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a
                                                href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP
                                                (<?php echo $hitung_Terarsip ?>)</a></li>
                                        <li class="list-inline-item"> | </li>
                                        <li class="list-inline-item"><a
                                                href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH
                                                (<?php echo $hitung_Terhapus ?>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table id="basic-datatables" class="table table-borderless" style="width:100%">
                                    <thead class="bg-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Tahun Ajaran</th>
                                            <th>Judul</th>
                                            <th>Status</th>
                                            <th>Tanggal Mulai Pendaftaran</th>
                                            <th>Tanggal Akhir Pendaftaran</th>
                                            <th>Biaya Pembelian Formulir</th>
                                            <th>Biaya PPDB</th>
                                            <th>Biaya Admin Xendit</th>
                                            <th>Masa Waktu Pembayaran Pembelian Formulir (Hari)</th>
                                            <th>Masa Waktu Pengisian Berkas (Hari)</th>
                                            <th>Masa Waktu Pembayaran PPDB Setelah Semua Terverifikasi (Hari)</th>
                                            <th>Umur Maksimal Pendaftar</th>
                                            <th>Total Target Siswa</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ((isset($list_datatable_master))) {
                                            foreach ($list_datatable_master as $data) {
                                                $nomor++; ?>
                                                <tr>
                                                    <td><?php echo $nomor ?></td>
                                                    <td>
                                                        <a
                                                            href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_PPDB"], $_GET['menu']); ?>">
                                                            <?php echo $data['Tahun_Ajaran'] ?>
                                                        </a>
                                                    </td>
                                                    <td><?php echo $data['Judul'] ?></td>
                                                    <td>
                                                        <?php if ($data['Status_PPDB'] == "Aktif") { ?>
                                                            <span class="badge badge-success"><?php echo $data['Status_PPDB'] ?></span>
                                                        <?php } else { ?>
                                                            <span class="badge badge-danger"><?php echo $data['Status_PPDB'] ?></span>
                                                        <?php } ?>

                                                    </td>
                                                    <td><?php echo tanggal_dan_waktu_24_jam_indonesia($data['Tanggal_Mulai_Pendaftaran']) ?>
                                                    </td>
                                                    <td><?php echo tanggal_dan_waktu_24_jam_indonesia($data['Tanggal_Akhir_Pendaftaran']) ?>
                                                    </td>
                                                    <td><?php echo rupiah($data['Biaya_Pendaftaran_Formulir']) ?></td>
                                                    <td><?php echo rupiah($data['Biaya_PPDB']) ?></td>
                                                    <td><?php echo rupiah($data['Biaya_Admin_Xendit']) ?></td>
                                                    <td>
                                                        <?php
                                                        if (($data['Masa_Waktu_Pembayaran_Pembelian_Formulir'] == "0") || ($data['Masa_Waktu_Pembayaran_Pembelian_Formulir'] == "")) {
                                                            echo "-";
                                                        } else {
                                                            echo $data['Masa_Waktu_Pembayaran_Pembelian_Formulir'];
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (($data['Masa_Waktu_Pengisian_Berkas'] == "0") || ($data['Masa_Waktu_Pengisian_Berkas'] == "")) {
                                                            echo "-";
                                                        } else {
                                                            echo $data['Masa_Waktu_Pengisian_Berkas'];
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (($data['Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi'] == "0") || ($data['Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi'] == "")) {
                                                            echo "-";
                                                        } else {
                                                            echo $data['Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi'];
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (($data['Umur_Maksimal_Pendaftar'] == "0") || ($data['Umur_Maksimal_Pendaftar'] == "")) {
                                                            echo "-";
                                                        } else {
                                                            echo $data['Umur_Maksimal_Pendaftar'];
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (($data['Total_Target_Siswa'] == "0") || ($data['Total_Target_Siswa'] == "")) {
                                                            echo "-";
                                                        } else {
                                                            echo $data['Total_Target_Siswa'];
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <a
                                                            href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_PPDB"], $_GET['menu']); ?>"><button
                                                                class="btn btn-xs btn-outline-warning"><i
                                                                    class="fas fa-edit"></i></button></a>
                                                        <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] == "Terhapus")) { ?>
                                                            <a href="#"
                                                                onclick="datatable_konfirmasi_restore_data_dari_tong_sampah('<?php echo $a_hash->encode($data["Id_PPDB"], $_GET['menu']); ?>')"><button
                                                                    class="btn btn-xs btn-outline-success"><i
                                                                        class="fas fa-redo"></i></button></a>

                                                            <a href="#"
                                                                onclick="datatable_konfirmasi_hapus_data_permanen('<?php echo $a_hash->encode($data["Id_PPDB"], $_GET['menu']); ?>')"><button
                                                                    class="btn btn-xs btn-outline-danger"><i
                                                                        class="fa fa-trash"></i></button></a>
                                                        <?php } else { ?>
                                                            <a href="#"
                                                                onclick="datatable_konfirmasi_hapus_data_ke_tong_sampah('<?php echo $a_hash->encode($data["Id_PPDB"], $_GET['menu']); ?>')"><button
                                                                    class="btn btn-xs btn-outline-danger"><i
                                                                        class="fa fa-trash"></i></button></a>
                                                        <?php } ?>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    function datatable_konfirmasi_hapus_data_permanen(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
                                        } else {

                                        }
                                    }

                                    function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
                                        var txt;
                                        var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
                                        if (r == true) {
                                            document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
                                        } else {

                                        }
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>