<table class="table table-bordered">
	<thead>
		<tr>
			<th>No</th>
			<th>Halaman</th>
			<th>Baca</th>
			<th>Tambah</th>
			<th>Update</th>
			<th>Hapus</th>
		</tr>

		<?php
		$nomor = 0;
		if ((isset($array_List_Halaman_Akses))) {
			foreach ($array_List_Halaman_Akses as $data) {
				if(isset($data['Sembunyikan'])){
					$Sembunyikan_Permission = $data['Sembunyikan'];	
				}else{
					$Sembunyikan_Permission = null;
				}
				
				$nomor++; ?>
				<?php 
				if(isset($_GET['edit'])){
					if($edit['Hak_Akses'] == "Super Administrator"){
						$Baca_Data_Sebelumnya = "Iya";
						$Simpan_Data_Sebelumnya = "Iya";
						$Update_Data_Sebelumnya = "Iya";
						$Hapus_Data_Sebelumnya = "Iya";
					}else{
						$Baca_Data_Sebelumnya = "";
						$Simpan_Data_Sebelumnya = "";
						$Update_Data_Sebelumnya = "";
						$Hapus_Data_Sebelumnya = "";

						if(isset($edit_hak_akses_detail)){
							foreach ($edit_hak_akses_detail as $data_sebelumnya) {
								if($data_sebelumnya['Kode_Halaman'] == $data['Kode_Halaman']){
									$Baca_Data_Sebelumnya = $data_sebelumnya['Baca_Data'];
									$Simpan_Data_Sebelumnya = $data_sebelumnya['Simpan_Data'];
									$Update_Data_Sebelumnya = $data_sebelumnya['Update_Data'];
									$Hapus_Data_Sebelumnya = $data_sebelumnya['Hapus_Data'];
									break;
								}
							}
						}
					}
				}
				?>
				<tr>
					<td><?php echo $nomor ?></td>
					<td>
						<?php echo $data['Nama_Halaman'] ?>
						<input readonly type="hidden" name="Nama_Halaman_<?php echo $data['Kode_Halaman']?>" value="<?php echo $data['Nama_Halaman'] ?>">
						<input readonly type="hidden" name="Kode_Halaman_<?php echo $data['Kode_Halaman']?>" value="<?php echo $data['Kode_Halaman'] ?>">
					</td>
					<td>
						<input type="checkbox" name="Baca_Data_<?php echo $data['Kode_Halaman']?>" value="Iya" <?php if(isset($_GET['edit'])){if($Baca_Data_Sebelumnya == "Iya"){echo "checked";}} ?> style="<?php if(isset($_GET['edit'])){if ((isset($Sembunyikan_Permission)) AND (in_array("Baca",$Sembunyikan_Permission))){echo "display:none";}} ?>" <?php if(isset($_GET['edit'])){if($edit['Hak_Akses'] == "Super Administrator"){echo "disabled";}} ?>>
					</td>
					<td>
						<input type="checkbox" name="Simpan_Data_<?php echo $data['Kode_Halaman']?>" value="Iya" <?php if(isset($_GET['edit'])){if($Simpan_Data_Sebelumnya == "Iya"){echo "checked";}} ?> style="<?php if(isset($_GET['edit'])){if ((isset($Sembunyikan_Permission)) AND (in_array("Simpan",$Sembunyikan_Permission))){echo "display:none";}} ?>" <?php if(isset($_GET['edit'])){if($edit['Hak_Akses'] == "Super Administrator"){echo "disabled";}} ?>>
					</td>
					<td>
						<input type="checkbox" name="Update_Data_<?php echo $data['Kode_Halaman']?>" value="Iya" <?php if(isset($_GET['edit'])){if($Update_Data_Sebelumnya == "Iya"){echo "checked";}} ?> style="<?php if(isset($_GET['edit'])){if ((isset($Sembunyikan_Permission)) AND (in_array("Update",$Sembunyikan_Permission))){echo "display:none";}} ?>" <?php if(isset($_GET['edit'])){if($edit['Hak_Akses'] == "Super Administrator"){echo "disabled";}} ?>>
					</td>
					<td>
						<input type="checkbox" name="Hapus_Data_<?php echo $data['Kode_Halaman']?>" value="Iya" <?php if(isset($_GET['edit'])){if($Hapus_Data_Sebelumnya == "Iya"){echo "checked";}} ?> style="<?php if(isset($_GET['edit'])){if ((isset($Sembunyikan_Permission)) AND (in_array("Hapus",$Sembunyikan_Permission))){echo "display:none";}} ?>" <?php if(isset($_GET['edit'])){if($edit['Hak_Akses'] == "Super Administrator"){echo "disabled";}} ?>>
					</td>
				</tr>
				<?php 
			} 
		}
		?>

	</thead>
</table>