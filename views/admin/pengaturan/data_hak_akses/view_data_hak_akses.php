<?php
include "controllers/admin/pengaturan/data_hak_akses/controller_data_hak_akses.php";
?>
<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Data Hak Akses</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="?menu=home">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo "?menu=" . $_GET['menu'] ?>">Data Hak Akses</a>
				</li>

				<?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<?php if (isset($_GET['tambah'])) { ?>
							<a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
						<?php } else { ?>
							<a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
						<?php } ?>
					</li>
				<?php } ?>

			</ul>
		</div>
		<?php
		//FORM TAMBAH/EDIT DATA
		if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
		?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<?php if (isset($_GET['tambah'])) { ?>
										<div class="card-title">Tambah Data</div>
									<?php } else { ?>
										<div class="card-title">Edit Data</div>
									<?php } ?>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
									<?php if (isset($_GET["edit"])) { ?>
										<script type="text/javascript">
											function konfirmasi_hapus_data_permanen() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_hapus_data_ke_tong_sampah() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_arsip_data() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Mengarsip Data Ini ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&arsip_data&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_restore_data_dari_arsip() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Mengeluarkan Data Ini Dari Arsip ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_arsip&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}

											function konfirmasi_restore_data_dari_tong_sampah() {
												var txt;
												var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
												if (r == true) {
													document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=<?php echo $_GET['id'] ?>'
												} else {

												}
											}
										</script>
										<ul class="list-inline">
											<li class="list-inline-item">
												<?php if ($edit['Status'] == "Aktif") { ?>
													<a href="#" onclick="konfirmasi_arsip_data()"><i class="fa fa-archive fa-md"> ARSIPKAN</i></a>
												<?php } elseif ($edit['Status'] == "Terarsip") { ?>
													<a href="#" onclick="konfirmasi_restore_data_dari_arsip()"><i class="fa fa-archive fa-md"> AKTIFKAN</i></a>
												<?php } elseif ($edit['Status'] == "Terhapus") { ?>
													<a href="#" onclick="konfirmasi_restore_data_dari_tong_sampah()"><i class="fa fa-archive fa-md"> RESTORE</i></a>
												<?php } ?>

											</li>
											<li class="list-inline-item"> | </li>
											<li class="list-inline-item">
												<?php if ($edit['Status'] == "Terhapus") { ?>
													<a href="#" onclick="konfirmasi_hapus_data_permanen()"><i class="fa fa-trash fa-md"> HAPUS </i></a>
												<?php } elseif (($edit['Status'] == "Aktif") or ($edit['Status'] == "Terarsip")) { ?>
													<a href="#" onclick="konfirmasi_hapus_data_ke_tong_sampah()"><i class="fa fa-trash fa-md"> HAPUS </i></a>
												<?php } ?>
											</li>
										</ul>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="card-body">
							<form method="POST" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group row">
											<label class="col-lg-3 control-label">Hak Akses</label>
											<div class="col-lg-9">
												<input <?php if (isset($_GET['edit'])) {
															if ($edit['Hak_Akses'] == "Super Administrator") {
																echo "readonly";
															}
														} ?> required type="text" class="form-control" name="Hak Akses" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																																	echo $_POST['Hak_Akses'];
																																} elseif (isset($_GET['edit'])) {
																																	echo $edit['Hak_Akses'];
																																} ?>">
											</div>
										</div>

										<div class="form-group row">
											<label class="col-lg-3 control-label">Deskripsi</label>
											<div class="col-lg-9">
												<textarea <?php if (isset($_GET['edit'])) {
																if ($edit['Hak_Akses'] == "Super Administrator") {
																	echo "readonly";
																}
															} ?> rows="3" class="form-control" name="Deskripsi"><?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																													echo $_POST['Deskripsi'];
																												} elseif (isset($_GET['edit'])) {
																													echo $edit['Deskripsi'];
																												} ?></textarea>
											</div>
										</div>
									</div>
									<hr>
									<div class="col-md-12">
										<?php include "view_data_hak_akses_detail.php"; ?>
									</div>

								</div>

								<div style="text-align: center;">
									<?php if (isset($_GET["tambah"])) {  ?>
										<input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
									<?php } elseif (isset($_GET["edit"])) { ?>
										<input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE" <?php if (isset($_GET['edit'])) {
																																if ($edit['Hak_Akses'] == "Super Administrator") {
																																	echo "disabled";
																																}
																															} ?>>
									<?php } ?>
									<input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'" class="btn btn-danger" value="BATAL">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>

		<?php
		//FORM LIST DATA
		if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
		?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="card-title">List Data Hak Akses <?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] != 'Aktif')) {
																			echo "(" . $_GET['filter_status'] . ")";
																		} ?></div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<a href="<?php echo $kehalaman ?>&tambah" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i> &nbsp; Tambah Baru</a>
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12" style="text-align: right;">
									<ul class="list-inline">
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Aktif">AKTIF (<?php echo $hitung_Aktif ?>)</a></li>
										<li class="list-inline-item"> | </li>
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terarsip">TERARSIP (<?php echo $hitung_Terarsip ?>)</a></li>
										<li class="list-inline-item"> | </li>
										<li class="list-inline-item"><a href="<?php echo $kehalaman ?>&filter_status=Terhapus">SAMPAH (<?php echo $hitung_Terhapus ?>)</a></li>
									</ul>
								</div>
							</div>
							<br>
							<div class="table-responsive">
								<table id="basic-datatables" class="table table-borderless" style="width:100%">
									<thead class="bg-light">
										<tr>
											<th>No</th>
											<th>Hak Akses</th>
											<th>Deskripsi</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ((isset($list_datatable_master))) {
											foreach ($list_datatable_master as $data) {
												$nomor++; ?>
												<tr>
													<td><?php echo $nomor ?></td>
													<td>
														<a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Hak_Akses"], $_GET['menu']); ?>">
															<?php echo $data['Hak_Akses'] ?>
														</a>
													</td>
													<td><?php echo $data['Deskripsi'] ?></td>
													<td>
														<a href="<?php echo $kehalaman ?>&edit&id=<?php echo $a_hash->encode($data["Id_Hak_Akses"], $_GET['menu']); ?>"><button class="btn btn-xs btn-outline-warning"><i class="fas fa-edit"></i></button></a>
														<?php if ((isset($_GET['filter_status'])) and ($_GET['filter_status'] == "Terhapus")) { ?>
															<a href="#" onclick="datatable_konfirmasi_restore_data_dari_tong_sampah('<?php echo $a_hash->encode($data["Id_Hak_Akses"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-success"><i class="fas fa-redo"></i></button></a>
															<a href="#" onclick="datatable_konfirmasi_hapus_data_permanen('<?php echo $a_hash->encode($data["Id_Hak_Akses"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
														<?php } else { ?>
															<a <?php if ($data['Hak_Akses'] == "Super Administrator") {
																	echo "style='display:none'";
																} ?> href="#" onclick="datatable_konfirmasi_hapus_data_ke_tong_sampah('<?php echo $a_hash->encode($data["Id_Hak_Akses"], $_GET['menu']); ?>')"><button class="btn btn-xs btn-outline-danger"><i class="fa fa-trash"></i></button></a>
														<?php } ?>

													</td>
												</tr>
											<?php } ?>
										<?php } ?>
									</tbody>
								</table>
								<script type="text/javascript">
									function datatable_konfirmasi_hapus_data_permanen(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Menghapus Permanen Data Ini ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&hapus_data_permanen&id=' + id_primary
										} else {

										}
									}

									function datatable_konfirmasi_hapus_data_ke_tong_sampah(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Menghapus Data Ini ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&hapus_data_ke_tong_sampah&id=' + id_primary
										} else {

										}
									}

									function datatable_konfirmasi_restore_data_dari_tong_sampah(id_primary) {
										var txt;
										var r = confirm("Apakah Anda Yakin Ingin Merestore Data Ini Dari Tong Sampah ?");
										if (r == true) {
											document.location.href = '<?php echo $kehalaman ?>&restore_data_dari_tong_sampah&id=' + id_primary
										} else {

										}
									}
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>