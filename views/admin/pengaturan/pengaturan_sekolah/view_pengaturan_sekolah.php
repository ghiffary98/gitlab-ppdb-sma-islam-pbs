<?php
include "controllers/admin/pengaturan/pengaturan_sekolah/controller_pengaturan_sekolah.php";
?>
<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Pengaturan Sekolah</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="?menu=home">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo "?menu=" . $_GET['menu'] ?>">Pengaturan Sekolah</a>
				</li>

				<?php if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) { ?>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<?php if (isset($_GET['tambah'])) { ?>
							<a href="<?php echo $Link_Sekarang ?>">Tambah Data</a>
						<?php } else { ?>
							<a href="<?php echo $Link_Sekarang ?>">Edit Data</a>
						<?php } ?>
					</li>
				<?php } ?>

			</ul>
		</div>
		<?php
		//FORM TAMBAH/EDIT DATA
		if ((isset($_GET["tambah"])) or (isset($_GET["edit"]))) {
		?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<?php if (isset($_GET['tambah'])) { ?>
										<div class="card-title">Tambah Data</div>
									<?php } else { ?>
										<div class="card-title">Edit Data</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="card-body">
							<form method="POST" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group row">
											<label class="col-lg-3 control-label">Nama Sekolah</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nama_Sekolah" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Nama_Sekolah'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Nama_Sekolah'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Deskripsi Singkat</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Deskripsi_Singkat" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																															echo $_POST['Deskripsi_Singkat'];
																														} elseif (isset($_GET['edit'])) {
																															echo $edit['Deskripsi_Singkat'];
																														} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Deksripsi Lengkap</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Deksripsi_Lengkap" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																															echo $_POST['Deksripsi_Lengkap'];
																														} elseif (isset($_GET['edit'])) {
																															echo $edit['Deksripsi_Lengkap'];
																														} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Alamat Lengkap</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Alamat_Lengkap" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																															echo $_POST['Alamat_Lengkap'];
																														} elseif (isset($_GET['edit'])) {
																															echo $edit['Alamat_Lengkap'];
																														} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Email Admin</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Email_Admin" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Email_Admin'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Email_Admin'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Email Customer Service</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Email_Customer_Service" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																																	echo $_POST['Email_Customer_Service'];
																																} elseif (isset($_GET['edit'])) {
																																	echo $edit['Email_Customer_Service'];
																																} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Email Support</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Email_Support" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Email_Support'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Email_Support'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Nomor Telpon</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nomor_Telpon" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Nomor_Telpon'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Nomor_Telpon'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Nomor Handphone</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nomor_Handphone" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																															echo $_POST['Nomor_Handphone'];
																														} elseif (isset($_GET['edit'])) {
																															echo $edit['Nomor_Handphone'];
																														} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Nama Facebook</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nama_Facebook" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Nama_Facebook'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Nama_Facebook'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Url Facebook</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Url_Facebook" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Url_Facebook'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Url_Facebook'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Nama Instagram</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nama_Instagram" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																															echo $_POST['Nama_Instagram'];
																														} elseif (isset($_GET['edit'])) {
																															echo $edit['Nama_Instagram'];
																														} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Url Instagram</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Url_Instagram" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Url_Instagram'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Url_Instagram'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Nama Twitter</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nama_Twitter" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Nama_Twitter'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Nama_Twitter'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Url Twitter</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Url_Twitter" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Url_Twitter'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Url_Twitter'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Nama Linkedin</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nama_Linkedin" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Nama_Linkedin'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Nama_Linkedin'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Url Linkedin</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Url_Linkedin" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Url_Linkedin'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Url_Linkedin'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Nama Youtube</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Nama_Youtube" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Nama_Youtube'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Nama_Youtube'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Url Youtube</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Url_Youtube" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																														echo $_POST['Url_Youtube'];
																													} elseif (isset($_GET['edit'])) {
																														echo $edit['Url_Youtube'];
																													} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Embed Google Maps</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Embed_Google_Maps" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																															echo $_POST['Embed_Google_Maps'];
																														} elseif (isset($_GET['edit'])) {
																															echo $edit['Embed_Google_Maps'];
																														} ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 control-label">Google Maps Url</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="Google_Maps_Url" value="<?php if ((isset($_POST['submit_simpan'])) or (isset($_POST['submit_update']))) {
																															echo $_POST['Google_Maps_Url'];
																														} elseif (isset($_GET['edit'])) {
																															echo $edit['Google_Maps_Url'];
																														} ?>">
											</div>
										</div>
									</div>
								</div>

								<div style="text-align: center;">
									<hr>
									<?php if (isset($_GET["tambah"])) { ?>
										<input type="submit" class="btn btn-primary" name="submit_simpan" value="SIMPAN">
									<?php } elseif (isset($_GET["edit"])) { ?>
										<input type="submit" class="btn btn-primary" name="submit_update" value="UPDATE">
									<?php } ?>
									<input type="button" onclick="document.location.href='<?php echo $kehalaman ?>'" class="btn btn-danger" value="BATAL">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>