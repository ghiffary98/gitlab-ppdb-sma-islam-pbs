<div class="container">
  <div class="row justify-content-center align-items-center" style="height: 100vh">
    <div class="col-lg-5">
      <div class="card">
        <div class="card-body">
          <div class="text-center align-items-center mt-2" style="align-self:center; align-content:center">
            <img src="assets/images/logo/logo_sma_soedirman.png" alt="SMA-Islam-PBS-Cijantung" style="height: 200px; width: auto" />
            <h3 class="mt-4"> <b>LOGIN ADMIN</b></h3>
            <h5 class="text-center">
              <b>PPDB <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?></b>
            </h5>
          </div>

          <div class="mt-1">&nbsp;</div>

          <form method="POST">
            <div class="form-group form-floating-label">
              <input id="emailFloatingLabel" name="Username" type="text" class="form-control input-border-bottom" required />
              <label for="emailFloatingLabel" class="placeholder">Username</label>
            </div>
            <div class="form-group form-floating-label">
              <input id="passwordFloatingLabel" name="Password" type="Password" class="form-control input-border-bottom" required />
              <label for="passwordFloatingLabel" class="placeholder">Password</label>
            </div>
            <div class="text-center mt-4">
              <button name="Submit_Login" type="submit" class="btn btn-block btn-primary fw-bold">
                Sign In
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>