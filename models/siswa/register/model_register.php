<?php
class a_register extends a_database
{
    private $Nama_Table = "tb_data_pendaftar";

    ###LOGIN
    function register($Id_PPDB, $Nomor_Pendaftaran, $Email, $Password, $Password_Pendaftar, $Nama_Lengkap, $Tempat_Lahir, $Tanggal_Lahir, $Alamat_Lengkap, $Nomor_Handphone)
    {
        #INPUTAN
        $Nomor_Pendaftaran = mysqli_real_escape_string($this->koneksi, strip_tags(trim($Nomor_Pendaftaran)));
        $Email = mysqli_real_escape_string($this->koneksi, strip_tags(trim($Email)));
        $Password = mysqli_real_escape_string($this->koneksi, strip_tags(trim($Password)));
        $Password_Pendaftar = mysqli_real_escape_string($this->koneksi, strip_tags(trim($Password_Pendaftar)));
        $Nama_Lengkap = mysqli_real_escape_string($this->koneksi, strip_tags(trim($Nama_Lengkap)));
        $Tempat_Lahir = mysqli_real_escape_string($this->koneksi, strip_tags(trim($Tempat_Lahir)));
        $Tanggal_Lahir = mysqli_real_escape_string($this->koneksi, strip_tags(trim($Tanggal_Lahir)));
        $Alamat_Lengkap = mysqli_real_escape_string($this->koneksi, strip_tags(trim($Alamat_Lengkap)));
        $Nomor_Handphone = mysqli_real_escape_string($this->koneksi, strip_tags(trim($Nomor_Handphone)));
        $Status_Pendaftaran = "Dalam Proses";
        $Waktu_Simpan_Data = date("Y-m-d H:i:s");
        $Status = "Aktif";


        #FUNGSI CEK APAKAH SUDAH PERNAH REGISTER ATAU BELUM BERDASARKAN EMAIL
        #SQL
        $sql = "SELECT * FROM $this->Nama_Table WHERE (Username = '$Email' OR Email = '$Email') AND Id_PPDB = '$Id_PPDB' AND (Status = 'Aktif' OR Status = 'Terarsip')";

        #FUNGSI
        $query = $this->koneksi->query($sql);

        #HASIL
        if ($query) {
            $hitung = mysqli_num_rows($query);
            if ($hitung > 0) {
                $result['Status'] = "Gagal";
                $result['Keterangan_Error'] = "Email Sudah Digunakan";
                #RETURN
                return $result;
                exit();
            }
        } else {
            $result['Status'] = "Gagal";
            $result['Keterangan_Error'] = "Terjadi Kesalahan Saat Membaca Data";
            #RETURN
            return $result;
            exit();
        }

        #FUNGSI CEK APAKAH SUDAH PERNAH REGISTER ATAU BELUM BERDASARKAN NOMOR HANDPHONE
        #SQL
        $sql = "SELECT * FROM $this->Nama_Table WHERE (Nomor_Handphone = '$Nomor_Handphone') AND Id_PPDB = '$Id_PPDB' AND (Status = 'Aktif' OR Status = 'Terarsip')";

        #FUNGSI
        $query = $this->koneksi->query($sql);

        #HASIL
        if ($query) {
            $hitung = mysqli_num_rows($query);
            if ($hitung > 0) {
                $result['Status'] = "Gagal";
                $result['Keterangan_Error'] = "Nomor Handphone Sudah Digunakan";
                #RETURN
                return $result;
                exit();
            }
        } else {
            $result['Status'] = "Gagal";
            $result['Keterangan_Error'] = "Terjadi Kesalahan Saat Membaca Data";
            #RETURN
            return $result;
            exit();
        }


        #SQL UNTUK MENDAPATKAN NOMOR AUTO INCREMENT YANG AKAN DI PAKAI
        $sql = "SHOW TABLE STATUS FROM $this->s_nama_database WHERE name LIKE '$this->Nama_Table'";
        $query = $this->koneksi->query($sql);
        if($query){
            $hitung = mysqli_num_rows($query);
            if($hitung > 0){
                $hasil = mysqli_fetch_assoc($query);
                $Nomor_Auto_Increment = $hasil['Auto_increment'];
            }else{
                $Nomor_Auto_Increment = "1";
            }
        }else{
            $Nomor_Auto_Increment = "1";
        }


        #FUNGSI REGISTER
        #SQL
        $sql = "INSERT INTO $this->Nama_Table (
                Id_PPDB,
                Nomor_Pendaftaran,
                Username,
                Email,
                Password,
                Password_Pendaftar,
                Nama_Lengkap,
                Tempat_Lahir,
                Tanggal_Lahir,
                Jalan,
                Nomor_Handphone,
                Status_Pendaftaran,
                Waktu_Simpan_Data,
                Status
            ) VALUES (
                '$Id_PPDB',
                '$Nomor_Pendaftaran',
                '$Email',
                '$Email',
                '$Password',
                '$Password_Pendaftar',
                '$Nama_Lengkap',
                '$Tempat_Lahir',
                '$Tanggal_Lahir',
                '$Alamat_Lengkap',
                '$Nomor_Handphone',
                '$Status_Pendaftaran',
                '$Waktu_Simpan_Data',
                '$Status'
            )";

        #FUNGSI
        $query = $this->koneksi->query($sql);

        #HASIL
        if ($query) {
            $result['Id'] = $Nomor_Auto_Increment;
            $result['Status'] = "Sukses";
        } else {
            $result['Status'] = "Gagal";
        }

        #RETURN
        return $result;
    }
}
