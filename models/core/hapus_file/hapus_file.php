<?php

class a_hapus_file {
	public function hapus_file($nama_file, $folder_penyimpanan){
		$status_hapus = 1;
		$target_hapus_file = $folder_penyimpanan . $nama_file;
		if (file_exists($target_hapus_file)) {
			unlink($target_hapus_file);
			$status_hapus = 1;
		} else {
			$status_hapus = 0;
			$array_keterangan_error[] = "File Tidak Ditemukan";
		}

		if ($status_hapus == 0) {
			$result['Status'] = "Gagal";
			if(!empty($array_keterangan_error)){
				$result['Keterangan_Error'] = $array_keterangan_error;
			}
		} else {
			$result['Status'] = "Sukses";
		}
		return $result;
	}
}
?>
