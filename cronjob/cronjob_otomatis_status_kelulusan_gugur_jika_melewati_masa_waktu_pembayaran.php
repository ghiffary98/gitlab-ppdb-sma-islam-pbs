<?php
include "../init.php";
include "../models/cronjob/model_cronjob.php";

$a_cronjob = new a_cronjob();

//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar" ID_PRIMARY "Id_Pendaftar"
$search_field_where = array("Status");
$search_criteria_where = array("<>");
$search_value_where = array("");
$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");

$result_data_relasi = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar",$search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result_data_relasi['Status'] == "Sukses") {
	$array_result_relasi_data_pendaftar = $result_data_relasi['Hasil'];
	$array_hasil_relasi_data_pendaftar;
	foreach ($array_result_relasi_data_pendaftar as $relasi_data_pendaftar) {
		$id_relasi_data_pendaftar = strval($relasi_data_pendaftar['Id_Pendaftar']);
		$array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$id_relasi_data_pendaftar] = $relasi_data_pendaftar;
	}


} else {
	$array_result_relasi_data_pendaftar = NULL;
}
//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar" ID_PRIMARY "Id_Pendaftar"
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cronjob</title>
</head>
<body>
	<center>
		<h1>Cronjob Otomatis Status Kelulusan Gugur Jika Melewati Masa Waktu Pembayaran</h1>
	</center>
	<center>
    <?php
			$search_field_where = array("Status","Status_PPDB","Masa_Waktu_Pembayaran_Pembelian_Formulir");
			$search_criteria_where = array("=","=","<>");
			$search_value_where = array("Aktif","Aktif","0");
			$search_connector_where = array("AND","AND","");

			$nomor = 0;

			$result = $a_cronjob->baca_data_dengan_filter("tb_data_ppdb",$search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$list_datatable_master_data_ppdb = $result['Hasil'];
				if(isset($list_datatable_master_data_ppdb)){
					foreach ($list_datatable_master_data_ppdb as $data_ppdb) {
                        ?>
                        <b><?php echo $data_ppdb['Judul']?></b>
                        <br>
                        Masa Waktu Pembayaran Pembelian Formulir : <?php echo $data_ppdb['Masa_Waktu_Pembayaran_Pembelian_Formulir']?> Hari
                        <table border='1'>
                            <tr>
                                <td>No</td>
                                <td>Nomor Pendaftaran</td>
                                <td>Nama Lengkap</td>
                                <td>Status Pembelian Formulir</td>
                                <td>Waktu Registrasi</td>
                                <td>Batas Tanggal Pembayaran Pembelian Formulir</td>
                                <td>Keterangan</td>
                            </tr>
                            <?php
                            $search_field_where = array("Status","Id_PPDB");
                            $search_criteria_where = array("=","=");
                            $search_value_where = array("Aktif","$data_ppdb[Id_PPDB]");
                            $search_connector_where = array("AND","");

                            $nomor = 0;

                            $result = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar",$search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

                            if ($result['Status'] == "Sukses") {
                                $list_datatable_master_data_pendaftar = $result['Hasil'];
                                if(isset($list_datatable_master_data_pendaftar)){
                                    foreach ($list_datatable_master_data_pendaftar as $data_pendaftar) {
                                        
                                        $search_field_where = array("Status","Status_Verifikasi_Pembelian_Formulir","Id_Pendaftar");
                                        $search_criteria_where = array("=","=","=");
                                        $search_value_where = array("Aktif","Belum Diverifikasi","$data_pendaftar[Id_Pendaftar]");
                                        $search_connector_where = array("AND","AND","");

                                        $nomor = 0;

                                        $result = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar_pembelian_formulir",$search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

                                        if ($result['Status'] == "Sukses") {
                                            $list_datatable_master_data_pendaftar_pembelian_formulir = $result['Hasil'];
                                            if(isset($list_datatable_master_data_pendaftar_pembelian_formulir)){
                                                foreach ($list_datatable_master_data_pendaftar_pembelian_formulir as $data) {
                                                    $nomor++; ?>
                                                    <?php
                                                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
                                                    if (isset($array_result_relasi_data_pendaftar)) {
                                                        $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                        if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'])) {
                                                            $data['Nomor_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'];
                                                        } else {
                                                            $data['Nomor_Pendaftaran'] = "";
                                                        }
                                                    } else {
                                                        $data['Nomor_Pendaftaran'] = "";
                                                    }
                                                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran

                                                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
                                                    if (isset($array_result_relasi_data_pendaftar)) {
                                                        $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                                        if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'])) {
                                                            $data['Nama_Lengkap'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'];
                                                        } else {
                                                            $data['Nama_Lengkap'] = "";
                                                        }
                                                    } else {
                                                        $data['Nama_Lengkap'] = "";
                                                    }
                                                    //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap

                                                    $Batas_Tanggal_Pembayaran_Pembelian_Formulir = date('Y-m-d', strtotime('+' . $data_ppdb['Masa_Waktu_Pembayaran_Pembelian_Formulir'] . ' days', strtotime(date('Y-m-d', strtotime($data['Waktu_Simpan_Data'])))));
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $nomor ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $data['Nomor_Pendaftaran'] ?>
                                                        </td>
                                                        <td><?php echo $data['Nama_Lengkap'] ?></td>
                                                        <td><?php echo $data['Status_Verifikasi_Pembelian_Formulir'] ?></td>
                                                        <td><?php echo $data['Waktu_Simpan_Data'] ?></td>
                                                        <td><?php echo $Batas_Tanggal_Pembayaran_Pembelian_Formulir ?></td>
                                                        <td>
                                                            <?php
                                                            $stringtime_Tanggal_Sekarang = strtotime($Tanggal_Sekarang);
                                                            $stringtime_Batas_Tanggal_Pembayaran_Pembelian_Formulir = strtotime($Batas_Tanggal_Pembayaran_Pembelian_Formulir);
                                                            
                                                            if ($stringtime_Tanggal_Sekarang > $stringtime_Batas_Tanggal_Pembayaran_Pembelian_Formulir) {
                                                                $form_field = array("Status_Kelulusan","Waktu_Update_Data");
                                                                $form_value = array("Gugur","$Waktu_Sekarang");
                                                                
                                                                $form_field_where = array("Id_Pendaftar", "Status_Kelulusan");
                                                                $form_criteria_where = array("=","<>");
                                                                $form_value_where = array("$data[Id_Pendaftar]","Gugur");
                                                                $form_connector_where = array("AND (","OR Status_Kelulusan IS NULL)");

                                                                $result_update = $a_cronjob->update_data("tb_data_pendaftar",$form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
                                                                if($result_update['Status'] == "Sukses"){
                                                                    echo "Status Kelulusan Menjadi Gugur";
                                                                }else{
                                                                    echo "Terjadi Error Pada Saat Akan Mengupdate Status Kelulusan Menjadi Gugur";
                                                                }
                                                            } else {
                                                            }
                                                            
                                                            ?>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                            ?>
                        </table>
                        <br>
                    <?php 
                    }
                }
            }
            ?>
	</center>
</body>
</html>