<?php
include "../init.php";
include "../models/cronjob/model_cronjob.php";
include "../models/global/pengumuman_pendaftar/model_pengumuman_pendaftar.php";
include "../controllers/global/pengumuman_pendaftar/controller_pengumuman_pendaftar.php";

$a_cronjob = new a_cronjob();
$a_pengumuman_pendaftar = new a_pengumuman_pendaftar();

//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar" ID_PRIMARY "Id_Pendaftar"
$search_field_where = array("Status");
$search_criteria_where = array("<>");
$search_value_where = array("");
$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");

$result_data_relasi = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar",$search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result_data_relasi['Status'] == "Sukses") {
	$array_result_relasi_data_pendaftar = $result_data_relasi['Hasil'];
	$array_hasil_relasi_data_pendaftar;
	foreach ($array_result_relasi_data_pendaftar as $relasi_data_pendaftar) {
		$id_relasi_data_pendaftar = strval($relasi_data_pendaftar['Id_Pendaftar']);
		$array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$id_relasi_data_pendaftar] = $relasi_data_pendaftar;
	}


} else {
	$array_result_relasi_data_pendaftar = NULL;
}
//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar" ID_PRIMARY "Id_Pendaftar"

#FUNGSI AMBIL LIST DATA PENDAFTAR
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_pembelian_formulir" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar_pembelian_formulir",$search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_pembelian_formulir = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_pembelian_formulir;
		foreach ($array_result_relasi_data_pendaftar_pembelian_formulir as $relasi_data_pendaftar_pembelian_formulir) {
			$id_relasi_data_pendaftar_pembelian_formulir = strval($relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$id_relasi_data_pendaftar_pembelian_formulir] = $relasi_data_pendaftar_pembelian_formulir;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_pembelian_formulir = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_pembelian_formulir" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_verifikasi_berkas" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar_verifikasi_berkas",$search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_verifikasi_berkas = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_verifikasi_berkas;
		foreach ($array_result_relasi_data_pendaftar_verifikasi_berkas as $relasi_data_pendaftar_verifikasi_berkas) {
			$id_relasi_data_pendaftar_verifikasi_berkas = strval($relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$id_relasi_data_pendaftar_verifikasi_berkas] = $relasi_data_pendaftar_verifikasi_berkas;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_verifikasi_berkas = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_verifikasi_berkas" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_pembayaran_ppdb" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar_pembayaran_ppdb",$search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_pembayaran_ppdb = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_pembayaran_ppdb;
		foreach ($array_result_relasi_data_pendaftar_pembayaran_ppdb as $relasi_data_pendaftar_pembayaran_ppdb) {
			$id_relasi_data_pendaftar_pembayaran_ppdb = strval($relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$id_relasi_data_pendaftar_pembayaran_ppdb] = $relasi_data_pendaftar_pembayaran_ppdb;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_pembayaran_ppdb = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_pembayaran_ppdb" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_siswa" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_cronjob->baca_data_dengan_filter("tb_data_siswa",$search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_siswa = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_siswa;
		foreach ($array_result_relasi_data_siswa as $relasi_data_siswa) {
			$id_relasi_data_siswa = strval($relasi_data_siswa['Id_Pendaftar']);
			$array_hasil_relasi_data_siswa['Id_Pendaftar'][$id_relasi_data_siswa] = $relasi_data_siswa;
		}
	
	
	}else{
		$array_result_relasi_data_siswa = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_siswa" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_nilai_rapor" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar_nilai_rapor",$search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_nilai_rapor = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_nilai_rapor;
		foreach ($array_result_relasi_data_pendaftar_nilai_rapor as $relasi_data_pendaftar_nilai_rapor) {
			$id_relasi_data_pendaftar_nilai_rapor = strval($relasi_data_pendaftar_nilai_rapor['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$id_relasi_data_pendaftar_nilai_rapor] = $relasi_data_pendaftar_nilai_rapor;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_nilai_rapor = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_nilai_rapor" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_program_layanan" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar_program_layanan",$search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_program_layanan = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_program_layanan;
		foreach ($array_result_relasi_data_pendaftar_program_layanan as $relasi_data_pendaftar_program_layanan) {
			$id_relasi_data_pendaftar_program_layanan = strval($relasi_data_pendaftar_program_layanan['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$id_relasi_data_pendaftar_program_layanan] = $relasi_data_pendaftar_program_layanan;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_program_layanan = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_program_layanan" ID_PRIMARY "Id_Pendaftar"
}


//FUNGSI UNTUK UPDATE OTOMATIS JIKA "Waktu_Sudah_Diverifikasi_Pembayaran_Pembelian_Formulir" Masih Kosong (Data Sebelumnya)
$search_field_where = array("Status","Status_Verifikasi_Pembelian_Formulir");
$search_criteria_where = array("=","=");
$search_value_where = array("Aktif","Sudah Diverifikasi");
$search_connector_where = array("AND","");

$nomor = 0;

$result = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar_pembelian_formulir",$search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
$list_datatable_master_data_pendaftar_pembelian_formulir = $result['Hasil'];
if(isset($list_datatable_master_data_pendaftar_pembelian_formulir)){
foreach ($list_datatable_master_data_pendaftar_pembelian_formulir as $data) {
    if(!($data['Waktu_Sudah_Diverifikasi_Pembayaran_Pembelian_Formulir'] <> "")){
        $form_field = array("Waktu_Sudah_Diverifikasi_Pembayaran_Pembelian_Formulir");
        $form_value = array("$data[Waktu_Update_Data]");

        $form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
        $form_criteria_where = array("=");
        $form_value_where = array("$data[Id_Pendaftar_Pembelian_Formulir]");
        $form_connector_where = array("");

        $result = $a_cronjob->update_data("tb_data_pendaftar_pembelian_formulir", $form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
        echo $data['Id_Pendaftar_Pembelian_Formulir'];
    }
    
}
}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cronjob</title>
</head>
<body>
	<center>
		<h1>Cronjob Otomatis Status Kelulusan Gugur Jika Melewati Masa Waktu Pengisian Berkas</h1>
	</center>
	<center>
    <?php
			$search_field_where = array("Status","Status_PPDB","Masa_Waktu_Pengisian_Berkas");
			$search_criteria_where = array("=","=","<>");
			$search_value_where = array("Aktif","Aktif","0");
			$search_connector_where = array("AND","AND","");

			$nomor = 0;

			$result = $a_cronjob->baca_data_dengan_filter("tb_data_ppdb",$search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$list_datatable_master_data_ppdb = $result['Hasil'];
				if(isset($list_datatable_master_data_ppdb)){
					foreach ($list_datatable_master_data_ppdb as $data_ppdb) {
                        ?>
                        <b><?php echo $data_ppdb['Judul']?></b>
                        <br>
                        Masa Waktu Pengisian Berkas : <?php echo $data_ppdb['Masa_Waktu_Pengisian_Berkas']?> Hari
                        <table border='1'>
                            <tr>
                                <td>No</td>
                                <td>Nomor Pendaftaran</td>
                                <td>Nama Lengkap</td>
                                <th>Pembelian Formulir</th>
                                <th>Verifikasi Data Diri</th>
                                <th>Verifikasi Nilai Rapor</th>
                                <th>Verifikasi Program Layanan</th>
                                <th>Verifikasi Berkas</th>
                                <th>Pembayaran PPDB</th>
                                <th>Waktu Sudah Diverifikasi Pembayaran Pembelian Formulir</th>
                                <th>Batas Tanggal Pengisian Berkas</th>
                                <th>Keterangan</th>
                            </tr>
                            <?php
                            $search_field_where = array("Status","Status_Verifikasi_Pembelian_Formulir");
                            $search_criteria_where = array("=","=");
                            $search_value_where = array("Aktif","Sudah Diverifikasi");
                            $search_connector_where = array("AND","");

                            $nomor = 0;

                            $result = $a_cronjob->baca_data_dengan_filter("tb_data_pendaftar_pembelian_formulir",$search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

                            if ($result['Status'] == "Sukses") {
                                $list_datatable_master_data_pendaftar_pembelian_formulir = $result['Hasil'];
                                if(isset($list_datatable_master_data_pendaftar_pembelian_formulir)){
                                    foreach ($list_datatable_master_data_pendaftar_pembelian_formulir as $data) {
                                        $nomor++; ?>
                                        <?php
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran
                                        if (isset($array_result_relasi_data_pendaftar)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'])) {
                                                $data['Nomor_Pendaftaran'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nomor_Pendaftaran'];
                                            } else {
                                                $data['Nomor_Pendaftaran'] = "";
                                            }
                                        } else {
                                            $data['Nomor_Pendaftaran'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nomor_Pendaftaran

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Id_PPDB
                                        if (isset($array_result_relasi_data_pendaftar)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_PPDB'])) {
                                                $data['Id_PPDB'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_PPDB'];
                                            } else {
                                                $data['Id_PPDB'] = "";
                                            }
                                        } else {
                                            $data['Id_PPDB'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Id_PPDB

                                        if($data['Id_PPDB'] <> $data_ppdb['Id_PPDB']){
                                            continue;
                                        }

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap
                                        if (isset($array_result_relasi_data_pendaftar)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'])) {
                                                $data['Nama_Lengkap'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Nama_Lengkap'];
                                            } else {
                                                $data['Nama_Lengkap'] = "";
                                            }
                                        } else {
                                            $data['Nama_Lengkap'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Nama_Lengkap

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Verifikasi_Data_Diri
                                        if (isset($array_result_relasi_data_pendaftar)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Data_Diri'])) {
                                                $data['Status_Verifikasi_Data_Diri'] = $array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Data_Diri'];
                                            } else {
                                                $data['Status_Verifikasi_Data_Diri'] = "Belum Diverifikasi";
                                            }
                                        } else {
                                            $data['Status_Verifikasi_Data_Diri'] = "Belum Diverifikasi";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar => Id_Pendaftar => Status_Verifikasi_Data_Diri

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_pembelian_formulir => Id_Pendaftar => Status_Verifikasi_Pembelian_Formulir
                                        if (isset($array_result_relasi_data_pendaftar_pembelian_formulir)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Pembelian_Formulir'])) {
                                                $data['Status_Verifikasi_Pembelian_Formulir'] = $array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Pembelian_Formulir'];
                                            } else {
                                                $data['Status_Verifikasi_Pembelian_Formulir'] = "";
                                            }
                                        } else {
                                            $data['Status_Verifikasi_Pembelian_Formulir'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_pembelian_formulir => Id_Pendaftar => Status_Verifikasi_Pembelian_Formulir

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_pembelian_formulir => Id_Pendaftar => Id_Pendaftar_Pembelian_Formulir
                                        if (isset($array_result_relasi_data_pendaftar_pembelian_formulir)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Pembelian_Formulir'])) {
                                                $data['Id_Pendaftar_Pembelian_Formulir'] = $array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Pembelian_Formulir'];
                                            } else {
                                                $data['Id_Pendaftar_Pembelian_Formulir'] = "";
                                            }
                                        } else {
                                            $data['Id_Pendaftar_Pembelian_Formulir'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_pembelian_formulir => Id_Pendaftar => Id_Pendaftar_Pembelian_Formulir


                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_verifikasi_berkas => Id_Pendaftar => Status_Verifikasi_Berkas
                                        if (isset($array_result_relasi_data_pendaftar_verifikasi_berkas)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Berkas'])) {
                                                $data['Status_Verifikasi_Berkas'] = $array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Berkas'];
                                            } else {
                                                $data['Status_Verifikasi_Berkas'] = "";
                                            }
                                        } else {
                                            $data['Status_Verifikasi_Berkas'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_verifikasi_berkas => Id_Pendaftar => Status_Verifikasi_Berkas

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_verifikasi_berkas => Id_Pendaftar => Id_Pendaftar_Verifikasi_Berkas
                                        if (isset($array_result_relasi_data_pendaftar_verifikasi_berkas)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Verifikasi_Berkas'])) {
                                                $data['Id_Pendaftar_Verifikasi_Berkas'] = $array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Verifikasi_Berkas'];
                                            } else {
                                                $data['Id_Pendaftar_Verifikasi_Berkas'] = "";
                                            }
                                        } else {
                                            $data['Id_Pendaftar_Verifikasi_Berkas'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_verifikasi_berkas => Id_Pendaftar => Id_Pendaftar_Verifikasi_Berkas

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_pembayaran_ppdb => Id_Pendaftar => Status_Verifikasi_Pembayaran_PPDB
                                        if (isset($array_result_relasi_data_pendaftar_pembayaran_ppdb)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Pembayaran_PPDB'])) {
                                                $data['Status_Verifikasi_Pembayaran_PPDB'] = $array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Pembayaran_PPDB'];
                                            } else {
                                                $data['Status_Verifikasi_Pembayaran_PPDB'] = "";
                                            }
                                        } else {
                                            $data['Status_Verifikasi_Pembayaran_PPDB'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_pembayaran_ppdb => Id_Pendaftar => Status_Verifikasi_Pembayaran_PPDB

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_pembayaran_ppdb => Id_Pendaftar => Id_Pendaftar_Pembayaran_PPDB
                                        if (isset($array_result_relasi_data_pendaftar_pembayaran_ppdb)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Pembayaran_PPDB'])) {
                                                $data['Id_Pendaftar_Pembayaran_PPDB'] = $array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Pembayaran_PPDB'];
                                            } else {
                                                $data['Id_Pendaftar_Pembayaran_PPDB'] = "";
                                            }
                                        } else {
                                            $data['Id_Pendaftar_Pembayaran_PPDB'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_pembayaran_ppdb => Id_Pendaftar => Id_Pendaftar_Pembayaran_PPDB

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_nilai_rapor => Id_Pendaftar => Status_Verifikasi_Nilai_Rapor
                                        if (isset($array_result_relasi_data_pendaftar_nilai_rapor)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Nilai_Rapor'])) {
                                                $data['Status_Verifikasi_Nilai_Rapor'] = $array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Nilai_Rapor'];
                                            } else {
                                                $data['Status_Verifikasi_Nilai_Rapor'] = "";
                                            }
                                        } else {
                                            $data['Status_Verifikasi_Nilai_Rapor'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_nilai_rapor => Id_Pendaftar => Status_Verifikasi_Nilai_Rapor
                                        
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_nilai_rapor => Id_Pendaftar => Id_Pendaftar_Nilai_Rapor
                                        if (isset($array_result_relasi_data_pendaftar_nilai_rapor)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Nilai_Rapor'])) {
                                                $data['Id_Pendaftar_Nilai_Rapor'] = $array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Nilai_Rapor'];
                                            } else {
                                                $data['Id_Pendaftar_Nilai_Rapor'] = "";
                                            }
                                        } else {
                                            $data['Id_Pendaftar_Nilai_Rapor'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_nilai_rapor => Id_Pendaftar => Id_Pendaftar_Nilai_Rapor

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_program_layanan => Id_Pendaftar => Status_Verifikasi_Program_Layanan
                                        if (isset($array_result_relasi_data_pendaftar_program_layanan)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Program_Layanan'])) {
                                                $data['Status_Verifikasi_Program_Layanan'] = $array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Status_Verifikasi_Program_Layanan'];
                                            } else {
                                                $data['Status_Verifikasi_Program_Layanan'] = "";
                                            }
                                        } else {
                                            $data['Status_Verifikasi_Program_Layanan'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_program_layanan => Id_Pendaftar => Status_Verifikasi_Program_Layanan

                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_program_layanan => Id_Pendaftar => Id_Pendaftar_Program_Layanan
                                        if (isset($array_result_relasi_data_pendaftar_program_layanan)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Program_Layanan'])) {
                                                $data['Id_Pendaftar_Program_Layanan'] = $array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar_Program_Layanan'];
                                            } else {
                                                $data['Id_Pendaftar_Program_Layanan'] = "";
                                            }
                                        } else {
                                            $data['Id_Pendaftar_Program_Layanan'] = "";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_pendaftar_program_layanan => Id_Pendaftar => Id_Pendaftar_Program_Layanan

                                        //BACA DATA RELASI ARRAY TABLE data_siswa => Id_Pendaftar => Id_Pendaftar
                                        if (isset($array_result_relasi_data_siswa)) {
                                            $Id_Pendaftar_Relasi = strval($data['Id_Pendaftar']);

                                            if (isset($array_hasil_relasi_data_siswa['Id_Pendaftar'][$Id_Pendaftar_Relasi]['Id_Pendaftar'])) {
                                                $Status_Salin_Ke_Data_Siswa_Master = "Sudah";
                                            } else {
                                                $Status_Salin_Ke_Data_Siswa_Master = "Belum";
                                            }
                                        } else {
                                            $Status_Salin_Ke_Data_Siswa_Master = "Belum";
                                        }
                                        //BACA DATA RELASI ARRAY TABLE data_siswa => Id_Pendaftar => Id_Pendaftar

                                        $Batas_Tanggal_Pengisian_Berkas = date('Y-m-d', strtotime('+' . $data_ppdb['Masa_Waktu_Pengisian_Berkas'] . ' days', strtotime(date('Y-m-d', strtotime($data['Waktu_Sudah_Diverifikasi_Pembayaran_Pembelian_Formulir'])))));
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $nomor ?>
                                            </td>
                                            <td>
                                                <?php echo $data['Nomor_Pendaftaran'] ?>
                                            </td>
                                            <td><?php echo $data['Nama_Lengkap'] ?></td>
                                            <td><?php echo $data['Status_Verifikasi_Pembelian_Formulir'] ?></td>
                                            <td><?php echo $data['Status_Verifikasi_Data_Diri'] ?></td>
                                            <td><?php echo $data['Status_Verifikasi_Nilai_Rapor'] ?></td>
                                            <td><?php echo $data['Status_Verifikasi_Program_Layanan'] ?></td>
                                            <td><?php echo $data['Status_Verifikasi_Berkas'] ?></td>
                                            <td><?php echo $data['Status_Verifikasi_Pembayaran_PPDB'] ?></td>
                                            <td><?php echo $data['Waktu_Sudah_Diverifikasi_Pembayaran_Pembelian_Formulir'] ?></td>
                                            <td><?php echo $Batas_Tanggal_Pengisian_Berkas ?></td>
                                            <td>
                                            <?php
                                                $stringtime_Tanggal_Sekarang = strtotime($Tanggal_Sekarang);
                                                $stringtime_Batas_Tanggal_Pengisian_Berkas = strtotime($Batas_Tanggal_Pengisian_Berkas);

                                                if(
                                                    ($data['Status_Verifikasi_Pembelian_Formulir'] <> "Sudah Diverifikasi") OR
                                                    ($data['Status_Verifikasi_Data_Diri'] <> "Sudah Diverifikasi") OR
                                                    ($data['Status_Verifikasi_Nilai_Rapor'] <> "Sudah Diverifikasi") OR
                                                    ($data['Status_Verifikasi_Program_Layanan'] <> "Sudah Diverifikasi") OR
                                                    ($data['Status_Verifikasi_Berkas'] <> "Sudah Diverifikasi")
                                                    ){
                                                        if ($stringtime_Tanggal_Sekarang > $stringtime_Batas_Tanggal_Pengisian_Berkas) {
                                                            $form_field = array("Status_Kelulusan","Waktu_Update_Data");
                                                            $form_value = array("Gugur","$Waktu_Sekarang");
                                                            
                                                            $form_field_where = array("Id_Pendaftar", "Status_Kelulusan");
                                                            $form_criteria_where = array("=","<>");
                                                            $form_value_where = array("$data[Id_Pendaftar]","Gugur");
                                                            $form_connector_where = array("AND (","OR Status_Kelulusan IS NULL)");
        
                                                            $result_update = $a_cronjob->update_data("tb_data_pendaftar",$form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
                                                            if($result_update['Status'] == "Sukses"){
                                                                //FUNGSI UNTUK MENYIMPAN PENGUMUMAN PENDAFTAR
                                                                $Waktu_Sekarang = date('Y-m-d H:i:s');
                                                                $Judul_Pengumuman_Untuk_Pendaftar = "Kelulusan";
                                                                $Isi_Pengumuman_Untuk_Pendaftar = "Mohon Maaf, Anda Tidak Lulus dalam PPDB ini, Dikarenakan telat mengisi semua Berkas PPDB yang dibutuhkan";
                                                                $Relasi = "Id_Pendaftar";
                                                                $Id_Relasi = "$data[Id_Pendaftar]";
                                                                
                                                                Simpan_Pengumuman_Pendaftar("$data[Id_Pendaftar]",$Relasi,"$Id_Relasi",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);


                                                                echo "Status Kelulusan Menjadi Gugur";
                                                            }else{
                                                                echo "Terjadi Error Pada Saat Akan Mengupdate Status Kelulusan Menjadi Gugur";
                                                            }
                                                        } else {
                                                            echo "Belum Melewati Batas Waktu Pengisian Berkas";
                                                        }
                                                    }else{
                                                        echo "Semua Data Sudah Diverifikasi Semua";
                                                    }

                                                
                                                
                                            ?>
                                            </td>
                                        </tr>

                                        <?php
                                    }
                                }
                            }
                            ?>
                        </table>
                        <br>
                    <?php 
                    }
                }
            }
            ?>
	</center>
</body>
</html>