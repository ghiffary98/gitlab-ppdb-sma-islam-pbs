<?php 
include "init.php";
include "models/admin/login/model_login.php";
include "models/admin/dashboard/model_dashboard.php";
include "models/global/aktivitas_pendaftar/model_aktivitas_pendaftar.php";
include "controllers/global/aktivitas_pendaftar/controller_aktivitas_pendaftar.php";
include "controllers/admin/dashboard/controller_dashboard.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Header -->
	<?php include 'components/header.php' ?>
</head>

<body>
	<div class="wrapper">

		<!-- Navbar -->
		<?php include 'components/navbar.php' ?>

		<!-- Sidebar -->
		<?php include 'components/sidebar_admin.php' ?>

		<div class="main-panel">

			<!-- Content -->
			<?php include 'components/content_admin.php' ?>

			<!-- Footer  -->
			<?php include 'components/footer.php' ?>
		</div>
	</div>

	<!-- Footer Javascript -->
	<?php include 'components/footer_javascript.php' ?>

</body>

</html>