-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 07, 2024 at 06:04 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_z_ppdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_aktivitas_pendaftar`
--

CREATE TABLE `tb_aktivitas_pendaftar` (
  `Id_Aktivitas_Pendaftar` int(1) NOT NULL,
  `Id_Pendaftar` int(1) NOT NULL,
  `Relasi` varchar(50) NOT NULL,
  `Id_Relasi` int(1) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Waktu_Simpan_Data` datetime NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_aktivitas_pendaftar`
--

INSERT INTO `tb_aktivitas_pendaftar` (`Id_Aktivitas_Pendaftar`, `Id_Pendaftar`, `Relasi`, `Id_Relasi`, `Judul`, `Deskripsi`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-18 09:28:23', 'Aktif'),
(2, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-18 09:44:56', 'Aktif'),
(3, 2, 'tb_data_pendaftar', 2, 'Login', 'Telah Melakukan Login', '2023-11-20 09:24:26', 'Aktif'),
(4, 2, 'tb_data_pendaftar_pembelian_formulir', 2, 'Pembelian Formulir', 'Telah Berhasil Melakukan Pembayaran Via Xendit', '2023-11-20 09:30:45', 'Aktif'),
(5, 2, 'tb_data_pendaftar', 2, 'Login', 'Telah Melakukan Login', '2023-11-20 09:34:54', 'Aktif'),
(6, 3, 'tb_data_pendaftar', 3, 'Login', 'Telah Melakukan Login', '2023-11-20 09:35:28', 'Aktif'),
(7, 3, 'tb_data_pendaftar', 3, 'Login', 'Telah Melakukan Login', '2023-11-20 09:38:48', 'Aktif'),
(8, 3, 'tb_data_pendaftar', 3, 'Login', 'Telah Melakukan Login', '2023-11-20 11:18:04', 'Aktif'),
(9, 3, 'tb_data_pendaftar', 3, 'Login', 'Telah Melakukan Login', '2023-11-20 11:45:31', 'Aktif'),
(10, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-20 12:26:14', 'Aktif'),
(11, 4, 'tb_data_pendaftar_pembelian_formulir', 4, 'Pembelian Formulir', 'Telah Berhasil Melakukan Pembayaran Via Xendit', '2023-11-20 12:38:20', 'Aktif'),
(12, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-20 13:05:37', 'Aktif'),
(13, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-20 13:19:10', 'Aktif'),
(14, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-20 13:25:26', 'Aktif'),
(15, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-20 13:38:08', 'Aktif'),
(16, 4, 'tb_data_pendaftar_nilai_rapor', 4, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-11-20 13:41:46', 'Aktif'),
(17, 4, 'tb_data_pendaftar_program_layanan', 4, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-11-20 13:42:55', 'Aktif'),
(18, 4, 'tb_data_pendaftar_program_layanan', 4, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-11-20 13:44:03', 'Aktif'),
(19, 4, 'tb_data_pendaftar_nilai_rapor', 4, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-11-20 13:51:31', 'Aktif'),
(20, 3, 'tb_data_pendaftar', 3, 'Login', 'Telah Melakukan Login', '2023-11-20 14:07:04', 'Aktif'),
(21, 3, 'tb_data_pendaftar', 3, 'Login', 'Telah Melakukan Login', '2023-11-20 14:10:10', 'Aktif'),
(22, 3, 'tb_data_pendaftar', 3, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-20 14:11:54', 'Aktif'),
(23, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-20 14:32:33', 'Aktif'),
(24, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-20 14:33:04', 'Aktif'),
(25, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-20 14:34:01', 'Aktif'),
(26, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-20 14:35:52', 'Aktif'),
(27, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-20 14:42:24', 'Aktif'),
(28, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-20 17:02:03', 'Aktif'),
(29, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-20 17:10:41', 'Aktif'),
(30, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-20 18:45:11', 'Aktif'),
(31, 5, 'tb_data_pendaftar', 5, 'Login', 'Telah Melakukan Login', '2023-11-20 18:48:56', 'Aktif'),
(32, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-21 07:07:56', 'Aktif'),
(33, 6, 'tb_data_pendaftar', 6, 'Login', 'Telah Melakukan Login', '2023-11-21 08:12:14', 'Aktif'),
(34, 7, 'tb_data_pendaftar', 7, 'Login', 'Telah Melakukan Login', '2023-11-21 08:12:20', 'Aktif'),
(35, 7, 'tb_data_pendaftar', 7, 'Login', 'Telah Melakukan Login', '2023-11-21 08:19:30', 'Aktif'),
(36, 7, 'tb_data_pendaftar_pembelian_formulir', 7, 'Pembelian Formulir', 'Telah Berhasil Melakukan Pembayaran Via Xendit', '2023-11-21 08:19:32', 'Aktif'),
(37, 7, 'tb_data_pendaftar', 7, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-21 08:42:13', 'Aktif'),
(38, 8, 'tb_data_pendaftar', 8, 'Login', 'Telah Melakukan Login', '2023-11-21 08:57:04', 'Aktif'),
(39, 8, 'tb_data_pendaftar', 8, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-21 09:01:05', 'Aktif'),
(40, 8, 'tb_data_pendaftar', 8, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-21 09:19:40', 'Aktif'),
(41, 6, 'tb_data_pendaftar', 6, 'Login', 'Telah Melakukan Login', '2023-11-21 09:24:07', 'Aktif'),
(42, 7, 'tb_data_pendaftar', 7, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-21 09:25:59', 'Aktif'),
(43, 6, 'tb_data_pendaftar', 6, 'Login', 'Telah Melakukan Login', '2023-11-21 09:31:38', 'Aktif'),
(44, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-21 10:09:21', 'Aktif'),
(45, 6, 'tb_data_pendaftar', 6, 'Login', 'Telah Melakukan Login', '2023-11-21 10:17:27', 'Aktif'),
(46, 6, 'tb_data_pendaftar', 6, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-21 10:24:43', 'Aktif'),
(47, 9, 'tb_data_pendaftar', 9, 'Login', 'Telah Melakukan Login', '2023-11-21 11:35:17', 'Aktif'),
(48, 9, 'tb_data_pendaftar', 9, 'Login', 'Telah Melakukan Login', '2023-11-21 11:40:50', 'Aktif'),
(49, 9, 'tb_data_pendaftar', 9, 'Login', 'Telah Melakukan Login', '2023-11-21 11:48:45', 'Aktif'),
(50, 9, 'tb_data_pendaftar', 9, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-21 11:54:31', 'Aktif'),
(51, 10, 'tb_data_pendaftar', 10, 'Login', 'Telah Melakukan Login', '2023-11-21 11:59:28', 'Aktif'),
(52, 10, 'tb_data_pendaftar_pembelian_formulir', 10, 'Pembelian Formulir', 'Telah Melakukan Upload Bukti Pembayaran Pembelian Formulir', '2023-11-21 12:01:47', 'Aktif'),
(53, 10, 'tb_data_pendaftar', 10, 'Login', 'Telah Melakukan Login', '2023-11-21 12:10:14', 'Aktif'),
(54, 10, 'tb_data_pendaftar', 10, 'Login', 'Telah Melakukan Login', '2023-11-21 13:09:33', 'Aktif'),
(55, 10, 'tb_data_pendaftar', 10, 'Login', 'Telah Melakukan Login', '2023-11-21 13:55:06', 'Aktif'),
(56, 10, 'tb_data_pendaftar', 10, 'Login', 'Telah Melakukan Login', '2023-11-21 14:15:20', 'Aktif'),
(57, 10, 'tb_data_pendaftar', 10, 'Login', 'Telah Melakukan Login', '2023-11-21 14:37:24', 'Aktif'),
(58, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-21 15:34:37', 'Aktif'),
(59, 10, 'tb_data_pendaftar', 10, 'Login', 'Telah Melakukan Login', '2023-11-21 16:29:13', 'Aktif'),
(60, 10, 'tb_data_pendaftar', 10, 'Login', 'Telah Melakukan Login', '2023-11-21 17:31:35', 'Aktif'),
(61, 10, 'tb_data_pendaftar', 10, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-21 17:56:30', 'Aktif'),
(62, 10, 'tb_data_pendaftar_nilai_rapor', 10, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-11-21 18:04:46', 'Aktif'),
(63, 10, 'tb_data_pendaftar_verifikasi_berkas', 10, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Akta Kelahiran', '2023-11-21 18:27:38', 'Aktif'),
(64, 10, 'tb_data_pendaftar_verifikasi_berkas', 10, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu Keluarga', '2023-11-21 18:33:28', 'Aktif'),
(65, 10, 'tb_data_pendaftar_verifikasi_berkas', 10, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu NISN', '2023-11-21 19:01:39', 'Aktif'),
(66, 10, 'tb_data_pendaftar_verifikasi_berkas', 10, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Rapor', '2023-11-21 19:06:12', 'Aktif'),
(67, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-21 19:07:32', 'Aktif'),
(68, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-22 08:08:43', 'Aktif'),
(69, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-22 09:39:52', 'Aktif'),
(70, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-22 09:43:11', 'Aktif'),
(71, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-22 09:43:34', 'Aktif'),
(72, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-22 10:03:38', 'Aktif'),
(73, 10, 'tb_data_pendaftar', 10, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-22 10:48:49', 'Aktif'),
(74, 12, 'tb_data_pendaftar', 12, 'Login', 'Telah Melakukan Login', '2023-11-22 19:33:09', 'Aktif'),
(75, 12, 'tb_data_pendaftar_pembelian_formulir', 11, 'Pembelian Formulir', 'Telah Berhasil Melakukan Pembayaran Via Xendit', '2023-11-22 19:36:33', 'Aktif'),
(76, 12, 'tb_data_pendaftar', 12, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-22 20:01:42', 'Aktif'),
(77, 12, 'tb_data_pendaftar', 12, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-22 20:04:36', 'Aktif'),
(78, 12, 'tb_data_pendaftar', 12, 'Login', 'Telah Melakukan Login', '2023-11-22 20:07:19', 'Aktif'),
(79, 12, 'tb_data_pendaftar_nilai_rapor', 11, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-11-22 20:11:58', 'Aktif'),
(80, 12, 'tb_data_pendaftar_program_layanan', 11, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-11-22 20:13:31', 'Aktif'),
(81, 12, 'tb_data_pendaftar', 12, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-22 20:16:27', 'Aktif'),
(82, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-23 08:21:57', 'Aktif'),
(83, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-23 08:23:56', 'Aktif'),
(84, 12, 'tb_data_pendaftar', 12, 'Login', 'Telah Melakukan Login', '2023-11-23 08:44:38', 'Aktif'),
(85, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-23 14:42:28', 'Aktif'),
(86, 18, 'tb_data_pendaftar', 18, 'Login', 'Telah Melakukan Login', '2023-11-24 09:25:45', 'Aktif'),
(87, 13, 'tb_data_pendaftar', 13, 'Login', 'Telah Melakukan Login', '2023-11-24 09:51:43', 'Aktif'),
(88, 14, 'tb_data_pendaftar', 14, 'Login', 'Telah Melakukan Login', '2023-11-24 09:52:44', 'Aktif'),
(89, 15, 'tb_data_pendaftar', 15, 'Login', 'Telah Melakukan Login', '2023-11-24 10:03:47', 'Aktif'),
(90, 16, 'tb_data_pendaftar', 16, 'Login', 'Telah Melakukan Login', '2023-11-24 10:04:17', 'Aktif'),
(91, 17, 'tb_data_pendaftar', 17, 'Login', 'Telah Melakukan Login', '2023-11-24 10:05:19', 'Aktif'),
(92, 19, 'tb_data_pendaftar', 19, 'Login', 'Telah Melakukan Login', '2023-11-24 10:06:34', 'Aktif'),
(93, 20, 'tb_data_pendaftar', 20, 'Login', 'Telah Melakukan Login', '2023-11-24 10:45:30', 'Aktif'),
(94, 20, 'tb_data_pendaftar_pembelian_formulir', 19, 'Pembelian Formulir', 'Telah Melakukan Upload Bukti Pembayaran Pembelian Formulir', '2023-11-24 10:45:47', 'Aktif'),
(95, 20, 'tb_data_pendaftar', 20, 'Login', 'Telah Melakukan Login', '2023-11-24 11:06:51', 'Aktif'),
(96, 20, 'tb_data_pendaftar', 20, 'Login', 'Telah Melakukan Login', '2023-11-24 12:42:43', 'Aktif'),
(97, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-24 12:55:41', 'Aktif'),
(98, 20, 'tb_data_pendaftar', 20, 'Login', 'Telah Melakukan Login', '2023-11-24 13:09:59', 'Aktif'),
(99, 20, 'tb_data_pendaftar', 20, 'Login', 'Telah Melakukan Login', '2023-11-24 14:19:53', 'Aktif'),
(100, 20, 'tb_data_pendaftar', 20, 'Login', 'Telah Melakukan Login', '2023-11-24 14:21:32', 'Aktif'),
(101, 20, 'tb_data_pendaftar', 20, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-24 14:27:11', 'Aktif'),
(102, 20, 'tb_data_pendaftar_verifikasi_berkas', 19, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu NISN', '2023-11-24 14:31:40', 'Aktif'),
(103, 20, 'tb_data_pendaftar_verifikasi_berkas', 19, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu Keluarga', '2023-11-24 14:32:10', 'Aktif'),
(104, 20, 'tb_data_pendaftar_verifikasi_berkas', 19, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Akta Kelahiran', '2023-11-24 14:32:49', 'Aktif'),
(105, 20, 'tb_data_pendaftar', 20, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-24 15:25:58', 'Aktif'),
(106, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-24 16:21:13', 'Aktif'),
(107, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-24 16:24:00', 'Aktif'),
(108, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-24 16:31:01', 'Aktif'),
(109, 20, 'tb_data_pendaftar_verifikasi_berkas', 19, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Rapor', '2023-11-24 19:26:50', 'Aktif'),
(110, 12, 'tb_data_pendaftar', 12, 'Login', 'Telah Melakukan Login', '2023-11-25 04:46:19', 'Aktif'),
(111, 12, 'tb_data_pendaftar', 12, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-25 04:47:34', 'Aktif'),
(113, 8, 'tb_data_pendaftar', 8, 'Login', 'Telah Melakukan Login', '2023-11-25 10:02:56', 'Aktif'),
(115, 20, 'tb_data_pendaftar_program_layanan', 19, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-11-25 10:11:33', 'Aktif'),
(116, 9, 'tb_data_pendaftar', 9, 'Login', 'Telah Melakukan Login', '2023-11-25 10:57:14', 'Aktif'),
(117, 9, 'tb_data_pendaftar', 9, 'Login', 'Telah Melakukan Login', '2023-11-25 11:00:24', 'Aktif'),
(120, 13, 'tb_data_pendaftar', 13, 'Login', 'Telah Melakukan Login', '2023-11-25 15:12:15', 'Aktif'),
(121, 9, 'tb_data_pendaftar', 9, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-25 20:28:16', 'Aktif'),
(122, 9, 'tb_data_pendaftar_program_layanan', 9, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-11-25 20:30:10', 'Aktif'),
(123, 9, 'tb_data_pendaftar_verifikasi_berkas', 9, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Akta Kelahiran', '2023-11-25 20:35:11', 'Aktif'),
(124, 9, 'tb_data_pendaftar_verifikasi_berkas', 9, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu Keluarga', '2023-11-25 20:37:20', 'Aktif'),
(125, 9, 'tb_data_pendaftar_verifikasi_berkas', 9, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu NISN', '2023-11-25 20:38:26', 'Aktif'),
(126, 13, 'tb_data_pendaftar', 13, 'Login', 'Telah Melakukan Login', '2023-11-26 16:04:54', 'Aktif'),
(127, 13, 'tb_data_pendaftar_pembelian_formulir', 13, 'Pembelian Formulir', 'Telah Berhasil Melakukan Pembayaran Via Xendit', '2023-11-26 16:16:56', 'Aktif'),
(128, 21, 'tb_data_pendaftar', 21, 'Login', 'Telah Melakukan Login', '2023-11-27 09:57:30', 'Aktif'),
(129, 13, 'tb_data_pendaftar', 13, 'Login', 'Telah Melakukan Login', '2023-11-27 10:02:31', 'Aktif'),
(130, 22, 'tb_data_pendaftar', 22, 'Login', 'Telah Melakukan Login', '2023-11-27 10:18:21', 'Aktif'),
(131, 22, 'tb_data_pendaftar_pembelian_formulir', 21, 'Pembelian Formulir', 'Telah Berhasil Melakukan Pembayaran Via Xendit', '2023-11-27 10:24:14', 'Aktif'),
(132, 14, 'tb_data_pendaftar', 14, 'Login', 'Telah Melakukan Login', '2023-11-27 11:23:24', 'Aktif'),
(133, 14, 'tb_data_pendaftar', 14, 'Login', 'Telah Melakukan Login', '2023-11-27 11:37:43', 'Aktif'),
(134, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-27 11:43:37', 'Aktif'),
(135, 10, 'tb_data_pendaftar', 10, 'Login', 'Telah Melakukan Login', '2023-11-27 15:44:12', 'Aktif'),
(136, 23, 'tb_data_pendaftar', 23, 'Login', 'Telah Melakukan Login', '2023-11-28 11:52:18', 'Aktif'),
(137, 23, 'tb_data_pendaftar', 23, 'Login', 'Telah Melakukan Login', '2023-11-28 11:52:46', 'Aktif'),
(138, 23, 'tb_data_pendaftar_pembelian_formulir', 22, 'Pembelian Formulir', 'Telah Berhasil Melakukan Pembayaran Via Xendit', '2023-11-28 11:59:40', 'Aktif'),
(139, 21, 'tb_data_pendaftar_pembelian_formulir', 20, 'Pembelian Formulir', 'Telah Berhasil Melakukan Pembayaran Via Xendit', '2023-11-28 13:09:10', 'Aktif'),
(140, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-29 07:27:27', 'Aktif'),
(141, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-29 07:34:30', 'Aktif'),
(142, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-29 13:26:14', 'Aktif'),
(143, 4, 'tb_data_pendaftar', 4, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-29 13:36:14', 'Aktif'),
(144, 4, 'tb_data_pendaftar_pembayaran_ppdb', 4, 'Pembayaran PPDB', 'Telah Melakukan Upload Bukti Pembayaran PPDB', '2023-11-29 13:48:18', 'Aktif'),
(145, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-29 16:08:05', 'Aktif'),
(146, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-29 21:45:07', 'Aktif'),
(147, 4, 'tb_data_pendaftar_nilai_rapor', 4, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-11-29 22:01:12', 'Aktif'),
(148, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-29 22:09:33', 'Aktif'),
(149, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-29 22:22:48', 'Aktif'),
(150, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-11-29 22:26:18', 'Aktif'),
(151, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-30 08:36:14', 'Aktif'),
(152, 1, 'tb_data_pendaftar', 1, 'Login', 'Telah Melakukan Login', '2023-11-30 09:07:26', 'Aktif'),
(153, 7, 'tb_data_pendaftar', 7, 'Login', 'Telah Melakukan Login', '2023-11-30 09:08:14', 'Aktif'),
(154, 7, 'tb_data_pendaftar', 7, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-11-30 09:54:26', 'Aktif'),
(155, 7, 'tb_data_pendaftar_nilai_rapor', 7, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-11-30 09:59:36', 'Aktif'),
(156, 7, 'tb_data_pendaftar_program_layanan', 7, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-11-30 10:03:07', 'Aktif'),
(157, 7, 'tb_data_pendaftar_verifikasi_berkas', 7, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Akta Kelahiran', '2023-11-30 10:04:40', 'Aktif'),
(158, 7, 'tb_data_pendaftar_verifikasi_berkas', 7, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu Keluarga', '2023-11-30 10:05:04', 'Aktif'),
(159, 7, 'tb_data_pendaftar_verifikasi_berkas', 7, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu NISN', '2023-11-30 10:05:32', 'Aktif'),
(160, 7, 'tb_data_pendaftar_verifikasi_berkas', 7, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Rapor', '2023-11-30 10:24:09', 'Aktif'),
(161, 24, 'tb_data_pendaftar', 24, 'Login', 'Telah Melakukan Login', '2023-11-30 16:03:07', 'Aktif'),
(162, 24, 'tb_data_pendaftar', 24, 'Login', 'Telah Melakukan Login', '2023-11-30 16:10:48', 'Aktif'),
(163, 24, 'tb_data_pendaftar_pembelian_formulir', 23, 'Pembelian Formulir', 'Telah Melakukan Upload Bukti Pembayaran Pembelian Formulir', '2023-11-30 16:12:23', 'Aktif'),
(164, 24, 'tb_data_pendaftar', 24, 'Login', 'Telah Melakukan Login', '2023-11-30 16:28:10', 'Aktif'),
(165, 24, 'tb_data_pendaftar', 24, 'Login', 'Telah Melakukan Login', '2023-12-01 12:48:59', 'Aktif'),
(166, 24, 'tb_data_pendaftar', 24, 'Login', 'Telah Melakukan Login', '2023-12-01 13:51:31', 'Aktif'),
(167, 25, 'tb_data_pendaftar', 25, 'Login', 'Telah Melakukan Login', '2023-12-01 15:41:59', 'Aktif'),
(168, 25, 'tb_data_pendaftar', 25, 'Login', 'Telah Melakukan Login', '2023-12-01 15:43:46', 'Aktif'),
(169, 21, 'tb_data_pendaftar', 21, 'Login', 'Telah Melakukan Login', '2023-12-02 11:02:31', 'Aktif'),
(170, 20, 'tb_data_pendaftar_program_layanan', 19, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-12-03 07:45:13', 'Aktif'),
(171, 20, 'tb_data_pendaftar', 20, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-12-03 07:54:14', 'Aktif'),
(172, 2, 'tb_data_pendaftar', 2, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-12-03 11:55:10', 'Aktif'),
(173, 2, 'tb_data_pendaftar_program_layanan', 2, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-12-03 11:58:35', 'Aktif'),
(174, 2, 'tb_data_pendaftar_program_layanan', 2, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-12-03 12:01:36', 'Aktif'),
(175, 2, 'tb_data_pendaftar_verifikasi_berkas', 2, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Rapor', '2023-12-03 12:06:12', 'Aktif'),
(176, 2, 'tb_data_pendaftar_verifikasi_berkas', 2, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu NISN', '2023-12-03 12:07:17', 'Aktif'),
(177, 2, 'tb_data_pendaftar_verifikasi_berkas', 2, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Akta Kelahiran', '2023-12-03 12:08:04', 'Aktif'),
(178, 2, 'tb_data_pendaftar_verifikasi_berkas', 2, 'Verifikasi Berkas', 'Telah Mengupload / Mengupdate Berkas Kartu Keluarga', '2023-12-03 12:08:53', 'Aktif'),
(179, 2, 'tb_data_pendaftar_nilai_rapor', 2, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-12-03 12:13:35', 'Aktif'),
(180, 2, 'tb_data_pendaftar_nilai_rapor', 2, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-12-03 12:13:47', 'Aktif'),
(181, 2, 'tb_data_pendaftar_nilai_rapor', 2, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-12-03 12:14:04', 'Aktif'),
(182, 2, 'tb_data_pendaftar_nilai_rapor', 2, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-12-03 12:15:05', 'Aktif'),
(183, 2, 'tb_data_pendaftar_nilai_rapor', 2, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-12-03 12:15:39', 'Aktif'),
(184, 2, 'tb_data_pendaftar_program_layanan', 2, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-12-03 12:15:53', 'Aktif'),
(185, 26, 'tb_data_pendaftar', 26, 'Login', 'Telah Melakukan Login', '2023-12-03 23:25:36', 'Aktif'),
(186, 26, 'tb_data_pendaftar_pembelian_formulir', 25, 'Pembelian Formulir', 'Telah Melakukan Upload Bukti Pembayaran Pembelian Formulir', '2023-12-03 23:29:51', 'Aktif'),
(187, 2, 'tb_data_pendaftar_nilai_rapor', 2, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-12-04 10:29:28', 'Aktif'),
(188, 27, 'tb_data_pendaftar', 27, 'Login', 'Telah Melakukan Login', '2023-12-04 19:19:50', 'Aktif'),
(189, 27, 'tb_data_pendaftar_pembelian_formulir', 26, 'Pembelian Formulir', 'Telah Melakukan Upload Bukti Pembayaran Pembelian Formulir', '2023-12-04 19:31:29', 'Aktif'),
(190, 27, 'tb_data_pendaftar', 27, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-12-04 20:03:10', 'Aktif'),
(191, 4, 'tb_data_pendaftar', 4, 'Login', 'Telah Melakukan Login', '2023-12-05 11:38:25', 'Aktif'),
(192, 27, 'tb_data_pendaftar', 27, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-12-05 11:44:10', 'Aktif'),
(193, 26, 'tb_data_pendaftar', 26, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-12-05 12:16:31', 'Aktif'),
(194, 15, 'tb_data_pendaftar', 15, 'Login', 'Telah Melakukan Login', '2023-12-05 12:44:17', 'Aktif'),
(195, 15, 'tb_data_pendaftar', 15, 'Login', 'Telah Melakukan Login', '2023-12-05 12:47:13', 'Aktif'),
(196, 28, 'tb_data_pendaftar', 28, 'Login', 'Telah Melakukan Login', '2023-12-05 12:50:19', 'Aktif'),
(197, 15, 'tb_data_pendaftar', 15, 'Login', 'Telah Melakukan Login', '2023-12-05 13:00:01', 'Aktif'),
(198, 7, 'tb_data_pendaftar', 7, 'Login', 'Telah Melakukan Login', '2023-12-05 13:35:54', 'Aktif'),
(199, 7, 'tb_data_pendaftar', 7, 'Login', 'Telah Melakukan Login', '2023-12-05 13:42:35', 'Aktif'),
(200, 7, 'tb_data_pendaftar', 7, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-12-05 13:45:04', 'Aktif'),
(201, 7, 'tb_data_pendaftar_nilai_rapor', 7, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-12-05 13:51:22', 'Aktif'),
(202, 7, 'tb_data_pendaftar_program_layanan', 7, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-12-05 13:51:31', 'Aktif'),
(203, 7, 'tb_data_pendaftar', 7, 'Login', 'Telah Melakukan Login', '2023-12-05 13:58:16', 'Aktif'),
(204, 7, 'tb_data_pendaftar', 7, 'Login', 'Telah Melakukan Login', '2023-12-05 14:02:54', 'Aktif'),
(205, 7, 'tb_data_pendaftar_pembayaran_ppdb', 7, 'Pembayaran PPDB', 'Telah Melakukan Upload Bukti Pembayaran PPDB', '2023-12-05 14:12:06', 'Aktif'),
(206, 15, 'tb_data_pendaftar', 15, 'Login', 'Telah Melakukan Login', '2023-12-05 14:58:51', 'Aktif'),
(207, 15, 'tb_data_pendaftar_pembelian_formulir', 15, 'Pembelian Formulir', 'Telah Berhasil Melakukan Pembayaran Via Xendit', '2023-12-05 14:58:53', 'Aktif'),
(208, 15, 'tb_data_pendaftar', 15, 'Login', 'Telah Melakukan Login', '2023-12-05 15:37:03', 'Aktif'),
(209, 15, 'tb_data_pendaftar', 15, 'Verifikasi Data Diri', 'Telah Mengupdate Formulir Data Diri', '2023-12-05 15:57:06', 'Aktif'),
(210, 15, 'tb_data_pendaftar_nilai_rapor', 15, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-12-05 16:04:51', 'Aktif'),
(211, 15, 'tb_data_pendaftar_program_layanan', 15, 'Program Layanan', 'Telah Mengupdate Program Layanan', '2023-12-05 16:05:45', 'Aktif'),
(212, 2, 'tb_data_pendaftar', 2, 'Login', 'Telah Melakukan Login', '2023-12-29 23:49:19', 'Aktif'),
(213, 2, 'tb_data_pendaftar_nilai_rapor', 2, 'Nilai Rapor', 'Telah Mengupdate Nilai Rapor', '2023-12-29 23:52:50', 'Aktif'),
(214, 2, 'tb_data_pendaftar', 2, 'Login', 'Telah Melakukan Login', '2024-01-04 21:49:17', 'Aktif'),
(215, 2, 'tb_data_pendaftar', 2, 'Login', 'Telah Melakukan Login', '2024-01-06 00:07:03', 'Aktif'),
(216, 2, 'tb_data_pendaftar', 2, 'Login', 'Telah Melakukan Login', '2024-01-07 02:39:46', 'Aktif'),
(217, 29, 'tb_data_pendaftar', 29, 'Login', 'Telah Melakukan Login', '2024-01-07 03:33:41', 'Aktif'),
(218, 31, 'tb_data_pendaftar', 31, 'Login', 'Telah Melakukan Login', '2024-01-07 03:34:01', 'Aktif'),
(219, 31, 'tb_data_pendaftar_pembelian_formulir', 29, 'Pembelian Formulir', 'Telah Melakukan Upload Bukti Pembayaran Pembelian Formulir', '2024-01-07 06:54:34', 'Aktif'),
(220, 30, 'tb_data_pendaftar', 30, 'Login', 'Telah Melakukan Login', '2024-01-07 23:39:48', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_aktivitas_pengguna`
--

CREATE TABLE `tb_aktivitas_pengguna` (
  `Id_Aktivitas_Pengguna` int(1) NOT NULL,
  `Id_Pengguna` int(1) NOT NULL,
  `Relasi` varchar(50) NOT NULL,
  `Id_Relasi` int(1) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Waktu_Simpan_Data` datetime NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_hak_akses`
--

CREATE TABLE `tb_data_hak_akses` (
  `Id_Hak_Akses` int(11) NOT NULL,
  `Hak_Akses` varchar(50) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Waktu_Update_Data` datetime NOT NULL,
  `Waktu_Simpan_Data` datetime NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_hak_akses`
--

INSERT INTO `tb_data_hak_akses` (`Id_Hak_Akses`, `Hak_Akses`, `Deskripsi`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'Super Administrator', 'Super Administrator', '2023-09-26 18:07:38', '2023-09-26 18:07:38', 'Aktif'),
(2, 'Administrator', 'Administrator', '0000-00-00 00:00:00', '2023-09-26 23:08:18', 'Aktif'),
(3, 'Staf', 'Staf', '2023-12-05 16:06:45', '2023-09-26 23:25:06', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_hak_akses_detail`
--

CREATE TABLE `tb_data_hak_akses_detail` (
  `Id_Hak_Akses_Detail` int(11) NOT NULL,
  `Id_Hak_Akses` int(11) NOT NULL,
  `Nama_Halaman` varchar(100) NOT NULL,
  `Kode_Halaman` varchar(100) NOT NULL,
  `Baca_Data` varchar(5) NOT NULL,
  `Simpan_Data` varchar(5) NOT NULL,
  `Update_Data` varchar(5) NOT NULL,
  `Hapus_Data` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_hak_akses_detail`
--

INSERT INTO `tb_data_hak_akses_detail` (`Id_Hak_Akses_Detail`, `Id_Hak_Akses`, `Nama_Halaman`, `Kode_Halaman`, `Baca_Data`, `Simpan_Data`, `Update_Data`, `Hapus_Data`) VALUES
(1, 2, 'Data Pendaftar', 'Data_Pendaftar', 'Iya', 'Iya', '', ''),
(2, 2, 'Pembayaran PPDB', 'Data_Pendaftar_Pembayaran_PPDB', 'Iya', 'Iya', '', ''),
(3, 2, 'Pembelian Formulir', 'Data_Pendaftar_Pembelian_Formulir', 'Iya', 'Iya', '', ''),
(4, 2, 'Verifikasi Berkas', 'Data_Pendaftar_Verifikasi_Berkas', 'Iya', 'Iya', '', ''),
(5, 2, 'Data PPDB', 'Data_PPDB', 'Iya', 'Iya', 'Iya', ''),
(6, 2, 'Data Siswa', 'Data_Siswa', 'Iya', 'Iya', '', ''),
(7, 3, 'Data Pendaftar', 'Data_Pendaftar', 'Iya', 'Iya', 'Iya', 'Iya'),
(8, 3, 'Pembayaran PPDB', 'Data_Pendaftar_Pembayaran_PPDB', 'Iya', 'Iya', 'Iya', 'Iya'),
(9, 3, 'Pembelian Formulir', 'Data_Pendaftar_Pembelian_Formulir', 'Iya', '', 'Iya', ''),
(10, 3, 'Verifikasi Berkas', 'Data_Pendaftar_Verifikasi_Berkas', 'Iya', 'Iya', 'Iya', 'Iya'),
(11, 3, 'Data PPDB', 'Data_PPDB', 'Iya', '', 'Iya', ''),
(12, 3, 'Data Siswa', 'Data_Siswa', 'Iya', 'Iya', 'Iya', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar`
--

CREATE TABLE `tb_data_pendaftar` (
  `Id_Pendaftar` int(11) NOT NULL,
  `Id_PPDB` int(11) NOT NULL,
  `Nomor_Pendaftaran` varchar(20) NOT NULL,
  `Username` varchar(100) NOT NULL,
  `Password` text NOT NULL,
  `Password_Pendaftar` text DEFAULT NULL,
  `NIK` varchar(50) DEFAULT NULL,
  `NIS` varchar(50) DEFAULT NULL,
  `NISN` varchar(50) DEFAULT NULL,
  `NIPD` varchar(50) DEFAULT NULL,
  `Nama_Lengkap` varchar(100) DEFAULT NULL,
  `Jenis_Kelamin` varchar(20) DEFAULT NULL,
  `Tempat_Lahir` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Agama` varchar(20) DEFAULT NULL,
  `Status_Dalam_Keluarga` varchar(20) DEFAULT NULL,
  `Jalan` text DEFAULT NULL,
  `Kelurahan` varchar(100) NOT NULL,
  `Kecamatan` varchar(100) DEFAULT NULL,
  `Kota` varchar(100) DEFAULT NULL,
  `Provinsi` varchar(100) DEFAULT NULL,
  `Id_Kelurahan` int(11) DEFAULT NULL,
  `Id_Kecamatan` int(11) DEFAULT NULL,
  `Id_Kota` int(11) DEFAULT NULL,
  `Id_Provinsi` int(11) DEFAULT NULL,
  `Kode_Pos` varchar(10) DEFAULT NULL,
  `Titik_Lintang_Alamat` varchar(50) DEFAULT NULL,
  `Titik_Bujur_Alamat` varchar(50) DEFAULT NULL,
  `Nomor_Handphone` varchar(15) DEFAULT NULL,
  `Nomor_Telepon` varchar(15) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Golongan_Darah` varchar(2) DEFAULT NULL,
  `Kebutuhan_Khusus` varchar(30) DEFAULT NULL,
  `Tinggi_Badan` int(11) DEFAULT NULL,
  `Berat_Badan` int(11) DEFAULT NULL,
  `Lingkar_Kepala` int(1) DEFAULT NULL,
  `Asal_Sekolah` varchar(100) DEFAULT NULL,
  `Alamat_Sekolah` text DEFAULT NULL,
  `Kelas` varchar(5) DEFAULT NULL,
  `Nomor_Peserta_Ujian_Nasional` varchar(50) DEFAULT NULL,
  `Nomor_Seri_Ijazah` varchar(50) DEFAULT NULL,
  `Penerima_KPS` varchar(5) DEFAULT NULL,
  `Penerima_KIP` varchar(5) DEFAULT NULL,
  `Penerima_KJP_DKI` varchar(5) DEFAULT NULL,
  `Nomor_KIP` varchar(50) DEFAULT NULL,
  `Nomor_KJP_DKI` varchar(50) DEFAULT NULL,
  `Nomor_KPS` varchar(50) DEFAULT NULL,
  `Nomor_KKS` varchar(50) DEFAULT NULL,
  `Nomor_KK` varchar(50) DEFAULT NULL,
  `Nomor_Registrasi_Akta_Lahir` varchar(50) DEFAULT NULL,
  `Anak_Ke` varchar(3) DEFAULT NULL,
  `Jumlah_Saudara` varchar(3) DEFAULT NULL,
  `Status_Perwalian` varchar(50) DEFAULT NULL,
  `NIK_Ayah` varchar(50) DEFAULT NULL,
  `Nama_Ayah` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Ayah` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir_Ayah` date DEFAULT NULL,
  `Jenjang_Pendidikan_Ayah` varchar(30) DEFAULT NULL,
  `Pekerjaan_Ayah` varchar(100) DEFAULT NULL,
  `Penghasilan_Ayah` varchar(100) DEFAULT NULL,
  `No_HP_Ayah` varchar(16) DEFAULT NULL,
  `NIK_Ibu` varchar(50) DEFAULT NULL,
  `Nama_Ibu` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Ibu` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir_Ibu` date DEFAULT NULL,
  `Jenjang_Pendidikan_Ibu` varchar(30) DEFAULT NULL,
  `Pekerjaan_Ibu` varchar(100) DEFAULT NULL,
  `Penghasilan_Ibu` varchar(100) DEFAULT NULL,
  `No_HP_Ibu` varchar(16) DEFAULT NULL,
  `NIK_Wali` varchar(50) DEFAULT NULL,
  `Nama_Wali` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Wali` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir_Wali` date DEFAULT NULL,
  `Jenjang_Pendidikan_Wali` varchar(30) DEFAULT NULL,
  `Pekerjaan_Wali` varchar(100) DEFAULT NULL,
  `Penghasilan_Wali` varchar(100) DEFAULT NULL,
  `No_HP_Wali` varchar(16) DEFAULT NULL,
  `Kendaraan_Yang_Dipakai_Kesekolah` varchar(100) DEFAULT NULL,
  `Jarak_Rumah_Ke_Sekolah` int(1) DEFAULT NULL,
  `Ukuran_Seragam` varchar(5) DEFAULT NULL,
  `Status_Verifikasi_Data_Diri` varchar(50) DEFAULT NULL,
  `Status_Pendaftaran` varchar(50) DEFAULT NULL,
  `Status_Kelulusan` varchar(50) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_pendaftar`
--

INSERT INTO `tb_data_pendaftar` (`Id_Pendaftar`, `Id_PPDB`, `Nomor_Pendaftaran`, `Username`, `Password`, `Password_Pendaftar`, `NIK`, `NIS`, `NISN`, `NIPD`, `Nama_Lengkap`, `Jenis_Kelamin`, `Tempat_Lahir`, `Tanggal_Lahir`, `Agama`, `Status_Dalam_Keluarga`, `Jalan`, `Kelurahan`, `Kecamatan`, `Kota`, `Provinsi`, `Id_Kelurahan`, `Id_Kecamatan`, `Id_Kota`, `Id_Provinsi`, `Kode_Pos`, `Titik_Lintang_Alamat`, `Titik_Bujur_Alamat`, `Nomor_Handphone`, `Nomor_Telepon`, `Email`, `Golongan_Darah`, `Kebutuhan_Khusus`, `Tinggi_Badan`, `Berat_Badan`, `Lingkar_Kepala`, `Asal_Sekolah`, `Alamat_Sekolah`, `Kelas`, `Nomor_Peserta_Ujian_Nasional`, `Nomor_Seri_Ijazah`, `Penerima_KPS`, `Penerima_KIP`, `Penerima_KJP_DKI`, `Nomor_KIP`, `Nomor_KJP_DKI`, `Nomor_KPS`, `Nomor_KKS`, `Nomor_KK`, `Nomor_Registrasi_Akta_Lahir`, `Anak_Ke`, `Jumlah_Saudara`, `Status_Perwalian`, `NIK_Ayah`, `Nama_Ayah`, `Tempat_Lahir_Ayah`, `Tanggal_Lahir_Ayah`, `Jenjang_Pendidikan_Ayah`, `Pekerjaan_Ayah`, `Penghasilan_Ayah`, `No_HP_Ayah`, `NIK_Ibu`, `Nama_Ibu`, `Tempat_Lahir_Ibu`, `Tanggal_Lahir_Ibu`, `Jenjang_Pendidikan_Ibu`, `Pekerjaan_Ibu`, `Penghasilan_Ibu`, `No_HP_Ibu`, `NIK_Wali`, `Nama_Wali`, `Tempat_Lahir_Wali`, `Tanggal_Lahir_Wali`, `Jenjang_Pendidikan_Wali`, `Pekerjaan_Wali`, `Penghasilan_Wali`, `No_HP_Wali`, `Kendaraan_Yang_Dipakai_Kesekolah`, `Jarak_Rumah_Ke_Sekolah`, `Ukuran_Seragam`, `Status_Verifikasi_Data_Diri`, `Status_Pendaftaran`, `Status_Kelulusan`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 1, 'P10001', 'anikrira323@gmail.com', '806aabbc9792300aa98735c3151e7c80', 'VmxaYWIyTXlSa2hTYTJoaFVqSm9jRmx0ZUV0ak1XeDBUVmR3VVZWVU1Eaz0=', NULL, NULL, NULL, NULL, 'Razan Muhammad Ihsan', NULL, 'Depok', '2009-08-23', NULL, NULL, 'Kav.Pesona blok D no.25 rt.014/01 kel.Pasirgunung Selatan Cimanggis Depok 16451', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '081295310953', NULL, 'anikrira323@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2023-12-05 07:00:02', '2023-11-18 09:27:43', 'Aktif'),
(2, 3, 'P30001', 'revaganaufalkusuma@gmail.com', '074f68a7bc614f7368b514ccc8f66011', 'Vmtaa2QxWnRVWGRPV0VaU1ZrWlZPUT09', '3276022506090004', '431921222303', '0095060671', '', 'REVAGA NAUFAL KUSUMA', 'Laki-laki', 'JAKARTA', '2009-06-25', 'Islam', 'Anak', 'Permata Puri Laguna A3 No. 5 Rt. 001 Rw. 021 Cimanggis', 'MEKARSARI', 'CIMANGGIS', 'KOTA DEPOK', 'JAWA BARAT', 2147483647, 3276040, 3276, 32, '16452', '', '', '085775093462', '', 'revaganaufalkusuma@gmail.com', 'B', 'Tidak', 168, 75, 58, 'SMP ISLAM PB SOEDIRMAN', 'Cibubur', 'X', '111', '', 'Tidak', 'Tidak', 'Tidak', '', '', '', '', '3276022303100012', '', '1', '1', 'Kandung', '3276020407800015', 'Webiandrie Kusuma Putera', 'Jakarta', '1980-07-04', 'S2', 'Karyawan Swasta', 'Rp. 5.000.000 - Rp. 20.000.000', '08128357889', '3276022506090004', 'Astariya Werdiningrum', 'Dumai', '1980-08-18', 'S2', 'Karyawan Swasta', 'Rp. 5.000.000 - Rp. 20.000.000', '081808620962', '', '', '', '0000-00-00', '', '', '', '', 'Mobil', 9, '2XL', 'Sudah Diverifikasi', 'Dalam Proses', 'Gugur', '2024-01-07 23:15:32', '2023-11-20 09:23:07', 'Aktif'),
(3, 3, 'P30002', 'mizaivadita@gmail.com', '946764cbdf0cb695f3c24acea9cc52fd', 'VmtaYVUyRnRWa1pOV0VaU1ZrWlZPUT09', '', '', '', '', 'MIZA IVADITA ANUHEA', 'Perempuan', 'JAKARTA', '2009-12-17', 'Islam', '', 'Jl. Dukuh V No. 78', '', '', '', '', NULL, NULL, NULL, NULL, '', '', '', '081385700251', '', 'mizaivadita@gmail.com', '', '', 0, 0, 0, '', '', 'X', '', '', '', '', NULL, '', NULL, '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', 0, NULL, 'Belum Diverifikasi', 'Dalam Proses', NULL, '2023-11-20 14:11:54', '2023-11-20 09:35:02', 'Aktif'),
(4, 3, 'P30003', 'ichsan.anwar.achmad@gmail.com', '799e211189ecd84e0b642d30a1944fa5', 'VjFod1MyTXlWblJWV0d4VFYwZG9UMWxYYzNkUFVUMDk=', '3276025008090003', '431921222216', '0099965936', '', 'ILMIRA TRIBUANA SANDYA PARAMITHA', 'Perempuan', 'Jakarta', '2009-08-10', 'Islam', '', 'WISMA HARAPAN 1 BLOK A NO. 10 RT 002/019', 'MEKARSARI', 'CIMANGGIS', 'KOTA DEPOK', 'JAWA BARAT', 2147483647, 3276040, 3276, 32, '16452', '', '', '087786697527', '087786697527', 'ichsan.anwar.achmad@gmail.com', 'B', 'Tidak', 160, 51, 0, 'SMP ISLAM AL-AZHAR 19 CIBUBUR', 'Jl. Jambore Raya No.9A Cibubur Ciracas Jakarta Timur', 'X', '', '', 'Tidak', 'Tidak', '', '', '', '', '', '3276021409090019', '', '3', '2', 'Kandung', '3276022706700006', 'Mohammad Ichsan', 'Jakarta', '1970-06-27', 'S2', 'Karyawan Swasta', 'Rp. 5.000.000 - Rp. 20.000.000', '08111377777', '3276024712700009', 'Verasari Rusnalita', 'Jakarta', '1970-08-07', 'S1', 'Karyawan Swasta', 'Rp. 5.000.000 - Rp. 20.000.000', '08121271270', '', '', '', '0000-00-00', '', '', '', '', 'Angkutan Umum', 10, 'L', 'Sudah Diverifikasi', 'Dalam Proses', 'Gugur', '2024-01-07 23:15:32', '2023-11-20 12:25:43', 'Aktif'),
(5, 3, 'P30004', 'yunita.wulan85@gmail.com', 'ff310d964053d39efab8edafaf2a01e8', 'VmpGamVGSXlUWGxUV0d4UFYwVndjbFpyV2xaUFVUMDk=', NULL, NULL, NULL, NULL, 'Dhirgham Nafi\' Arhab Adiwidianto', NULL, 'Jakarta', '2009-02-11', NULL, NULL, 'Jl. Kenanga 1 No. 37 RT 02/02, Kalisari, Pasar Rebo, Jakarta Timur, 13790', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '08569851935', NULL, 'yunita.wulan85@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2023-12-28 23:08:07', '2023-11-20 18:48:39', 'Aktif'),
(6, 3, 'P30005', 'bunda.naidza@gmail.com', '799e211189ecd84e0b642d30a1944fa5', 'VjFod1MyTXlWblJWV0d4VFYwZG9UMWxYYzNkUFVUMDk=', '3175046210080005', '', '0081706514', '', 'DZAKIYYA AZKA RAMADHAN', 'Laki-laki', 'JAKARTA', '1980-12-06', 'Islam', '', 'Jln  Pam Ciburial, RT.01/RW.11, Sukamantri, Kec. Tamansari, Kab. Bogor, Jawa Barat 16610', '', '', '', '', NULL, NULL, NULL, NULL, '', '', '', '082122570650', '', 'bunda.naidza@gmail.com', 'A', 'Tidak', 165, 60, 31, 'SMP AL MINHAJ  TAMANSARI', 'Jalan Pam Ciburial, RT.01/RW.11, Sukamantri, Kec. Tamansari, Kabupaten Bogor, Jawa Barat 16610', 'X', '', '', 'Tidak', 'Tidak', NULL, '', NULL, '', '', '3175040701095288', '', '1', '1', '', '', 'Muhamad Ramadhan', 'JAKARTA', '0000-00-00', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', 0, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-21 08:11:20', 'Aktif'),
(7, 3, 'P30006', 'anikrira323@gmail.com', 'd42ddda556f601692a3c082e46e11188', 'VmpGYWFrMVhUa2hTYWxwVlYwZGpPUT09', '3276022308090005', '2276', '0092153325', '', 'Razan Muhammad Ihsan', '', 'Depok', '2009-08-23', 'Islam', 'anak', 'Kav. Pesona blok D no.25 rt.014/01', 'PASIR GUNUNG SELATAN', 'CIMANGGIS', 'KOTA DEPOK', 'JAWA BARAT', 2147483647, 3276040, 3276, 32, '16451', '', '', '081282727853', '', 'razanmuhammadihsan.2308@gmail.com', 'B', 'Tidak', 166, 51, 0, 'SMP ISLAM AL-MA\'RUF', 'Jl. Raya Lapangan Tembak, Cibubur', 'X', '0092153325', '', 'Tidak', 'Tidak', 'Tidak', '', '', '', '', '327602231009049', '', '2', '1', 'Kandung', '3276023103800009', 'Ary Darsono', 'Jakarta', '1980-03-31', 'S2', 'Wiraswasta', 'Rp 5.000.000 - Rp 20.000.0000', '088905786564', '3276026902800007', 'Anik Mulyani', 'Jakarta', '1980-02-29', 'S1', 'Ibu Rumah Tangga', 'Rp 5.000.000 - Rp 20.000.0000', '081295310953', '', '', '', '0000-00-00', '', '', '', '', 'Motor', 4, 'L', 'Sudah Diverifikasi', 'Dalam Proses', 'Gugur', '2024-01-07 23:15:32', '2023-11-21 08:12:07', 'Aktif'),
(8, 3, 'P30007', 'Rindrapradipanugraha15@gmail.com', '799e211189ecd84e0b642d30a1944fa5', 'VjFod1MyTXlWblJWV0d4VFYwZG9UMWxYYzNkUFVUMDk=', '', '', '0091707817', '', 'RINDA PRADIPA NUGRAHA', 'Laki-laki', 'Yogyakarta', '2009-01-15', 'Islam', '', 'Cibubur Residence Cluster Punewood Blok D4 no 7', '', '', '', '', NULL, NULL, NULL, NULL, '', '', '', '081212509840', '', 'Rindrapradipanugraha15@gmail.com', 'B', 'Tidak', 166, 55, 0, '', 'Jln. Suryadarma 1A, perumahan bumi dirgantara permai', 'X', '', '', '', '', NULL, '', NULL, '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', 0, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-21 08:56:18', 'Aktif'),
(9, 3, 'P30008', 'ararya.adiyatma@gmail.com', '8c3b574610feb6d70abef6c4daba13f5', 'Vm14U1EyTXhXa2RYV0dSVFYwZG9UMWxYYzNkUFVUMDk=', '3275102911080002', '2232', '0084257789', '', 'ARARYA ADIYATMA RASYIDIN', 'Laki-laki', 'BEKASI', '2008-11-29', 'Islam', 'Anak', 'JL. MAWAR V NO. 14 BLOK CS 6 RT 010 RW 011', 'Jatisampurna', 'Jatisampurna', 'Bekasi kota', 'Jawa Barat', NULL, NULL, NULL, NULL, '17433', '', '', '081213584772', '08119790680', 'ararya.adiyatma@gmail.com', 'A', 'Tidak', 170, 75, 0, 'SMP ISLAM AL-MA\'RUF', 'Jl. Raya Lapangan Tembak Cibubur', 'X', '', '', 'Tidak', 'Tidak', NULL, '', NULL, '', '', '3275101711080032', 'AL.638.0119598', '1', '1', 'Kandung', '3275102109770001', 'Idhi Heru Supriyadi', 'Yogyakarta', '1977-09-21', 'S2', 'Karyawan BUMN', 'Rp. 5.000.000 - Rp. 20.000.000', '08119790680', '3275105205830007', 'Hari Milaningsih', 'Boyolali', '1983-05-12', 'S1', 'Ibu Rumah Tangga', 'Tidak Berpenghasilan', '085719125936', '3275102109770001', 'Idhi Heru Supriyadi', 'Yogyakarta', '1977-09-21', 'S2', 'Karyawan BUMN', 'Rp. 5.000.000 - Rp. 20.000.000', '08119790680', 'Mobil', 10, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-21 11:26:51', 'Aktif'),
(10, 3, 'P30009', 'irawardhana13@gmail.com', '74cb83fa796a321b87c9746125703dec', 'VmpKMGExSXlTbGRqU0ZKVFYwZDRVRmR1YjNkUFVUMDk=', '3175042602090002', '11864', '0094913146', '', 'Daffa Raditya Febrio Wardhana', 'Laki-laki', 'Jakarta', '2009-02-26', 'Islam', 'Anak', 'Jln. Akustik Blok E14 rt.5/rw.8 Kav. DKI, Cipayung, Jakarta Timur', 'Cipayung', 'Cipayung', 'Jakarta Timur', 'DKI Jakarta', NULL, NULL, NULL, NULL, '13840', '', '', '081395000516', '', 'irawardhana13@gmail.com', 'B', 'Tidak', 180, 91, 63, 'SMP ISLAM PB SOEDIRMAN', 'Jln. Raya Bogor Km. 24,  Cijantung, Jakarta Timur', 'X', '', '', 'Tidak', 'Tidak', NULL, '', NULL, '', '', '3175040303100018', '6988/KLU/JS/2009', '1', '2', 'Kandung', '3175040406810007', 'Adhitya Wardhana', 'Jakarta', '1981-06-04', 'S1', 'Karyawan Swasta', 'Rp. 5.000.000 - Rp. 20.000.000', '081386222995', '3175045307810001', 'Ira Herawati', 'Jakarta', '1981-07-13', 'S1', 'Ibu Rumah Tangga', 'Tidak Berpenghasilan', '08158812137', '', '', '', '0000-00-00', '', '', '', '', 'Mobil', 5, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-21 11:59:21', 'Aktif'),
(11, 3, 'P30010', 'aishazr28@gmail.com', '799e211189ecd84e0b642d30a1944fa5', 'VjFod1MyTXlWblJWV0d4VFYwZG9UMWxYYzNkUFVUMDk=', NULL, NULL, NULL, NULL, 'AISHA ZAHRA RATIFAH', NULL, 'JAKARTA', '2008-11-28', NULL, NULL, 'JL. RAYA CONDET', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '085311680581', NULL, 'aishazr28@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', NULL, NULL, '2023-11-22 18:32:09', 'Aktif'),
(12, 3, 'P30011', 'decyantini@gmail.com', '9debf79411225a6256f750b55a6e472d', 'VmtaYVUxTnRWbkpPVlZaWFZrVndVRnBJYjNkUFVUMDk=', '3174047011090001', '21220735', '0099670183', '', 'NAJLAA HUWAINA ZAHRA', 'Perempuan', 'Jakarta', '2009-11-30', 'Islam', 'Anak Bungsu', 'CIBUBUR RESIDENCE BLK A3 NO.2, JL.RAYA ALTERNATIF CIBUBUR KM.2', 'Jatisampurna', 'Jatisampurna', 'Bekasi', 'Jawa Barat', NULL, NULL, NULL, NULL, '17433', 'LS 6.374037', 'BT 106.912196', '08161627626', '02184312512', 'decyantini@gmail.com', 'B', 'Tidak', 165, 65, 58, '', 'BRIGHT ON JUNIOR HIGH SCHOOL, Jl Alternatif Cibubur, Kav. DDN No.7, Cimanggis, Depok 16454', 'X', '', '', 'Tidak', 'Tidak', NULL, '', NULL, '', '', '3174040601097765', 'AL5000941064', '3', '2', 'Kandung', '3174040506690010', 'Moh. Teguh Hindarwan', 'Jakarta', '1969-06-05', 'S1', 'Karyawan Swasta', 'Lebih dari Rp 20.000.000', '08164852451', '3174045512710007', 'Decyantini Lompatanata', 'Surabaya', '1971-12-15', 'S1', 'Karyawan Swasta', 'Lebih dari Rp 20.000.000', '08161627626', '', '', '', '0000-00-00', '', '', '', '', 'Mobil', 12, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-22 19:32:51', 'Aktif'),
(13, 3, 'P30012', 'apel8no1@gmail.com', '799e211189ecd84e0b642d30a1944fa5', 'VjFod1MyTXlWblJWV0d4VFYwZG9UMWxYYzNkUFVUMDk=', NULL, NULL, NULL, NULL, 'ELVINDHIA ZHENITHA MAHENDRI', NULL, 'DEPOK', '2009-03-18', NULL, NULL, 'JL.APEL RAYA BLOK A8 NO1', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '08129427172', NULL, 'apel8no1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', NULL, NULL, '2023-11-23 16:18:08', 'Aktif'),
(14, 3, 'P30013', 'lakeishacallysthaalzena@gmail.com', '8c3b574610feb6d70abef6c4daba13f5', 'Vm14U1EyTXhXa2RYV0dSVFYwZG9UMWxYYzNkUFVUMDk=', NULL, NULL, NULL, NULL, 'LAKEISHA CALLYSTHA ALZENA', NULL, 'DEPOK', '2009-08-05', NULL, NULL, 'Jl. Sadar RTM, Perum. Villa Bougenville Blok F3/1', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '085711694713', NULL, 'lakeishacallysthaalzena@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Menunggu Hasil Lulus', '2023-12-05 12:53:34', '2023-11-23 16:25:51', 'Aktif'),
(15, 3, 'P30014', 'ikhtiarashazia@gmail.com', '8c3b574610feb6d70abef6c4daba13f5', 'Vm14U1EyTXhXa2RYV0dSVFYwZG9UMWxYYzNkUFVUMDk=', '3275104205090003', '431921222215', '0093260396', '', 'IKHTIARA SHAZIA MEHLIKA', 'Perempuan', 'JAKARTA', '2009-05-02', 'Islam', 'ANAK', 'Perum Citragran Cluster Terrace Garden blok G35 no 20', 'JATIKARYA', 'JATISAMPURNA', 'KOTA BEKASI', 'JAWA BARAT', 2147483647, 3275011, 3275, 32, '17435', '', '', '081318054739', '', 'ikhtiarashazia@gmail.com', 'A', 'Tidak', 160, 58, 0, 'SMP ISLAM AL-AZHAR 19 CIBUBUR', 'JL. JAMBORE RAYA NO 9A CIBUBUR, CIRACAS, JAKARTA TIMUR', 'X', '', '', 'Tidak', 'Tidak', 'Tidak', '', '', '', '', '3275102906090020', '', '1', '1', 'Kandung', '3275102201820001', 'RULLY HILYAN DWIPUTRA', 'MAGELANG', '1982-01-22', 'S1', 'Karyawan Swasta', '', '081806071243', '', 'IRA TRI HANDAYANI DJUNDJUNAN', 'BANDUNG', '1984-04-04', 'S1', 'Karyawan Swasta', '', '08172398423', '', '', '', '0000-00-00', '', '', '', '', '', 0, 'L', 'Sudah Diverifikasi', 'Dalam Proses', 'Gugur', '2024-01-07 23:15:32', '2023-11-23 16:30:48', 'Aktif'),
(16, 3, 'P30015', 'zenitasofia2@gmail.com', '8c3b574610feb6d70abef6c4daba13f5', 'Vm14U1EyTXhXa2RYV0dSVFYwZG9UMWxYYzNkUFVUMDk=', NULL, NULL, NULL, NULL, 'ZENITHA SHOFIA KARA', NULL, 'JAKARTA', '2009-06-10', NULL, NULL, 'JL. CEMARA NO. 15 RT 006 RW 014', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '082123526058', NULL, 'zenitasofia2@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2023-12-05 07:00:02', '2023-11-24 08:53:04', 'Aktif'),
(17, 3, 'P30016', 'faiqbarzian@gmail.com', '8c3b574610feb6d70abef6c4daba13f5', 'Vm14U1EyTXhXa2RYV0dSVFYwZG9UMWxYYzNkUFVUMDk=', NULL, NULL, NULL, NULL, 'KEANDRA FAIQ BARZIAN', NULL, 'HULU SUNGAI UTARA', '2009-02-03', NULL, NULL, 'CIBUBUR RESIDENCE BLOK B2/5', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '081242515248', NULL, 'faiqbarzian@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2023-12-05 07:00:02', '2023-11-24 08:58:00', 'Aktif'),
(18, 3, 'P30017', 'rishantiningtyas.kp@gmail.com', '8c3b574610feb6d70abef6c4daba13f5', 'Vm14U1EyTXhXa2RYV0dSVFYwZG9UMWxYYzNkUFVUMDk=', NULL, NULL, NULL, NULL, 'RISHANTININGTYAS KUSUMA PUTRI', NULL, 'JAKARTA', '2009-05-29', NULL, NULL, 'Jl. Tiga Berlian VII, Blok C5, No.118. Komp. Krama Yudha', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '082171787323', NULL, 'rishantiningtyas.kp@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-24 09:07:48', 'Aktif'),
(19, 3, 'P30018', 'astarimeita@gmail.com', '8c3b574610feb6d70abef6c4daba13f5', 'Vm14U1EyTXhXa2RYV0dSVFYwZG9UMWxYYzNkUFVUMDk=', NULL, NULL, NULL, NULL, 'NUGRAHA PRATAMA ADHYNATA', NULL, 'DEPOK', '2008-08-13', NULL, NULL, 'PERUM CINAGKA INDAH', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0895391798902', NULL, 'astarimeita@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2023-12-05 07:00:02', '2023-11-24 09:12:05', 'Aktif'),
(20, 3, 'P30019', 'sekarpuspitosari@gmail.com', '8c3b574610feb6d70abef6c4daba13f5', 'Vm14U1EyTXhXa2RYV0dSVFYwZG9UMWxYYzNkUFVUMDk=', '3175104306091001', '', '0094241946', '', 'SEKAR PUSPITO SARI', 'Perempuan', 'JAKARTA', '2009-06-03', 'Islam', 'Anak Kandung', 'Raya Ceger no.73 rt.002 rw.01', 'Ceger', 'Cipayung', 'Jakarta Timur', 'DKI Jakarta', 0, 0, 0, 0, '13820', '-6.311097', '106.888670', '081913030509', '081212804409', 'sekarpuspitosari@gmail.com', 'A', 'Tidak', 115, 60, 3, 'SMP NIZAMIA ANDALUSIA', 'SMP Nizamia Andalusia  Jl. Raya Mabes', 'X', '', '', 'Tidak', 'Tidak', 'Tidak', '', '', '', '', '3175100803120007', '', '2', '1', 'Kandung', '-', 'SIKIT GUNAWAN, S.H.', 'SOLO', '1963-03-10', 'S1', 'Sudah Meninggal', 'Tidak Berpenghasilan', '0', '3175106412760006', 'RAHAYU WIJAYANTI', 'JAKARTA', '1976-12-24', 'S2', 'PNS/TNI/POLRI', 'Rp. 5.000.000 - Rp. 20.000.000', '081212804409', '', '', '', '0000-00-00', '', '', '', '', 'Mobil', 10, '3XL', 'Sudah Diverifikasi', 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-24 10:44:47', 'Aktif'),
(21, 3, 'P30020', 'raissa6usamah@gmail.com', 'c451ffeb42070ddd6c526b6e67e09bd6', 'VmxSR2ExUXlSa1pPV0U1WFltNUNZVlp1Y0VkaU1XeFdWV3RhYkZaVVJUSldWbEpHVUZFOVBRPT0=', NULL, NULL, NULL, NULL, 'RAISSA NUR AMALIA', NULL, 'Semarang', '2008-06-06', NULL, NULL, 'Jln. Dukuh V No. 28 RT. 007/RW.002 Dukuh, Kramat Jati, Jakarta Timur 13550', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '088293575369', NULL, 'raissa6usamah@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-27 09:56:43', 'Aktif'),
(22, 3, 'P30021', 'winne.widiantini@gmail.com', '4603a18514bdeb70655eb4c29e29f68e', 'Vm0xMGEySXlSa2RpUm14VVltdEtjVlpyVm5KbFZsSldWV3R3VVZWVU1Eaz0=', NULL, NULL, NULL, NULL, 'RAZZAN ATH THAARIQ AZKA SUMBODO', NULL, 'Depok', '2009-02-26', NULL, NULL, 'Komplek Wisma Harapan I Blok E1 No. 7 Mekarsari Cimanggis Depok', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '081381339695', NULL, 'winne.widiantini@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-27 10:18:12', 'Aktif'),
(23, 3, 'P30022', 'fakhrijal082@gmail.com', 'd1101df2b14ef8fd6aefdabf90b8e26b', 'VmpGYWFrMVhSWGhqUm14VlltdHdZVlpyWkd0T1VUMDk=', NULL, NULL, NULL, NULL, 'LUTFIATUN NISA', NULL, 'Gampong Drien', '2009-03-11', NULL, NULL, 'Jl. Sawo No.65 Rt 03 Rw 10', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '085270124210', NULL, 'fakhrijal082@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-28 11:51:24', 'Aktif'),
(24, 3, 'P30023', 'riamandasari0803@gmail.com', 'cea57a5d877acd8d54820ebdc67192cd', 'VjFSSmVGSXlTbGRqU0ZKVFYwZDRVRmxYTVhwUFVUMDk=', NULL, NULL, NULL, NULL, 'RAFFA ADRIAN PUTRA', NULL, 'Jakarta', '2009-02-26', NULL, NULL, 'Jln H Taiman Timur 1 RT 10 RW O9 No.311 Kp. Gedong Pasar Rebo-Jakarta Timur 13760', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '081286855632', NULL, 'riamandasari0803@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-11-30 16:02:52', 'Aktif'),
(25, 3, 'P30024', 'dawirizky2020@gmail.com', '00187aa1351acd00db6aa61628c82653', 'VjFSSmVGSXlTbGRpUmxaVVYwZFNUMVZyWkhwUFVUMDk=', NULL, NULL, NULL, NULL, 'RAFASHAH RIZKY ASKARILLAH', NULL, 'Jakarta', '2009-12-05', NULL, NULL, 'Gg. 100 Jl. Lenteng Agung Rt. 002/02, No. 147 Jagakarsa-Jakarta Selatan', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '08568182839', NULL, 'dawirizky2020@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-12-01 15:37:45', 'Aktif'),
(26, 3, 'P30025', 'novaya.alfian@gmail.com', '93c512c33854b4427b98543910af363c', 'VmtaYVUyRnRVWGRQVlZaU1ZrWktZVlpxVG05a1JteFhXa1ZrYTFWVU1Eaz0=', '3175040503091002', '', '0095726866', '', 'ALFATHIR AFKARI ALI FIANOV', 'Laki-laki', 'Jakarta', '2009-03-05', 'Islam', 'anak kandung', 'Ali Town House Jl. H. Baing No. 111 RT.11 RW.04 Kel. Tengah Kec. Kramat Jati Condet Jakarta Timur 13540', 'KAMPUNG TENGAH', 'KRAMAT JATI', 'KOTA JAKARTA TIMUR', 'DKI JAKARTA', 2147483647, 3172050, 3172, 31, '13540', '', '', '0811547547', '02187794747', 'novaya.alfian@gmail.com', 'B', 'Tidak', 172, 62, 0, 'SMPS IT BUAHATI ISLAMIC SCHOOL', 'Jl. H. Baing no. 99 RT.11 RW.04 Kel. Tengah Kec Kramat Jati Jakarta Timur', 'X', '', '', 'Tidak', '', 'Tidak', '', '', '', '', '3175042911111021', 'AL.637.0252064', '1', '2', 'Kandung', '3175082011780014', 'Muhammad Alfian', 'Ponorogo', '1978-11-20', 'S1', 'Karyawan Swasta', 'Rp. 5.000.000 - Rp. 20.000.000', '085287756855', '3175046811761001', 'Novaya Sagita Utami', 'Singkawang', '1976-11-28', 'S1', 'Karyawan Swasta', 'Rp. 5.000.000 - Rp. 20.000.000', '0811547547', '', '', '', '0000-00-00', '', '', '', '', 'Motor', 4, 'XL', NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:23:43', '2023-12-03 23:25:27', 'Aktif'),
(27, 3, 'P30026', 'zackrymuhammadaziz0@gmail.com', '01466bd9ffba7e4b5451d02e63ec2995', 'VjJ4amVGSXlSblJTV0hCVVlXeGFUMVpyVm5Oa1VUMDk=', '3175042010080004', '11967', '0084494640', '', 'ZACKRY MUHAMMAD AZIZ', 'Laki-laki', 'Jakarta', '2008-10-20', 'Islam', 'Anak', 'jln dato tonggara 1 rt7 rw11 no42 kramat jati', 'KRAMAT JATI', 'KRAMAT JATI', 'KOTA JAKARTA TIMUR', 'DKI JAKARTA', 2147483647, 3172050, 3172, 31, '', '', '', '085961471246', '', 'zackrymuhammadaziz0@gmail.com', 'O', 'Tidak', 171, 54, 0, 'SMP ISLAM PB SOEDIRMAN', 'Jln raya bogor', 'X', '', '', 'Tidak', 'Tidak', 'Tidak', '', '', '', '', '3175041901095253', 'AL.500.0642040', '2', '1', 'Kandung', '3175041301760012', 'Muhammad haidir', 'Jakarta', '1976-01-13', 'D3', 'Wirausaha', 'Lebih dari Rp 20.000.000', '081218437122', '3175046105760012', 'Elia harmila', 'Bandung', '1976-05-21', 'D3', 'Ibu Rumah Tangga', 'Tidak Berpenghasilan', '081282262206', '', '', '', '0000-00-00', '', '', '', '', 'Mobil', 10, 'M', NULL, 'Dalam Proses', 'Gugur', '2024-01-07 23:18:54', '2023-12-04 19:19:31', 'Aktif'),
(28, 3, 'P30027', '777piscessa@gmail.com', '5f63d0af26d96a27a49a5529e4cbb0ab', 'Vm14V2EyTXlWbk5oTTJ4WFltNUNjVlJYZEZabGJGSjFZMGR3VVZWVU1Eaz0=', NULL, NULL, NULL, NULL, 'SARTIKA DWI PISCESSA SH', NULL, 'JAKARTA', '1991-02-25', NULL, NULL, 'Jalan Nakula, Cluster Gardenia Harjamukti Blok A4, Sukatani, Tapos, Depok', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '081377779614', NULL, '777piscessa@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', NULL, NULL, '2023-12-05 12:50:04', 'Aktif'),
(29, 3, 'P30028', 'siswatest1@gmail.com', '9adc9fd4d3963fe1e3b0fddf417cdec7', 'VmpGU1MxUXdNVWhWYmtaVFYwZDRUMXBYZUVaUFVUMDk=', NULL, NULL, NULL, NULL, 'SISWA 1', NULL, 'BOGOR', '2009-02-02', NULL, NULL, 'Bogor', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0801', NULL, 'siswatest1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', NULL, NULL, '2024-01-07 03:29:49', 'Aktif'),
(30, 3, 'P30029', 'siswatest2@gmail.com', '9adc9fd4d3963fe1e3b0fddf417cdec7', 'VmpGU1MxUXdNVWhWYmtaVFYwZDRUMXBYZUVaUFVUMDk=', NULL, NULL, NULL, NULL, 'SISWA 2', NULL, 'BOGOR', '2009-02-02', NULL, NULL, 'Bogor', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0802', NULL, 'siswatest2@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dalam Proses', NULL, NULL, '2024-01-07 03:30:27', 'Aktif'),
(31, 3, 'P30030', 'siswatest3@gmail.com', '9adc9fd4d3963fe1e3b0fddf417cdec7', 'VmpGU1MxUXdNVWhWYmtaVFYwZDRUMXBYZUVaUFVUMDk=', NULL, NULL, NULL, NULL, 'SISWA 3', NULL, 'BOGOR', '2009-02-02', NULL, NULL, 'Bogor', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0803', NULL, 'siswatest3@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sudah Diverifikasi', 'Dalam Proses', 'Menunggu Hasil Lulus', '2024-01-07 07:44:10', '2024-01-07 03:33:17', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_nilai_rapor`
--

CREATE TABLE `tb_data_pendaftar_nilai_rapor` (
  `Id_Pendaftar_Nilai_Rapor` int(11) NOT NULL,
  `Id_Pendaftar` int(11) DEFAULT NULL,
  `Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Prakarya` varchar(6) DEFAULT NULL,
  `Status_Verifikasi_Nilai_Rapor` varchar(50) DEFAULT NULL,
  `Nilai_SKL_Ijazah` varchar(6) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_pendaftar_nilai_rapor`
--

INSERT INTO `tb_data_pendaftar_nilai_rapor` (`Id_Pendaftar_Nilai_Rapor`, `Id_Pendaftar`, `Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti`, `Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan`, `Nilai_Semester_1_Bahasa_Indonesia`, `Nilai_Semester_1_Bahasa_Inggris`, `Nilai_Semester_1_Matematika`, `Nilai_Semester_1_IPA`, `Nilai_Semester_1_IPS`, `Nilai_Semester_1_Seni_Dan_Budaya`, `Nilai_Semester_1_PJOK`, `Nilai_Semester_1_Prakarya`, `Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti`, `Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan`, `Nilai_Semester_2_Bahasa_Indonesia`, `Nilai_Semester_2_Bahasa_Inggris`, `Nilai_Semester_2_Matematika`, `Nilai_Semester_2_IPA`, `Nilai_Semester_2_IPS`, `Nilai_Semester_2_Seni_Dan_Budaya`, `Nilai_Semester_2_PJOK`, `Nilai_Semester_2_Prakarya`, `Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti`, `Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan`, `Nilai_Semester_3_Bahasa_Indonesia`, `Nilai_Semester_3_Bahasa_Inggris`, `Nilai_Semester_3_Matematika`, `Nilai_Semester_3_IPA`, `Nilai_Semester_3_IPS`, `Nilai_Semester_3_Seni_Dan_Budaya`, `Nilai_Semester_3_PJOK`, `Nilai_Semester_3_Prakarya`, `Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti`, `Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan`, `Nilai_Semester_4_Bahasa_Indonesia`, `Nilai_Semester_4_Bahasa_Inggris`, `Nilai_Semester_4_Matematika`, `Nilai_Semester_4_IPA`, `Nilai_Semester_4_IPS`, `Nilai_Semester_4_Seni_Dan_Budaya`, `Nilai_Semester_4_PJOK`, `Nilai_Semester_4_Prakarya`, `Status_Verifikasi_Nilai_Rapor`, `Nilai_SKL_Ijazah`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-18 09:28:25', 'Aktif'),
(2, 2, '95', '89', '86', '92', '88', '96', '94', '92', '85', '91', '95', '92', '92', '85', '82', '96', '95', '92', '83', '90', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Sudah Diverifikasi', '90', '2024-01-06 01:38:00', '2023-11-20 09:24:27', 'Aktif'),
(3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-20 09:35:29', 'Aktif'),
(4, 4, '90', '92', '92', '88', '85', '94', '86', '88', '86', '87', '91', '88', '92', '93', '85', '85', '92', '90', '91', '97', '90', '89', '92', '97', '95', '87', '94', '92', '90', '95', '89', '89', '91', '97', '90', '89', '93', '96', '89', '91', 'Sudah Diverifikasi', '', '2023-11-29 22:22:07', '2023-11-20 12:26:15', 'Aktif'),
(5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-20 18:48:58', 'Aktif'),
(6, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-21 08:12:16', 'Aktif'),
(7, 7, '95', '89', '93', '90', '87', '87', '89', '87', '89', '88', '92', '87', '88', '85', '84', '87', '85', '86', '85', '84', '85', '85', '84', '89', '84', '83', '83', '88', '85', '86', '85', '82', '88', '90', '84', '84', '90', '86', '84', '85', 'Sudah Diverifikasi', '50', '2024-01-02 00:35:28', '2023-11-21 08:12:22', 'Aktif'),
(8, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-21 08:57:06', 'Aktif'),
(9, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-21 11:35:18', 'Aktif'),
(10, 10, '83', '82', '76', '92', '79', '76', '79', '84', '84', '83', '79', '82', '80', '83', '76', '78', '80', '79', '76', '77', '78', '83', '83', '87', '78', '80', '85', '85', '82', '83', '78', '84', '82', '91', '78', '80', '81', '84', '81', '84', 'Menunggu Verifikasi', '', '2023-11-21 18:04:46', '2023-11-21 11:59:30', 'Aktif'),
(11, 12, '94', '97', '93', '97', '88', '89', '93', '96', '90', '96', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Menunggu Verifikasi', '', '2023-11-22 20:11:58', '2023-11-22 19:33:13', 'Aktif'),
(12, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 09:25:46', 'Aktif'),
(13, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 09:51:44', 'Aktif'),
(14, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 09:52:45', 'Aktif'),
(15, 15, '97', '90', '92', '89', '93', '99', '88', '85', '85', '84', '94', '93', '95', '93', '91', '94', '95', '92', '88', '92', '91', '88', '90', '96', '92', '93', '89', '91', '88', '90', '88', '92', '91', '96', '100', '88', '88', '94', '90', '91', 'Sudah Diverifikasi', '', '2023-12-05 16:16:23', '2023-11-24 10:03:48', 'Aktif'),
(16, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 10:04:19', 'Aktif'),
(17, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 10:05:20', 'Aktif'),
(18, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 10:06:36', 'Aktif'),
(19, 20, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Sudah Diverifikasi', '', '2024-01-02 01:05:06', '2023-11-24 10:45:31', 'Aktif'),
(20, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-27 09:57:34', 'Aktif'),
(21, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-27 10:18:24', 'Aktif'),
(22, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-28 11:52:20', 'Aktif'),
(23, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-30 16:03:09', 'Aktif'),
(24, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-12-01 15:42:04', 'Aktif'),
(25, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-12-03 23:25:38', 'Aktif'),
(26, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-12-04 19:19:51', 'Aktif'),
(27, 28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', '', NULL, '2023-12-05 12:50:20', 'Aktif'),
(28, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, NULL, '2024-01-07 03:33:42', 'Aktif'),
(29, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sudah Diverifikasi', NULL, '2024-01-07 07:39:55', '2024-01-07 03:34:01', 'Aktif'),
(30, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, NULL, '2024-01-07 23:39:48', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_pembayaran_ppdb`
--

CREATE TABLE `tb_data_pendaftar_pembayaran_ppdb` (
  `Id_Pendaftar_Pembayaran_PPDB` int(11) NOT NULL,
  `Id_Pendaftar` int(11) NOT NULL,
  `Bukti_Pembayaran_PPDB` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Pembayaran_PPDB` varchar(50) DEFAULT NULL,
  `JSON_Response_Pembayaran_Xendit` text NOT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_pendaftar_pembayaran_ppdb`
--

INSERT INTO `tb_data_pendaftar_pembayaran_ppdb` (`Id_Pendaftar_Pembayaran_PPDB`, `Id_Pendaftar`, `Bukti_Pembayaran_PPDB`, `Status_Verifikasi_Pembayaran_PPDB`, `JSON_Response_Pembayaran_Xendit`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 1, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-18 09:28:25', 'Aktif'),
(2, 2, NULL, 'Sudah Diverifikasi', '', '2024-01-07 02:39:02', '2023-11-20 09:24:27', 'Aktif'),
(3, 3, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-20 09:35:29', 'Aktif'),
(4, 4, 'bukti_pembayaran_pembayaran_ppdb_4_3_4.jpg', 'Sudah Diverifikasi', '', '2023-11-29 22:25:58', '2023-11-20 12:26:15', 'Aktif'),
(5, 5, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-20 18:48:58', 'Aktif'),
(6, 6, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-21 08:12:16', 'Aktif'),
(7, 7, 'bukti_pembayaran_pembayaran_ppdb_7_3_7.jpg', 'Sudah Diverifikasi', '', '2023-12-05 14:50:39', '2023-11-21 08:12:22', 'Aktif'),
(8, 8, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-21 08:57:06', 'Aktif'),
(9, 9, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-21 11:35:18', 'Aktif'),
(10, 10, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-21 11:59:30', 'Aktif'),
(11, 12, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-22 19:33:13', 'Aktif'),
(12, 18, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 09:25:46', 'Aktif'),
(13, 13, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 09:51:44', 'Aktif'),
(14, 14, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 09:52:45', 'Aktif'),
(15, 15, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 10:03:48', 'Aktif'),
(16, 16, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 10:04:19', 'Aktif'),
(17, 17, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 10:05:20', 'Aktif'),
(18, 19, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-24 10:06:36', 'Aktif'),
(19, 20, NULL, 'Sudah Diverifikasi', '', '2024-01-02 01:04:55', '2023-11-24 10:45:31', 'Aktif'),
(20, 21, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-27 09:57:34', 'Aktif'),
(21, 22, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-27 10:18:24', 'Aktif'),
(22, 23, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-28 11:52:20', 'Aktif'),
(23, 24, NULL, 'Belum Diverifikasi', '', NULL, '2023-11-30 16:03:09', 'Aktif'),
(24, 25, NULL, 'Belum Diverifikasi', '', NULL, '2023-12-01 15:42:04', 'Aktif'),
(25, 26, NULL, 'Belum Diverifikasi', '', NULL, '2023-12-03 23:25:38', 'Aktif'),
(26, 27, NULL, 'Belum Diverifikasi', '', NULL, '2023-12-04 19:19:51', 'Aktif'),
(27, 28, NULL, 'Belum Diverifikasi', '', NULL, '2023-12-05 12:50:20', 'Aktif'),
(28, 29, NULL, 'Belum Diverifikasi', '', NULL, '2024-01-07 03:33:42', 'Aktif'),
(29, 31, NULL, 'Sudah Diverifikasi', '', '2024-01-07 07:43:47', '2024-01-07 03:34:01', 'Aktif'),
(30, 30, NULL, 'Belum Diverifikasi', '', NULL, '2024-01-07 23:39:48', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_pembelian_formulir`
--

CREATE TABLE `tb_data_pendaftar_pembelian_formulir` (
  `Id_Pendaftar_Pembelian_Formulir` int(11) NOT NULL,
  `Id_Pendaftar` int(11) NOT NULL,
  `Bukti_Pembayaran_Pembelian_Formulir` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Pembelian_Formulir` varchar(50) DEFAULT NULL,
  `JSON_Response_Pembayaran_Xendit` text NOT NULL,
  `Waktu_Sudah_Diverifikasi_Pembayaran_Pembelian_Formulir` datetime DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_pendaftar_pembelian_formulir`
--

INSERT INTO `tb_data_pendaftar_pembelian_formulir` (`Id_Pendaftar_Pembelian_Formulir`, `Id_Pendaftar`, `Bukti_Pembayaran_Pembelian_Formulir`, `Status_Verifikasi_Pembelian_Formulir`, `JSON_Response_Pembayaran_Xendit`, `Waktu_Sudah_Diverifikasi_Pembayaran_Pembelian_Formulir`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 1, NULL, 'Belum Diverifikasi', '{\"id\":\"655b3125fd11e2ac7d23eb91\",\"external_id\":\"Invoice-Formulir-P10001\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"status\":\"PENDING\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":500000,\"payer_email\":\"anikrira323@gmail.com\",\"description\":\"Pembelian Formulir #P10001\",\"expiry_date\":\"2023-11-21T10:12:53.861Z\",\"invoice_url\":\"https://checkout.xendit.co/web/655b3125fd11e2ac7d23eb91\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":500000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":500000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":500000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":500000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"should_send_email\":false,\"created\":\"2023-11-20T10:12:54.536Z\",\"updated\":\"2023-11-20T10:12:54.536Z\",\"currency\":\"IDR\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', NULL, '2023-11-20 17:12:52', '2023-11-18 09:28:25', 'Aktif'),
(2, 2, NULL, 'Sudah Diverifikasi', '{\"id\":\"655ac36c399dcc4fbfb9e257\",\"external_id\":\"Invoice-Formulir-P30001\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"BNI\",\"paid_at\":\"2023-11-20T02:30:07.000Z\",\"payer_email\":\"revaganaufalkusuma@gmail.com\",\"description\":\"Pembelian Formulir #P30001\",\"expiry_date\":\"2023-11-21T02:24:44.635Z\",\"invoice_url\":\"https://checkout-staging.xendit.co/v2/655ac36c399dcc4fbfb9e257\",\"available_banks\":[{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"880857242514\"},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"BCA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"CIMB\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[{\"retail_outlet_name\":\"ALFAMART\"},{\"retail_outlet_name\":\"INDOMARET\"}],\"available_ewallets\":[{\"ewallet_type\":\"OVO\"},{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"SHOPEEPAY\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[{\"direct_debit_type\":\"DD_BRI\"}],\"available_paylaters\":[],\"should_exclude_credit_card\":false,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-11-20T02:24:45.115Z\",\"updated\":\"2023-11-20T02:30:09.019Z\",\"currency\":\"IDR\",\"payment_channel\":\"BNI\",\"payment_destination\":\"880857242514\",\"payment_id\":\"2f3ba4f5-7f9f-4c1d-b718-b1058187ea93\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', '2024-01-01 09:24:43', '2024-01-01 09:24:43', '2023-11-20 09:24:27', 'Aktif'),
(3, 3, NULL, 'Sudah Diverifikasi', '', '2024-01-07 23:04:19', '2024-01-07 23:04:19', '2023-11-20 09:35:29', 'Aktif'),
(4, 4, NULL, 'Sudah Diverifikasi', '{\"id\":\"655aee3af06f0a24b4b49689\",\"external_id\":\"Invoice-Formulir-P30003\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"BNI\",\"paid_at\":\"2023-11-20T05:37:39.000Z\",\"payer_email\":\"ichsan.anwar.achmad@gmail.com\",\"description\":\"Pembelian Formulir #P30003\",\"expiry_date\":\"2023-11-21T05:27:22.245Z\",\"invoice_url\":\"https://checkout.xendit.co/web/655aee3af06f0a24b4b49689\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"8930824211538557\"},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-11-20T05:27:22.923Z\",\"updated\":\"2023-11-20T05:37:41.212Z\",\"currency\":\"IDR\",\"payment_channel\":\"BNI\",\"payment_destination\":\"8930824211538557\",\"payment_id\":\"f83056d5-bf64-4ac8-bd1a-210474c071eb\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', '2023-11-20 12:27:20', '2023-11-20 12:27:20', '2023-11-20 12:26:15', 'Aktif'),
(5, 5, NULL, 'Belum Diverifikasi', '', NULL, NULL, '2023-11-20 18:48:58', 'Aktif'),
(6, 6, NULL, 'Sudah Diverifikasi', '', '2023-11-21 08:17:03', '2023-11-21 08:17:03', '2023-11-21 08:12:16', 'Aktif'),
(7, 7, NULL, 'Sudah Diverifikasi', '{\"id\":\"655c03ff66026de2eadb8c14\",\"external_id\":\"Invoice-Formulir-P30006\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"MANDIRI\",\"paid_at\":\"2023-11-21T01:13:57.000Z\",\"payer_email\":\"anikrira323@gmail.com\",\"description\":\"Pembelian Formulir #P30006\",\"expiry_date\":\"2023-11-22T01:12:31.375Z\",\"invoice_url\":\"https://checkout.xendit.co/web/655c03ff66026de2eadb8c14\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"889088242413866\"},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-11-21T01:12:32.085Z\",\"updated\":\"2023-11-21T01:13:59.785Z\",\"currency\":\"IDR\",\"payment_channel\":\"MANDIRI\",\"payment_destination\":\"889088242413866\",\"payment_id\":\"dbe9355c-d201-497d-9b74-80a9229a39e0\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', '2023-11-21 08:12:30', '2023-11-21 08:12:30', '2023-11-21 08:12:22', 'Aktif'),
(8, 8, NULL, 'Sudah Diverifikasi', '', '2023-11-21 08:58:34', '2023-11-21 08:58:34', '2023-11-21 08:57:06', 'Aktif'),
(9, 9, NULL, 'Sudah Diverifikasi', '', '2023-11-21 11:40:16', '2023-11-21 11:40:16', '2023-11-21 11:35:18', 'Aktif'),
(10, 10, 'bukti_pembayaran_pembelian_formulir_10_3_10.jpg', 'Sudah Diverifikasi', '', '2023-11-21 15:33:19', '2023-11-21 15:33:19', '2023-11-21 11:59:30', 'Aktif'),
(11, 12, NULL, 'Sudah Diverifikasi', '{\"id\":\"655df54d3db6a73993f79264\",\"external_id\":\"Invoice-Formulir-P30011\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"BNI\",\"paid_at\":\"2023-11-22T12:35:53.000Z\",\"payer_email\":\"decyantini@gmail.com\",\"description\":\"Pembelian Formulir #P30011\",\"expiry_date\":\"2023-11-23T12:34:22.056Z\",\"invoice_url\":\"https://checkout.xendit.co/web/655df54d3db6a73993f79264\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"8930824202529005\"},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-11-22T12:34:22.613Z\",\"updated\":\"2023-11-22T12:35:55.349Z\",\"currency\":\"IDR\",\"payment_channel\":\"BNI\",\"payment_destination\":\"8930824202529005\",\"payment_id\":\"d10614e7-63cd-4f78-b067-68595cdd0e40\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', '2023-11-22 19:34:21', '2023-11-22 19:34:21', '2023-11-22 19:33:13', 'Aktif'),
(12, 18, NULL, 'Sudah Diverifikasi', '{\"id\":\"65600a56560ad62a8fdb0962\",\"external_id\":\"Invoice-Formulir-P30017\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"MANDIRI\",\"paid_at\":\"2023-11-25T01:24:52.000Z\",\"payer_email\":\"rishantiningtyas.kp@gmail.com\",\"description\":\"Pembelian Formulir #P30017\",\"expiry_date\":\"2023-11-25T02:28:38.984Z\",\"invoice_url\":\"https://checkout.xendit.co/web/65600a56560ad62a8fdb0962\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"889088242602952\"},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"8930824201318178\"},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-11-24T02:28:39.496Z\",\"updated\":\"2023-11-25T01:24:54.448Z\",\"currency\":\"IDR\",\"payment_channel\":\"MANDIRI\",\"payment_destination\":\"889088242602952\",\"payment_id\":\"d9a666be-615a-40ca-b234-77dd2e94f710\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', '2023-11-25 10:03:00', '2023-11-25 10:03:00', '2023-11-24 09:25:46', 'Aktif'),
(13, 13, NULL, 'Menunggu Verifikasi', '{\"id\":\"65630a605dd636721bc486aa\",\"external_id\":\"Invoice-Formulir-P30012\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"MANDIRI\",\"paid_at\":\"2023-11-26T09:15:06.000Z\",\"payer_email\":\"apel8no1@gmail.com\",\"description\":\"Pembelian Formulir #P30012\",\"expiry_date\":\"2023-11-27T09:05:36.638Z\",\"invoice_url\":\"https://checkout.xendit.co/web/65630a605dd636721bc486aa\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"889088242537669\"},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-11-26T09:05:37.328Z\",\"updated\":\"2023-11-26T09:15:08.503Z\",\"currency\":\"IDR\",\"payment_channel\":\"MANDIRI\",\"payment_destination\":\"889088242537669\",\"payment_id\":\"f45fbe64-fc00-4e34-8bc8-eab09f9983bc\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', NULL, '2024-01-07 07:02:19', '2023-11-24 09:51:44', 'Aktif'),
(14, 14, NULL, 'Belum Diverifikasi', '{\"id\":\"65641d2a1400064a5261a4f7\",\"external_id\":\"Invoice-Formulir-P30013\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"status\":\"PENDING\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"payer_email\":\"lakeishacallysthaalzena@gmail.com\",\"description\":\"Pembelian Formulir #P30013\",\"expiry_date\":\"2023-11-28T04:38:02.342Z\",\"invoice_url\":\"https://checkout.xendit.co/web/65641d2a1400064a5261a4f7\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"should_send_email\":false,\"created\":\"2023-11-27T04:38:03.018Z\",\"updated\":\"2023-11-27T04:38:03.018Z\",\"currency\":\"IDR\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', NULL, '2023-11-27 11:38:01', '2023-11-24 09:52:45', 'Aktif'),
(15, 15, 'bukti_pembayaran_pembelian_formulir_15_3_15.jpg', 'Sudah Diverifikasi', '{\"id\":\"656ebca226da1515a26e590e\",\"external_id\":\"Invoice-Formulir-P30014\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"MANDIRI\",\"paid_at\":\"2023-12-05T07:39:55.000Z\",\"payer_email\":\"ikhtiarashazia@gmail.com\",\"description\":\"Pembelian Formulir #P30014\",\"expiry_date\":\"2023-12-06T06:01:06.897Z\",\"invoice_url\":\"https://checkout.xendit.co/web/656ebca226da1515a26e590e\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"889088242226410\"},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-12-05T06:01:07.608Z\",\"updated\":\"2023-12-05T07:39:57.244Z\",\"currency\":\"IDR\",\"payment_channel\":\"MANDIRI\",\"payment_destination\":\"889088242226410\",\"payment_id\":\"00055d17-0b05-49d2-a9f9-c2afe6c020bc\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', '2023-12-05 15:04:33', '2023-12-05 15:04:33', '2023-11-24 10:03:48', 'Aktif'),
(16, 16, NULL, 'Belum Diverifikasi', '', NULL, NULL, '2023-11-24 10:04:19', 'Aktif'),
(17, 17, NULL, 'Belum Diverifikasi', '', NULL, NULL, '2023-11-24 10:05:20', 'Aktif'),
(18, 19, NULL, 'Belum Diverifikasi', '', NULL, NULL, '2023-11-24 10:06:36', 'Aktif'),
(19, 20, 'bukti_pembayaran_pembelian_formulir_20_3_19.jpg', 'Sudah Diverifikasi', '', '2023-11-24 10:47:23', '2023-11-24 10:47:23', '2023-11-24 10:45:31', 'Aktif'),
(20, 21, NULL, 'Sudah Diverifikasi', '{\"id\":\"65658335935a75711718468e\",\"external_id\":\"Invoice-Formulir-P30020\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"MANDIRI\",\"paid_at\":\"2023-11-28T06:08:34.000Z\",\"payer_email\":\"raissa6usamah@gmail.com\",\"description\":\"Pembelian Formulir #P30020\",\"expiry_date\":\"2023-11-29T06:05:41.338Z\",\"invoice_url\":\"https://checkout.xendit.co/web/65658335935a75711718468e\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"889088242511371\"},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-11-28T06:05:41.918Z\",\"updated\":\"2023-11-28T06:08:36.640Z\",\"currency\":\"IDR\",\"payment_channel\":\"MANDIRI\",\"payment_destination\":\"889088242511371\",\"payment_id\":\"447924d3-dd68-45f7-b35c-f4478aaa5e8d\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', '2023-11-28 13:05:39', '2023-11-28 13:05:39', '2023-11-27 09:57:34', 'Aktif'),
(21, 22, NULL, 'Sudah Diverifikasi', '{\"id\":\"65640ab9140006c0ad6132e2\",\"external_id\":\"Invoice-Formulir-P30021\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"BNI\",\"paid_at\":\"2023-11-27T03:22:51.000Z\",\"payer_email\":\"winne.widiantini@gmail.com\",\"description\":\"Pembelian Formulir #P30021\",\"expiry_date\":\"2023-11-28T03:19:21.324Z\",\"invoice_url\":\"https://checkout.xendit.co/web/65640ab9140006c0ad6132e2\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"8930824209704074\"},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-11-27T03:19:22.036Z\",\"updated\":\"2023-11-27T03:22:53.803Z\",\"currency\":\"IDR\",\"payment_channel\":\"BNI\",\"payment_destination\":\"8930824209704074\",\"payment_id\":\"03c9589c-9f92-4531-b07e-a5501e71ebb6\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', '2023-11-27 10:19:20', '2023-11-27 10:19:20', '2023-11-27 10:18:24', 'Aktif'),
(22, 23, NULL, 'Sudah Diverifikasi', '{\"id\":\"65657231935a75a78917ddbc\",\"external_id\":\"Invoice-Formulir-P30022\",\"user_id\":\"616d22c9682a06dfd5d538fc\",\"payment_method\":\"BANK_TRANSFER\",\"status\":\"SETTLED\",\"merchant_name\":\"SMA Islam PB Soedirman\",\"merchant_profile_picture_url\":\"https://xnd-companies.s3.amazonaws.com/prod/1634609717544_281.png\",\"amount\":355000,\"paid_amount\":355000,\"bank_code\":\"MANDIRI\",\"paid_at\":\"2023-11-28T04:59:11.000Z\",\"payer_email\":\"fakhrijal082@gmail.com\",\"description\":\"Pembelian Formulir #P30022\",\"expiry_date\":\"2023-11-29T04:53:05.968Z\",\"invoice_url\":\"https://checkout.xendit.co/web/65657231935a75a78917ddbc\",\"available_banks\":[{\"bank_code\":\"BRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0},{\"bank_code\":\"MANDIRI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"889088242302362\"},{\"bank_code\":\"BNI\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0,\"bank_account_number\":\"8930824212245292\"},{\"bank_code\":\"PERMATA\",\"collection_type\":\"POOL\",\"transfer_amount\":355000,\"bank_branch\":\"Virtual Account\",\"account_holder_name\":\"SMA ISLAM PB SOEDIRMAN\",\"identity_amount\":0}],\"available_retail_outlets\":[],\"available_ewallets\":[{\"ewallet_type\":\"DANA\"},{\"ewallet_type\":\"LINKAJA\"}],\"available_qr_codes\":[{\"qr_code_type\":\"QRIS\"}],\"available_direct_debits\":[],\"available_paylaters\":[],\"should_exclude_credit_card\":true,\"adjusted_received_amount\":355000,\"should_send_email\":false,\"created\":\"2023-11-28T04:53:06.660Z\",\"updated\":\"2023-11-28T04:59:13.802Z\",\"currency\":\"IDR\",\"payment_channel\":\"MANDIRI\",\"payment_destination\":\"889088242302362\",\"payment_id\":\"77883a88-1ebf-41f8-8080-a3ed73182c67\",\"customer_notification_preference\":{\"invoice_created\":[\"email\"],\"invoice_reminder\":[\"email\",\"whatsapp\"],\"invoice_expired\":[\"whatsapp\",\"email\"],\"invoice_paid\":[\"whatsapp\",\"email\"]}}', '2023-11-28 11:53:05', '2023-11-28 11:53:05', '2023-11-28 11:52:20', 'Aktif'),
(23, 24, 'bukti_pembayaran_pembelian_formulir_24_3_23.jpg', 'Sudah Diverifikasi', '', '2023-12-01 15:43:09', '2023-12-01 15:43:09', '2023-11-30 16:03:09', 'Aktif'),
(24, 25, NULL, 'Sudah Diverifikasi', '', '2023-12-01 15:43:26', '2023-12-01 15:43:26', '2023-12-01 15:42:04', 'Aktif'),
(25, 26, 'bukti_pembayaran_pembelian_formulir_26_3_25.pdf', 'Sudah Diverifikasi', '', '2023-12-05 11:40:51', '2023-12-05 11:40:51', '2023-12-03 23:25:38', 'Aktif'),
(26, 27, 'bukti_pembayaran_pembelian_formulir_27_3_26.jpeg', 'Sudah Diverifikasi', '', '2023-12-04 19:56:16', '2023-12-04 19:56:16', '2023-12-04 19:19:51', 'Aktif'),
(27, 28, NULL, 'Belum Diverifikasi', '', NULL, NULL, '2023-12-05 12:50:20', 'Aktif'),
(28, 29, NULL, 'Belum Diverifikasi', '', NULL, NULL, '2024-01-07 03:33:42', 'Aktif'),
(29, 31, 'bukti_pembayaran_pembelian_formulir_31_3_29.jpeg', 'Sudah Diverifikasi', '', '2024-01-07 07:22:10', '2024-01-07 07:22:10', '2024-01-07 03:34:01', 'Aktif'),
(30, 30, NULL, 'Sudah Diverifikasi', '', '2024-01-07 23:53:06', '2024-01-07 23:53:06', '2024-01-07 23:39:48', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_program_layanan`
--

CREATE TABLE `tb_data_pendaftar_program_layanan` (
  `Id_Pendaftar_Program_Layanan` int(11) NOT NULL,
  `Id_Pendaftar` int(11) NOT NULL,
  `Program` varchar(50) DEFAULT NULL,
  `Layanan_1` varchar(50) DEFAULT NULL,
  `Layanan_2` varchar(50) DEFAULT NULL,
  `Layanan_3` varchar(50) DEFAULT NULL,
  `Layanan_4` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Program_Layanan` varchar(50) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_pendaftar_program_layanan`
--

INSERT INTO `tb_data_pendaftar_program_layanan` (`Id_Pendaftar_Program_Layanan`, `Id_Pendaftar`, `Program`, `Layanan_1`, `Layanan_2`, `Layanan_3`, `Layanan_4`, `Status_Verifikasi_Program_Layanan`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-18 09:28:25', 'Aktif'),
(2, 2, 'MIPA', 'PCE', 'Biologi', 'Teknologi', 'Tahfidz', 'Sudah Diverifikasi', '2024-01-06 01:23:04', '2023-11-20 09:24:27', 'Aktif'),
(3, 3, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-20 09:35:29', 'Aktif'),
(4, 4, 'MIPA', 'PCE', 'Teknologi', 'Biologi', 'Tahfidz', 'Sudah Diverifikasi', '2023-11-29 22:22:18', '2023-11-20 12:26:15', 'Aktif'),
(5, 5, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-20 18:48:58', 'Aktif'),
(6, 6, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-21 08:12:16', 'Aktif'),
(7, 7, 'MIPA', 'Teknologi', 'Biologi', 'Tahfidz', 'PCE', 'Sudah Diverifikasi', '2023-12-05 13:54:56', '2023-11-21 08:12:22', 'Aktif'),
(8, 8, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-21 08:57:06', 'Aktif'),
(9, 9, 'MIPA', 'PCE', 'Biologi', 'Teknologi', 'Tahfidz', 'Menunggu Verifikasi', '2023-11-25 20:30:10', '2023-11-21 11:35:18', 'Aktif'),
(10, 10, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-21 11:59:30', 'Aktif'),
(11, 12, 'MIPA', 'Biologi', 'Teknologi', 'PCE', 'Tahfidz', 'Menunggu Verifikasi', '2023-11-22 20:13:31', '2023-11-22 19:33:13', 'Aktif'),
(12, 18, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 09:25:46', 'Aktif'),
(13, 13, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 09:51:44', 'Aktif'),
(14, 14, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 09:52:45', 'Aktif'),
(15, 15, 'MIPA', 'Teknologi', 'Biologi', 'PCE', 'Tahfidz', 'Sudah Diverifikasi', '2023-12-05 16:16:53', '2023-11-24 10:03:48', 'Aktif'),
(16, 16, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 10:04:19', 'Aktif'),
(17, 17, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 10:05:20', 'Aktif'),
(18, 19, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 10:06:36', 'Aktif'),
(19, 20, 'MIPA', 'PCE', 'Biologi', 'Teknologi', 'Tahfidz', 'Sudah Diverifikasi', '2023-12-05 11:41:33', '2023-11-24 10:45:31', 'Aktif'),
(20, 21, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-27 09:57:34', 'Aktif'),
(21, 22, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-27 10:18:24', 'Aktif'),
(22, 23, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-28 11:52:20', 'Aktif'),
(23, 24, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-30 16:03:09', 'Aktif'),
(24, 25, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-12-01 15:42:04', 'Aktif'),
(25, 26, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-12-03 23:25:38', 'Aktif'),
(26, 27, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-12-04 19:19:51', 'Aktif'),
(27, 28, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-12-05 12:50:20', 'Aktif'),
(28, 29, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2024-01-07 03:33:42', 'Aktif'),
(29, 31, NULL, NULL, NULL, NULL, NULL, 'Sudah Diverifikasi', '2024-01-07 07:41:08', '2024-01-07 03:34:01', 'Aktif'),
(30, 30, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2024-01-07 23:39:48', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_verifikasi_berkas`
--

CREATE TABLE `tb_data_pendaftar_verifikasi_berkas` (
  `Id_Pendaftar_Verifikasi_Berkas` int(11) NOT NULL,
  `Id_Pendaftar` int(11) NOT NULL,
  `Akta_Kelahiran` varchar(50) DEFAULT NULL,
  `Kartu_Keluarga` varchar(50) DEFAULT NULL,
  `Rapor` varchar(50) DEFAULT NULL,
  `Kartu_NISN` varchar(50) DEFAULT NULL,
  `SKL_Ijazah` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Akta_Kelahiran` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Kartu_Keluarga` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Rapor` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Kartu_NISN` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_SKL_Ijazah` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Berkas` varchar(50) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_pendaftar_verifikasi_berkas`
--

INSERT INTO `tb_data_pendaftar_verifikasi_berkas` (`Id_Pendaftar_Verifikasi_Berkas`, `Id_Pendaftar`, `Akta_Kelahiran`, `Kartu_Keluarga`, `Rapor`, `Kartu_NISN`, `SKL_Ijazah`, `Status_Verifikasi_Akta_Kelahiran`, `Status_Verifikasi_Kartu_Keluarga`, `Status_Verifikasi_Rapor`, `Status_Verifikasi_Kartu_NISN`, `Status_Verifikasi_SKL_Ijazah`, `Status_Verifikasi_Berkas`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-18 09:28:25', 'Aktif'),
(2, 2, 'berkas_akte_kelahiran_2_3_2.jpg', 'berkas_kartu_keluarga_2_3_2.jpg', 'berkas_rapor_2_3_2.pdf', 'berkas_kartu_nisn_2_3_2.jpg', NULL, 'Sedang Diverifikasi', 'Sedang Diverifikasi', 'Sedang Diverifikasi', 'Sedang Diverifikasi', NULL, 'Sudah Diverifikasi', '2023-12-05 15:16:55', '2023-11-20 09:24:27', 'Aktif'),
(3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-20 09:35:29', 'Aktif'),
(4, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sudah Diverifikasi', '2023-12-05 12:34:18', '2023-11-20 12:26:15', 'Aktif'),
(5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-20 18:48:58', 'Aktif'),
(6, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-21 08:12:16', 'Aktif'),
(7, 7, 'berkas_akte_kelahiran_7_3_7.jpg', 'berkas_kartu_keluarga_7_3_7.jpg', 'berkas_rapor_7_3_7.pdf', 'berkas_kartu_nisn_7_3_7.jpg', NULL, 'Sedang Diverifikasi', 'Sedang Diverifikasi', 'Sedang Diverifikasi', 'Sedang Diverifikasi', NULL, 'Sudah Diverifikasi', '2023-12-05 13:55:51', '2023-11-21 08:12:22', 'Aktif'),
(8, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-21 08:57:06', 'Aktif'),
(9, 9, 'berkas_akte_kelahiran_9_3_9.jpg', 'berkas_kartu_keluarga_9_3_9.pdf', NULL, 'berkas_kartu_nisn_9_3_9.jpg', NULL, 'Sedang Diverifikasi', 'Sedang Diverifikasi', NULL, 'Sedang Diverifikasi', NULL, 'Belum Diverifikasi', '2023-11-25 20:38:26', '2023-11-21 11:35:18', 'Aktif'),
(10, 10, 'berkas_akte_kelahiran_10_3_10.pdf', 'berkas_kartu_keluarga_10_3_10.pdf', 'berkas_rapor_10_3_10.pdf', 'berkas_kartu_nisn_10_3_10.pdf', NULL, 'Sedang Diverifikasi', 'Sedang Diverifikasi', 'Sedang Diverifikasi', 'Sedang Diverifikasi', NULL, 'Belum Diverifikasi', '2023-11-21 19:06:12', '2023-11-21 11:59:30', 'Aktif'),
(11, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-22 19:33:13', 'Aktif'),
(12, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 09:25:46', 'Aktif'),
(13, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 09:51:44', 'Aktif'),
(14, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 09:52:45', 'Aktif'),
(15, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 10:03:48', 'Aktif'),
(16, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 10:04:19', 'Aktif'),
(17, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 10:05:20', 'Aktif'),
(18, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-24 10:06:36', 'Aktif'),
(19, 20, 'berkas_akte_kelahiran_20_3_19.pdf', 'berkas_kartu_keluarga_20_3_19.pdf', 'berkas_rapor_20_3_19.pdf', 'berkas_kartu_nisn_20_3_19.pdf', NULL, 'Sedang Diverifikasi', 'Sedang Diverifikasi', 'Sedang Diverifikasi', 'Sedang Diverifikasi', NULL, 'Sudah Diverifikasi', '2023-12-05 11:42:21', '2023-11-24 10:45:31', 'Aktif'),
(20, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-27 09:57:34', 'Aktif'),
(21, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-27 10:18:24', 'Aktif'),
(22, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-28 11:52:20', 'Aktif'),
(23, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-11-30 16:03:09', 'Aktif'),
(24, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-12-01 15:42:04', 'Aktif'),
(25, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-12-03 23:25:38', 'Aktif'),
(26, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-12-04 19:19:51', 'Aktif'),
(27, 28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2023-12-05 12:50:20', 'Aktif'),
(28, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2024-01-07 03:33:42', 'Aktif'),
(29, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sudah Diverifikasi', '2024-01-07 07:41:18', '2024-01-07 03:34:01', 'Aktif'),
(30, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Belum Diverifikasi', NULL, '2024-01-07 23:39:48', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pengguna`
--

CREATE TABLE `tb_data_pengguna` (
  `Id_Pengguna` int(11) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Nama_Lengkap` varchar(100) DEFAULT NULL,
  `Id_Hak_Akses` int(11) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Nomor_Handphone` text DEFAULT NULL,
  `Jenis_Kelamin` varchar(10) DEFAULT NULL,
  `Tempat_Lahir` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Alamat_Lengkap` text DEFAULT NULL,
  `Token_Login` text NOT NULL,
  `Waktu_Terakhir_Login` datetime DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime NOT NULL,
  `Status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_data_pengguna`
--

INSERT INTO `tb_data_pengguna` (`Id_Pengguna`, `Username`, `Password`, `Nama_Lengkap`, `Id_Hak_Akses`, `Email`, `Nomor_Handphone`, `Jenis_Kelamin`, `Tempat_Lahir`, `Tanggal_Lahir`, `Alamat_Lengkap`, `Token_Login`, `Waktu_Terakhir_Login`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'admin', '9adc9fd4d3963fe1e3b0fddf417cdec7', 'Admin Production', 1, 'ryansemtrrc@gmail.com', '081617466672', 'Laki-Laki', 'Bogor', '1999-02-02', '', '', '2023-09-23 19:11:40', '2023-11-18 09:24:44', '2023-09-23 19:11:40', 'Aktif'),
(2, 'ryan', 'f835a44d4655335e0e2a9e5ac82e3177', 'Ryan Aja', NULL, 'ryansemtrrc@gmail.com', '081617466672', 'Laki-Laki', 'Bigir', '1999-02-02', 'TEST', '', NULL, '2023-11-18 09:24:35', '2023-09-24 01:57:16', 'Terhapus'),
(3, 'stafftu1', 'a31808288140c267899a704ccea8de4f', 'Staff', 3, 'TU', '12345', 'Laki-Laki', 'Bogor', '1999-02-02', 'Bogor', '', NULL, '2023-12-28 23:20:28', '2023-12-28 23:19:46', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pengumuman`
--

CREATE TABLE `tb_data_pengumuman` (
  `Id_Pengumuman` int(11) NOT NULL,
  `Judul_Pengumuman` varchar(100) NOT NULL,
  `Isi_Pengumuman` text DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_pengumuman`
--

INSERT INTO `tb_data_pengumuman` (`Id_Pengumuman`, `Judul_Pengumuman`, `Isi_Pengumuman`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'Lolos Seleksi Berkas', 'Selamat untuk siswa yang telah lolos berkas. Silakan melakukan pembayaran PPDB jika belum melakukan pembayaran. Info admin PPDB', '2023-11-29 21:23:09', '2023-11-29 16:06:26', 'Terhapus');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_ppdb`
--

CREATE TABLE `tb_data_ppdb` (
  `Id_PPDB` int(11) NOT NULL,
  `Tahun_Ajaran` varchar(30) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `Deskripsi` text DEFAULT NULL,
  `Tanggal_Mulai_Pendaftaran` date DEFAULT NULL,
  `Tanggal_Akhir_Pendaftaran` date DEFAULT NULL,
  `No_Rekening_Utama` varchar(100) DEFAULT NULL,
  `No_Rekening_Cadangan` varchar(100) DEFAULT NULL,
  `Biaya_Pendaftaran_Formulir` int(11) DEFAULT NULL,
  `Biaya_PPDB` int(11) DEFAULT NULL,
  `Biaya_Admin_Xendit` int(11) DEFAULT NULL,
  `Masa_Waktu_Pembayaran_Pembelian_Formulir` int(11) NOT NULL,
  `Masa_Waktu_Pengisian_Berkas` int(1) NOT NULL,
  `Umur_Maksimal_Pendaftar` int(1) NOT NULL,
  `Status_PPDB` varchar(30) DEFAULT NULL,
  `Foto_Alur_Pendaftaran` text DEFAULT NULL,
  `Informasi_Tambahan` text DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_ppdb`
--

INSERT INTO `tb_data_ppdb` (`Id_PPDB`, `Tahun_Ajaran`, `Judul`, `Deskripsi`, `Tanggal_Mulai_Pendaftaran`, `Tanggal_Akhir_Pendaftaran`, `No_Rekening_Utama`, `No_Rekening_Cadangan`, `Biaya_Pendaftaran_Formulir`, `Biaya_PPDB`, `Biaya_Admin_Xendit`, `Masa_Waktu_Pembayaran_Pembelian_Formulir`, `Masa_Waktu_Pengisian_Berkas`, `Umur_Maksimal_Pendaftar`, `Status_PPDB`, `Foto_Alur_Pendaftaran`, `Informasi_Tambahan`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, '2023/2024', 'PPDB 2023/2024', 'PPDB 2023/2024', '2023-10-05', '2023-10-31', 'Mandiri 421321312312 an Ryan Anggowo', 'BCA 1231231232 an Ryan Anggowo', 500000, 400000, NULL, 0, 0, 0, 'Tidak Aktif', NULL, NULL, '2023-11-27 10:48:54', '2023-10-04 00:27:23', 'Terhapus'),
(2, '2022/2023', 'PPDB 2022/2023', 'PPDB 2022/2023', '2022-01-01', '2022-01-30', 'Mandiri 421321312312 an Ryan Anggowo', 'BCA 1231231232 an Ryan Anggowo', 500000, 400000, NULL, 0, 0, 0, 'Tidak Aktif', NULL, NULL, '2023-11-27 10:48:51', '2023-10-04 01:47:59', 'Terhapus'),
(3, '2024/2025', 'PPDB 2024/2025', 'PPDB 2024/2025', '2023-11-01', '2024-04-30', 'Muamalat 3500011645 a.n. Yayasan Masjid PB Sudirman-SMA', 'Muamalat 3500011645 a.n. Yayasan Masjid PB Sudirman-SMA', 350000, 17500000, 5000, 10, 14, 21, 'Aktif', 'foto_alur_pendaftaran_3.jpeg', 'TEST 12345', '2024-01-07 05:23:13', '2023-11-20 09:11:36', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_siswa`
--

CREATE TABLE `tb_data_siswa` (
  `Id_Siswa` int(11) NOT NULL,
  `Id_Pendaftar` int(11) DEFAULT NULL,
  `Id_PPDB` int(11) DEFAULT NULL,
  `NIS_Saat_Ini` varchar(100) DEFAULT NULL,
  `NIK` varchar(50) DEFAULT NULL,
  `NIS` varchar(50) DEFAULT NULL,
  `NISN` varchar(50) DEFAULT NULL,
  `NIPD` varchar(50) DEFAULT NULL,
  `Nama_Lengkap` varchar(100) DEFAULT NULL,
  `Jenis_Kelamin` varchar(20) DEFAULT NULL,
  `Tempat_Lahir` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Agama` varchar(20) DEFAULT NULL,
  `Status_Dalam_Keluarga` varchar(20) DEFAULT NULL,
  `Jalan` text DEFAULT NULL,
  `Kelurahan` varchar(100) DEFAULT NULL,
  `Kecamatan` varchar(100) DEFAULT NULL,
  `Kota` varchar(100) DEFAULT NULL,
  `Provinsi` varchar(100) DEFAULT NULL,
  `Id_Kelurahan` int(11) DEFAULT NULL,
  `Id_Kecamatan` int(11) DEFAULT NULL,
  `Id_Kota` int(11) DEFAULT NULL,
  `Id_Provinsi` int(11) DEFAULT NULL,
  `Kode_Pos` varchar(10) DEFAULT NULL,
  `Titik_Lintang_Alamat` varchar(50) DEFAULT NULL,
  `Titik_Bujur_Alamat` varchar(50) DEFAULT NULL,
  `Nomor_Handphone` varchar(15) DEFAULT NULL,
  `Nomor_Telepon` varchar(15) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Golongan_Darah` varchar(2) DEFAULT NULL,
  `Kebutuhan_Khusus` varchar(30) DEFAULT NULL,
  `Tinggi_Badan` int(11) DEFAULT NULL,
  `Berat_Badan` int(11) DEFAULT NULL,
  `Lingkar_Kepala` int(1) DEFAULT NULL,
  `Asal_Sekolah` varchar(100) DEFAULT NULL,
  `Alamat_Sekolah` text DEFAULT NULL,
  `Kelas` varchar(5) DEFAULT NULL,
  `Nomor_Peserta_Ujian_Nasional` varchar(50) DEFAULT NULL,
  `Nomor_Seri_Ijazah` varchar(50) DEFAULT NULL,
  `Penerima_KPS` varchar(5) DEFAULT NULL,
  `Penerima_KIP` varchar(5) DEFAULT NULL,
  `Penerima_KJP_DKI` varchar(5) DEFAULT NULL,
  `Nomor_KIP` varchar(50) DEFAULT NULL,
  `Nomor_KJP_DKI` varchar(50) DEFAULT NULL,
  `Nomor_KPS` varchar(50) DEFAULT NULL,
  `Nomor_KKS` varchar(50) DEFAULT NULL,
  `Nomor_KK` varchar(50) DEFAULT NULL,
  `Nomor_Registrasi_Akta_Lahir` varchar(50) DEFAULT NULL,
  `Anak_Ke` varchar(3) DEFAULT NULL,
  `Jumlah_Saudara` varchar(3) DEFAULT NULL,
  `Status_Perwalian` varchar(50) DEFAULT NULL,
  `NIK_Ayah` varchar(50) DEFAULT NULL,
  `Nama_Ayah` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Ayah` varchar(50) NOT NULL,
  `Tanggal_Lahir_Ayah` date DEFAULT NULL,
  `Jenjang_Pendidikan_Ayah` varchar(30) DEFAULT NULL,
  `Pekerjaan_Ayah` varchar(100) DEFAULT NULL,
  `Penghasilan_Ayah` varchar(100) DEFAULT NULL,
  `No_HP_Ayah` varchar(16) DEFAULT NULL,
  `NIK_Ibu` varchar(50) DEFAULT NULL,
  `Nama_Ibu` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Ibu` varchar(50) NOT NULL,
  `Tanggal_Lahir_Ibu` date DEFAULT NULL,
  `Jenjang_Pendidikan_Ibu` varchar(30) DEFAULT NULL,
  `Pekerjaan_Ibu` varchar(100) DEFAULT NULL,
  `Penghasilan_Ibu` varchar(100) DEFAULT NULL,
  `No_HP_Ibu` varchar(16) DEFAULT NULL,
  `NIK_Wali` varchar(50) DEFAULT NULL,
  `Nama_Wali` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Wali` varchar(50) NOT NULL,
  `Tanggal_Lahir_Wali` date DEFAULT NULL,
  `Jenjang_Pendidikan_Wali` varchar(30) DEFAULT NULL,
  `Pekerjaan_Wali` varchar(100) DEFAULT NULL,
  `Penghasilan_Wali` varchar(100) DEFAULT NULL,
  `No_HP_Wali` varchar(16) DEFAULT NULL,
  `Kendaraan_Yang_Dipakai_Kesekolah` varchar(100) DEFAULT NULL,
  `Jarak_Rumah_Ke_Sekolah` int(1) DEFAULT NULL,
  `Ukuran_Seragam` varchar(5) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_siswa`
--

INSERT INTO `tb_data_siswa` (`Id_Siswa`, `Id_Pendaftar`, `Id_PPDB`, `NIS_Saat_Ini`, `NIK`, `NIS`, `NISN`, `NIPD`, `Nama_Lengkap`, `Jenis_Kelamin`, `Tempat_Lahir`, `Tanggal_Lahir`, `Agama`, `Status_Dalam_Keluarga`, `Jalan`, `Kelurahan`, `Kecamatan`, `Kota`, `Provinsi`, `Id_Kelurahan`, `Id_Kecamatan`, `Id_Kota`, `Id_Provinsi`, `Kode_Pos`, `Titik_Lintang_Alamat`, `Titik_Bujur_Alamat`, `Nomor_Handphone`, `Nomor_Telepon`, `Email`, `Golongan_Darah`, `Kebutuhan_Khusus`, `Tinggi_Badan`, `Berat_Badan`, `Lingkar_Kepala`, `Asal_Sekolah`, `Alamat_Sekolah`, `Kelas`, `Nomor_Peserta_Ujian_Nasional`, `Nomor_Seri_Ijazah`, `Penerima_KPS`, `Penerima_KIP`, `Penerima_KJP_DKI`, `Nomor_KIP`, `Nomor_KJP_DKI`, `Nomor_KPS`, `Nomor_KKS`, `Nomor_KK`, `Nomor_Registrasi_Akta_Lahir`, `Anak_Ke`, `Jumlah_Saudara`, `Status_Perwalian`, `NIK_Ayah`, `Nama_Ayah`, `Tempat_Lahir_Ayah`, `Tanggal_Lahir_Ayah`, `Jenjang_Pendidikan_Ayah`, `Pekerjaan_Ayah`, `Penghasilan_Ayah`, `No_HP_Ayah`, `NIK_Ibu`, `Nama_Ibu`, `Tempat_Lahir_Ibu`, `Tanggal_Lahir_Ibu`, `Jenjang_Pendidikan_Ibu`, `Pekerjaan_Ibu`, `Penghasilan_Ibu`, `No_HP_Ibu`, `NIK_Wali`, `Nama_Wali`, `Tempat_Lahir_Wali`, `Tanggal_Lahir_Wali`, `Jenjang_Pendidikan_Wali`, `Pekerjaan_Wali`, `Penghasilan_Wali`, `No_HP_Wali`, `Kendaraan_Yang_Dipakai_Kesekolah`, `Jarak_Rumah_Ke_Sekolah`, `Ukuran_Seragam`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(3, 0, 3, '', '3276025008090003', '431921222216', '0099965936', '', 'ILMIRA TRIBUANA SANDYA PARAMITHA', 'Perempuan', 'Jakarta', '2009-08-10', 'Islam', '', 'WISMA HARAPAN 1 BLOK A NO. 10 RT 002/019', 'MEKARSARI', 'CIMANGGIS', 'KOTA DEPOK', 'JAWA BARAT', 2147483647, 3276040, 3276, 32, '16452', '', '', '087786697527', '087786697527', 'ichsan.anwar.achmad@gmail.com', 'B', 'Tidak', 160, 51, 0, 'SMP ISLAM AL-AZHAR 19 CIBUBUR', 'Jl. Jambore Raya No.9A Cibubur Ciracas Jakarta Timur', 'X', '', '', 'Tidak', 'Tidak', '', '', '', '', '', '3276021409090019', '', '3', '2', 'Kandung', '3276022706700006', 'Mohammad Ichsan', 'Jakarta', '1970-06-27', 'S2', 'Karyawan Swasta', 'Rp. 5.000.000 - Rp. 20.000.000', '08111377777', '3276024712700009', 'Verasari Rusnalita', 'Jakarta', '1970-08-07', 'S1', 'Karyawan Swasta', 'Rp. 5.000.000 - Rp. 20.000.000', '08121271270', '', '', '', '0000-00-00', '', '', '', '', 'Angkutan Umum', 10, 'L', '2024-01-02 00:49:39', '2023-12-06 01:17:42', 'Aktif'),
(4, 7, 3, '', '3276022308090005', '2276', '0092153325', '', 'Razan Muhammad Ihsan', '', 'Depok', '2009-08-23', 'Islam', 'anak', 'Kav. Pesona blok D no.25 rt.014/01', 'PASIR GUNUNG SELATAN', 'CIMANGGIS', 'KOTA DEPOK', 'JAWA BARAT', 2147483647, 3276040, 3276, 32, '16451', '', '', '081282727853', '', 'razanmuhammadihsan.2308@gmail.com', 'B', 'Tidak', 166, 51, 0, 'SMP ISLAM AL-MA\'RUF', 'Jl. Raya Lapangan Tembak, Cibubur', 'X', '0092153325', '', 'Tidak', 'Tidak', 'Tidak', '', '', '', '', '327602231009049', '', '2', '1', 'Kandung', '3276023103800009', 'Ary Darsono', 'Jakarta', '1980-03-31', 'S2', 'Wiraswasta', 'Rp 5.000.000 - Rp 20.000.0000', '088905786564', '3276026902800007', 'Anik Mulyani', 'Jakarta', '1980-02-29', 'S1', 'Ibu Rumah Tangga', 'Rp 5.000.000 - Rp 20.000.0000', '081295310953', '', '', '', '0000-00-00', '', '', '', '', 'Motor', 4, 'L', NULL, '2024-01-02 00:37:22', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_siswa_nilai_rapor`
--

CREATE TABLE `tb_data_siswa_nilai_rapor` (
  `Id_Siswa_Nilai_Rapor` int(11) NOT NULL,
  `Id_Siswa` int(11) DEFAULT NULL,
  `Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_SKL_Ijazah` varchar(6) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_data_siswa_nilai_rapor`
--

INSERT INTO `tb_data_siswa_nilai_rapor` (`Id_Siswa_Nilai_Rapor`, `Id_Siswa`, `Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti`, `Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan`, `Nilai_Semester_1_Bahasa_Indonesia`, `Nilai_Semester_1_Bahasa_Inggris`, `Nilai_Semester_1_Matematika`, `Nilai_Semester_1_IPA`, `Nilai_Semester_1_IPS`, `Nilai_Semester_1_Seni_Dan_Budaya`, `Nilai_Semester_1_PJOK`, `Nilai_Semester_1_Prakarya`, `Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti`, `Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan`, `Nilai_Semester_2_Bahasa_Indonesia`, `Nilai_Semester_2_Bahasa_Inggris`, `Nilai_Semester_2_Matematika`, `Nilai_Semester_2_IPA`, `Nilai_Semester_2_IPS`, `Nilai_Semester_2_Seni_Dan_Budaya`, `Nilai_Semester_2_PJOK`, `Nilai_Semester_2_Prakarya`, `Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti`, `Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan`, `Nilai_Semester_3_Bahasa_Indonesia`, `Nilai_Semester_3_Bahasa_Inggris`, `Nilai_Semester_3_Matematika`, `Nilai_Semester_3_IPA`, `Nilai_Semester_3_IPS`, `Nilai_Semester_3_Seni_Dan_Budaya`, `Nilai_Semester_3_PJOK`, `Nilai_Semester_3_Prakarya`, `Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti`, `Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan`, `Nilai_Semester_4_Bahasa_Indonesia`, `Nilai_Semester_4_Bahasa_Inggris`, `Nilai_Semester_4_Matematika`, `Nilai_Semester_4_IPA`, `Nilai_Semester_4_IPS`, `Nilai_Semester_4_Seni_Dan_Budaya`, `Nilai_Semester_4_PJOK`, `Nilai_Semester_4_Prakarya`, `Nilai_SKL_Ijazah`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 4, '95', '89', '93', '90', '87', '87', '89', '87', '89', '88', '92', '87', '88', '85', '84', '87', '85', '86', '85', '84', '85', '85', '84', '89', '84', '83', '83', '88', '85', '86', '85', '82', '88', '90', '84', '84', '90', '86', '84', '85', '50', NULL, '2024-01-02 00:37:22', 'Status'),
(2, 3, '3', '2', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '43', NULL, '2024-01-02 00:49:39', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_asal_sekolah`
--

CREATE TABLE `tb_pengaturan_asal_sekolah` (
  `Id_Pengaturan_Asal_Sekolah` int(11) NOT NULL,
  `Nama_Sekolah` varchar(100) DEFAULT NULL,
  `Alamat_Sekolah` text DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pengaturan_asal_sekolah`
--

INSERT INTO `tb_pengaturan_asal_sekolah` (`Id_Pengaturan_Asal_Sekolah`, `Nama_Sekolah`, `Alamat_Sekolah`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'SMK Wikrama Bogor', '', '2023-11-20 12:46:50', '2023-10-13 06:09:47', 'Terhapus'),
(2, 'SMK Tridarma', '', '2023-11-20 12:46:45', '2023-10-15 01:46:35', 'Terhapus'),
(3, 'SMP ISLAM AL-AZHAR 19 CIBUBUR', '', '2023-11-20 12:58:57', '2023-11-20 12:46:39', 'Aktif'),
(4, 'SMP ISLAM PB SOEDIRMAN', '', '2023-11-20 12:59:22', '2023-11-20 12:53:11', 'Aktif'),
(5, 'SMP NEGERI 102 JAKARTA', NULL, NULL, '2023-11-20 13:26:10', 'Aktif'),
(6, 'SMP NEGERI 103 JAKARTA', NULL, NULL, '2023-11-20 13:26:59', 'Aktif'),
(7, 'SMP NEGERI 179 JAKARTA', NULL, NULL, '2023-11-20 13:27:55', 'Aktif'),
(8, 'SMP NEGERI 223 JAKARTA', NULL, NULL, '2023-11-20 13:33:05', 'Aktif'),
(9, 'SMP NEGERI 217 JAKARTA', NULL, NULL, '2023-11-20 13:33:32', 'Aktif'),
(10, 'SMP NOAH JAKARTA', NULL, NULL, '2023-11-20 13:40:52', 'Aktif'),
(11, 'SMP IT PONDOK DUTA', NULL, NULL, '2023-11-20 13:41:20', 'Aktif'),
(12, 'SMP GLOBAL ISLAMIC SCHOOL JAKARTA', NULL, NULL, '2023-11-20 13:44:07', 'Aktif'),
(13, 'SMP IT NURUL FIKRI', NULL, NULL, '2023-11-20 13:57:50', 'Aktif'),
(14, 'SMP SHIDQIA ISLAMIC SCHOOL', NULL, NULL, '2023-11-21 09:19:19', 'Aktif'),
(15, 'SMP AL MINHAJ  TAMANSARI', NULL, NULL, '2023-11-21 09:29:15', 'Aktif'),
(16, 'SMPS IT BUAHATI ISLAMIC SCHOOL', NULL, NULL, '2023-11-21 12:44:10', 'Aktif'),
(17, 'SMP EMIISc Jakarta', NULL, NULL, '2023-11-21 12:44:47', 'Aktif'),
(18, 'SMP NEGERI 49 JAKARTA', NULL, NULL, '2023-11-21 12:45:32', 'Aktif'),
(19, 'SMP NEGERI 20 JAKARTA', NULL, NULL, '2023-11-21 12:46:03', 'Aktif'),
(20, 'SMP NEGERI 106 JAKARTA', NULL, NULL, '2023-11-21 12:46:42', 'Aktif'),
(21, 'SMP KARAKTER', NULL, NULL, '2023-11-21 13:01:35', 'Aktif'),
(22, 'SMP BRIGHTON JHS', NULL, NULL, '2023-11-23 09:58:05', 'Aktif'),
(23, 'SMP IT AT TAUFIQ AL ISLAMY', '', '2023-11-23 10:44:13', '2023-11-23 10:42:41', 'Aktif'),
(24, 'SMP ISLAM AL IKHLAS', NULL, NULL, '2023-11-23 10:48:06', 'Aktif'),
(25, 'SMP ISLAM AL-MA\'RUF', NULL, NULL, '2023-11-23 14:22:15', 'Aktif'),
(26, 'SMP NIZAMIA ANDALUSIA', NULL, NULL, '2023-11-24 15:21:24', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
--

CREATE TABLE `tb_pengaturan_kendaraan_yang_dipakai_kesekolah` (
  `Id_Pengaturan_Kendaraan_Yang_Dipakai_Kesekolah` int(11) NOT NULL,
  `Nama_Kendaraan` varchar(100) NOT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
--

INSERT INTO `tb_pengaturan_kendaraan_yang_dipakai_kesekolah` (`Id_Pengaturan_Kendaraan_Yang_Dipakai_Kesekolah`, `Nama_Kendaraan`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'Angkutan Umum', '2023-10-18 22:39:18', '2023-10-13 06:13:51', 'Aktif'),
(2, 'Motor', '2023-10-18 22:39:21', '2023-10-15 01:46:45', 'Aktif'),
(3, 'Mobil', '2023-10-18 22:39:22', '2023-10-15 01:46:48', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_pekerjaan_orang_tua`
--

CREATE TABLE `tb_pengaturan_pekerjaan_orang_tua` (
  `Id_Pengaturan_Pekerjaan_Orang_Tua` int(11) NOT NULL,
  `Pekerjaan_Orang_Tua` varchar(100) NOT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pengaturan_pekerjaan_orang_tua`
--

INSERT INTO `tb_pengaturan_pekerjaan_orang_tua` (`Id_Pengaturan_Pekerjaan_Orang_Tua`, `Pekerjaan_Orang_Tua`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'Karyawan Swasta', '2023-11-20 13:29:06', '2023-10-13 05:55:53', 'Aktif'),
(2, 'PNS/TNI/POLRI', '2023-11-20 13:29:30', '2023-10-15 01:46:07', 'Aktif'),
(3, 'Ibu Rumah Tangga', '2023-10-18 22:40:00', '2023-10-15 01:46:13', 'Aktif'),
(4, 'Karyawati', '2023-11-20 13:32:14', '2023-11-20 13:27:53', 'Terhapus'),
(5, 'Nelayan', NULL, '2023-11-20 13:29:49', 'Aktif'),
(6, 'Petani', NULL, '2023-11-20 13:29:57', 'Aktif'),
(7, 'Peternak', NULL, '2023-11-20 13:30:05', 'Aktif'),
(8, 'Pedagang Kecil', NULL, '2023-11-20 13:30:19', 'Aktif'),
(9, 'Pedagang Besar', NULL, '2023-11-20 13:30:32', 'Aktif'),
(10, 'Wiraswasta', NULL, '2023-11-20 13:30:54', 'Aktif'),
(11, 'Wirausaha', NULL, '2023-11-20 13:31:03', 'Aktif'),
(12, 'Buruh', NULL, '2023-11-20 13:31:09', 'Aktif'),
(13, 'Pensiunan', NULL, '2023-11-20 13:31:19', 'Aktif'),
(14, 'Tenaga Kerja Indonesia', NULL, '2023-11-20 13:31:31', 'Aktif'),
(15, 'Karyawan BUMN', NULL, '2023-11-20 13:31:47', 'Aktif'),
(16, 'Sudah Meninggal', NULL, '2023-11-20 13:32:00', 'Aktif'),
(17, 'Lainnya', NULL, '2023-11-20 13:32:09', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_penghasilan_orang_tua`
--

CREATE TABLE `tb_pengaturan_penghasilan_orang_tua` (
  `Id_Pengaturan_Penghasilan_Orang_Tua` int(11) NOT NULL,
  `Penghasilan_Orang_Tua` varchar(100) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pengaturan_penghasilan_orang_tua`
--

INSERT INTO `tb_pengaturan_penghasilan_orang_tua` (`Id_Pengaturan_Penghasilan_Orang_Tua`, `Penghasilan_Orang_Tua`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'Kurang dari Rp 500.000', '2023-11-20 13:12:48', '2023-10-13 05:56:45', 'Aktif'),
(2, 'Rp. 2.000.000 s/d Rp. 4.000.000', '2023-11-20 13:06:48', '2023-10-15 01:46:24', 'Terhapus'),
(3, 'Rp  1.000.000 - Rp 1.999.999', '2023-11-21 16:33:49', '2023-11-20 13:16:22', 'Aktif'),
(4, 'Rp 2.000.000 - Rp 4.999.999', '2023-11-21 16:38:21', '2023-11-20 13:19:53', 'Aktif'),
(5, 'Rp. 5.000.000 - Rp. 20.000.000', '2023-11-21 16:36:13', '2023-11-20 13:20:29', 'Aktif'),
(6, 'Tidak Berpenghasilan', NULL, '2023-11-20 13:21:00', 'Aktif'),
(7, 'Lebih dari Rp 20.000.000', NULL, '2023-11-21 16:36:35', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_sekolah`
--

CREATE TABLE `tb_pengaturan_sekolah` (
  `Id_Pengaturan_Sekolah` int(11) NOT NULL,
  `Nama_Sekolah` text DEFAULT NULL,
  `Deskripsi_Singkat` text DEFAULT NULL,
  `Deksripsi_Lengkap` text DEFAULT NULL,
  `Alamat_Lengkap` text DEFAULT NULL,
  `Email_Admin` text DEFAULT NULL,
  `Email_Customer_Service` text DEFAULT NULL,
  `Email_Support` text DEFAULT NULL,
  `Nomor_Telpon` varchar(13) DEFAULT NULL,
  `Nomor_Handphone` varchar(13) DEFAULT NULL,
  `Nama_Facebook` text DEFAULT NULL,
  `Url_Facebook` text DEFAULT NULL,
  `Nama_Instagram` text DEFAULT NULL,
  `Url_Instagram` text DEFAULT NULL,
  `Nama_Twitter` text DEFAULT NULL,
  `Url_Twitter` text DEFAULT NULL,
  `Nama_Linkedin` text DEFAULT NULL,
  `Url_Linkedin` text DEFAULT NULL,
  `Nama_Youtube` text DEFAULT NULL,
  `Url_Youtube` text DEFAULT NULL,
  `Embed_Google_Maps` text DEFAULT NULL,
  `Google_Maps_Url` text DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengaturan_sekolah`
--

INSERT INTO `tb_pengaturan_sekolah` (`Id_Pengaturan_Sekolah`, `Nama_Sekolah`, `Deskripsi_Singkat`, `Deksripsi_Lengkap`, `Alamat_Lengkap`, `Email_Admin`, `Email_Customer_Service`, `Email_Support`, `Nomor_Telpon`, `Nomor_Handphone`, `Nama_Facebook`, `Url_Facebook`, `Nama_Instagram`, `Url_Instagram`, `Nama_Twitter`, `Url_Twitter`, `Nama_Linkedin`, `Url_Linkedin`, `Nama_Youtube`, `Url_Youtube`, `Embed_Google_Maps`, `Google_Maps_Url`, `Waktu_Update_Data`, `Waktu_Simpan_Data`) VALUES
(1, 'SMA ISLAM PB SOEDIRMAN', 'SMA ISLAM PB SOEDIRMAN', 'SMA ISLAM PB SOEDIRMAN', 'Jl. Raya Bogor KM.24. Cijantung Jakarta Timur 13770, RT.3/RW.4, Cijantung, Kec. Ps. Rebo, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13770', 'info@smaislampbs.sch.id', 'info@smaislampbs.sch.id', 'info@smaislampbs.sch.id', '021 8400387', '0858 8900 890', 'SMA ISLAM PB SOEDIRMAN', 'https://www.facebook.com/smaipbsoedirmanjkt', 'SMA ISLAM PB SOEDIRMAN', 'https://www.instagram.com/smaipbsoedirmanjkt/', 'SMA ISLAM PB SOEDIRMAN', 'https://twitter.com/tryout_mandiri', 'SMA ISLAM PB SOEDIRMAN', 'https://id.linkedin.com/in/', 'SMA ISLAM PB SOEDIRMAN', 'https://www.youtube.com/channel/UCfsZYeDgVeFcEL7Z8kOORlQ', '!1m14!1m8!1m3!1d15862.462788132443!2d106.8618662!3d-6.314109!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ed9c293792b9%3A0x333d3cee5ffa4ec3!2sSMA%20Islam%20PB%20Soedirman%20Cijantung!5e0!3m2!1sid!2sid!4v1700791563451!5m2!1sid!2sid', 'https://maps.app.goo.gl/3Dg18Q4bpCj1CycV6', '2023-11-24 09:06:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman_pendaftar`
--

CREATE TABLE `tb_pengumuman_pendaftar` (
  `Id_Pengumuman_Pendaftar` int(11) NOT NULL,
  `Id_Pendaftar` int(1) NOT NULL,
  `Relasi` varchar(50) NOT NULL,
  `Id_Relasi` int(1) NOT NULL,
  `Judul_Pengumuman` varchar(100) NOT NULL,
  `Isi_Pengumuman` text DEFAULT NULL,
  `Status_Dibaca` varchar(15) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pengumuman_pendaftar`
--

INSERT INTO `tb_pengumuman_pendaftar` (`Id_Pengumuman_Pendaftar`, `Id_Pendaftar`, `Relasi`, `Id_Relasi`, `Judul_Pengumuman`, `Isi_Pengumuman`, `Status_Dibaca`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 2, 'Id_Pendaftar', 2, 'Verifikasi Data Diri', 'Selamat, data diri Anda Sudah Diverifikasi', 'Belum Dibaca', '2024-01-07 02:45:28', '2024-01-06 00:50:57', 'Terhapus'),
(3, 2, 'Id_Pendaftar', 2, 'Verifikasi Data Diri', 'Selamat, data diri Anda \'Sudah Diverifikasi\' oleh Admin', 'Belum Dibaca', '2024-01-07 02:45:28', '2024-01-06 01:08:00', 'Terhapus'),
(4, 2, 'Id_Pendaftar', 2, 'Verifikasi Data Diri', 'Verifikasi data diri Anda telah di ubah statusnya menjadi \'Belum Diverifikasi\' Oleh Admin, pastikan data diri Anda telah di isi dengan benar', 'Belum Dibaca', '2024-01-07 02:45:28', '2024-01-06 01:08:03', 'Terhapus'),
(5, 2, 'Id_Pendaftar', 2, 'Verifikasi Data Diri', 'Selamat, data diri Anda \'Sudah Diverifikasi\' oleh Admin', 'Belum Dibaca', '2024-01-07 02:45:28', '2024-01-06 01:08:27', 'Terhapus'),
(6, 2, 'Id_Pendaftar', 2, 'Program Layanan', 'Selamat, Program Layanan Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', '2024-01-07 02:45:28', '2024-01-06 01:23:04', 'Terhapus'),
(7, 2, 'Id_Pendaftar_Pembayaran_PPDB', 2, 'Pembayaran PPDB', 'Selamat, Pembayaran PPDB Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', '2024-01-07 02:45:28', '2024-01-07 02:39:02', 'Terhapus'),
(8, 2, 'Id_Pendaftar_Program_Layanan', 2, 'Kelulusan', 'Mohon Maaf, Anda Tidak Lulus dalam PPDB ini', 'Belum Dibaca', '2024-01-07 02:49:01', '2024-01-07 02:42:50', 'Terhapus'),
(9, 2, 'Id_Pendaftar_Program_Layanan', 2, 'Kelulusan', 'Mohon Maaf, Anda Tidak Lulus dalam PPDB ini', 'Belum Dibaca', '2024-01-07 02:49:01', '2024-01-07 02:43:03', 'Terhapus'),
(10, 2, 'Id_Pendaftar_Program_Layanan', 2, 'Kelulusan', 'Mohon Maaf, Anda Tidak Lulus dalam PPDB ini', 'Belum Dibaca', '2024-01-07 02:49:01', '2024-01-07 02:45:28', 'Terhapus'),
(11, 2, 'Id_Pendaftar_Program_Layanan', 2, 'Kelulusan', 'Mohon Maaf, Anda Tidak Lulus dalam PPDB ini', 'Belum Dibaca', '2024-01-07 02:49:01', '2024-01-07 02:48:54', 'Terhapus'),
(12, 2, 'Id_Pendaftar_Program_Layanan', 2, 'Kelulusan', 'Selamat Anda Lulus dalam PPDB ini', 'Belum Dibaca', NULL, '2024-01-07 02:49:01', 'Aktif'),
(13, 31, 'Id_Pendaftar_Pembelian_Formulir', 32, 'Pembelian Formulir', 'Harap selesaikan Pembayaran PPDB maksimal 10 Hari dari Anda Register. Jika Tidak, maka akan otomatis Gugur', 'Belum Dibaca', NULL, '2024-01-07 03:33:17', 'Aktif'),
(14, 31, 'Id_Pendaftar_Pembelian_Formulir', 29, 'Pembelian Formulir', 'Selamat, Pembelian Formulir Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', NULL, '2024-01-07 07:22:11', 'Aktif'),
(15, 31, 'Id_Pendaftar', 31, 'Verifikasi Data Diri', 'Verifikasi Data Diri Anda telah di ubah statusnya menjadi \'Belum Diverifikasi\' Oleh Admin, pastikan Data Diri Anda telah di isi dengan benar', 'Belum Dibaca', NULL, '2024-01-07 07:26:04', 'Aktif'),
(16, 31, 'Id_Pendaftar', 31, 'Verifikasi Data Diri', 'Selamat, Data Diri Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', NULL, '2024-01-07 07:26:17', 'Aktif'),
(17, 29, 'Id_Pendaftar_Nilai_Rapor', 29, 'Nilai Rapor', 'Selamat, Nilai Rapor Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', '2024-01-07 07:39:50', '2024-01-07 07:35:33', 'Terhapus'),
(18, 29, 'Id_Pendaftar_Nilai_Rapor', 29, 'Nilai Rapor', 'Selamat, Nilai Rapor Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', '2024-01-07 07:39:50', '2024-01-07 07:35:45', 'Terhapus'),
(19, 29, 'Id_Pendaftar_Nilai_Rapor', 29, 'Nilai Rapor', 'Selamat, Nilai Rapor Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', '2024-01-07 07:39:50', '2024-01-07 07:38:18', 'Terhapus'),
(20, 31, 'Id_Pendaftar_Nilai_Rapor', 29, 'Nilai Rapor', 'Selamat, Nilai Rapor Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', NULL, '2024-01-07 07:39:55', 'Aktif'),
(21, 31, 'Id_Pendaftar_Program_Layanan', 29, 'Program Layanan', 'Selamat, Program Layanan Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', NULL, '2024-01-07 07:41:08', 'Aktif'),
(22, 31, 'Id_Pendaftar_Verifikasi_Berkas', 29, 'Berkas', 'Selamat, Berkas Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', NULL, '2024-01-07 07:41:18', 'Aktif'),
(23, 31, 'Id_Pendaftar_Pembayaran_PPDB', 29, 'Pembayaran PPDB', 'Selamat, Pembayaran PPDB Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', '2024-01-07 07:42:24', '2024-01-07 07:41:28', 'Terhapus'),
(24, 31, 'Id_Pendaftar_Pembayaran_PPDB', 29, 'Pembayaran PPDB', 'Selamat, Pembayaran PPDB Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', '2024-01-07 07:42:24', '2024-01-07 07:42:21', 'Terhapus'),
(25, 31, 'Id_Pendaftar_Pembayaran_PPDB', 29, 'Pembayaran PPDB', 'Selamat, Pembayaran PPDB Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', NULL, '2024-01-07 07:43:47', 'Aktif'),
(26, 31, 'Id_Pendaftar', 31, 'Kelulusan', 'Selamat Anda Lulus dalam PPDB ini', 'Belum Dibaca', NULL, '2024-01-07 07:44:01', 'Aktif'),
(27, 3, 'Id_Pendaftar_Pembelian_Formulir', 3, 'Pembelian Formulir', 'Selamat, Pembelian Formulir Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', NULL, '2024-01-07 23:04:19', 'Aktif'),
(28, 30, 'Id_Pendaftar_Pembelian_Formulir', 30, 'Pembelian Formulir', 'Selamat, Pembelian Formulir Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', '2024-01-07 23:53:00', '2024-01-07 23:43:18', 'Terhapus'),
(29, 30, 'Id_Pendaftar_Pembelian_Formulir', 30, 'Pembelian Formulir', 'Selamat, Pembelian Formulir Anda \'Sudah Diverifikasi\' Oleh Admin', 'Belum Dibaca', '2024-01-07 23:53:00', '2024-01-07 23:45:51', 'Terhapus'),
(30, 30, 'Id_Pendaftar_Pembelian_Formulir', 30, 'Pembelian Formulir', 'Selamat, Pembelian Formulir Anda \'Sudah Diverifikasi\' Oleh Admin. Harap selesaikan Pengisian Berkas maksimal 14 Hari dari Admin memverifikasi Pembayaran Ini. Jika Tidak, maka akan otomatis Gugur', 'Belum Dibaca', '2024-01-07 23:53:00', '2024-01-07 23:50:30', 'Terhapus'),
(31, 30, 'Id_Pendaftar_Pembelian_Formulir', 30, 'Pembelian Formulir', 'Selamat, Pembelian Formulir Anda \'Sudah Diverifikasi\' Oleh Admin. Harap selesaikan Pengisian Berkas maksimal 14 Hari dari Admin memverifikasi Pembayaran Ini. Jika Tidak, maka otomatis Status PPDB Anda yakni \'Gugur\'', 'Belum Dibaca', NULL, '2024-01-07 23:53:06', 'Aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_aktivitas_pendaftar`
--
ALTER TABLE `tb_aktivitas_pendaftar`
  ADD PRIMARY KEY (`Id_Aktivitas_Pendaftar`);

--
-- Indexes for table `tb_aktivitas_pengguna`
--
ALTER TABLE `tb_aktivitas_pengguna`
  ADD PRIMARY KEY (`Id_Aktivitas_Pengguna`);

--
-- Indexes for table `tb_data_hak_akses`
--
ALTER TABLE `tb_data_hak_akses`
  ADD PRIMARY KEY (`Id_Hak_Akses`);

--
-- Indexes for table `tb_data_hak_akses_detail`
--
ALTER TABLE `tb_data_hak_akses_detail`
  ADD PRIMARY KEY (`Id_Hak_Akses_Detail`);

--
-- Indexes for table `tb_data_pendaftar`
--
ALTER TABLE `tb_data_pendaftar`
  ADD PRIMARY KEY (`Id_Pendaftar`);

--
-- Indexes for table `tb_data_pendaftar_nilai_rapor`
--
ALTER TABLE `tb_data_pendaftar_nilai_rapor`
  ADD PRIMARY KEY (`Id_Pendaftar_Nilai_Rapor`);

--
-- Indexes for table `tb_data_pendaftar_pembayaran_ppdb`
--
ALTER TABLE `tb_data_pendaftar_pembayaran_ppdb`
  ADD PRIMARY KEY (`Id_Pendaftar_Pembayaran_PPDB`);

--
-- Indexes for table `tb_data_pendaftar_pembelian_formulir`
--
ALTER TABLE `tb_data_pendaftar_pembelian_formulir`
  ADD PRIMARY KEY (`Id_Pendaftar_Pembelian_Formulir`);

--
-- Indexes for table `tb_data_pendaftar_program_layanan`
--
ALTER TABLE `tb_data_pendaftar_program_layanan`
  ADD PRIMARY KEY (`Id_Pendaftar_Program_Layanan`);

--
-- Indexes for table `tb_data_pendaftar_verifikasi_berkas`
--
ALTER TABLE `tb_data_pendaftar_verifikasi_berkas`
  ADD PRIMARY KEY (`Id_Pendaftar_Verifikasi_Berkas`);

--
-- Indexes for table `tb_data_pengguna`
--
ALTER TABLE `tb_data_pengguna`
  ADD PRIMARY KEY (`Id_Pengguna`);

--
-- Indexes for table `tb_data_pengumuman`
--
ALTER TABLE `tb_data_pengumuman`
  ADD PRIMARY KEY (`Id_Pengumuman`);

--
-- Indexes for table `tb_data_ppdb`
--
ALTER TABLE `tb_data_ppdb`
  ADD PRIMARY KEY (`Id_PPDB`);

--
-- Indexes for table `tb_data_siswa`
--
ALTER TABLE `tb_data_siswa`
  ADD PRIMARY KEY (`Id_Siswa`);

--
-- Indexes for table `tb_data_siswa_nilai_rapor`
--
ALTER TABLE `tb_data_siswa_nilai_rapor`
  ADD PRIMARY KEY (`Id_Siswa_Nilai_Rapor`);

--
-- Indexes for table `tb_pengaturan_asal_sekolah`
--
ALTER TABLE `tb_pengaturan_asal_sekolah`
  ADD PRIMARY KEY (`Id_Pengaturan_Asal_Sekolah`);

--
-- Indexes for table `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
--
ALTER TABLE `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
  ADD PRIMARY KEY (`Id_Pengaturan_Kendaraan_Yang_Dipakai_Kesekolah`);

--
-- Indexes for table `tb_pengaturan_pekerjaan_orang_tua`
--
ALTER TABLE `tb_pengaturan_pekerjaan_orang_tua`
  ADD PRIMARY KEY (`Id_Pengaturan_Pekerjaan_Orang_Tua`);

--
-- Indexes for table `tb_pengaturan_penghasilan_orang_tua`
--
ALTER TABLE `tb_pengaturan_penghasilan_orang_tua`
  ADD PRIMARY KEY (`Id_Pengaturan_Penghasilan_Orang_Tua`);

--
-- Indexes for table `tb_pengaturan_sekolah`
--
ALTER TABLE `tb_pengaturan_sekolah`
  ADD PRIMARY KEY (`Id_Pengaturan_Sekolah`);

--
-- Indexes for table `tb_pengumuman_pendaftar`
--
ALTER TABLE `tb_pengumuman_pendaftar`
  ADD PRIMARY KEY (`Id_Pengumuman_Pendaftar`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_aktivitas_pendaftar`
--
ALTER TABLE `tb_aktivitas_pendaftar`
  MODIFY `Id_Aktivitas_Pendaftar` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;

--
-- AUTO_INCREMENT for table `tb_aktivitas_pengguna`
--
ALTER TABLE `tb_aktivitas_pengguna`
  MODIFY `Id_Aktivitas_Pengguna` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_data_hak_akses`
--
ALTER TABLE `tb_data_hak_akses`
  MODIFY `Id_Hak_Akses` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_data_hak_akses_detail`
--
ALTER TABLE `tb_data_hak_akses_detail`
  MODIFY `Id_Hak_Akses_Detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar`
--
ALTER TABLE `tb_data_pendaftar`
  MODIFY `Id_Pendaftar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_nilai_rapor`
--
ALTER TABLE `tb_data_pendaftar_nilai_rapor`
  MODIFY `Id_Pendaftar_Nilai_Rapor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_pembayaran_ppdb`
--
ALTER TABLE `tb_data_pendaftar_pembayaran_ppdb`
  MODIFY `Id_Pendaftar_Pembayaran_PPDB` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_pembelian_formulir`
--
ALTER TABLE `tb_data_pendaftar_pembelian_formulir`
  MODIFY `Id_Pendaftar_Pembelian_Formulir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_program_layanan`
--
ALTER TABLE `tb_data_pendaftar_program_layanan`
  MODIFY `Id_Pendaftar_Program_Layanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_verifikasi_berkas`
--
ALTER TABLE `tb_data_pendaftar_verifikasi_berkas`
  MODIFY `Id_Pendaftar_Verifikasi_Berkas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_data_pengguna`
--
ALTER TABLE `tb_data_pengguna`
  MODIFY `Id_Pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_data_pengumuman`
--
ALTER TABLE `tb_data_pengumuman`
  MODIFY `Id_Pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_data_ppdb`
--
ALTER TABLE `tb_data_ppdb`
  MODIFY `Id_PPDB` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_data_siswa`
--
ALTER TABLE `tb_data_siswa`
  MODIFY `Id_Siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_data_siswa_nilai_rapor`
--
ALTER TABLE `tb_data_siswa_nilai_rapor`
  MODIFY `Id_Siswa_Nilai_Rapor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_pengaturan_asal_sekolah`
--
ALTER TABLE `tb_pengaturan_asal_sekolah`
  MODIFY `Id_Pengaturan_Asal_Sekolah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
--
ALTER TABLE `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
  MODIFY `Id_Pengaturan_Kendaraan_Yang_Dipakai_Kesekolah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_pengaturan_pekerjaan_orang_tua`
--
ALTER TABLE `tb_pengaturan_pekerjaan_orang_tua`
  MODIFY `Id_Pengaturan_Pekerjaan_Orang_Tua` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_pengaturan_penghasilan_orang_tua`
--
ALTER TABLE `tb_pengaturan_penghasilan_orang_tua`
  MODIFY `Id_Pengaturan_Penghasilan_Orang_Tua` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_pengumuman_pendaftar`
--
ALTER TABLE `tb_pengumuman_pendaftar`
  MODIFY `Id_Pengumuman_Pendaftar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
