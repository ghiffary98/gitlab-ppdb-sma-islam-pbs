-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 18, 2023 at 02:06 AM
-- Server version: 10.6.15-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u152269537_db_prod_ppdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_aktivitas_pendaftar`
--

CREATE TABLE `tb_aktivitas_pendaftar` (
  `Id_Aktivitas_Pendaftar` int(1) NOT NULL,
  `Id_Pendaftar` int(1) NOT NULL,
  `Relasi` varchar(50) NOT NULL,
  `Id_Relasi` int(1) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Waktu_Simpan_Data` datetime NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_aktivitas_pengguna`
--

CREATE TABLE `tb_aktivitas_pengguna` (
  `Id_Aktivitas_Pengguna` int(1) NOT NULL,
  `Id_Pengguna` int(1) NOT NULL,
  `Relasi` varchar(50) NOT NULL,
  `Id_Relasi` int(1) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Waktu_Simpan_Data` datetime NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_hak_akses`
--

CREATE TABLE `tb_data_hak_akses` (
  `Id_Hak_Akses` int(11) NOT NULL,
  `Hak_Akses` varchar(50) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Waktu_Update_Data` datetime NOT NULL,
  `Waktu_Simpan_Data` datetime NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_data_hak_akses`
--

INSERT INTO `tb_data_hak_akses` (`Id_Hak_Akses`, `Hak_Akses`, `Deskripsi`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'Super Administrator', 'Super Administrator', '2023-09-26 18:07:38', '2023-09-26 18:07:38', 'Aktif'),
(2, 'Administrator', 'Administrator', '0000-00-00 00:00:00', '2023-09-26 23:08:18', 'Aktif'),
(3, 'Staf', 'Staf', '2023-09-26 23:30:17', '2023-09-26 23:25:06', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_hak_akses_detail`
--

CREATE TABLE `tb_data_hak_akses_detail` (
  `Id_Hak_Akses_Detail` int(11) NOT NULL,
  `Id_Hak_Akses` int(11) NOT NULL,
  `Nama_Halaman` varchar(100) NOT NULL,
  `Kode_Halaman` varchar(100) NOT NULL,
  `Baca_Data` varchar(5) NOT NULL,
  `Simpan_Data` varchar(5) NOT NULL,
  `Update_Data` varchar(5) NOT NULL,
  `Hapus_Data` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_data_hak_akses_detail`
--

INSERT INTO `tb_data_hak_akses_detail` (`Id_Hak_Akses_Detail`, `Id_Hak_Akses`, `Nama_Halaman`, `Kode_Halaman`, `Baca_Data`, `Simpan_Data`, `Update_Data`, `Hapus_Data`) VALUES
(1, 2, 'Data Pendaftar', 'Data_Pendaftar', 'Iya', 'Iya', '', ''),
(2, 2, 'Pembayaran PPDB', 'Data_Pendaftar_Pembayaran_PPDB', 'Iya', 'Iya', '', ''),
(3, 2, 'Pembelian Formulir', 'Data_Pendaftar_Pembelian_Formulir', 'Iya', 'Iya', '', ''),
(4, 2, 'Verifikasi Berkas', 'Data_Pendaftar_Verifikasi_Berkas', 'Iya', 'Iya', '', ''),
(5, 2, 'Data PPDB', 'Data_PPDB', 'Iya', 'Iya', 'Iya', ''),
(6, 2, 'Data Siswa', 'Data_Siswa', 'Iya', 'Iya', '', ''),
(7, 3, 'Data Pendaftar', 'Data_Pendaftar', 'Iya', 'Iya', 'Iya', 'Iya'),
(8, 3, 'Pembayaran PPDB', 'Data_Pendaftar_Pembayaran_PPDB', '', 'Iya', 'Iya', 'Iya'),
(9, 3, 'Pembelian Formulir', 'Data_Pendaftar_Pembelian_Formulir', '', '', 'Iya', ''),
(10, 3, 'Verifikasi Berkas', 'Data_Pendaftar_Verifikasi_Berkas', 'Iya', 'Iya', 'Iya', 'Iya'),
(11, 3, 'Data PPDB', 'Data_PPDB', '', '', 'Iya', ''),
(12, 3, 'Data Siswa', 'Data_Siswa', '', 'Iya', 'Iya', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar`
--

CREATE TABLE `tb_data_pendaftar` (
  `Id_Pendaftar` int(11) NOT NULL,
  `Id_PPDB` int(11) NOT NULL,
  `Nomor_Pendaftaran` varchar(20) NOT NULL,
  `Username` varchar(100) NOT NULL,
  `Password` text NOT NULL,
  `Password_Pendaftar` text DEFAULT NULL,
  `NIK` varchar(50) DEFAULT NULL,
  `NIS` varchar(50) DEFAULT NULL,
  `NISN` varchar(50) DEFAULT NULL,
  `NIPD` varchar(50) DEFAULT NULL,
  `Nama_Lengkap` varchar(100) DEFAULT NULL,
  `Jenis_Kelamin` varchar(20) DEFAULT NULL,
  `Tempat_Lahir` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Agama` varchar(20) DEFAULT NULL,
  `Status_Dalam_Keluarga` varchar(20) DEFAULT NULL,
  `Jalan` text DEFAULT NULL,
  `Kelurahan` varchar(100) NOT NULL,
  `Kecamatan` varchar(100) DEFAULT NULL,
  `Kota` varchar(100) DEFAULT NULL,
  `Provinsi` varchar(100) DEFAULT NULL,
  `Kode_Pos` varchar(10) DEFAULT NULL,
  `Titik_Lintang_Alamat` varchar(50) DEFAULT NULL,
  `Titik_Bujur_Alamat` varchar(50) DEFAULT NULL,
  `Nomor_Handphone` varchar(15) DEFAULT NULL,
  `Nomor_Telepon` varchar(15) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Golongan_Darah` varchar(2) DEFAULT NULL,
  `Kebutuhan_Khusus` varchar(30) DEFAULT NULL,
  `Tinggi_Badan` int(11) DEFAULT NULL,
  `Berat_Badan` int(11) DEFAULT NULL,
  `Lingkar_Kepala` int(1) DEFAULT NULL,
  `Asal_Sekolah` varchar(100) DEFAULT NULL,
  `Alamat_Sekolah` text DEFAULT NULL,
  `Kelas` varchar(5) DEFAULT NULL,
  `Nomor_Peserta_Ujian_Nasional` varchar(50) DEFAULT NULL,
  `Nomor_Seri_Ijazah` varchar(50) DEFAULT NULL,
  `Penerima_KPS` varchar(5) DEFAULT NULL,
  `Penerima_KIP` varchar(5) DEFAULT NULL,
  `Nomor_KIP` varchar(50) DEFAULT NULL,
  `Nomor_KPS` varchar(50) DEFAULT NULL,
  `Nomor_KKS` varchar(50) DEFAULT NULL,
  `Nomor_KK` varchar(50) DEFAULT NULL,
  `Nomor_Registrasi_Akta_Lahir` varchar(50) DEFAULT NULL,
  `Anak_Ke` varchar(3) DEFAULT NULL,
  `Jumlah_Saudara` varchar(3) DEFAULT NULL,
  `Status_Perwalian` varchar(50) DEFAULT NULL,
  `NIK_Ayah` varchar(50) DEFAULT NULL,
  `Nama_Ayah` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Ayah` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir_Ayah` date DEFAULT NULL,
  `Jenjang_Pendidikan_Ayah` varchar(30) DEFAULT NULL,
  `Pekerjaan_Ayah` varchar(100) DEFAULT NULL,
  `Penghasilan_Ayah` varchar(100) DEFAULT NULL,
  `No_HP_Ayah` varchar(16) DEFAULT NULL,
  `NIK_Ibu` varchar(50) DEFAULT NULL,
  `Nama_Ibu` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Ibu` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir_Ibu` date DEFAULT NULL,
  `Jenjang_Pendidikan_Ibu` varchar(30) DEFAULT NULL,
  `Pekerjaan_Ibu` varchar(100) DEFAULT NULL,
  `Penghasilan_Ibu` varchar(100) DEFAULT NULL,
  `No_HP_Ibu` varchar(16) DEFAULT NULL,
  `NIK_Wali` varchar(50) DEFAULT NULL,
  `Nama_Wali` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Wali` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir_Wali` date DEFAULT NULL,
  `Jenjang_Pendidikan_Wali` varchar(30) DEFAULT NULL,
  `Pekerjaan_Wali` varchar(100) DEFAULT NULL,
  `Penghasilan_Wali` varchar(100) DEFAULT NULL,
  `No_HP_Wali` varchar(16) DEFAULT NULL,
  `Kendaraan_Yang_Dipakai_Kesekolah` varchar(100) DEFAULT NULL,
  `Jarak_Rumah_Ke_Sekolah` int(1) DEFAULT NULL,
  `Status_Verifikasi_Data_Diri` varchar(50) DEFAULT NULL,
  `Status_Pendaftaran` varchar(50) DEFAULT NULL,
  `Status_Kelulusan` varchar(50) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_nilai_rapor`
--

CREATE TABLE `tb_data_pendaftar_nilai_rapor` (
  `Id_Pendaftar_Nilai_Rapor` int(11) NOT NULL,
  `Id_Pendaftar` int(11) DEFAULT NULL,
  `Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_1_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_2_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_3_Prakarya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Bahasa_Indonesia` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Bahasa_Inggris` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Matematika` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_IPA` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_IPS` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Seni_Dan_Budaya` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_PJOK` varchar(6) DEFAULT NULL,
  `Nilai_Semester_4_Prakarya` varchar(6) DEFAULT NULL,
  `Status_Verifikasi_Nilai_Rapor` varchar(50) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_pembayaran_ppdb`
--

CREATE TABLE `tb_data_pendaftar_pembayaran_ppdb` (
  `Id_Pendaftar_Pembayaran_PPDB` int(11) NOT NULL,
  `Id_Pendaftar` int(11) NOT NULL,
  `Bukti_Pembayaran_PPDB` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Pembayaran_PPDB` varchar(50) DEFAULT NULL,
  `JSON_Response_Pembayaran_Xendit` text NOT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_pembelian_formulir`
--

CREATE TABLE `tb_data_pendaftar_pembelian_formulir` (
  `Id_Pendaftar_Pembelian_Formulir` int(11) NOT NULL,
  `Id_Pendaftar` int(11) NOT NULL,
  `Bukti_Pembayaran_Pembelian_Formulir` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Pembelian_Formulir` varchar(50) DEFAULT NULL,
  `JSON_Response_Pembayaran_Xendit` text NOT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_program_layanan`
--

CREATE TABLE `tb_data_pendaftar_program_layanan` (
  `Id_Pendaftar_Program_Layanan` int(11) NOT NULL,
  `Id_Pendaftar` int(11) NOT NULL,
  `Program` varchar(50) DEFAULT NULL,
  `Layanan_1` varchar(50) DEFAULT NULL,
  `Layanan_2` varchar(50) DEFAULT NULL,
  `Layanan_3` varchar(50) DEFAULT NULL,
  `Layanan_4` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Program_Layanan` varchar(50) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pendaftar_verifikasi_berkas`
--

CREATE TABLE `tb_data_pendaftar_verifikasi_berkas` (
  `Id_Pendaftar_Verifikasi_Berkas` int(11) NOT NULL,
  `Id_Pendaftar` int(11) NOT NULL,
  `Akta_Kelahiran` varchar(50) DEFAULT NULL,
  `Kartu_Keluarga` varchar(50) DEFAULT NULL,
  `Rapor` varchar(50) DEFAULT NULL,
  `Kartu_NISN` varchar(50) DEFAULT NULL,
  `SKL_Ijazah` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Akta_Kelahiran` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Kartu_Keluarga` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Rapor` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Kartu_NISN` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_SKL_Ijazah` varchar(50) DEFAULT NULL,
  `Status_Verifikasi_Berkas` varchar(50) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pengguna`
--

CREATE TABLE `tb_data_pengguna` (
  `Id_Pengguna` int(11) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Nama_Lengkap` varchar(100) DEFAULT NULL,
  `Id_Hak_Akses` int(11) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Nomor_Handphone` text DEFAULT NULL,
  `Jenis_Kelamin` varchar(10) DEFAULT NULL,
  `Tempat_Lahir` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Alamat_Lengkap` text DEFAULT NULL,
  `Token_Login` text NOT NULL,
  `Waktu_Terakhir_Login` datetime DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime NOT NULL,
  `Status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tb_data_pengguna`
--

INSERT INTO `tb_data_pengguna` (`Id_Pengguna`, `Username`, `Password`, `Nama_Lengkap`, `Id_Hak_Akses`, `Email`, `Nomor_Handphone`, `Jenis_Kelamin`, `Tempat_Lahir`, `Tanggal_Lahir`, `Alamat_Lengkap`, `Token_Login`, `Waktu_Terakhir_Login`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'admin', '9adc9fd4d3963fe1e3b0fddf417cdec7', 'Ryan Anggowo', 1, 'ryansemtrrc@gmail.com', '081617466672', 'Laki-Laki', 'Bogor', '1999-02-02', NULL, '', '2023-09-23 19:11:40', '2023-10-20 23:19:29', '2023-09-23 19:11:40', 'Aktif'),
(2, 'ryan', 'f835a44d4655335e0e2a9e5ac82e3177', 'Ryan Aja', NULL, 'ryansemtrrc@gmail.com', '081617466672', 'Laki-Laki', 'Bigir', '1999-02-02', 'TEST', '', NULL, '2023-09-26 22:14:55', '2023-09-24 01:57:16', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_pengumuman`
--

CREATE TABLE `tb_data_pengumuman` (
  `Id_Pengumuman` int(11) NOT NULL,
  `Judul_Pengumuman` varchar(100) NOT NULL,
  `Isi_Pengumuman` text DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_ppdb`
--

CREATE TABLE `tb_data_ppdb` (
  `Id_PPDB` int(11) NOT NULL,
  `Tahun_Ajaran` varchar(30) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `Deskripsi` text DEFAULT NULL,
  `Tanggal_Mulai_Pendaftaran` date DEFAULT NULL,
  `Tanggal_Akhir_Pendaftaran` date DEFAULT NULL,
  `No_Rekening_Utama` varchar(100) DEFAULT NULL,
  `No_Rekening_Cadangan` varchar(100) DEFAULT NULL,
  `Biaya_Pendaftaran_Formulir` int(11) DEFAULT NULL,
  `Biaya_PPDB` int(11) DEFAULT NULL,
  `Status_PPDB` varchar(30) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_data_ppdb`
--

INSERT INTO `tb_data_ppdb` (`Id_PPDB`, `Tahun_Ajaran`, `Judul`, `Deskripsi`, `Tanggal_Mulai_Pendaftaran`, `Tanggal_Akhir_Pendaftaran`, `No_Rekening_Utama`, `No_Rekening_Cadangan`, `Biaya_Pendaftaran_Formulir`, `Biaya_PPDB`, `Status_PPDB`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, '2023/2024', 'PPDB 2023/2024', 'PPDB 2023/2024', '2023-10-05', '2023-10-31', 'Mandiri 421321312312 an Ryan Anggowo', 'BCA 1231231232 an Ryan Anggowo', 500000, 400000, 'Aktif', '2023-11-15 19:23:07', '2023-10-04 00:27:23', 'Aktif'),
(2, '2022/2023', 'PPDB 2022/2023', 'PPDB 2022/2023', '2022-01-01', '2022-01-30', 'Mandiri 421321312312 an Ryan Anggowo', 'BCA 1231231232 an Ryan Anggowo', 500000, 400000, 'Tidak Aktif', '2023-11-13 23:37:53', '2023-10-04 01:47:59', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_siswa`
--

CREATE TABLE `tb_data_siswa` (
  `Id_Siswa` int(11) NOT NULL,
  `Id_Pendaftar` int(11) DEFAULT NULL,
  `Id_PPDB` int(11) DEFAULT NULL,
  `NIS_Saat_Ini` varchar(100) DEFAULT NULL,
  `NIK` varchar(50) DEFAULT NULL,
  `NIS` varchar(50) DEFAULT NULL,
  `NISN` varchar(50) DEFAULT NULL,
  `NIPD` varchar(50) DEFAULT NULL,
  `Nama_Lengkap` varchar(100) DEFAULT NULL,
  `Jenis_Kelamin` varchar(20) DEFAULT NULL,
  `Tempat_Lahir` varchar(50) DEFAULT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Agama` varchar(20) DEFAULT NULL,
  `Status_Dalam_Keluarga` varchar(20) DEFAULT NULL,
  `Jalan` text DEFAULT NULL,
  `Kelurahan` varchar(100) DEFAULT NULL,
  `Kecamatan` varchar(100) DEFAULT NULL,
  `Kota` varchar(100) DEFAULT NULL,
  `Provinsi` varchar(100) DEFAULT NULL,
  `Kode_Pos` varchar(10) DEFAULT NULL,
  `Titik_Lintang_Alamat` varchar(50) DEFAULT NULL,
  `Titik_Bujur_Alamat` varchar(50) DEFAULT NULL,
  `Nomor_Handphone` varchar(15) DEFAULT NULL,
  `Nomor_Telepon` varchar(15) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Golongan_Darah` varchar(2) DEFAULT NULL,
  `Kebutuhan_Khusus` varchar(30) DEFAULT NULL,
  `Tinggi_Badan` int(11) DEFAULT NULL,
  `Berat_Badan` int(11) DEFAULT NULL,
  `Lingkar_Kepala` int(1) DEFAULT NULL,
  `Asal_Sekolah` varchar(100) DEFAULT NULL,
  `Alamat_Sekolah` text DEFAULT NULL,
  `Kelas` varchar(5) DEFAULT NULL,
  `Nomor_Peserta_Ujian_Nasional` varchar(50) DEFAULT NULL,
  `Nomor_Seri_Ijazah` varchar(50) DEFAULT NULL,
  `Penerima_KPS` varchar(5) DEFAULT NULL,
  `Penerima_KIP` varchar(5) DEFAULT NULL,
  `Nomor_KIP` varchar(50) DEFAULT NULL,
  `Nomor_KPS` varchar(50) DEFAULT NULL,
  `Nomor_KKS` varchar(50) DEFAULT NULL,
  `Nomor_KK` varchar(50) DEFAULT NULL,
  `Nomor_Registrasi_Akta_Lahir` varchar(50) DEFAULT NULL,
  `Anak_Ke` varchar(3) DEFAULT NULL,
  `Jumlah_Saudara` varchar(3) DEFAULT NULL,
  `Status_Perwalian` varchar(50) DEFAULT NULL,
  `NIK_Ayah` varchar(50) DEFAULT NULL,
  `Nama_Ayah` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Ayah` varchar(50) NOT NULL,
  `Tanggal_Lahir_Ayah` date DEFAULT NULL,
  `Jenjang_Pendidikan_Ayah` varchar(30) DEFAULT NULL,
  `Pekerjaan_Ayah` varchar(100) DEFAULT NULL,
  `Penghasilan_Ayah` varchar(100) DEFAULT NULL,
  `No_HP_Ayah` varchar(16) DEFAULT NULL,
  `NIK_Ibu` varchar(50) DEFAULT NULL,
  `Nama_Ibu` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Ibu` varchar(50) NOT NULL,
  `Tanggal_Lahir_Ibu` date DEFAULT NULL,
  `Jenjang_Pendidikan_Ibu` varchar(30) DEFAULT NULL,
  `Pekerjaan_Ibu` varchar(100) DEFAULT NULL,
  `Penghasilan_Ibu` varchar(100) DEFAULT NULL,
  `No_HP_Ibu` varchar(16) DEFAULT NULL,
  `NIK_Wali` varchar(50) DEFAULT NULL,
  `Nama_Wali` varchar(100) DEFAULT NULL,
  `Tempat_Lahir_Wali` varchar(50) NOT NULL,
  `Tanggal_Lahir_Wali` date DEFAULT NULL,
  `Jenjang_Pendidikan_Wali` varchar(30) DEFAULT NULL,
  `Pekerjaan_Wali` varchar(100) DEFAULT NULL,
  `Penghasilan_Wali` varchar(100) DEFAULT NULL,
  `No_HP_Wali` varchar(16) DEFAULT NULL,
  `Kendaraan_Yang_Dipakai_Kesekolah` varchar(100) DEFAULT NULL,
  `Jarak_Rumah_Ke_Sekolah` int(1) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_asal_sekolah`
--

CREATE TABLE `tb_pengaturan_asal_sekolah` (
  `Id_Pengaturan_Asal_Sekolah` int(11) NOT NULL,
  `Nama_Sekolah` varchar(100) DEFAULT NULL,
  `Alamat_Sekolah` text DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_pengaturan_asal_sekolah`
--

INSERT INTO `tb_pengaturan_asal_sekolah` (`Id_Pengaturan_Asal_Sekolah`, `Nama_Sekolah`, `Alamat_Sekolah`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'SMK Wikrama Bogor', '', '2023-10-18 22:38:35', '2023-10-13 06:09:47', 'Aktif'),
(2, 'SMK Tridarma', '', '2023-10-18 22:38:40', '2023-10-15 01:46:35', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
--

CREATE TABLE `tb_pengaturan_kendaraan_yang_dipakai_kesekolah` (
  `Id_Pengaturan_Kendaraan_Yang_Dipakai_Kesekolah` int(11) NOT NULL,
  `Nama_Kendaraan` varchar(100) NOT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
--

INSERT INTO `tb_pengaturan_kendaraan_yang_dipakai_kesekolah` (`Id_Pengaturan_Kendaraan_Yang_Dipakai_Kesekolah`, `Nama_Kendaraan`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'Angkutan Umum', '2023-10-18 22:39:18', '2023-10-13 06:13:51', 'Aktif'),
(2, 'Motor', '2023-10-18 22:39:21', '2023-10-15 01:46:45', 'Aktif'),
(3, 'Mobil', '2023-10-18 22:39:22', '2023-10-15 01:46:48', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_pekerjaan_orang_tua`
--

CREATE TABLE `tb_pengaturan_pekerjaan_orang_tua` (
  `Id_Pengaturan_Pekerjaan_Orang_Tua` int(11) NOT NULL,
  `Pekerjaan_Orang_Tua` varchar(100) NOT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_pengaturan_pekerjaan_orang_tua`
--

INSERT INTO `tb_pengaturan_pekerjaan_orang_tua` (`Id_Pengaturan_Pekerjaan_Orang_Tua`, `Pekerjaan_Orang_Tua`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'Pegawai Swasta', '2023-10-18 22:39:56', '2023-10-13 05:55:53', 'Aktif'),
(2, 'Pegawai Negeri', '2023-10-18 22:39:58', '2023-10-15 01:46:07', 'Aktif'),
(3, 'Ibu Rumah Tangga', '2023-10-18 22:40:00', '2023-10-15 01:46:13', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_penghasilan_orang_tua`
--

CREATE TABLE `tb_pengaturan_penghasilan_orang_tua` (
  `Id_Pengaturan_Penghasilan_Orang_Tua` int(11) NOT NULL,
  `Penghasilan_Orang_Tua` varchar(100) DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_pengaturan_penghasilan_orang_tua`
--

INSERT INTO `tb_pengaturan_penghasilan_orang_tua` (`Id_Pengaturan_Penghasilan_Orang_Tua`, `Penghasilan_Orang_Tua`, `Waktu_Update_Data`, `Waktu_Simpan_Data`, `Status`) VALUES
(1, 'Rp. 1.000.000 s/d Rp. 2.000.000', '2023-10-18 22:40:28', '2023-10-13 05:56:45', 'Aktif'),
(2, 'Rp. 2.000.000 s/d Rp. 4.000.000', '2023-10-18 22:40:30', '2023-10-15 01:46:24', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaturan_sekolah`
--

CREATE TABLE `tb_pengaturan_sekolah` (
  `Id_Pengaturan_Sekolah` int(11) NOT NULL,
  `Nama_Sekolah` text DEFAULT NULL,
  `Deskripsi_Singkat` text DEFAULT NULL,
  `Deksripsi_Lengkap` text DEFAULT NULL,
  `Alamat_Lengkap` text DEFAULT NULL,
  `Email_Admin` text DEFAULT NULL,
  `Email_Customer_Service` text DEFAULT NULL,
  `Email_Support` text DEFAULT NULL,
  `Nomor_Telpon` varchar(13) DEFAULT NULL,
  `Nomor_Handphone` varchar(13) DEFAULT NULL,
  `Nama_Facebook` text DEFAULT NULL,
  `Url_Facebook` text DEFAULT NULL,
  `Nama_Instagram` text DEFAULT NULL,
  `Url_Instagram` text DEFAULT NULL,
  `Nama_Twitter` text DEFAULT NULL,
  `Url_Twitter` text DEFAULT NULL,
  `Nama_Linkedin` text DEFAULT NULL,
  `Url_Linkedin` text DEFAULT NULL,
  `Nama_Youtube` text DEFAULT NULL,
  `Url_Youtube` text DEFAULT NULL,
  `Embed_Google_Maps` text DEFAULT NULL,
  `Google_Maps_Url` text DEFAULT NULL,
  `Waktu_Update_Data` datetime DEFAULT NULL,
  `Waktu_Simpan_Data` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tb_pengaturan_sekolah`
--

INSERT INTO `tb_pengaturan_sekolah` (`Id_Pengaturan_Sekolah`, `Nama_Sekolah`, `Deskripsi_Singkat`, `Deksripsi_Lengkap`, `Alamat_Lengkap`, `Email_Admin`, `Email_Customer_Service`, `Email_Support`, `Nomor_Telpon`, `Nomor_Handphone`, `Nama_Facebook`, `Url_Facebook`, `Nama_Instagram`, `Url_Instagram`, `Nama_Twitter`, `Url_Twitter`, `Nama_Linkedin`, `Url_Linkedin`, `Nama_Youtube`, `Url_Youtube`, `Embed_Google_Maps`, `Google_Maps_Url`, `Waktu_Update_Data`, `Waktu_Simpan_Data`) VALUES
(1, 'SMA ISLAM PB SOEDIRMAN', 'SMA ISLAM PB SOEDIRMAN', 'SMA ISLAM PB SOEDIRMAN', 'TEST', 'admin@aaa.com', 'cs@aaa.com', 'support@aaa.com', '081210626564', '081210626564', 'SMA ISLAM PB SOEDIRMAN', 'https://www.facebook.com/tryoutmandiri-104959291887230/', 'SMA ISLAM PB SOEDIRMAN', 'https://www.instagram.com/tryoutmandiri/', 'SMA ISLAM PB SOEDIRMAN', 'https://twitter.com/tryout_mandiri', 'SMA ISLAM PB SOEDIRMAN', 'https://id.linkedin.com/in/', 'SMA ISLAM PB SOEDIRMAN', 'https://www.youtube.com/channel/UCDPZ2X3etDsaaXhlt3hN9YQ', '!1m18!1m12!1m3!1d3962.266623907489!2d106.81593001462812!3d-6.737293667748635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69cb89deb53a8d%3A0xbc1ff6e7aad06709!2sCigombong%2C%20Srogol%2C%20Cigombong%2C%20Bogor%2C%20Jawa%20Barat%2016110!5e0!3m2!1sid!2sid!4v1614264003380!5m2!1sid!2sid', '#', '2023-10-04 00:02:56', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_aktivitas_pendaftar`
--
ALTER TABLE `tb_aktivitas_pendaftar`
  ADD PRIMARY KEY (`Id_Aktivitas_Pendaftar`);

--
-- Indexes for table `tb_aktivitas_pengguna`
--
ALTER TABLE `tb_aktivitas_pengguna`
  ADD PRIMARY KEY (`Id_Aktivitas_Pengguna`);

--
-- Indexes for table `tb_data_hak_akses`
--
ALTER TABLE `tb_data_hak_akses`
  ADD PRIMARY KEY (`Id_Hak_Akses`);

--
-- Indexes for table `tb_data_hak_akses_detail`
--
ALTER TABLE `tb_data_hak_akses_detail`
  ADD PRIMARY KEY (`Id_Hak_Akses_Detail`);

--
-- Indexes for table `tb_data_pendaftar`
--
ALTER TABLE `tb_data_pendaftar`
  ADD PRIMARY KEY (`Id_Pendaftar`);

--
-- Indexes for table `tb_data_pendaftar_nilai_rapor`
--
ALTER TABLE `tb_data_pendaftar_nilai_rapor`
  ADD PRIMARY KEY (`Id_Pendaftar_Nilai_Rapor`);

--
-- Indexes for table `tb_data_pendaftar_pembayaran_ppdb`
--
ALTER TABLE `tb_data_pendaftar_pembayaran_ppdb`
  ADD PRIMARY KEY (`Id_Pendaftar_Pembayaran_PPDB`);

--
-- Indexes for table `tb_data_pendaftar_pembelian_formulir`
--
ALTER TABLE `tb_data_pendaftar_pembelian_formulir`
  ADD PRIMARY KEY (`Id_Pendaftar_Pembelian_Formulir`);

--
-- Indexes for table `tb_data_pendaftar_program_layanan`
--
ALTER TABLE `tb_data_pendaftar_program_layanan`
  ADD PRIMARY KEY (`Id_Pendaftar_Program_Layanan`);

--
-- Indexes for table `tb_data_pendaftar_verifikasi_berkas`
--
ALTER TABLE `tb_data_pendaftar_verifikasi_berkas`
  ADD PRIMARY KEY (`Id_Pendaftar_Verifikasi_Berkas`);

--
-- Indexes for table `tb_data_pengguna`
--
ALTER TABLE `tb_data_pengguna`
  ADD PRIMARY KEY (`Id_Pengguna`);

--
-- Indexes for table `tb_data_pengumuman`
--
ALTER TABLE `tb_data_pengumuman`
  ADD PRIMARY KEY (`Id_Pengumuman`);

--
-- Indexes for table `tb_data_ppdb`
--
ALTER TABLE `tb_data_ppdb`
  ADD PRIMARY KEY (`Id_PPDB`);

--
-- Indexes for table `tb_data_siswa`
--
ALTER TABLE `tb_data_siswa`
  ADD PRIMARY KEY (`Id_Siswa`);

--
-- Indexes for table `tb_pengaturan_asal_sekolah`
--
ALTER TABLE `tb_pengaturan_asal_sekolah`
  ADD PRIMARY KEY (`Id_Pengaturan_Asal_Sekolah`);

--
-- Indexes for table `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
--
ALTER TABLE `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
  ADD PRIMARY KEY (`Id_Pengaturan_Kendaraan_Yang_Dipakai_Kesekolah`);

--
-- Indexes for table `tb_pengaturan_pekerjaan_orang_tua`
--
ALTER TABLE `tb_pengaturan_pekerjaan_orang_tua`
  ADD PRIMARY KEY (`Id_Pengaturan_Pekerjaan_Orang_Tua`);

--
-- Indexes for table `tb_pengaturan_penghasilan_orang_tua`
--
ALTER TABLE `tb_pengaturan_penghasilan_orang_tua`
  ADD PRIMARY KEY (`Id_Pengaturan_Penghasilan_Orang_Tua`);

--
-- Indexes for table `tb_pengaturan_sekolah`
--
ALTER TABLE `tb_pengaturan_sekolah`
  ADD PRIMARY KEY (`Id_Pengaturan_Sekolah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_aktivitas_pendaftar`
--
ALTER TABLE `tb_aktivitas_pendaftar`
  MODIFY `Id_Aktivitas_Pendaftar` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_aktivitas_pengguna`
--
ALTER TABLE `tb_aktivitas_pengguna`
  MODIFY `Id_Aktivitas_Pengguna` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_data_hak_akses`
--
ALTER TABLE `tb_data_hak_akses`
  MODIFY `Id_Hak_Akses` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_data_hak_akses_detail`
--
ALTER TABLE `tb_data_hak_akses_detail`
  MODIFY `Id_Hak_Akses_Detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar`
--
ALTER TABLE `tb_data_pendaftar`
  MODIFY `Id_Pendaftar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_nilai_rapor`
--
ALTER TABLE `tb_data_pendaftar_nilai_rapor`
  MODIFY `Id_Pendaftar_Nilai_Rapor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_pembayaran_ppdb`
--
ALTER TABLE `tb_data_pendaftar_pembayaran_ppdb`
  MODIFY `Id_Pendaftar_Pembayaran_PPDB` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_pembelian_formulir`
--
ALTER TABLE `tb_data_pendaftar_pembelian_formulir`
  MODIFY `Id_Pendaftar_Pembelian_Formulir` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_program_layanan`
--
ALTER TABLE `tb_data_pendaftar_program_layanan`
  MODIFY `Id_Pendaftar_Program_Layanan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_data_pendaftar_verifikasi_berkas`
--
ALTER TABLE `tb_data_pendaftar_verifikasi_berkas`
  MODIFY `Id_Pendaftar_Verifikasi_Berkas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_data_pengguna`
--
ALTER TABLE `tb_data_pengguna`
  MODIFY `Id_Pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_data_pengumuman`
--
ALTER TABLE `tb_data_pengumuman`
  MODIFY `Id_Pengumuman` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_data_ppdb`
--
ALTER TABLE `tb_data_ppdb`
  MODIFY `Id_PPDB` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_data_siswa`
--
ALTER TABLE `tb_data_siswa`
  MODIFY `Id_Siswa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_pengaturan_asal_sekolah`
--
ALTER TABLE `tb_pengaturan_asal_sekolah`
  MODIFY `Id_Pengaturan_Asal_Sekolah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
--
ALTER TABLE `tb_pengaturan_kendaraan_yang_dipakai_kesekolah`
  MODIFY `Id_Pengaturan_Kendaraan_Yang_Dipakai_Kesekolah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_pengaturan_pekerjaan_orang_tua`
--
ALTER TABLE `tb_pengaturan_pekerjaan_orang_tua`
  MODIFY `Id_Pengaturan_Pekerjaan_Orang_Tua` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_pengaturan_penghasilan_orang_tua`
--
ALTER TABLE `tb_pengaturan_penghasilan_orang_tua`
  MODIFY `Id_Pengaturan_Penghasilan_Orang_Tua` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
