<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar = new a_data_pendaftar();
$a_data_pendaftar_pembayaran_ppdb = new a_data_pendaftar_pembayaran_ppdb();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
$a_data_pendaftar_verifikasi_berkas = new a_data_pendaftar_verifikasi_berkas();
$a_data_pendaftar_nilai_rapor = new a_data_pendaftar_nilai_rapor();
$a_data_pendaftar_program_layanan = new a_data_pendaftar_program_layanan();
$a_data_pengumuman = new a_data_pengumuman();
$a_data_ppdb = new a_data_ppdb();
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGUMUMAN
$search_field_where = array("Status");
$search_criteria_where = array("=");
$search_value_where = array("Aktif");
$search_connector_where = array("");

$nomor = 0;

$result = $a_data_pengumuman->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$list_data_pengumuman = $result['Hasil'];
}

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA AKTIVITAS PENDAFTAR
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "ORDER BY Id_Aktivitas_Pendaftar DESC");

$nomor = 0;

$result = $a_aktivitas_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$list_data_aktivitas_pendaftar = $result['Hasil'];
}

#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
$ada_update_data_pendaftar_otomatis = "Tidak";

//FUNGSI AMBIL DATA PENDAFTAR
$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$u_data_user[Id_Pendaftar]");

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar = $result['Hasil'];
} else {
	$edit_data_pendaftar = null;
}

//FUNGSI AMBIL DATA PPDB
$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

if ($result['Status'] == "Sukses") {
	$edit_data_ppdb = $result['Hasil'];
} else {
	$edit_data_ppdb = null;
}

//FUNGSI AMBIL DATA PEMBELIAN FORMULIR
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "");

$result = $a_data_pendaftar_pembelian_formulir->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar_pembelian_formulir = $result['Hasil'][0];
} else {
	$edit_data_pendaftar_pembelian_formulir = null;
}

if (!isset($edit_data_pendaftar_pembelian_formulir)) {
	//JIKA TIDAK ADA DATA SAMA SEKALI, MAKA OTOMATIS CREATE DATA
	$form_field = array("Id_Pendaftar_Pembelian_Formulir", "Id_Pendaftar", "Status_Verifikasi_Pembelian_Formulir", "Waktu_Simpan_Data", "Status");
	$form_value = array(NULL, "$u_data_user[Id_Pendaftar]", "Belum Diverifikasi", "$Waktu_Sekarang", "Aktif");
	$result = $a_data_pendaftar_pembelian_formulir->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		$ada_update_data_pendaftar_otomatis = "Iya";
		// echo "<script>document.location.href='$Link_Sekarang'</script>";
		// exit();
	}
}

//FUNGSI AMBIL DATA PEMBELIAN FORMULIR
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "");

$result = $a_data_pendaftar_verifikasi_berkas->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar_verifikasi_berkas = $result['Hasil'][0];
} else {
	$edit_data_pendaftar_verifikasi_berkas = null;
}

if (!isset($edit_data_pendaftar_verifikasi_berkas)) {
	//JIKA TIDAK ADA DATA SAMA SEKALI, MAKA OTOMATIS CREATE DATA
	$form_field = array("Id_Pendaftar_Verifikasi_Berkas", "Id_Pendaftar", "Status_Verifikasi_Berkas", "Waktu_Simpan_Data", "Status");
	$form_value = array(NULL, "$u_data_user[Id_Pendaftar]", "Belum Diverifikasi", "$Waktu_Sekarang", "Aktif");
	$result = $a_data_pendaftar_verifikasi_berkas->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		$ada_update_data_pendaftar_otomatis = "Iya";
		// echo "<script>document.location.href='$Link_Sekarang'</script>";
		// exit();
	}
}

//FUNGSI AMBIL DATA PEMBAYARAN PPDB
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "");

$result = $a_data_pendaftar_pembayaran_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar_pembayaran_ppdb = $result['Hasil'][0];
} else {
	$edit_data_pendaftar_pembayaran_ppdb = null;
}

if (!isset($edit_data_pendaftar_pembayaran_ppdb)) {
	//JIKA TIDAK ADA DATA SAMA SEKALI, MAKA OTOMATIS CREATE DATA
	$form_field = array("Id_Pendaftar_Pembayaran_PPDB", "Id_Pendaftar", "Status_Verifikasi_Pembayaran_PPDB", "Waktu_Simpan_Data", "Status");
	$form_value = array(NULL, "$u_data_user[Id_Pendaftar]", "Belum Diverifikasi", "$Waktu_Sekarang", "Aktif");
	$result = $a_data_pendaftar_pembayaran_ppdb->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		$ada_update_data_pendaftar_otomatis = "Iya";
		// echo "<script>document.location.href='$Link_Sekarang'</script>";
		// exit();
	}
}

//FUNGSI AMBIL DATA NILAI RAPOR
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "");

$result = $a_data_pendaftar_nilai_rapor->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar_nilai_rapor = $result['Hasil'][0];
} else {
	$edit_data_pendaftar_nilai_rapor = null;
}

if (!isset($edit_data_pendaftar_nilai_rapor)) {
	//JIKA TIDAK ADA DATA SAMA SEKALI, MAKA OTOMATIS CREATE DATA
	$form_field = array("Id_Pendaftar_Nilai_Rapor", "Id_Pendaftar", "Status_Verifikasi_Nilai_Rapor", "Waktu_Simpan_Data", "Status");
	$form_value = array(NULL, "$u_data_user[Id_Pendaftar]", "Belum Diverifikasi", "$Waktu_Sekarang", "Aktif");
	$result = $a_data_pendaftar_nilai_rapor->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		$ada_update_data_pendaftar_otomatis = "Iya";
		// echo "<script>document.location.href='$Link_Sekarang'</script>";
		// exit();
	}
}

//FUNGSI AMBIL DATA PROGRAM LAYANAN
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "");

$result = $a_data_pendaftar_program_layanan->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar_program_layanan = $result['Hasil'][0];
} else {
	$edit_data_pendaftar_program_layanan = null;
}

if (!isset($edit_data_pendaftar_program_layanan)) {
	//JIKA TIDAK ADA DATA SAMA SEKALI, MAKA OTOMATIS CREATE DATA
	$form_field = array("Id_Pendaftar_Program_Layanan", "Id_Pendaftar", "Status_Verifikasi_Program_Layanan", "Waktu_Simpan_Data", "Status");
	$form_value = array(NULL, "$u_data_user[Id_Pendaftar]", "Belum Diverifikasi", "$Waktu_Sekarang", "Aktif");
	$result = $a_data_pendaftar_program_layanan->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		$ada_update_data_pendaftar_otomatis = "Iya";
		// echo "<script>document.location.href='$Link_Sekarang'</script>";
		// exit();
	}
}



if ($ada_update_data_pendaftar_otomatis == "Iya") {
	echo "<script>document.location.href='$Link_Sekarang'</script>";
	exit();
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
function Cek_Data_Kosong_Pada_Verifikasi_Data_Diri($jsonData)
{
	// Kolom yang dikecualikan
	$excludedColumns = [
		"Id_Pendaftar",
		"Id_PPDB",
		"Nomor_Pendaftaran",
		"Username",
		"Password",

		"NIK_Wali",
		"Nama_Wali",
		"Tahun_Lahir_Wali",
		"Jenjang_Pendidikan_Wali",
		"Pekerjaan_Wali",
		"Penghasilan_Wali",

		"Status_Pendaftaran",
		"Status_Kelulusan",
		"Waktu_Update_Data",
		"Waktu_Simpan_Data",
		"Status"
	];

	// Konversi JSON ke array asosiatif
	$data = json_decode($jsonData, true);

	// Iterasi melalui data dan periksa apakah ada yang kosong
	foreach ($data as $key => $value) {
		if (!in_array($key, $excludedColumns) && (is_null($value) || $value === "")) {
			return true; // Ada data kosong
		}
	}

	return false; // Semua data diisi
}

function Cek_Data_Kosong_Pada_Verifikasi_Berkas($jsonData)
{
	// Kolom yang dikecualikan
	$excludedColumns = [
		"Id_Pendaftar_Verifikasi_Berkas",
		"Id_Pendaftar",

		"Status_Verifikasi_Akta_Kelahiran",
		"Status_Verifikasi_Kartu_Keluarga",
		"Status_Verifikasi_Rapor",
		"Status_Verifikasi_Kartu_NISN",
		"Status_Verifikasi_SKL_Ijazah",
		"Status_Verifikasi_Berkas",

		"Waktu_Update_Data",
		"Waktu_Simpan_Data",
		"Status"
	];

	// Konversi JSON ke array asosiatif
	$data = json_decode($jsonData, true);

	// Iterasi melalui data dan periksa apakah ada yang kosong
	foreach ($data as $key => $value) {
		if (!in_array($key, $excludedColumns) && (is_null($value) || $value === "")) {
			return true; // Ada data kosong
		}
	}

	return false; // Semua data diisi
}


function Cek_Data_Kosong_Pada_Verifikasi_Nilai_Rapor($jsonData)
{
	// Kolom yang dikecualikan
	$excludedColumns = [
		"Id_Pendaftar_Nilai_Rapor",
		"Id_Pendaftar",

		"Status_Verifikasi_Nilai_Rapor",

		"Waktu_Update_Data",
		"Waktu_Simpan_Data",
		"Status"
	];

	// Konversi JSON ke array asosiatif
	$data = json_decode($jsonData, true);

	// Iterasi melalui data dan periksa apakah ada yang kosong
	foreach ($data as $key => $value) {
		if (!in_array($key, $excludedColumns) && (is_null($value) || $value === "")) {
			return true; // Ada data kosong
		}
	}

	return false; // Semua data diisi
}

function Cek_Data_Kosong_Pada_Verifikasi_Program_Layanan($jsonData)
{
	// Kolom yang dikecualikan
	$excludedColumns = [
		"Id_Pendaftar_Program_Layanan",
		"Id_Pendaftar",

		"Status_Verifikasi_Program_Layanan",

		"Waktu_Update_Data",
		"Waktu_Simpan_Data",
		"Status"
	];

	// Konversi JSON ke array asosiatif
	$data = json_decode($jsonData, true);

	// Iterasi melalui data dan periksa apakah ada yang kosong
	foreach ($data as $key => $value) {
		if (!in_array($key, $excludedColumns) && (is_null($value) || $value === "")) {
			return true; // Ada data kosong
		}
	}

	return false; // Semua data diisi
}

#-----------------------------------------------------------------------------------
#FUNGSI BACA DATA PEMBAYARAN PPDB YANG SUDAH ADA
if ((isset($edit_data_pendaftar_pembayaran_ppdb['JSON_Response_Pembayaran_Xendit'])) and ($edit_data_pendaftar_pembayaran_ppdb['JSON_Response_Pembayaran_Xendit'] <> "")  and ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] <> "Sudah Diverifikasi")) {
	$data_pembayaran_xendit = json_decode($edit_data_pendaftar_pembayaran_ppdb['JSON_Response_Pembayaran_Xendit'], TRUE);
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.xendit.co/v2/invoices/' . $data_pembayaran_xendit['id'],
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'Authorization: ' . $Auth_XENDIT
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$edit_pembayaran_xendit = json_decode($response, TRUE);

	if (
		((isset($edit_pembayaran_xendit['status'])) and (isset($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB']))) and
		($edit_pembayaran_xendit['status'] == "SETTLED") and ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] <> "Sudah Diverifikasi")
	) {
		#UPDATE OTOMATIS SUDAH DIVERIFIKASI
		$form_field = array("Status_Verifikasi_Pembayaran_PPDB", "JSON_Response_Pembayaran_Xendit");
		$form_value = array("Sudah Diverifikasi", "$response");

		$form_field_where = array("Id_Pendaftar_Pembayaran_PPDB");
		$form_criteria_where = array("=");
		$form_value_where = array("$edit_data_pendaftar_pembayaran_ppdb[Id_Pendaftar_Pembayaran_PPDB]");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembayaran_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		if ($result['Status'] == "Sukses") {
			Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_pembayaran_ppdb", $edit_data_pendaftar_pembayaran_ppdb['Id_Pendaftar_Pembayaran_PPDB'], "Pembayaran PPDB", "Telah Berhasil Melakukan Pembayaran Via Xendit");
		}


		$edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] = "Sudah Diverifikasi";
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI BACA DATA PEMBAYARAN PEMBELIAN FORMULIR YANG SUDAH ADA
if ((isset($edit_data_pendaftar_pembelian_formulir['JSON_Response_Pembayaran_Xendit'])) and ($edit_data_pendaftar_pembelian_formulir['JSON_Response_Pembayaran_Xendit'] <> "") and ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] <> "Sudah Diverifikasi")) {
	$data_pembayaran_xendit = json_decode($edit_data_pendaftar_pembelian_formulir['JSON_Response_Pembayaran_Xendit'], TRUE);

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.xendit.co/v2/invoices/' . $data_pembayaran_xendit['id'],
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'Authorization: ' . $Auth_XENDIT
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$edit_pembayaran_xendit = json_decode($response, TRUE);

	if (
		((isset($edit_pembayaran_xendit['status'])) and (isset($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir']))) and
		($edit_pembayaran_xendit['status'] == "SETTLED") and ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] <> "Sudah Diverifikasi")
	) {
		#UPDATE OTOMATIS SUDAH DIVERIFIKASI
		$form_field = array("Status_Verifikasi_Pembelian_Formulir", "JSON_Response_Pembayaran_Xendit");
		$form_value = array("Sudah Diverifikasi", "$response");

		$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
		$form_criteria_where = array("=");
		$form_value_where = array("$edit_data_pendaftar_pembelian_formulir[Id_Pendaftar_Pembelian_Formulir]");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		if ($result['Status'] == "Sukses") {
			Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_pembelian_formulir", $edit_data_pendaftar_pembelian_formulir['Id_Pendaftar_Pembelian_Formulir'], "Pembelian Formulir", "Telah Berhasil Melakukan Pembayaran Via Xendit");
		}


		$edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] = "Sudah Diverifikasi";
	}
}


#-----------------------------------------------------------------------------------
#FUNGSI STATUS MASING-MASING STEP
//STATUS PEMBELIAN FORMULIR
if ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Belum Diverifikasi") {
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
} elseif ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi") {
	$Keterangan_Pembelian_Formulir = "Sudah Bayar";
} elseif ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sedang Diverifikasi") {
	$Keterangan_Pembelian_Formulir = "Sedang Diverifikasi";
} elseif ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Verifikasi Ditolak") {
	$Keterangan_Pembelian_Formulir = "Pembayaran Gagal, Silahkan Lakukan Pembayaran Ulang";
} else {
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}

//STATUS VERIFIKASI DATA DIRI
// echo json_encode($edit_data_pendaftar);
if ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Sudah Diverifikasi") {
	$Keterangan_Verifikasi_Data_Diri = "Sudah Lengkap dan Sudah Diverifikasi";
} elseif (Cek_Data_Kosong_Pada_Verifikasi_Data_Diri(json_encode($edit_data_pendaftar))) {
	$Keterangan_Verifikasi_Data_Diri = "Belum Lengkap";
} else {
	if ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Belum Diverifikasi") {
		$Keterangan_Verifikasi_Data_Diri = "Sudah Lengkap tetapi Belum Diverifikasi";
	} elseif ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Sudah Diverifikasi") {
		$Keterangan_Verifikasi_Data_Diri = "Sudah Lengkap dan Sudah Diverifikasi";
	} elseif ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Menunggu Verifikasi") {
		$Keterangan_Verifikasi_Data_Diri = "Sudah Lengkap dan Menunggu Verifikasi";
	} elseif ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Sedang Diverifikasi") {
		$Keterangan_Verifikasi_Data_Diri = "Sudah Lengkap dan Sedang Diverifikasi";
	} elseif ($edit_data_pendaftar['Status_Verifikasi_Data_Diri'] == "Verifikasi Ditolak") {
		$Keterangan_Verifikasi_Data_Diri = "Sudah Lengkap namun Verifikasi Ditolak";
	} else {
		$Keterangan_Verifikasi_Data_Diri = "Belum Lengkap";
	}
}


//STATUS VERIFIKASI BERKAS
// echo json_encode($edit_data_pendaftar_verifikasi_berkas);
if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Sudah Diverifikasi") {
	$Keterangan_Verifikasi_Berkas = "Sudah Lengkap dan Sudah Diverifikasi";
} elseif (Cek_Data_Kosong_Pada_Verifikasi_Berkas(json_encode($edit_data_pendaftar_verifikasi_berkas))) {
	$Keterangan_Verifikasi_Berkas = "Belum Lengkap";
} else {
	if ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Belum Diverifikasi") {
		$Keterangan_Verifikasi_Berkas = "Sudah Lengkap tetapi Belum Diverifikasi";
	} elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Sudah Diverifikasi") {
		$Keterangan_Verifikasi_Berkas = "Sudah Lengkap dan Sudah Diverifikasi";
	} elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Menunggu Verifikasi") {
		$Keterangan_Verifikasi_Berkas = "Sudah Lengkap dan Menunggu Verifikasi";
	} elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Sedang Diverifikasi") {
		$Keterangan_Verifikasi_Berkas = "Sudah Lengkap dan Sedang Diverifikasi";
	} elseif ($edit_data_pendaftar_verifikasi_berkas['Status_Verifikasi_Berkas'] == "Verifikasi Ditolak") {
		$Keterangan_Verifikasi_Berkas = "Sudah Lengkap namun Verifikasi Ditolak";
	} else {
		$Keterangan_Verifikasi_Berkas = "Belum Lengkap";
	}
}

//STATUS PEMBAYARAN PPDB
if ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Belum Diverifikasi") {
	$Keterangan_Pembayaran_PPDB = "Belum Bayar";
} elseif ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Sudah Diverifikasi") {
	$Keterangan_Pembayaran_PPDB = "Sudah Bayar";
} elseif ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Sedang Diverifikasi") {
	$Keterangan_Pembayaran_PPDB = "Sedang Diverifikasi";
} elseif ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] == "Verifikasi Ditolak") {
	$Keterangan_Pembayaran_PPDB = "Pembayaran Gagal, Silahkan Lakukan Pembayaran Ulang";
} else {
	$Keterangan_Pembayaran_PPDB = "Belum Bayar";
}

//STATUS ISI RAPOR
// echo json_encode($edit_data_pendaftar_nilai_rapor);
if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
	$Keterangan_Nilai_Rapor = "Sudah Lengkap dan Sudah Diverifikasi";
} elseif (Cek_Data_Kosong_Pada_Verifikasi_Nilai_Rapor(json_encode($edit_data_pendaftar_nilai_rapor))) {
	$Keterangan_Nilai_Rapor = "Belum Lengkap";
} else {
	if ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Belum Diverifikasi") {
		$Keterangan_Nilai_Rapor = "Sudah Lengkap tetapi Belum Diverifikasi";
	} elseif ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sudah Diverifikasi") {
		$Keterangan_Nilai_Rapor = "Sudah Lengkap dan Sudah Diverifikasi";
	} elseif ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Menunggu Verifikasi") {
		$Keterangan_Nilai_Rapor = "Sudah Lengkap dan Menunggu Verifikasi";
	} elseif ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Sedang Diverifikasi") {
		$Keterangan_Nilai_Rapor = "Sudah Lengkap dan Sedang Diverifikasi";
	} elseif ($edit_data_pendaftar_nilai_rapor['Status_Verifikasi_Nilai_Rapor'] == "Verifikasi Ditolak") {
		$Keterangan_Nilai_Rapor = "Sudah Lengkap namun Verifikasi Ditolak";
	} else {
		$Keterangan_Nilai_Rapor = "Belum Lengkap";
	}
}



//STATUS PROGRAM LAYANAN
// echo json_encode($edit_data_pendaftar_program_layanan);
if ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Sudah Diverifikasi") {
	$Keterangan_Pilih_Layanan = "Sudah Lengkap dan Sudah Diverifikasi";
} elseif (Cek_Data_Kosong_Pada_Verifikasi_Program_Layanan(json_encode($edit_data_pendaftar_program_layanan))) {
	$Keterangan_Pilih_Layanan = "Belum Lengkap";
} else {
	if ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Belum Diverifikasi") {
		$Keterangan_Pilih_Layanan = "Sudah Lengkap tetapi Belum Diverifikasi";
	} elseif ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Sudah Diverifikasi") {
		$Keterangan_Pilih_Layanan = "Sudah Lengkap dan Sudah Diverifikasi";
	} elseif ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Menunggu Verifikasi") {
		$Keterangan_Pilih_Layanan = "Sudah Lengkap dan Menunggu Verifikasi";
	} elseif ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Sedang Diverifikasi") {
		$Keterangan_Pilih_Layanan = "Sudah Lengkap dan Sedang Diverifikasi";
	} elseif ($edit_data_pendaftar_program_layanan['Status_Verifikasi_Program_Layanan'] == "Verifikasi Ditolak") {
		$Keterangan_Pilih_Layanan = "Sudah Lengkap namun Verifikasi Ditolak";
	} else {
		$Keterangan_Pilih_Layanan = "Belum Lengkap";
	}
}
#-----------------------------------------------------------------------------------