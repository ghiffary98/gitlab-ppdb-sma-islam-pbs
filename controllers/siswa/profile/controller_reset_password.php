<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar = new a_data_pendaftar();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}
#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
//FUNGSI AMBIL DATA PENDAFTAR
$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$u_data_user[Id_Pendaftar]");

if ($result['Status'] == "Sukses") {
    $edit_data_pendaftar = $result['Hasil'];
} else {
    $edit_data_pendaftar = null;
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	$Password_Lama = $a_hash_password->hash_password($_POST['Password_Lama']);
	if($Password_Lama <> $edit_data_pendaftar['Password']){
		echo "<script>alert('Password Lama Tidak Cocok !');document.location.href='$Link_Sekarang'</script>";
		exit();
	}

	$_POST['Password_Pendaftar'] = $a_hash->encode($_POST["Password_Baru"]);

	$Password_Baru = $a_hash_password->hash_password($_POST['Password_Baru']);
	$Konfirmasi_Password_Baru = $a_hash_password->hash_password($_POST['Konfirmasi_Password_Baru']);

	if($Konfirmasi_Password_Baru <> $Password_Baru){
		echo "<script>alert('Password Baru dan Konfirmasi Password Baru Tidak Sama !');document.location.href='$Link_Sekarang'</script>";
		exit();
	}

	$form_field = array("Password", "Password_Pendaftar", "Waktu_Update_Data");

	$form_value = array("$Password_Baru", "$_POST[Password_Pendaftar]", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar");
	$form_criteria_where = array("=");
	$form_value_where = array("$u_data_user[Id_Pendaftar]");
	$form_connector_where = array("");

	$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {
		echo "<script>alert('Password Berhasil Diubah, Silahkan Login Kembali');document.location.href='login.php'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengubah Password');document.location.href='$Link_Sekarang'</script>";
	}

}
#-----------------------------------------------------------------------------------
?>