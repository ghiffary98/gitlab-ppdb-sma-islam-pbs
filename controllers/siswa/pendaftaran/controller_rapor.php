<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar_nilai_rapor = new a_data_pendaftar_nilai_rapor();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN

//FUNGSI AMBIL DATA PEMBELIAN FORMULIR
$search_field_where = array("Status","Id_Pendaftar");
$search_criteria_where = array("=","=");
$search_value_where = array("Aktif","$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND","");

$result = $a_data_pendaftar_pembelian_formulir->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
    $edit_data_pendaftar_pembelian_formulir = $result['Hasil'][0];
} else {
    $edit_data_pendaftar_pembelian_formulir = null;
}

//STATUS PEMBELIAN FORMULIR
if($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Belum Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Sudah Bayar";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sedang Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Sedang Diverifikasi";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Verifikasi Ditolak"){
	$Keterangan_Pembelian_Formulir = "Pembayaran Gagal, Silahkan Lakukan Pembayaran Ulang";
}else{
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
//FUNGSI AMBIL DATA PENDAFTAR
$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$u_data_user[Id_Pendaftar]");

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar = $result['Hasil'];
} else {
	$edit_data_pendaftar = null;
}

//FUNGSI AMBIL DATA PPDB
$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

if ($result['Status'] == "Sukses") {
	$edit_data_ppdb = $result['Hasil'];
} else {
	$edit_data_ppdb = null;
}

//FUNGSI AMBIL DATA NILAI RAPOR
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "");

$result = $a_data_pendaftar_nilai_rapor->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar_nilai_rapor = $result['Hasil'][0];
} else {
	$edit_data_pendaftar_nilai_rapor = null;
}

if (!isset($edit_data_pendaftar_nilai_rapor)) {
	//JIKA TIDAK ADA DATA SAMA SEKALI, MAKA OTOMATIS CREATE DATA
	$form_field = array("Id_Pendaftar_Nilai_Rapor", "Id_Pendaftar", "Status_Verifikasi_Nilai_Rapor", "Waktu_Simpan_Data", "Status");
	$form_value = array(NULL, "$u_data_user[Id_Pendaftar]", "Belum Diverifikasi", "$Waktu_Sekarang", "Aktif");
	$result = $a_data_pendaftar_nilai_rapor->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		echo "<script>document.location.href='$Link_Sekarang'</script>";
		exit();
	}
}

if($edit_data_pendaftar['Status_Kelulusan'] == "Gugur"){
	echo "<script>alert('Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir');document.location.href='dashboard.php?menu=pengumuman_kelulusan'</script>";
	exit();
}

if($Keterangan_Pembelian_Formulir == "Belum Bayar"){
	echo "<script>alert('Harap Lakukan Pembelian Formulir Terlebih Dahulu !');document.location.href='dashboard.php'</script>";
	exit();
}
#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {

	$form_field = array("Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti","Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti","Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti","Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti","Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan","Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan","Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan","Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan","Nilai_Semester_1_Bahasa_Indonesia","Nilai_Semester_2_Bahasa_Indonesia","Nilai_Semester_3_Bahasa_Indonesia","Nilai_Semester_4_Bahasa_Indonesia","Nilai_Semester_1_Bahasa_Inggris","Nilai_Semester_2_Bahasa_Inggris","Nilai_Semester_3_Bahasa_Inggris","Nilai_Semester_4_Bahasa_Inggris","Nilai_Semester_1_Matematika","Nilai_Semester_2_Matematika","Nilai_Semester_3_Matematika","Nilai_Semester_4_Matematika","Nilai_Semester_1_IPA","Nilai_Semester_2_IPA","Nilai_Semester_3_IPA","Nilai_Semester_4_IPA","Nilai_Semester_1_IPS","Nilai_Semester_2_IPS","Nilai_Semester_3_IPS","Nilai_Semester_4_IPS","Nilai_Semester_1_Seni_Dan_Budaya","Nilai_Semester_2_Seni_Dan_Budaya","Nilai_Semester_3_Seni_Dan_Budaya","Nilai_Semester_4_Seni_Dan_Budaya","Nilai_Semester_1_PJOK","Nilai_Semester_2_PJOK","Nilai_Semester_3_PJOK","Nilai_Semester_4_PJOK","Nilai_Semester_1_Prakarya","Nilai_Semester_2_Prakarya","Nilai_Semester_3_Prakarya","Nilai_Semester_4_Prakarya","Nilai_SKL_Ijazah","Status_Verifikasi_Nilai_Rapor","Waktu_Update_Data");

	$form_value = array("$_POST[Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti]","$_POST[Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti]","$_POST[Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti]","$_POST[Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti]","$_POST[Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan]","$_POST[Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan]","$_POST[Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan]","$_POST[Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan]","$_POST[Nilai_Semester_1_Bahasa_Indonesia]","$_POST[Nilai_Semester_2_Bahasa_Indonesia]","$_POST[Nilai_Semester_3_Bahasa_Indonesia]","$_POST[Nilai_Semester_4_Bahasa_Indonesia]","$_POST[Nilai_Semester_1_Bahasa_Inggris]","$_POST[Nilai_Semester_2_Bahasa_Inggris]","$_POST[Nilai_Semester_3_Bahasa_Inggris]","$_POST[Nilai_Semester_4_Bahasa_Inggris]","$_POST[Nilai_Semester_1_Matematika]","$_POST[Nilai_Semester_2_Matematika]","$_POST[Nilai_Semester_3_Matematika]","$_POST[Nilai_Semester_4_Matematika]","$_POST[Nilai_Semester_1_IPA]","$_POST[Nilai_Semester_2_IPA]","$_POST[Nilai_Semester_3_IPA]","$_POST[Nilai_Semester_4_IPA]","$_POST[Nilai_Semester_1_IPS]","$_POST[Nilai_Semester_2_IPS]","$_POST[Nilai_Semester_3_IPS]","$_POST[Nilai_Semester_4_IPS]","$_POST[Nilai_Semester_1_Seni_Dan_Budaya]","$_POST[Nilai_Semester_2_Seni_Dan_Budaya]","$_POST[Nilai_Semester_3_Seni_Dan_Budaya]","$_POST[Nilai_Semester_4_Seni_Dan_Budaya]","$_POST[Nilai_Semester_1_PJOK]","$_POST[Nilai_Semester_2_PJOK]","$_POST[Nilai_Semester_3_PJOK]","$_POST[Nilai_Semester_4_PJOK]","$_POST[Nilai_Semester_1_Prakarya]","$_POST[Nilai_Semester_2_Prakarya]","$_POST[Nilai_Semester_3_Prakarya]","$_POST[Nilai_Semester_4_Prakarya]","$_POST[Nilai_SKL_Ijazah]","Menunggu Verifikasi","$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar");
	$form_criteria_where = array("=");
	$form_value_where = array("$u_data_user[Id_Pendaftar]");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_nilai_rapor->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";

		Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_nilai_rapor", $edit_data_pendaftar_nilai_rapor['Id_Pendaftar_Nilai_Rapor'], "Nilai Rapor", "Telah Mengupdate Nilai Rapor");
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

?>