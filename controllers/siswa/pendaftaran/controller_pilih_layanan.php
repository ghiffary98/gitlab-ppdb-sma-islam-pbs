<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar_program_layanan = new a_data_pendaftar_program_layanan();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN

//FUNGSI AMBIL DATA PEMBELIAN FORMULIR
$search_field_where = array("Status","Id_Pendaftar");
$search_criteria_where = array("=","=");
$search_value_where = array("Aktif","$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND","");

$result = $a_data_pendaftar_pembelian_formulir->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
    $edit_data_pendaftar_pembelian_formulir = $result['Hasil'][0];
} else {
    $edit_data_pendaftar_pembelian_formulir = null;
}

//STATUS PEMBELIAN FORMULIR
if($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Belum Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Sudah Bayar";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sedang Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Sedang Diverifikasi";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Verifikasi Ditolak"){
	$Keterangan_Pembelian_Formulir = "Pembayaran Gagal, Silahkan Lakukan Pembayaran Ulang";
}else{
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
//FUNGSI AMBIL DATA PENDAFTAR
$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$u_data_user[Id_Pendaftar]");

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar = $result['Hasil'];
} else {
	$edit_data_pendaftar = null;
}

//FUNGSI AMBIL DATA PPDB
$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

if ($result['Status'] == "Sukses") {
	$edit_data_ppdb = $result['Hasil'];
} else {
	$edit_data_ppdb = null;
}

//FUNGSI AMBIL DATA NILAI RAPOR
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "");

$result = $a_data_pendaftar_program_layanan->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar_program_layanan = $result['Hasil'][0];
} else {
	$edit_data_pendaftar_program_layanan = null;
}

if (!isset($edit_data_pendaftar_program_layanan)) {
	//JIKA TIDAK ADA DATA SAMA SEKALI, MAKA OTOMATIS CREATE DATA
	$form_field = array("Id_Pendaftar_Program_Layanan", "Id_Pendaftar", "Status_Verifikasi_Program_Layanan", "Waktu_Simpan_Data", "Status");
	$form_value = array(NULL, "$u_data_user[Id_Pendaftar]", "Belum Diverifikasi", "$Waktu_Sekarang", "Aktif");
	$result = $a_data_pendaftar_program_layanan->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		echo "<script>document.location.href='$Link_Sekarang'</script>";
		exit();
	}
}

if($edit_data_pendaftar['Status_Kelulusan'] == "Gugur"){
	echo "<script>alert('Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir');document.location.href='dashboard.php?menu=pengumuman_kelulusan'</script>";
	exit();
}

if($Keterangan_Pembelian_Formulir == "Belum Bayar"){
	echo "<script>alert('Harap Lakukan Pembelian Formulir Terlebih Dahulu !');document.location.href='dashboard.php'</script>";
	exit();
}
#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {

	$form_field = array("Program","Layanan_1","Layanan_2","Layanan_3","Layanan_4","Status_Verifikasi_Program_Layanan","Waktu_Update_Data");

	$form_value = array("$_POST[Program]","$_POST[Layanan_1]","$_POST[Layanan_2]","$_POST[Layanan_3]","$_POST[Layanan_4]","Menunggu Verifikasi","$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar");
	$form_criteria_where = array("=");
	$form_value_where = array("$u_data_user[Id_Pendaftar]");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_program_layanan->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";

		Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_program_layanan", $edit_data_pendaftar_program_layanan['Id_Pendaftar_Program_Layanan'], "Program Layanan", "Telah Mengupdate Program Layanan");
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

?>