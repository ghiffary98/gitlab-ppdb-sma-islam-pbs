<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar_verifikasi_berkas = new a_data_pendaftar_verifikasi_berkas();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
$folder_penyimpanan_file_berkas_akte_kelahiran = "media/berkas/akte_kelahiran/";
$folder_penyimpanan_file_berkas_kartu_keluarga = "media/berkas/kartu_keluarga/";
$folder_penyimpanan_file_berkas_rapor = "media/berkas/rapor/";
$folder_penyimpanan_file_berkas_kartu_nisn = "media/berkas/kartu_nisn/";
$folder_penyimpanan_file_berkas_skl_ijazah = "media/berkas/skl_ijazah/";

//FUNGSI AMBIL DATA PEMBELIAN FORMULIR
$search_field_where = array("Status","Id_Pendaftar");
$search_criteria_where = array("=","=");
$search_value_where = array("Aktif","$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND","");

$result = $a_data_pendaftar_pembelian_formulir->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
    $edit_data_pendaftar_pembelian_formulir = $result['Hasil'][0];
} else {
    $edit_data_pendaftar_pembelian_formulir = null;
}

//STATUS PEMBELIAN FORMULIR
if($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Belum Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Sudah Bayar";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sedang Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Sedang Diverifikasi";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Verifikasi Ditolak"){
	$Keterangan_Pembelian_Formulir = "Pembayaran Gagal, Silahkan Lakukan Pembayaran Ulang";
}else{
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
//FUNGSI AMBIL DATA PENDAFTAR
$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$u_data_user[Id_Pendaftar]");

if ($result['Status'] == "Sukses") {
    $edit_data_pendaftar = $result['Hasil'];
} else {
    $edit_data_pendaftar = null;
}

//FUNGSI AMBIL DATA PPDB
$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

if ($result['Status'] == "Sukses") {
    $edit_data_ppdb = $result['Hasil'];
} else {
    $edit_data_ppdb = null;
}

//FUNGSI AMBIL DATA PEMBELIAN FORMULIR
$search_field_where = array("Status","Id_Pendaftar");
$search_criteria_where = array("=","=");
$search_value_where = array("Aktif","$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND","");

$result = $a_data_pendaftar_verifikasi_berkas->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
    $edit_data_pendaftar_verifikasi_berkas = $result['Hasil'][0];
} else {
    $edit_data_pendaftar_verifikasi_berkas = null;
}

if(!isset($edit_data_pendaftar_verifikasi_berkas)){
    //JIKA TIDAK ADA DATA SAMA SEKALI, MAKA OTOMATIS CREATE DATA
    $form_field = array("Id_Pendaftar_Verifikasi_Berkas", "Id_Pendaftar", "Status_Verifikasi_Berkas", "Waktu_Simpan_Data", "Status");
	$form_value = array(NULL, "$u_data_user[Id_Pendaftar]", "Belum Diverifikasi", "$Waktu_Sekarang", "Aktif");
	$result = $a_data_pendaftar_verifikasi_berkas->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
        echo "<script>document.location.href='$Link_Sekarang'</script>";
        exit();
    }
}

if($edit_data_pendaftar['Status_Kelulusan'] == "Gugur"){
	echo "<script>alert('Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir');document.location.href='dashboard.php?menu=pengumuman_kelulusan'</script>";
	exit();
}

if($Keterangan_Pembelian_Formulir == "Belum Bayar"){
	echo "<script>alert('Harap Lakukan Pembelian Formulir Terlebih Dahulu !');document.location.href='dashboard.php'</script>";
	exit();
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	$form_field = array("Waktu_Update_Data");
	$form_value = array("$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$edit_data_pendaftar_verifikasi_berkas[Id_Pendaftar_Verifikasi_Berkas]");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {
		//FUNGSI UPLOAD FILE Akta_Kelahiran
        if(isset($_FILES['Akta_Kelahiran'])){
            if ($_FILES['Akta_Kelahiran']['size'] <> 0 && $_FILES['Akta_Kelahiran']['error'] == 0) {
                $post_file_upload = $_FILES['Akta_Kelahiran'];
                $path_file_upload = $_FILES['Akta_Kelahiran']['name'];
                $ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
                $ukuran_file_upload = $_FILES['Akta_Kelahiran']['size'];
                $nama_file_upload = "berkas_akte_kelahiran_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'];
                $folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_akte_kelahiran;
                $tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
                $maksimum_ukuran_file_upload = 100000000;

                $result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

                if ($result_upload_file['Status'] == "Sukses") {
                    Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_verifikasi_berkas", $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'], "Verifikasi Berkas", "Telah Mengupload / Mengupdate Berkas Akta Kelahiran");

                    $form_field = array("Akta_Kelahiran","Status_Verifikasi_Akta_Kelahiran");
                    $form_value = array("$nama_file_upload.$ext_file_upload","Sedang Diverifikasi");
                    $form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
                    $form_criteria_where = array("=");
                    $form_value_where = array("$edit_data_pendaftar_verifikasi_berkas[Id_Pendaftar_Verifikasi_Berkas]");
                    $form_connector_where = array("");

                    $result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
                } else {
                }
            }
        }
		//FUNGSI UPLOAD FILE Akta_Kelahiran

		//FUNGSI UPLOAD FILE Kartu_Keluarga
        if(isset($_FILES['Kartu_Keluarga'])){
            if ($_FILES['Kartu_Keluarga']['size'] <> 0 && $_FILES['Kartu_Keluarga']['error'] == 0) {
                $post_file_upload = $_FILES['Kartu_Keluarga'];
                $path_file_upload = $_FILES['Kartu_Keluarga']['name'];
                $ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
                $ukuran_file_upload = $_FILES['Kartu_Keluarga']['size'];
                $nama_file_upload = "berkas_kartu_keluarga_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'];
                $folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_kartu_keluarga;
                $tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
                $maksimum_ukuran_file_upload = 100000000;

                $result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

                if ($result_upload_file['Status'] == "Sukses") {
                    Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_verifikasi_berkas", $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'], "Verifikasi Berkas", "Telah Mengupload / Mengupdate Berkas Kartu Keluarga");

                    $form_field = array("Kartu_Keluarga","Status_Verifikasi_Kartu_Keluarga");
                    $form_value = array("$nama_file_upload.$ext_file_upload","Sedang Diverifikasi");
                    $form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
                    $form_criteria_where = array("=");
                    $form_value_where = array("$edit_data_pendaftar_verifikasi_berkas[Id_Pendaftar_Verifikasi_Berkas]");
                    $form_connector_where = array("");

                    $result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
                } else {
                }
            }
        }
		//FUNGSI UPLOAD FILE Kartu_Keluarga

		//FUNGSI UPLOAD FILE Rapor
        if(isset($_FILES['Rapor'])){
            if ($_FILES['Rapor']['size'] <> 0 && $_FILES['Rapor']['error'] == 0) {
                $post_file_upload = $_FILES['Rapor'];
                $path_file_upload = $_FILES['Rapor']['name'];
                $ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
                $ukuran_file_upload = $_FILES['Rapor']['size'];
                $nama_file_upload = "berkas_rapor_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'];
                $folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_rapor;
                $tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
                $maksimum_ukuran_file_upload = 100000000;

                $result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

                if ($result_upload_file['Status'] == "Sukses") {
                    Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_verifikasi_berkas", $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'], "Verifikasi Berkas", "Telah Mengupload / Mengupdate Berkas Rapor");

                    $form_field = array("Rapor","Status_Verifikasi_Rapor");
                    $form_value = array("$nama_file_upload.$ext_file_upload","Sedang Diverifikasi");
                    $form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
                    $form_criteria_where = array("=");
                    $form_value_where = array("$edit_data_pendaftar_verifikasi_berkas[Id_Pendaftar_Verifikasi_Berkas]");
                    $form_connector_where = array("");

                    $result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
                } else {
                }
            }
        }
		//FUNGSI UPLOAD FILE Rapor

		//FUNGSI UPLOAD FILE Kartu_NISN
        if(isset($_FILES['Kartu_NISN'])){    
            if ($_FILES['Kartu_NISN']['size'] <> 0 && $_FILES['Kartu_NISN']['error'] == 0) {
                $post_file_upload = $_FILES['Kartu_NISN'];
                $path_file_upload = $_FILES['Kartu_NISN']['name'];
                $ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
                $ukuran_file_upload = $_FILES['Kartu_NISN']['size'];
                $nama_file_upload = "berkas_kartu_nisn_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'];
                $folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_kartu_nisn;
                $tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
                $maksimum_ukuran_file_upload = 100000000;

                $result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

                if ($result_upload_file['Status'] == "Sukses") {
                    Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_verifikasi_berkas", $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'], "Verifikasi Berkas", "Telah Mengupload / Mengupdate Berkas Kartu NISN");

                    $form_field = array("Kartu_NISN","Status_Verifikasi_Kartu_NISN");
                    $form_value = array("$nama_file_upload.$ext_file_upload","Sedang Diverifikasi");
                    $form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
                    $form_criteria_where = array("=");
                    $form_value_where = array("$edit_data_pendaftar_verifikasi_berkas[Id_Pendaftar_Verifikasi_Berkas]");
                    $form_connector_where = array("");

                    $result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
                } else {
                }
            }
        }
		//FUNGSI UPLOAD FILE Kartu_NISN

		//FUNGSI UPLOAD FILE SKL_Ijazah
        if(isset($_FILES['SKL_Ijazah'])){    
            if ($_FILES['SKL_Ijazah']['size'] <> 0 && $_FILES['SKL_Ijazah']['error'] == 0) {
                $post_file_upload = $_FILES['SKL_Ijazah'];
                $path_file_upload = $_FILES['SKL_Ijazah']['name'];
                $ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
                $ukuran_file_upload = $_FILES['SKL_Ijazah']['size'];
                $nama_file_upload = "berkas_skl_ijazah_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'];
                $folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_skl_ijazah;
                $tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
                $maksimum_ukuran_file_upload = 100000000;

                $result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

                if ($result_upload_file['Status'] == "Sukses") {
                    Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_verifikasi_berkas", $edit_data_pendaftar_verifikasi_berkas['Id_Pendaftar_Verifikasi_Berkas'], "Verifikasi Berkas", "Telah Mengupload / Mengupdate Berkas SKL / Ijazah");

                    $form_field = array("SKL_Ijazah","Status_Verifikasi_SKL_Ijazah");
                    $form_value = array("$nama_file_upload.$ext_file_upload","Sedang Diverifikasi");
                    $form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
                    $form_criteria_where = array("=");
                    $form_value_where = array("$edit_data_pendaftar_verifikasi_berkas[Id_Pendaftar_Verifikasi_Berkas]");
                    $form_connector_where = array("");

                    $result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
                } else {
                }
            }
        }
		//FUNGSI UPLOAD FILE SKL_Ijazah				
		echo "<script>alert('File Berhasil Terupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------
?>