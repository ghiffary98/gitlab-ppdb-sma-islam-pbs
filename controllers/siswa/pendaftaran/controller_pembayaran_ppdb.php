<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar_pembayaran_ppdb = new a_data_pendaftar_pembayaran_ppdb();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
$folder_penyimpanan_file_bukti_pembayaran_ppdb = "media/bukti_pembayaran_ppdb/";

//FUNGSI AMBIL DATA PEMBELIAN FORMULIR
$search_field_where = array("Status","Id_Pendaftar");
$search_criteria_where = array("=","=");
$search_value_where = array("Aktif","$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND","");

$result = $a_data_pendaftar_pembelian_formulir->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
    $edit_data_pendaftar_pembelian_formulir = $result['Hasil'][0];
} else {
    $edit_data_pendaftar_pembelian_formulir = null;
}

//STATUS PEMBELIAN FORMULIR
if($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Belum Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Sudah Bayar";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sedang Diverifikasi"){
	$Keterangan_Pembelian_Formulir = "Sedang Diverifikasi";
}elseif($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Verifikasi Ditolak"){
	$Keterangan_Pembelian_Formulir = "Pembayaran Gagal, Silahkan Lakukan Pembayaran Ulang";
}else{
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
//FUNGSI AMBIL DATA PENDAFTAR
$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$u_data_user[Id_Pendaftar]");

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar = $result['Hasil'];
} else {
	$edit_data_pendaftar = null;
}

//FUNGSI AMBIL DATA PPDB
$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

if ($result['Status'] == "Sukses") {
	$edit_data_ppdb = $result['Hasil'];
} else {
	$edit_data_ppdb = null;
}

//FUNGSI AMBIL DATA PEMBAYARAN PPDB
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "");

$result = $a_data_pendaftar_pembayaran_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar_pembayaran_ppdb = $result['Hasil'][0];
} else {
	$edit_data_pendaftar_pembayaran_ppdb = null;
}

if (!isset($edit_data_pendaftar_pembayaran_ppdb)) {
	//JIKA TIDAK ADA DATA SAMA SEKALI, MAKA OTOMATIS CREATE DATA
	$form_field = array("Id_Pendaftar_Pembayaran_PPDB", "Id_Pendaftar", "Status_Verifikasi_Pembayaran_PPDB", "Waktu_Simpan_Data", "Status");
	$form_value = array(NULL, "$u_data_user[Id_Pendaftar]", "Belum Diverifikasi", "$Waktu_Sekarang", "Aktif");
	$result = $a_data_pendaftar_pembayaran_ppdb->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		echo "<script>document.location.href='$Link_Sekarang'</script>";
		exit();
	}
}

if($edit_data_pendaftar['Status_Kelulusan'] == "Gugur"){
	echo "<script>alert('Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir');document.location.href='dashboard.php?menu=pengumuman_kelulusan'</script>";
	exit();
}

if($Keterangan_Pembelian_Formulir == "Belum Bayar"){
	echo "<script>alert('Harap Lakukan Pembelian Formulir Terlebih Dahulu !');document.location.href='dashboard.php'</script>";
	exit();
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI BACA DATA PEMBAYARAN YANG SUDAH ADA
if ((isset($edit_data_pendaftar_pembayaran_ppdb['JSON_Response_Pembayaran_Xendit'])) AND ($edit_data_pendaftar_pembayaran_ppdb['JSON_Response_Pembayaran_Xendit'] <> "")) {
	$data_pembayaran_xendit = json_decode($edit_data_pendaftar_pembayaran_ppdb['JSON_Response_Pembayaran_Xendit'], TRUE);

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.xendit.co/v2/invoices/' . $data_pembayaran_xendit['id'],
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'Authorization: ' . $Auth_XENDIT
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$edit_pembayaran_xendit = json_decode($response, TRUE);

	if (
		((isset($edit_pembayaran_xendit['status'])) AND (isset($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB']))) AND
		($edit_pembayaran_xendit['status'] == "SETTLED") AND ($edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] <> "Sudah Diverifikasi")
		) {
		#UPDATE OTOMATIS SUDAH DIVERIFIKASI
		$form_field = array("Status_Verifikasi_Pembayaran_PPDB", "JSON_Response_Pembayaran_Xendit");
		$form_value = array("Sudah Diverifikasi", "$response");

		$form_field_where = array("Id_Pendaftar_Pembayaran_PPDB");
		$form_criteria_where = array("=");
		$form_value_where = array("$edit_data_pendaftar_pembayaran_ppdb[Id_Pendaftar_Pembayaran_PPDB]");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembayaran_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		if ($result['Status'] == "Sukses") {
			Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_pembayaran_ppdb", $edit_data_pendaftar_pembayaran_ppdb['Id_Pendaftar_Pembayaran_PPDB'], "Pembayaran PPDB", "Telah Berhasil Melakukan Pembayaran Via Xendit");
		}


		$edit_data_pendaftar_pembayaran_ppdb['Status_Verifikasi_Pembayaran_PPDB'] = "Sudah Diverifikasi";
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI BAYAR
if (isset($_POST['submit_bayar'])) {

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.xendit.co/v2/invoices',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => '{
		"external_id": "Invoice-PPDB-' . $u_data_user['Nomor_Pendaftaran'] . '",
		"amount": ' . ($edit_data_ppdb['Biaya_PPDB'] + $edit_data_ppdb['Biaya_Admin_Xendit']) . ',
		"payer_email": "' . $u_data_user['Email'] . '",
		"description": "Pembayaran PPDB #' . $u_data_user['Nomor_Pendaftaran'] . '"
	}',
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'Authorization: ' . $Auth_XENDIT
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$return = json_decode($response, TRUE);

	if ((isset($return['invoice_url'])) and ($return['invoice_url'] <> "")) {
		$form_field = array("Waktu_Update_Data", "JSON_Response_Pembayaran_Xendit");
		$form_value = array("$Waktu_Sekarang", "$response");

		$form_field_where = array("Id_Pendaftar_Pembayaran_PPDB");
		$form_criteria_where = array("=");
		$form_value_where = array("$edit_data_pendaftar_pembayaran_ppdb[Id_Pendaftar_Pembayaran_PPDB]");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembayaran_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>document.location.href='" . $return['invoice_url'] . "'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Akan Membuat Pembayaran');document.location.href='$Link_Sekarang'</script>";
	}
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_upload_bukti'])) {

	$form_field = array("Waktu_Update_Data");
	$form_value = array("$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Pembayaran_PPDB");
	$form_criteria_where = array("=");
	$form_value_where = array("$edit_data_pendaftar_pembayaran_ppdb[Id_Pendaftar_Pembayaran_PPDB]");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_pembayaran_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		//FUNGSI UPLOAD FILE Bukti_Pembayaran_PPDB
		if ($_FILES['Bukti_Pembayaran_PPDB']['size'] <> 0 && $_FILES['Bukti_Pembayaran_PPDB']['error'] == 0) {
			$post_file_upload = $_FILES['Bukti_Pembayaran_PPDB'];
			$path_file_upload = $_FILES['Bukti_Pembayaran_PPDB']['name'];
			$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
			$ukuran_file_upload = $_FILES['Bukti_Pembayaran_PPDB']['size'];
			$nama_file_upload = "bukti_pembayaran_pembayaran_ppdb_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit_data_pendaftar_pembayaran_ppdb['Id_Pendaftar_Pembayaran_PPDB'];
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_bukti_pembayaran_ppdb;
			$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
			$maksimum_ukuran_file_upload = 100000000;

			$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

			if ($result_upload_file['Status'] == "Sukses") {


				// UPDATE STATUS => MENUNGGU VERIFIKASI
				$form_field = array("Bukti_Pembayaran_PPDB", "Status_Verifikasi_Pembayaran_PPDB");
				$form_value = array("$nama_file_upload.$ext_file_upload", "Menunggu Verifikasi");


				$form_field_where = array("Id_Pendaftar_Pembayaran_PPDB");
				$form_criteria_where = array("=");
				$form_value_where = array("$edit_data_pendaftar_pembayaran_ppdb[Id_Pendaftar_Pembayaran_PPDB]");
				$form_connector_where = array("");

				$result = $a_data_pendaftar_pembayaran_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			} else {
			}
		}
		//FUNGSI UPLOAD FILE Bukti_Pembayaran_PPDB

		Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar_pembayaran_ppdb", $edit_data_pendaftar_pembayaran_ppdb['Id_Pendaftar_Pembayaran_PPDB'], "Pembayaran PPDB", "Telah Melakukan Upload Bukti Pembayaran PPDB");

		echo "<script>alert('Upload Bukti Berhasil');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------
