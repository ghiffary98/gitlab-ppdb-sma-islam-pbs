<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_pengaturan_pekerjaan_orang_tua = new a_pengaturan_pekerjaan_orang_tua();
$a_pengaturan_penghasilan_orang_tua = new a_pengaturan_penghasilan_orang_tua();
$a_pengaturan_kendaraan_yang_dipakai_kesekolah = new a_pengaturan_kendaraan_yang_dipakai_kesekolah();
$a_pengaturan_asal_sekolah = new a_pengaturan_asal_sekolah();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}
#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN

//FUNGSI AMBIL DATA PEMBELIAN FORMULIR
$search_field_where = array("Status", "Id_Pendaftar");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Pendaftar]");
$search_connector_where = array("AND", "");

$result = $a_data_pendaftar_pembelian_formulir->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar_pembelian_formulir = $result['Hasil'][0];
} else {
	$edit_data_pendaftar_pembelian_formulir = null;
}

//STATUS PEMBELIAN FORMULIR
if ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Belum Diverifikasi") {
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
} elseif ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi") {
	$Keterangan_Pembelian_Formulir = "Sudah Bayar";
} elseif ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Sedang Diverifikasi") {
	$Keterangan_Pembelian_Formulir = "Sedang Diverifikasi";
} elseif ($edit_data_pendaftar_pembelian_formulir['Status_Verifikasi_Pembelian_Formulir'] == "Verifikasi Ditolak") {
	$Keterangan_Pembelian_Formulir = "Pembayaran Gagal, Silahkan Lakukan Pembayaran Ulang";
} else {
	$Keterangan_Pembelian_Formulir = "Belum Bayar";
}

//HAPUS DUPLIKAT DATA PADA SELECT OPTION
function Hapus_Duplikat_Data_Pada_Select_Option($array_object, $Hapus_Berdasarkan_Nama_Field)
{
	$data = $array_object;
	$uniqueValues = array();
	$filteredData = array();

	foreach ($data as $item) {
		$hapus_berdasarkan_field = $item[$Hapus_Berdasarkan_Nama_Field];

		if (!in_array($hapus_berdasarkan_field, $uniqueValues)) {
			$uniqueValues[] = $hapus_berdasarkan_field;
			$filteredData[] = $item;
		}
	}

	$filteredJsonData = $filteredData;
	return $filteredJsonData;
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
//FUNGSI AMBIL DATA PENDAFTAR
$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$u_data_user[Id_Pendaftar]");

if ($result['Status'] == "Sukses") {
	$edit_data_pendaftar = $result['Hasil'];
} else {
	$edit_data_pendaftar = null;
}

//FUNGSI AMBIL DATA PPDB
$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

if ($result['Status'] == "Sukses") {
	$edit_data_ppdb = $result['Hasil'];
} else {
	$edit_data_ppdb = null;
}

if ($edit_data_pendaftar['Status_Kelulusan'] == "Gugur") {
	echo "<script>alert('Mohon Maaf, Anda Telah Gugur Dikarenakan Telah Melewati Masa Waktu Pembayaran Pembelian Formulir');document.location.href='dashboard.php?menu=pengumuman_kelulusan'</script>";
	exit();
}

if ($Keterangan_Pembelian_Formulir == "Belum Bayar") {
	echo "<script>alert('Harap Lakukan Pembelian Formulir Terlebih Dahulu !');document.location.href='dashboard.php'</script>";
	exit();
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {

	if (!isset($_POST['Jenis_Kelamin'])) {
		$_POST['Jenis_Kelamin'] = "";
	}

	if (!isset($_POST['Penerima_KPS'])) {
		$_POST['Penerima_KPS'] = "";
	}

	if (!isset($_POST['Penerima_KIP'])) {
		$_POST['Penerima_KIP'] = "";
	}

	if (!isset($_POST['Penerima_KJP_DKI'])) {
		$_POST['Penerima_KJP_DKI'] = "";
	}

	if (!isset($_POST['Status_Perwalian'])) {
		$_POST['Status_Perwalian'] = "";
	}

	$form_field = array("NIK", "NIS", "NISN", "NIK", "NIPD", "Nama_Lengkap", "Jenis_Kelamin", "Tempat_Lahir", "Tanggal_Lahir", "Agama", "Status_Dalam_Keluarga", "Jalan", "Kelurahan", "Kecamatan", "Kota", "Provinsi", "Id_Kelurahan", "Id_Kecamatan", "Id_Kota", "Id_Provinsi", "Kode_Pos", "Titik_Lintang_Alamat", "Titik_Bujur_Alamat", "Nomor_Handphone", "Nomor_Telepon", "Email", "Golongan_Darah", "Kebutuhan_Khusus", "Tinggi_Badan", "Berat_Badan", "Lingkar_Kepala", "Asal_Sekolah", "Alamat_Sekolah", "Kelas", "Nomor_Peserta_Ujian_Nasional", "Nomor_Seri_Ijazah", "Penerima_KPS", "Penerima_KIP", "Nomor_KIP", "Penerima_KJP_DKI", "Nomor_KJP_DKI", "Nomor_KPS", "Nomor_KKS", "Nomor_KK", "Nomor_Registrasi_Akta_Lahir", "Anak_Ke", "Jumlah_Saudara", "Status_Perwalian", "NIK_Ayah", "Nama_Ayah", "Tempat_Lahir_Ayah", "Tanggal_Lahir_Ayah", "Jenjang_Pendidikan_Ayah", "Pekerjaan_Ayah", "Penghasilan_Ayah", "No_HP_Ayah", "NIK_Ibu", "Nama_Ibu", "Tempat_Lahir_Ibu", "Tanggal_Lahir_Ibu", "Jenjang_Pendidikan_Ibu", "Pekerjaan_Ibu", "Penghasilan_Ibu", "No_HP_Ibu", "NIK_Wali", "Nama_Wali", "Tempat_Lahir_Wali", "Tanggal_Lahir_Wali", "Jenjang_Pendidikan_Wali", "Pekerjaan_Wali", "Penghasilan_Wali", "No_HP_Wali", "Kendaraan_Yang_Dipakai_Kesekolah", "Jarak_Rumah_Ke_Sekolah", "Ukuran_Seragam", "Waktu_Update_Data");

	$form_value = array("$_POST[NIK]", "$_POST[NIS]", "$_POST[NISN]", "$_POST[NIK]", "$_POST[NIPD]", "$_POST[Nama_Lengkap]", "$_POST[Jenis_Kelamin]", "$_POST[Tempat_Lahir]", "$_POST[Tanggal_Lahir]", "$_POST[Agama]", "$_POST[Status_Dalam_Keluarga]", "$_POST[Jalan]", "$_POST[Kelurahan]", "$_POST[Kecamatan]", "$_POST[Kota]", "$_POST[Provinsi]", "$_POST[Id_Kelurahan]", "$_POST[Id_Kecamatan]", "$_POST[Id_Kota]", "$_POST[Id_Provinsi]", "$_POST[Kode_Pos]", "$_POST[Titik_Lintang_Alamat]", "$_POST[Titik_Bujur_Alamat]", "$_POST[Nomor_Handphone]", "$_POST[Nomor_Telepon]", "$_POST[Email]", "$_POST[Golongan_Darah]", "$_POST[Kebutuhan_Khusus]", "$_POST[Tinggi_Badan]", "$_POST[Berat_Badan]", "$_POST[Lingkar_Kepala]", "$_POST[Asal_Sekolah]", "$_POST[Alamat_Sekolah]", "$_POST[Kelas]", "$_POST[Nomor_Peserta_Ujian_Nasional]", "$_POST[Nomor_Seri_Ijazah]", "$_POST[Penerima_KPS]", "$_POST[Penerima_KIP]", "$_POST[Nomor_KIP]", "$_POST[Penerima_KJP_DKI]", "$_POST[Nomor_KJP_DKI]", "$_POST[Nomor_KPS]", "$_POST[Nomor_KKS]", "$_POST[Nomor_KK]", "$_POST[Nomor_Registrasi_Akta_Lahir]", "$_POST[Anak_Ke]", "$_POST[Jumlah_Saudara]", "$_POST[Status_Perwalian]", "$_POST[NIK_Ayah]", "$_POST[Nama_Ayah]", "$_POST[Tempat_Lahir_Ayah]", "$_POST[Tanggal_Lahir_Ayah]", "$_POST[Jenjang_Pendidikan_Ayah]", "$_POST[Pekerjaan_Ayah]", "$_POST[Penghasilan_Ayah]", "$_POST[No_HP_Ayah]", "$_POST[NIK_Ibu]", "$_POST[Nama_Ibu]", "$_POST[Tempat_Lahir_Ibu]", "$_POST[Tanggal_Lahir_Ibu]", "$_POST[Jenjang_Pendidikan_Ibu]", "$_POST[Pekerjaan_Ibu]", "$_POST[Penghasilan_Ibu]", "$_POST[No_HP_Ibu]", "$_POST[NIK_Wali]", "$_POST[Nama_Wali]", "$_POST[Tempat_Lahir_Wali]", "$_POST[Tanggal_Lahir_Wali]", "$_POST[Jenjang_Pendidikan_Wali]", "$_POST[Pekerjaan_Wali]", "$_POST[Penghasilan_Wali]", "$_POST[No_HP_Wali]", "$_POST[Kendaraan_Yang_Dipakai_Kesekolah]", "$_POST[Jarak_Rumah_Ke_Sekolah]", "$_POST[Ukuran_Seragam]", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar");
	$form_criteria_where = array("=");
	$form_value_where = array("$u_data_user[Id_Pendaftar]");
	$form_connector_where = array("");

	$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where, "Iya");

	if ($result['Status'] == "Sukses") {

		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";

		Simpan_Aktivitas_Pendaftar($u_data_user['Id_Pendaftar'], "tb_data_pendaftar", $u_data_user['Id_Pendaftar'], "Verifikasi Data Diri", "Telah Mengupdate Formulir Data Diri");
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PPDB
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_ppdb = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN PEKERJAAN ORANG TUA
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_pekerjaan_orang_tua->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_pekerjaan_orang_tua = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN PENGHASILAN ORANG TUA
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_penghasilan_orang_tua->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_penghasilan_orang_tua = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN KENDARAAN YANG DIPAKAI KESEKOLAH
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_kendaraan_yang_dipakai_kesekolah->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_kendaraan_yang_dipakai_kesekolah = $result['Hasil'];
	}
}
#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN ASAL SEKOLAH
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_asal_sekolah->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_asal_sekolah = $result['Hasil'];
	}
}
