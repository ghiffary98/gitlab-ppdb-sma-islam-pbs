<?php

#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_ppdb = new a_data_ppdb();
$a_data_ppdb_syarat_dan_ketentuan = new a_data_ppdb_syarat_dan_ketentuan();
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI REDIRECT KE PPDB AKTIF TERBARU
if (!isset($_GET['id_ppdb'])) {
    $search_field_where = array("Status", "Status_PPDB");
    $search_criteria_where = array("=", "=");
    $search_value_where = array("Aktif", "Aktif");
    $search_connector_where = array("AND", "ORDER BY Id_PPDB DESC");

    $result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        $data_ppdb_terbaru = $result['Hasil'][0];
        $Link_Id_PPDB_Terbaru = $a_hash->encode($data_ppdb_terbaru['Id_PPDB']);
        echo "<script>document.location.href='persyaratan.php?id_ppdb=$Link_Id_PPDB_Terbaru'</script>";
    }
}

if (isset($_GET['id_ppdb'])) {
    $Get_Id_PPDB = $a_hash->decode($_GET['id_ppdb']);

    $search_field_where = array("Status", "Status_PPDB", "Id_PPDB");
    $search_criteria_where = array("=", "=", "=");
    $search_value_where = array("Aktif", "Aktif", "$Get_Id_PPDB");
    $search_connector_where = array("AND", "AND", "ORDER BY Id_PPDB DESC");

    $result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        $data_ppdb = $result['Hasil'][0];
    } else {
        echo "<script>document.location.href='login.php'</script>";
    }
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK SYARAT DAN KETENTUAN
if (isset($_GET['id_ppdb'])) {
	$search_field_where = array("Status","Id_PPDB","Posisi");
	$search_criteria_where = array("=","=","=");
	$search_value_where = array("Aktif","$Get_Id_PPDB","Ketentuan Umum");
	$search_connector_where = array("AND","AND","");

	$nomor = 0;

	$result = $a_data_ppdb_syarat_dan_ketentuan->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_syarat_dan_ketentuan_data_ketentuan = $result['Hasil'];
	}
}

if (isset($_GET['id_ppdb'])) {
	$search_field_where = array("Status","Id_PPDB","Posisi");
	$search_criteria_where = array("=","=","=");
	$search_value_where = array("Aktif","$Get_Id_PPDB","Catatan");
	$search_connector_where = array("AND","AND","");

	$nomor = 0;

	$result = $a_data_ppdb_syarat_dan_ketentuan->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_syarat_dan_ketentuan_data_catatan = $result['Hasil'];
	}
}
#-----------------------------------------------------------------------------------
