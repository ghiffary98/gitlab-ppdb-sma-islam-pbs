<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_register = new a_register();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_pengumuman_pendaftar = new a_pengumuman_pendaftar();
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI REDIRECT KE PPDB AKTIF TERBARU
if (!isset($_GET['id_ppdb'])) {
    $search_field_where = array("Status", "Status_PPDB");
    $search_criteria_where = array("=", "=");
    $search_value_where = array("Aktif", "Aktif");
    $search_connector_where = array("AND", "ORDER BY Id_PPDB DESC");

    $result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        $data_ppdb_terbaru = $result['Hasil'][0];
        $Link_Id_PPDB_Terbaru = $a_hash->encode($data_ppdb_terbaru['Id_PPDB']);
        echo "<script>document.location.href='register.php?id_ppdb=$Link_Id_PPDB_Terbaru'</script>";
    }
}

if (isset($_GET['id_ppdb'])) {
    $Get_Id_PPDB = $a_hash->decode($_GET['id_ppdb']);

    $search_field_where = array("Status", "Status_PPDB", "Id_PPDB");
    $search_criteria_where = array("=", "=", "=");
    $search_value_where = array("Aktif", "Aktif", "$Get_Id_PPDB");
    $search_connector_where = array("AND", "AND", "ORDER BY Id_PPDB DESC");

    $result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        $data_ppdb = $result['Hasil'][0];
    } else {
        echo "<script>document.location.href='login.php'</script>";
    }
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI REGISTER
if (isset($_POST['Submit_Daftar'])) {
    #FUNGSI UNTUK MENDAPATKAN NOMOR PENDAFTARAN TERBARU FORMAT P1001
    $Get_Id_PPDB = $a_hash->decode($_GET['id_ppdb']);
    $search_field_where = array("Status","Id_PPDB");
    $search_criteria_where = array("<>","=");
    $search_value_where = array("","$Get_Id_PPDB");
    $search_connector_where = array("AND","ORDER BY Nomor_Pendaftaran DESC");

    $Select_Table_Lainnya = "";

    $result = $a_data_pendaftar->baca_data_dengan_select_table_lain_lalu_filter($Select_Table_Lainnya, $search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        
        if ($result['Hasil'][0]['Nomor_Pendaftaran'] === null) {
            $Id_Berikutnya = 1;
        } else {
            $Id_Saat_Ini = $result['Hasil'][0]['Nomor_Pendaftaran'];
            $Id_Berikutnya = str_replace('P' . $Get_Id_PPDB , '', $Id_Saat_Ini);
            $Id_Berikutnya = intval($Id_Berikutnya);
            $Id_Berikutnya = $Id_Berikutnya + 1;
        }
    }else{
        $Id_Berikutnya = 1;
    }


    $Nomor_Pendaftaran = 'P' . $Get_Id_PPDB  . str_pad($Id_Berikutnya, 4, '0', STR_PAD_LEFT);
    

    $_POST['Password_Pendaftar'] = $a_hash->encode($_POST["Password"]);
    $_POST['Password'] = $a_hash_password->hash_password($_POST['Password']);

    $Get_Id_PPDB = $a_hash->decode($_GET['id_ppdb']);
    $result = $a_register->register($Get_Id_PPDB, $Nomor_Pendaftaran, $_POST['Email'], $_POST['Password'], $_POST['Password_Pendaftar'], $_POST['Nama_Lengkap'], $_POST['Tempat_Lahir'], $_POST['Tanggal_Lahir'], $_POST['Alamat_Lengkap'], $_POST['Nomor_Handphone']);

    if ($result['Status'] == "Sukses") {
        $Id_Pendaftar_Ini = $result['Id'];
        $Judul_Pengumuman_Untuk_Pendaftar = "Pembelian Formulir";

        if((isset($data_ppdb)) AND ($data_ppdb['Masa_Waktu_Pembayaran_Pembelian_Formulir'] <> 0)){
            $Isi_Pengumuman_Untuk_Pendaftar = "Harap selesaikan Pembelian Formulir maksimal ".$data_ppdb['Masa_Waktu_Pembayaran_Pembelian_Formulir']." Hari dari Anda Register. Jika Tidak, maka otomatis Status PPDB Anda menjadi 'Gugur'";
        }else{
            $Isi_Pengumuman_Untuk_Pendaftar = "Harap selesaikan Pembelian Formulir";
        }

        Simpan_Pengumuman_Pendaftar("$Id_Pendaftar_Ini","Id_Pendaftar_Pembelian_Formulir","$Id_Pendaftar_Ini",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);

        echo "<script>alert('Registrasi Berhasil');document.location.href='login.php'</script>";
    } else {
        if (
            (isset($result['Keterangan_Error'])) and
            ($result['Keterangan_Error'] == "Email Sudah Digunakan")
        ) {
            echo "<script>alert('Email Sudah Digunakan');document.location.href='$Link_Sekarang'</script>";
        } else if (
            (isset($result['Keterangan_Error'])) and
            ($result['Keterangan_Error'] == "Nomor Handphone Sudah Digunakan")
        ) {
            echo "<script>alert('Nomor Handphone Sudah Digunakan');document.location.href='$Link_Sekarang'</script>";
        } else {
            echo "<script>alert('Terjadi Kesalahan Saat Registrasi');document.location.href='$Link_Sekarang'</script>";
        }
    }
}

#-----------------------------------------------------------------------------------
#FUNGSI MENDAPATKAN TANGGAL UMUR MAKSIMAL PENDAFTARAN DARI TANGGAL AKHIR PENDAFTARAN
if((isset($data_ppdb['Umur_Maksimal_Pendaftar'])) AND ($data_ppdb['Umur_Maksimal_Pendaftar'] <> 0)){
    $Tanggal_Terakhir_Pendaftaran_PPDB = new DateTime($data_ppdb['Tanggal_Akhir_Pendaftaran']);
    $Umur_Maksimal_Pendaftar = $data_ppdb['Umur_Maksimal_Pendaftar'];

    // Buat salinan objek DateTime untuk menghindari perubahan pada objek asli
    $Tanggal_Maksimal_Umur_Pendaftaran = clone $Tanggal_Terakhir_Pendaftaran_PPDB;
    $Tanggal_Maksimal_Umur_Pendaftaran->sub(new DateInterval('P' . $Umur_Maksimal_Pendaftar . 'Y'));

    $Tanggal_Batas_Maksimal_Umur_Pendaftaran = $Tanggal_Maksimal_Umur_Pendaftaran->format('Y-m-d');
}else{
    $Tanggal_Batas_Maksimal_Umur_Pendaftaran = "1900-01-01";
}

#-----------------------------------------------------------------------------------
