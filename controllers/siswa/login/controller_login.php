<?php
$a_login = new a_login();

setcookie ("Cookie_1_Login","",time()+ (86400 * 365));
setcookie ("Cookie_2_Login","",time()+ (86400 * 365));
setcookie ("Cookie_3_Login","",time()+ (86400 * 365));
setcookie ("Id_PPDB","",time()+ (86400 * 365));
unset($_COOKIE['Cookie_1_Login']); 
unset($_COOKIE['Cookie_2_Login']); 
unset($_COOKIE['Cookie_3_Login']); 
unset($_COOKIE['Id_PPDB']); 

#-----------------------------------------------------------------------------------
#FUNGSI REDIRECT KE PPDB AKTIF TERBARU
if (!isset($_GET['id_ppdb'])) {
    $search_field_where = array("Status", "Status_PPDB");
    $search_criteria_where = array("=", "=");
    $search_value_where = array("Aktif", "Aktif");
    $search_connector_where = array("AND", "ORDER BY Id_PPDB DESC");

    $result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        $data_ppdb_terbaru = $result['Hasil'][0];
        $Link_Id_PPDB_Terbaru = $a_hash->encode($data_ppdb_terbaru['Id_PPDB']);
        echo "<script>document.location.href='login.php?id_ppdb=$Link_Id_PPDB_Terbaru'</script>";
    }
}

if (isset($_GET['id_ppdb'])) {
    $Get_Id_PPDB = $a_hash->decode($_GET['id_ppdb']);

    $search_field_where = array("Status", "Status_PPDB", "Id_PPDB");
    $search_criteria_where = array("=", "=", "=");
    $search_value_where = array("Aktif", "Aktif", "$Get_Id_PPDB");
    $search_connector_where = array("AND", "AND", "ORDER BY Id_PPDB DESC");

    $result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        $data_ppdb = $result['Hasil'][0];
    } else {
        echo "<script>document.location.href='login.php'</script>";
    }
}
#-----------------------------------------------------------------------------------

if (isset($_POST['Submit_Login'])) {
    $_POST['Username'] = $_POST['Username'];
    $_POST['Password'] = $a_hash_password->hash_password($_POST['Password']);

    $result = $a_login->login($_POST['Username'], $_POST['Password']);
    if ($result['Status'] == "Sukses") {
        $data_login = $result['Hasil'];

        $Id_User = $a_hash->encode($data_login['Id_Pendaftar'], "Id_Pendaftar");
        $Login_Sebagai = $a_hash->encode("Siswa", "Login_Sebagai");
        $Password = $a_hash->encode($data_login['Password'], "Password");

        setcookie("Cookie_1_Login", $Id_User, time() + (86400 * 365)); //LOGIN ID_PENGGUNA
        setcookie("Cookie_2_Login", $Password, time() + (86400 * 365)); //LOGIN PASSWORD
        setcookie("Cookie_3_Login", $Login_Sebagai, time() + (86400 * 365)); //LOGIN SEBAGAI

        setcookie ("Id_PPDB",$data_login['Id_PPDB'],time()+ (86400 * 365)); //LOGIN Id_PPDB

        Simpan_Aktivitas_Pendaftar($data_login['Id_Pendaftar'], "tb_data_pendaftar", $data_login['Id_Pendaftar'], "Login", "Telah Melakukan Login");

        echo "<script>alert('Login Berhasil');document.location.href='dashboard.php'</script>";
    } else {
        echo "<script>alert('Email/Nomor Handphone dan Password Salah');document.location.href='$Link_Sekarang'</script>";
    }
}
?>