<?php
$a_data_pendaftar = new a_data_pendaftar();
$a_data_pendaftar_pembayaran_ppdb = new a_data_pendaftar_pembayaran_ppdb();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
$a_data_pendaftar_verifikasi_berkas = new a_data_pendaftar_verifikasi_berkas();
$a_data_pengumuman = new a_data_pengumuman();
$a_data_ppdb = new a_data_ppdb();
$a_data_ppdb_syarat_dan_ketentuan = new a_data_ppdb_syarat_dan_ketentuan();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
$a_data_pendaftar_verifikasi_berkas = new a_data_pendaftar_verifikasi_berkas();
$a_data_pendaftar_pembayaran_ppdb = new a_data_pendaftar_pembayaran_ppdb();
$a_login = new a_login();

setcookie ("Cookie_1_Login","",time()+ (86400 * 365));
setcookie ("Cookie_2_Login","",time()+ (86400 * 365));
setcookie ("Cookie_3_Login","",time()+ (86400 * 365));
setcookie ("Id_PPDB","",time()+ (86400 * 365));
unset($_COOKIE['Cookie_1_Login']); 
unset($_COOKIE['Cookie_2_Login']); 
unset($_COOKIE['Cookie_3_Login']); 
unset($_COOKIE['Id_PPDB']); 

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
$folder_penyimpanan_file_foto_alur_pendaftaran = "media/foto_alur_pendaftaran/";
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI REDIRECT KE PPDB AKTIF TERBARU
if (!isset($_GET['id_ppdb'])) {
    $search_field_where = array("Status", "Status_PPDB");
    $search_criteria_where = array("=", "=");
    $search_value_where = array("Aktif", "Aktif");
    $search_connector_where = array("AND", "ORDER BY Id_PPDB DESC");

    $result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        $data_ppdb_terbaru = $result['Hasil'][0];
        $Link_Id_PPDB_Terbaru = $a_hash->encode($data_ppdb_terbaru['Id_PPDB']);
        echo "<script>document.location.href='homepage.php?id_ppdb=$Link_Id_PPDB_Terbaru'</script>";
    }
}

if (isset($_GET['id_ppdb'])) {
    $Get_Id_PPDB = $a_hash->decode($_GET['id_ppdb']);

    $search_field_where = array("Status", "Status_PPDB", "Id_PPDB");
    $search_criteria_where = array("=", "=", "=");
    $search_value_where = array("Aktif", "Aktif", "$Get_Id_PPDB");
    $search_connector_where = array("AND", "AND", "ORDER BY Id_PPDB DESC");

    $result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        $data_ppdb = $result['Hasil'][0];
    } else {
        echo "<script>document.location.href='homepage.php'</script>";
    }
}

//FUNGSI UNTUK HITUNG DATA-DATA
//Total Pendaftar
$count_field_where = array("Status","Id_PPDB");
$count_criteria_where = array("=","=");
$count_connector_where = array("AND","");
$count_value_where = array("Aktif","$Get_Id_PPDB");

$hitung_Total_Pendaftar = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Pendaftar = $hitung_Total_Pendaftar['Hasil'];

//Total Pembelian Formulir
$count_field_where = array("Status","Status_Verifikasi_Pembelian_Formulir","(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_pembelian_formulir.Id_Pendaftar)");
$count_criteria_where = array("=","=","=");
$count_connector_where = array("AND","AND","");
$count_value_where = array("Aktif","Sudah Diverifikasi","$Get_Id_PPDB");

$hitung_Total_Pembelian_Formulir = $a_data_pendaftar_pembelian_formulir->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Pembelian_Formulir = $hitung_Total_Pembelian_Formulir['Hasil'];

//Total Lolos Berkas
$count_field_where = array("Status","Status_Verifikasi_Berkas","(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_verifikasi_berkas.Id_Pendaftar)");
$count_criteria_where = array("=","=","=");
$count_connector_where = array("AND","AND","");
$count_value_where = array("Aktif","Sudah Diverifikasi","$Get_Id_PPDB");

$hitung_Total_Lolos_Berkas = $a_data_pendaftar_verifikasi_berkas->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Lolos_Berkas = $hitung_Total_Lolos_Berkas['Hasil'];

//Total Lunas PPDB
$count_field_where = array("Status","Status_Verifikasi_Pembayaran_PPDB","(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_pembayaran_ppdb.Id_Pendaftar)");
$count_criteria_where = array("=","=","=");
$count_connector_where = array("AND","AND","");
$count_value_where = array("Aktif","Sudah Diverifikasi","$Get_Id_PPDB");

$hitung_Total_Lunas_PPDB = $a_data_pendaftar_pembayaran_ppdb->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Lunas_PPDB = $hitung_Total_Lunas_PPDB['Hasil'];

//Total Pendaftar Diterima
$count_field_where = array("Status","Status_Kelulusan","Id_PPDB");
$count_criteria_where = array("=","=","=");
$count_connector_where = array("AND","AND","");
$count_value_where = array("Aktif","Lulus","$Get_Id_PPDB");

$hitung_Total_Pendaftar_Diterima = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Pendaftar_Diterima = $hitung_Total_Pendaftar_Diterima['Hasil'];


//Total Pendaftar Tidak Diterima
$count_field_where = array("Status","Status_Kelulusan","Id_PPDB");
$count_criteria_where = array("=","=","=");
$count_connector_where = array("AND","AND","");
$count_value_where = array("Aktif","Tidak Lulus","$Get_Id_PPDB");

$hitung_Total_Pendaftar_Tidak_Diterima = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Pendaftar_Tidak_Diterima = $hitung_Total_Pendaftar_Tidak_Diterima['Hasil'];
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE PENGUMUMAN
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
    $search_field_where = array("Status","Id_PPDB");
    $search_criteria_where = array("=","=");
    $search_value_where = array("Aktif","$Get_Id_PPDB");
    $search_connector_where = array("AND","");

    $nomor = 0;

    $result = $a_data_pengumuman->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

    if ($result['Status'] == "Sukses") {
        $list_datatable_pengumuman = $result['Hasil'];
    }
}
#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK SYARAT DAN KETENTUAN
if (isset($_GET['id_ppdb'])) {
	$search_field_where = array("Status","Id_PPDB","Posisi");
	$search_criteria_where = array("=","=","=");
	$search_value_where = array("Aktif","$Get_Id_PPDB","Ketentuan Umum");
	$search_connector_where = array("AND","AND","");

	$nomor = 0;

	$result = $a_data_ppdb_syarat_dan_ketentuan->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_syarat_dan_ketentuan_data_ketentuan = $result['Hasil'];
	}
}

if (isset($_GET['id_ppdb'])) {
	$search_field_where = array("Status","Id_PPDB","Posisi");
	$search_criteria_where = array("=","=","=");
	$search_value_where = array("Aktif","$Get_Id_PPDB","Catatan");
	$search_connector_where = array("AND","AND","");

	$nomor = 0;

	$result = $a_data_ppdb_syarat_dan_ketentuan->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_syarat_dan_ketentuan_data_catatan = $result['Hasil'];
	}
}
?>