<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_pengumuman_pendaftar = new a_pengumuman_pendaftar();
#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
#FUNGSI SIMPAN DATA DENGAN FUNCTION
function Simpan_Pengumuman_Pendaftar($Id_Pendaftar,$Relasi,$Id_Relasi,$Judul_Pengumuman,$Isi_Pengumuman){
	$a_pengumuman_pendaftar = new a_pengumuman_pendaftar();
	$Waktu_Sekarang = date('Y-m-d H:i:s');
	
	$form_field = array("Id_Pengumuman_Pendaftar","Id_Pendaftar","Relasi","Id_Relasi","Judul_Pengumuman","Isi_Pengumuman","Status_Dibaca","Waktu_Simpan_Data","Status");
	$form_value = array(NULL,"$Id_Pendaftar","$Relasi","$Id_Relasi","$Judul_Pengumuman","$Isi_Pengumuman","Belum Dibaca","$Waktu_Sekarang","Aktif");

	$result = $a_pengumuman_pendaftar->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		$return['Status'] = "Sukses";
	} else {
		$return['Status'] = "Gagal";
	}
	return $return;
}
#-----------------------------------------------------------------------------------
?>