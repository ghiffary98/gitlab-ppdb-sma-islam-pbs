<?php
$a_login = new a_login();

setcookie ("Cookie_1_Login","",time()+ (86400 * 365));
setcookie ("Cookie_2_Login","",time()+ (86400 * 365));
setcookie ("Cookie_3_Login","",time()+ (86400 * 365));
setcookie ("Id_PPDB","",time()+ (86400 * 365));
unset($_COOKIE['Cookie_1_Login']); 
unset($_COOKIE['Cookie_2_Login']); 
unset($_COOKIE['Cookie_3_Login']); 
unset($_COOKIE['Id_PPDB']); 

if(isset($_POST['Submit_Login'])){
	$_POST['Username'] = $_POST['Username'];
	$_POST['Password'] = $a_hash_password->hash_password($_POST['Password']);

	$result = $a_login->login($_POST['Username'], $_POST['Password']);
	if($result['Status'] == "Sukses"){
		$data_login = $result['Hasil'];

		$Id_User = $a_hash->encode($data_login['Id_Pengguna'],"Id_Pengguna");
		$Login_Sebagai = $a_hash->encode("Admin","Login_Sebagai");
		$Password = $a_hash->encode($data_login['Password'],"Password");
	
		setcookie ("Cookie_1_Login",$Id_User,time()+ (86400 * 365)); //LOGIN ID_PENGGUNA
		setcookie ("Cookie_2_Login",$Password,time()+ (86400 * 365)); //LOGIN PASSWORD
		setcookie ("Cookie_3_Login",$Login_Sebagai,time()+ (86400 * 365)); //LOGIN SEBAGAI

		//CEK JIKA ADA YANG AKTIF, MAKA AKAN OTOMATIS MASUK KE YANG PPDB AKTIF
		$search_field_where = array("Status","Status_PPDB");
		$search_criteria_where = array("=","=");
		$search_value_where = array("Aktif","Aktif");
		$search_connector_where = array("AND","ORDER BY Id_PPDB DESC LIMIT 1");

		$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

		if($result["Status"] == "Sukses"){
			$data_login_ppdb = $result['Hasil'][0];
			setcookie ("Id_PPDB",$data_login_ppdb['Id_PPDB'],time()+ (86400 * 365)); //LOGIN Id_PPDB
		}else{
			//JIKA TIDAK ADA YANG AKTIF, MAKA AKAN OTOMATIS MASUK KE PPDB TERBARU
			$search_field_where = array("Status");
			$search_criteria_where = array("=");
			$search_value_where = array("Aktif");
			$search_connector_where = array("ORDER BY Id_PPDB DESC LIMIT 1");

			$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);
			if($result["Status"] == "Sukses"){
				$data_login_ppdb = $result['Hasil'][0];
				setcookie ("Id_PPDB",$data_login_ppdb['Id_PPDB'],time()+ (86400 * 365)); //LOGIN Id_PPDB
			}else{
				setcookie ("Id_PPDB","",time()+ (86400 * 365)); //LOGIN Id_PPDB
			}
		}
		

		echo "<script>alert('Login Berhasil');document.location.href='dashboard_admin.php'</script>";
	}else{
		echo "<script>alert('Username atau Password Salah');document.location.href='$Link_Sekarang'</script>";
	}
}
?>