<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar = new a_data_pendaftar();
$a_data_pendaftar_pembayaran_ppdb = new a_data_pendaftar_pembayaran_ppdb();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
$a_data_pendaftar_verifikasi_berkas = new a_data_pendaftar_verifikasi_berkas();
$a_data_pengumuman = new a_data_pengumuman();
$a_data_ppdb = new a_data_ppdb();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
$a_data_pendaftar_verifikasi_berkas = new a_data_pendaftar_verifikasi_berkas();
$a_data_pendaftar_pembayaran_ppdb = new a_data_pendaftar_pembayaran_ppdb();

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGUMUMAN
$search_field_where = array("Status");
$search_criteria_where = array("=");
$search_value_where = array("Aktif");
$search_connector_where = array("");

$nomor = 0;

$result = $a_data_pengumuman->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$list_data_pengumuman = $result['Hasil'];
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA AKTIVITAS PENDAFTAR
$search_field_where = array("Status");
$search_criteria_where = array("=");
$search_value_where = array("Aktif");
$search_connector_where = array("ORDER BY Id_Aktivitas_Pendaftar DESC");

$nomor = 0;

$result = $a_aktivitas_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$list_data_aktivitas_pendaftar = $result['Hasil'];
}

//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar" ID_PRIMARY "Id_Pendaftar"
$search_field_where = array("Status");
$search_criteria_where = array("<>");
$search_value_where = array("");
$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");

$result_data_relasi = $a_data_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result_data_relasi['Status'] == "Sukses") {
	$array_result_relasi_data_pendaftar = $result_data_relasi['Hasil'];
	$array_hasil_relasi_data_pendaftar;
	foreach ($array_result_relasi_data_pendaftar as $relasi_data_pendaftar) {
		$id_relasi_data_pendaftar = strval($relasi_data_pendaftar['Id_Pendaftar']);
		$array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$id_relasi_data_pendaftar] = $relasi_data_pendaftar;
	}


} else {
	$array_result_relasi_data_pendaftar = NULL;
}
//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar" ID_PRIMARY "Id_Pendaftar"


//FUNGSI UNTUK HITUNG DATA-DATA
//Total Target Siswa
$result = $a_data_ppdb->baca_data_id("Id_PPDB", $Id_PPDB_Saat_Ini);
if ($result['Status'] == "Sukses") {
	$Total_Target_Siswa = $result['Hasil']['Total_Target_Siswa'];
} else {
	$Total_Target_Siswa = 0;
}


//Total Pendaftar
$count_field_where = array("Status", "Id_PPDB");
$count_criteria_where = array("=", "=");
$count_connector_where = array("AND", "");
$count_value_where = array("Aktif", "$Id_PPDB_Saat_Ini");

$hitung_Total_Pendaftar = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Pendaftar = $hitung_Total_Pendaftar['Hasil'];

//Total Pembelian Formulir
$count_field_where = array("Status", "Status_Verifikasi_Pembelian_Formulir", "(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_pembelian_formulir.Id_Pendaftar)");
$count_criteria_where = array("=", "=", "=");
$count_connector_where = array("AND", "AND", "");
$count_value_where = array("Aktif", "Sudah Diverifikasi", "$Id_PPDB_Saat_Ini");

$hitung_Total_Pembelian_Formulir = $a_data_pendaftar_pembelian_formulir->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Pembelian_Formulir = $hitung_Total_Pembelian_Formulir['Hasil'];

//Total Lolos Berkas
$count_field_where = array("Status", "Status_Verifikasi_Berkas", "(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_verifikasi_berkas.Id_Pendaftar)");
$count_criteria_where = array("=", "=", "=");
$count_connector_where = array("AND", "AND", "");
$count_value_where = array("Aktif", "Sudah Diverifikasi", "$Id_PPDB_Saat_Ini");

$hitung_Total_Lolos_Berkas = $a_data_pendaftar_verifikasi_berkas->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Lolos_Berkas = $hitung_Total_Lolos_Berkas['Hasil'];

//Total Lunas PPDB
$count_field_where = array("Status", "Status_Verifikasi_Pembayaran_PPDB", "(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_pembayaran_ppdb.Id_Pendaftar)");
$count_criteria_where = array("=", "=", "=");
$count_connector_where = array("AND", "AND", "");
$count_value_where = array("Aktif", "Sudah Diverifikasi", "$Id_PPDB_Saat_Ini");

$hitung_Total_Lunas_PPDB = $a_data_pendaftar_pembayaran_ppdb->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Lunas_PPDB = $hitung_Total_Lunas_PPDB['Hasil'];

//Total Pendaftar Diterima
$count_field_where = array("Status", "Status_Kelulusan", "Id_PPDB");
$count_criteria_where = array("=", "=", "=");
$count_connector_where = array("AND", "AND", "");
$count_value_where = array("Aktif", "Lulus", "$Id_PPDB_Saat_Ini");

$hitung_Total_Pendaftar_Diterima = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Pendaftar_Diterima = $hitung_Total_Pendaftar_Diterima['Hasil'];


//Total Pendaftar Tidak Diterima
$count_field_where = array("Status", "Status_Kelulusan", "Id_PPDB");
$count_criteria_where = array("=", "=", "=");
$count_connector_where = array("AND", "AND", "");
$count_value_where = array("Aktif", "Tidak Lulus", "$Id_PPDB_Saat_Ini");

$hitung_Total_Pendaftar_Tidak_Diterima = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Total_Pendaftar_Tidak_Diterima = $hitung_Total_Pendaftar_Tidak_Diterima['Hasil'];

//UPDATE STATUS PPDB
if (isset($_POST['Update_Status_PPDB_Menjadi_Aktif'])) {
	$form_field = array("Status_PPDB", "Waktu_Update_Data");

	$form_value = array("Aktif", "$Waktu_Sekarang");

	$form_field_where = array("Id_PPDB");
	$form_criteria_where = array("=");
	$form_value_where = array("$Id_PPDB_Saat_Ini");
	$form_connector_where = array("");

	$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {
		echo "<script>alert('Berhasil Mengaktifkan PPDB Ini');document.location.href='$Link_Sekarang';</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Status PPDB');document.location.href='$Link_Sekarang';</script>";
	}
}

if (isset($_POST['Update_Status_PPDB_Menjadi_Tidak_Aktif'])) {
	$form_field = array("Status_PPDB", "Waktu_Update_Data");

	$form_value = array("Tidak Aktif", "$Waktu_Sekarang");

	$form_field_where = array("Id_PPDB");
	$form_criteria_where = array("=");
	$form_value_where = array("$Id_PPDB_Saat_Ini");
	$form_connector_where = array("");

	$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {
		echo "<script>alert('Berhasil Menutup PPDB Ini');document.location.href='$Link_Sekarang';</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Status PPDB');document.location.href='$Link_Sekarang';</script>";
	}
}