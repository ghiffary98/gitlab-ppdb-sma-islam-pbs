<?php 
$a_dashboard = new a_dashboard();
$a_login = new a_login();

//FUNGSI CEK LOGIN
if(!((isset($_COOKIE['Cookie_1_Login'])) AND (isset($_COOKIE['Cookie_2_Login'])) AND (isset($_COOKIE['Cookie_3_Login'])))){
    echo "<script>alert('Silahkan Login Kembali !!!');document.location.href='login_admin.php?redirect=".$a_hash->encode_link_kembali($Link_Sekarang)."';</script>";
    exit();
}else{
    //UNTUK CEK COOKIE LOGIN APAKAH BENAR DATA TERSEBUT ADA PADA DATABASE
    $cek_login_Id_User = $a_hash->decode($_COOKIE['Cookie_1_Login'],"Id_User");
    $cek_login_Password = $a_hash->decode($_COOKIE['Cookie_2_Login'],"Password");
    $cek_login_Login_Sebagai = $a_hash->decode($_COOKIE['Cookie_3_Login'],"Login_Sebagai");

    $result = $a_login->login_id_dan_password($cek_login_Id_User, $cek_login_Password);

    if($result['Status'] == "Sukses"){
        $u_data_user = $result['Hasil'];

         // Get the user's full name
         $userFullName = $u_data_user['Nama_Lengkap'];

         // Split the full name into words
         $nameParts = explode(' ', $userFullName);
 
         // Initialize a variable to store the initials
         $initials = '';
 
         // Loop through the name parts and extract the first character from each
         foreach ($nameParts as $namePart) {
             $initials .= substr($namePart, 0, 1);
         }
 
         // Convert the initials to uppercase
         $initials = strtoupper($initials);
    }else{
        echo "<script>alert('Silahkan Login Kembali !!!');document.location.href='login_admin.php?redirect=".$a_hash->encode_link_kembali($Link_Sekarang)."';</script>";
        exit();
    }
    //UNTUK CEK COOKIE LOGIN APAKAH BENAR DATA TERSEBUT ADA PADA DATABASE
}



$search_field_where = array("Status");
$search_criteria_where = array("=");
$search_value_where = array("Aktif");
$search_connector_where = array("");

$nomor = 0;

$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
    $list_data_ppdb = $result['Hasil'];
}

if(isset($_POST['Pindah_PPDB'])){
    setcookie ("Id_PPDB",$_POST['Id_PPDB'],time()+ (86400 * 365)); //LOGIN Id_PPDB
    echo "<script>document.location.href='$Link_Sekarang';</script>";
    exit();
}