<?php
header( "Access-Control-Allow-Origin: *" );
header( "Access-Control-Allow-Credentials: true" );
header( "Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS" );
header( "Access-Control-Max-Age: 604800" );
header( "Access-Control-Request-Headers: x-requested-with" );
header( "Access-Control-Allow-Headers: x-requested-with, x-requested-by" );

include "../../../config/database.php";
include "../../../models/global/pengaturan_asal_sekolah/model_pengaturan_asal_sekolah.php";
$a_pengaturan_asal_sekolah = new a_pengaturan_asal_sekolah();

$Nama_Sekolah = $_GET['nama_sekolah'];

$search_field_where = array("Status", "Nama_Sekolah");
$search_criteria_where = array("=", "LIKE");
$search_value_where = array("Aktif", "%$Nama_Sekolah%");
$search_connector_where = array("AND", "ORDER BY Id_Pengaturan_Asal_Sekolah DESC LIMIT 1");

$result = $a_pengaturan_asal_sekolah->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);
if ($result['Status'] == "Sukses") {
	$data_sekolah = $result['Hasil'][0];
    $alamat_sekolah = $data_sekolah['Alamat_Sekolah'];
    $data['Alamat'] = $alamat_sekolah;
} else {
    $data['Alamat'] = "";
}

echo json_encode($data);

?>