<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar = new a_data_pendaftar();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI SIMPAN DATA (CREATE)
if (isset($_POST['submit_simpan'])) {
	$_POST['Password'] = $a_hash_password->hash_password($_POST['Password']);

	$form_field = array("Id_Pendaftar", "Id_PPDB", "Nomor_Pendaftaran", "Username", "Password", "NIS", "NISN", "Nama_Lengkap", "Jenis_Kelamin", "Tempat_Lahir", "Tanggal_Lahir", "Agama", "Status_Dalam_Keluarga", "Jalan", "Kecamatan", "Kota", "Provinsi", "Kode_Pos", "Nomor_Handphone", "Email", "Status_Pendaftaran", "Waktu_Simpan_Data", "Status");

	$form_value = array(NULL, "$_POST[Id_PPDB]", "$_POST[Nomor_Pendaftaran]", "$_POST[Username]", "$_POST[Password]", "$_POST[NIS]", "$_POST[NISN]", "$_POST[Nama_Lengkap]", "$_POST[Jenis_Kelamin]", "$_POST[Tempat_Lahir]", "$_POST[Tanggal_Lahir]", "$_POST[Agama]", "$_POST[Status_Dalam_Keluarga]", "$_POST[Jalan]", "$_POST[Kecamatan]", "$_POST[Kota]", "$_POST[Provinsi]", "$_POST[Kode_Pos]", "$_POST[Nomor_Handphone]", "$_POST[Email]", "$_POST[Status_Pendaftaran]", "$Waktu_Sekarang", "Aktif");

	$result = $a_data_pendaftar->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		echo "<script>alert('Data Tersimpan');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menyimpan Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
if (isset($_GET['edit'])) {

	$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit = $result['Hasil'];
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	if ($_POST['Password'] <> "") {
		$_POST['Password'] = $a_hash_password->hash_password($_POST['Password']);

		$form_field = array("Id_PPDB", "Nomor_Pendaftaran", "Username", "Password", "NIS", "NISN", "Nama_Lengkap", "Jenis_Kelamin", "Tempat_Lahir", "Tanggal_Lahir", "Agama", "Status_Dalam_Keluarga", "Jalan", "Kecamatan", "Kota", "Provinsi", "Kode_Pos", "Nomor_Handphone", "Email", "Status_Pendaftaran", "Waktu_Update_Data");

		$form_value = array("$_POST[Id_PPDB]", "$_POST[Nomor_Pendaftaran]", "$_POST[Username]", "$_POST[Password]", "$_POST[NIS]", "$_POST[NISN]", "$_POST[Nama_Lengkap]", "$_POST[Jenis_Kelamin]", "$_POST[Tempat_Lahir]", "$_POST[Tanggal_Lahir]", "$_POST[Agama]", "$_POST[Status_Dalam_Keluarga]", "$_POST[Jalan]", "$_POST[Kecamatan]", "$_POST[Kota]", "$_POST[Provinsi]", "$_POST[Kode_Pos]", "$_POST[Nomor_Handphone]", "$_POST[Email]", "$_POST[Status_Pendaftaran]", "$Waktu_Sekarang");
	} else {
		$form_field = array("Id_PPDB", "Nomor_Pendaftaran", "Username", "NIS", "NISN", "Nama_Lengkap", "Jenis_Kelamin", "Tempat_Lahir", "Tanggal_Lahir", "Agama", "Status_Dalam_Keluarga", "Jalan", "Kecamatan", "Kota", "Provinsi", "Kode_Pos", "Nomor_Handphone", "Email", "Status_Pendaftaran", "Waktu_Update_Data");

		$form_value = array("$_POST[Id_PPDB]", "$_POST[Nomor_Pendaftaran]", "$_POST[Username]", "$_POST[NIS]", "$_POST[NISN]", "$_POST[Nama_Lengkap]", "$_POST[Jenis_Kelamin]", "$_POST[Tempat_Lahir]", "$_POST[Tanggal_Lahir]", "$_POST[Agama]", "$_POST[Status_Dalam_Keluarga]", "$_POST[Jalan]", "$_POST[Kecamatan]", "$_POST[Kota]", "$_POST[Provinsi]", "$_POST[Kode_Pos]", "$_POST[Nomor_Handphone]", "$_POST[Email]", "$_POST[Status_Pendaftaran]", "$Waktu_Sekarang");
	}

	$form_field_where = array("Id_Pendaftar");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where, "Iya");

	if ($result['Status'] == "Sukses") {

		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI DELETE DATA (DELETE)
if (isset($_GET['hapus_data_ke_tong_sampah'])) {

	$result = $a_data_pendaftar->hapus_data_ke_tong_sampah("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$form_value = array("$Waktu_Sekarang");
		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}
}

if (isset($_GET['arsip_data'])) {

	$result = $a_data_pendaftar->arsip_data("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Dipindahkan Ke Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Memindahkan Data Ke Arsip');document.location.href='$kehalaman'</script>";
	}
}

if (isset($_GET['restore_data_dari_arsip'])) {

	$result = $a_data_pendaftar->restore_data_dari_arsip("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Berhasil Di Keluarkan Dari Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengeluarkan Data Dari Arsip');document.location.href='$kehalaman'</script>";
	}
}

if (isset($_GET['restore_data_dari_tong_sampah'])) {

	$result = $a_data_pendaftar->restore_data_dari_tong_sampah("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Di Restore Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Restore Data Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	}
}

if (isset($_GET['hapus_data_permanen'])) {

	$result = $a_data_pendaftar->hapus_data_permanen("Id_Pendaftar", $Get_Id_Primary);
	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus Permanen');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}
}

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI HITUNG DATA (COUNT)

$count_field_where = array("Status");

$count_criteria_where = array("=");

$count_connector_where = array("");

//DATA AKTIF
$count_value_where = array("Aktif");
$hitung_Aktif = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Aktif = $hitung_Aktif['Hasil'];
//DATA TERARSIP
$count_value_where = array("Terarsip");
$hitung_Terarsip = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terarsip = $hitung_Terarsip['Hasil'];
//DATA TERHAPUS (SAMPAH)
$count_value_where = array("Terhapus");
$hitung_Terhapus = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terhapus = $hitung_Terhapus['Hasil'];
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	if (isset($_GET['filter_status'])) {
		$filter_status = $_GET['filter_status'];
	} else {
		$filter_status = "Aktif";
	}

	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("$filter_status");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_datatable_master = $result['Hasil'];
	}
}
