<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pengguna = new a_data_pengguna();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN

//HAPUS DUPLIKAT DATA PADA SELECT OPTION
function Hapus_Duplikat_Data_Pada_Select_Option($array_object, $Hapus_Berdasarkan_Nama_Field)
{
	$data = $array_object;
	$uniqueValues = array();
	$filteredData = array();

	foreach ($data as $item) {
		$hapus_berdasarkan_field = $item[$Hapus_Berdasarkan_Nama_Field];

		if (!in_array($hapus_berdasarkan_field, $uniqueValues)) {
			$uniqueValues[] = $hapus_berdasarkan_field;
			$filteredData[] = $item;
		}
	}

	$filteredJsonData = $filteredData;
	return $filteredJsonData;
}

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI SIMPAN DATA (CREATE)
if (isset($_POST['submit_simpan'])) {
	$_POST['Password'] = $a_hash_password->hash_password($_POST['Password']);

	$form_field = array("Id_Pengguna", "Username", "Password", "Id_Hak_Akses", "Email", "Nama_Lengkap", "Jenis_Kelamin", "Tempat_Lahir", "Tanggal_Lahir", "Alamat_Lengkap", "Nomor_Handphone", "Waktu_Simpan_Data", "Status");

	$form_value = array(NULL, "$_POST[Username]", "$_POST[Password]", "$_POST[Id_Hak_Akses]", "$_POST[Email]", "$_POST[Nama_Lengkap]", "$_POST[Jenis_Kelamin]", "$_POST[Tempat_Lahir]", "$_POST[Tanggal_Lahir]", "$_POST[Alamat_Lengkap]", "$_POST[Nomor_Handphone]", "$Waktu_Sekarang", "Aktif");

	$result = $a_data_pengguna->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		echo "<script>alert('Data Tersimpan');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menyimpan Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
if (isset($_GET['edit'])) {

	$result = $a_data_pengguna->baca_data_id("Id_Pengguna", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit = $result['Hasil'];
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	if ($_POST['Password'] <> "") {
		$_POST['Password'] = $a_hash_password->hash_password($_POST['Password']);

		$form_field = array("Username", "Password", "Id_Hak_Akses", "Email", "Nama_Lengkap", "Jenis_Kelamin", "Tempat_Lahir", "Tanggal_Lahir", "Alamat_Lengkap", "Nomor_Handphone", "Waktu_Update_Data");

		$form_value = array("$_POST[Username]", "$_POST[Password]", "$_POST[Id_Hak_Akses]", "$_POST[Email]", "$_POST[Nama_Lengkap]", "$_POST[Jenis_Kelamin]", "$_POST[Tempat_Lahir]", "$_POST[Tanggal_Lahir]", "$_POST[Alamat_Lengkap]", "$_POST[Nomor_Handphone]", "$Waktu_Sekarang");

	} else {
		$form_field = array("Username", "Id_Hak_Akses", "Email", "Nama_Lengkap", "Jenis_Kelamin", "Tempat_Lahir", "Tanggal_Lahir", "Alamat_Lengkap", "Nomor_Handphone", "Waktu_Update_Data");

		$form_value = array("$_POST[Username]", "$_POST[Id_Hak_Akses]", "$_POST[Email]", "$_POST[Nama_Lengkap]", "$_POST[Jenis_Kelamin]", "$_POST[Tempat_Lahir]", "$_POST[Tanggal_Lahir]", "$_POST[Alamat_Lengkap]", "$_POST[Nomor_Handphone]", "$Waktu_Sekarang");
	}

	$form_field_where = array("Id_Pengguna");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pengguna->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI DELETE DATA (DELETE)
if (isset($_GET['hapus_data_ke_tong_sampah'])) {

	$result = $a_data_pengguna->hapus_data_ke_tong_sampah("Id_Pengguna", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pengguna");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pengguna->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['arsip_data'])) {

	$result = $a_data_pengguna->arsip_data("Id_Pengguna", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pengguna");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pengguna->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Dipindahkan Ke Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Memindahkan Data Ke Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_arsip'])) {

	$result = $a_data_pengguna->restore_data_dari_arsip("Id_Pengguna", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pengguna");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pengguna->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Berhasil Di Keluarkan Dari Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengeluarkan Data Dari Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_tong_sampah'])) {

	$result = $a_data_pengguna->restore_data_dari_tong_sampah("Id_Pengguna", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pengguna");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pengguna->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Di Restore Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Restore Data Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['hapus_data_permanen'])) {

	$result = $a_data_pengguna->hapus_data_permanen("Id_Pengguna", $Get_Id_Primary);
	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pengguna");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pengguna->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus Permanen');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI HITUNG DATA (COUNT)

$count_field_where = array("Status");

$count_criteria_where = array("=");

$count_connector_where = array("");

//DATA AKTIF
$count_value_where = array("Aktif");
$hitung_Aktif = $a_data_pengguna->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Aktif = $hitung_Aktif['Hasil'];
//DATA TERARSIP
$count_value_where = array("Terarsip");
$hitung_Terarsip = $a_data_pengguna->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terarsip = $hitung_Terarsip['Hasil'];
//DATA TERHAPUS (SAMPAH)
$count_value_where = array("Terhapus");
$hitung_Terhapus = $a_data_pengguna->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terhapus = $hitung_Terhapus['Hasil'];
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	if (isset($_GET['filter_status'])) {
		$filter_status = $_GET['filter_status'];
	} else {
		$filter_status = "Aktif";
	}

	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("$filter_status");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_pengguna->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_datatable_master = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA HAK AKSES
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_hak_akses = $result['Hasil'];
	}
}
?>