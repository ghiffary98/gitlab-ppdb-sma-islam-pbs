<?php 
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_hak_akses = new a_data_hak_akses();
$a_data_hak_akses_detail = new a_data_hak_akses_detail();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if(isset($_GET['url_kembali'])){
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
}else{
	if(isset($_GET['filter_status'])){
		$kehalaman = "?menu=".$_GET['menu']."&filter_status=".$_GET['filter_status'];
	}else{
		$kehalaman = "?menu=".$_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if(isset($_GET['id'])){
	$Get_Id_Primary = $a_hash->decode($_GET['id'],$_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
#-----------------------------------------------------------------------------------
//UNTUK TABLE DETAIL HAK AKSES MENGENAI BACA TAMBAH UPDATE HAPUS
if(((isset($_GET["tambah"])) OR (isset($_GET["edit"])))){
	$array_List_Halaman_Akses = [	    
		[
		    "Kode_Halaman" => "Verifikasi_Pendaftar",
		    "Nama_Halaman" => "Verifikasi Pendaftar",
		    "Sembunyikan" => ["Simpan"],
		],
		[
		    "Kode_Halaman" => "Verifikasi_Pembelian_Formulir",
		    "Nama_Halaman" => "Verifikasi Pembelian Formulir",
		    "Sembunyikan" => ["Simpan"],
		],
		[
		    "Kode_Halaman" => "Verifikasi_Data_Diri",
		    "Nama_Halaman" => "Verifikasi Data Diri",
		    "Sembunyikan" => ["Simpan"],
		],
		[
		    "Kode_Halaman" => "Verifikasi_Nilai_Rapor",
		    "Nama_Halaman" => "Verifikasi Nilai Rapor",
		    "Sembunyikan" => ["Simpan"],
		],
		[
		    "Kode_Halaman" => "Verifikasi_Program_Layanan",
		    "Nama_Halaman" => "Verifikasi Program Layanan",
		    "Sembunyikan" => ["Simpan"],
		],
		[
		    "Kode_Halaman" => "Verifikasi_Verifikasi_Berkas",
		    "Nama_Halaman" => "Verifikasi Verifikasi Berkas",
		    "Sembunyikan" => ["Simpan"],
		],
		[
		    "Kode_Halaman" => "Verifikasi_Pembayaran_PPDB",
		    "Nama_Halaman" => "Verifikasi Pembayaran PPDB",
		    "Sembunyikan" => ["Simpan"],
		],
		[
		    "Kode_Halaman" => "Verifikasi_Kelulusan",
		    "Nama_Halaman" => "Verifikasi Kelulusan",
		    "Sembunyikan" => ["Simpan","Hapus"],
		],
		[
		    "Kode_Halaman" => "Laporan_Rekap_Pendaftar",
		    "Nama_Halaman" => "Laporan Rekap Pendaftar",
		    "Sembunyikan" => ["Simpan","Update","Hapus"],
		],
		[
		    "Kode_Halaman" => "Laporan_Rekap_Pembelian_Formulir",
		    "Nama_Halaman" => "Laporan Rekap Pembelian Formulir",
		    "Sembunyikan" => ["Simpan","Update","Hapus"],
		],
		[
		    "Kode_Halaman" => "Laporan_Lolos_Berkas",
		    "Nama_Halaman" => "Laporan Lolos Berkas",
		    "Sembunyikan" => ["Simpan","Update","Hapus"],
		],
		[
		    "Kode_Halaman" => "Laporan_Rekap_Lunas_PPDB",
		    "Nama_Halaman" => "Laporan Rekap Lunas PPDB",
		    "Sembunyikan" => ["Simpan","Update","Hapus"],
		],
		[
		    "Kode_Halaman" => "Laporan_Siswa_Diterima",
		    "Nama_Halaman" => "Laporan Siswa Diterima",
		    "Sembunyikan" => ["Simpan","Update","Hapus"],
		],
		[
		    "Kode_Halaman" => "Data_Siswa",
		    "Nama_Halaman" => "Data Siswa",
		],
		[
		    "Kode_Halaman" => "Data_Pengumuman",
		    "Nama_Halaman" => "Data Pengumuman",
		],
		[
		    "Kode_Halaman" => "Data_PPDB",
		    "Nama_Halaman" => "Data PPDB",
		],
		[
		    "Kode_Halaman" => "Pengaturan_Sekolah",
		    "Nama_Halaman" => "Pengaturan Sekolah",
		    "Sembunyikan" => ["Simpan", "Hapus"],
		],
		[
		    "Kode_Halaman" => "Pengaturan_Asal_Sekolah",
		    "Nama_Halaman" => "Pengaturan Asal Sekolah",
		],
		[
		    "Kode_Halaman" => "Pengaturan_Pekerjaan_Orang_Tua",
		    "Nama_Halaman" => "Pengaturan Pekerjaan Orang Tua",
		],
		[
		    "Kode_Halaman" => "Pengaturan_Penghasilan_Orang_Tua",
		    "Nama_Halaman" => "Pengaturan Penghasilan Orang Tua",
		],
		[
		    "Kode_Halaman" => "Pengaturan_Kendaraan_Yang_Dipakai_Kesekolah",
		    "Nama_Halaman" => "Pengaturan Kendaraan Yang Dipakai Kesekolah",
		],
	];
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI SIMPAN DATA (CREATE)
if(isset($_POST['submit_simpan'])){
	$form_field = array("Id_Hak_Akses","Hak_Akses","Deskripsi","Waktu_Simpan_Data","Status");

	$form_value = array(NULL,"$_POST[Hak_Akses]","$_POST[Deskripsi]","$Waktu_Sekarang","Aktif");

	$result = $a_data_hak_akses->tambah_data($form_field,$form_value);

	if($result['Status'] == "Sukses"){
		$Id_Hak_Akses = $result['Id'];
		if ((isset($array_List_Halaman_Akses))) {
			foreach ($array_List_Halaman_Akses as $data) {
				$Kode_Halaman = $data['Kode_Halaman'];
				$Nama_Halaman = $_POST['Nama_Halaman_' . $Kode_Halaman];

				if(isset($_POST['Baca_Data_' . $Kode_Halaman])){
					$Baca_Data = "Iya";
				}else{
					$Baca_Data = "";
				}
				
				if(isset($_POST['Simpan_Data_' . $Kode_Halaman])){
					$Simpan_Data = "Iya";
				}else{
					$Simpan_Data = "";
				}
				
				if(isset($_POST['Update_Data_' . $Kode_Halaman])){
					$Update_Data = "Iya";
				}else{
					$Update_Data = "";
				}
				
				if(isset($_POST['Hapus_Data_' . $Kode_Halaman])){
					$Hapus_Data = "Iya";
				}else{
					$Hapus_Data = "";
				}
				


				$form_field = array("Id_Hak_Akses_Detail","Id_Hak_Akses","Nama_Halaman","Kode_Halaman","Baca_Data","Simpan_Data","Update_Data","Hapus_Data");

				$form_value = array(NULL,"$Id_Hak_Akses","$Nama_Halaman","$Kode_Halaman","$Baca_Data","$Simpan_Data","$Update_Data","$Hapus_Data");

				$result = $a_data_hak_akses_detail->tambah_data($form_field,$form_value);
			}
		}

		echo "<script>alert('Data Tersimpan');document.location.href='$kehalaman'</script>";
	}else{
		echo "<script>alert('Terjadi Kesalahan Saat Menyimpan Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
if(isset($_GET['edit'])){

	$result = $a_data_hak_akses->baca_data_id("Id_Hak_Akses",$Get_Id_Primary);

	if($result['Status'] == "Sukses"){
		$edit = $result['Hasil'];

		$search_field_where = array("Id_Hak_Akses");
		$search_criteria_where = array("=");
		$search_value_where = array("$Get_Id_Primary");
		$search_connector_where = array("");

		$nomor = 0;

		$result = $a_data_hak_akses_detail->baca_data_dengan_filter($search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);

		if($result['Status'] == "Sukses"){
			$edit_hak_akses_detail = $result['Hasil'];
		}
	}
	else{
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if(isset($_POST['submit_update']))
{
	$form_field = array("Hak_Akses","Deskripsi","Waktu_Update_Data");

	$form_value = array("$_POST[Hak_Akses]","$_POST[Deskripsi]","$Waktu_Sekarang");

	$form_field_where = array("Id_Hak_Akses");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_hak_akses->update_data($form_field,$form_value,$form_field_where,$form_criteria_where,$form_value_where,$form_connector_where);

	if($result['Status'] == "Sukses"){

		if ((isset($array_List_Halaman_Akses))) {
			foreach ($array_List_Halaman_Akses as $data) {
				$count_field_where = array("Id_Hak_Akses","Kode_Halaman");
				$count_criteria_where = array("=","=");
				$count_value_where = array("$Get_Id_Primary",$data['Kode_Halaman']);
				$count_connector_where = array("AND","");

				$cek_data = $a_data_hak_akses_detail->hitung_data_dengan_filter($count_field_where,$count_criteria_where,$count_value_where,$count_connector_where);
				$cek_data = $cek_data['Hasil'];

				if($cek_data > 0){
					//UPDATE DATA
					$Kode_Halaman = $data['Kode_Halaman'];
					$Nama_Halaman = $_POST['Nama_Halaman_' . $Kode_Halaman];

					if(isset($_POST['Baca_Data_' . $Kode_Halaman])){
						$Baca_Data = "Iya";
					}else{
						$Baca_Data = "";
					}
					
					if(isset($_POST['Simpan_Data_' . $Kode_Halaman])){
						$Simpan_Data = "Iya";
					}else{
						$Simpan_Data = "";
					}
					
					if(isset($_POST['Update_Data_' . $Kode_Halaman])){
						$Update_Data = "Iya";
					}else{
						$Update_Data = "";
					}
					
					if(isset($_POST['Hapus_Data_' . $Kode_Halaman])){
						$Hapus_Data = "Iya";
					}else{
						$Hapus_Data = "";
					}
					

					$form_field = array("Baca_Data","Simpan_Data","Update_Data","Hapus_Data");

					$form_value = array("$Baca_Data","$Simpan_Data","$Update_Data","$Hapus_Data");

					$form_field_where = array("Id_Hak_Akses","Kode_Halaman");
					$form_criteria_where = array("=","=");
					$form_value_where = array("$Get_Id_Primary",$data['Kode_Halaman']);
					$form_connector_where = array("AND","");

					$result = $a_data_hak_akses_detail->update_data($form_field,$form_value,$form_field_where,$form_criteria_where,$form_value_where,$form_connector_where);
				}else{
					//TAMBAH DATA BARU
					$Kode_Halaman = $data['Kode_Halaman'];
					$Nama_Halaman = $_POST['Nama_Halaman_' . $Kode_Halaman];

					if(isset($_POST['Baca_Data_' . $Kode_Halaman])){
						$Baca_Data = "Iya";
					}else{
						$Baca_Data = "";
					}
					
					if(isset($_POST['Simpan_Data_' . $Kode_Halaman])){
						$Simpan_Data = "Iya";
					}else{
						$Simpan_Data = "";
					}
					
					if(isset($_POST['Update_Data_' . $Kode_Halaman])){
						$Update_Data = "Iya";
					}else{
						$Update_Data = "";
					}
					
					if(isset($_POST['Hapus_Data_' . $Kode_Halaman])){
						$Hapus_Data = "Iya";
					}else{
						$Hapus_Data = "";
					}
					

					$form_field = array("Id_Hak_Akses_Detail","Id_Hak_Akses","Nama_Halaman","Kode_Halaman","Baca_Data","Simpan_Data","Update_Data","Hapus_Data");

					$form_value = array(NULL,"$Get_Id_Primary","$Nama_Halaman","$Kode_Halaman","$Baca_Data","$Simpan_Data","$Update_Data","$Hapus_Data");

					$result = $a_data_hak_akses_detail->tambah_data($form_field,$form_value);
				}

			}
		}

		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";
	}else{
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI DELETE DATA (DELETE)
if(isset($_GET['hapus_data_ke_tong_sampah'])){

	$result = $a_data_hak_akses->hapus_data_ke_tong_sampah("Id_Hak_Akses",$Get_Id_Primary);

	if($result['Status'] == "Sukses"){

		$form_field = array("Waktu_Update_Data");
		$form_field_where = array("Id_Hak_Akses");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$form_value = array("$Waktu_Sekarang");
		$result = $a_data_hak_akses->update_data($form_field,$form_value,$form_field_where,$form_criteria_where,$form_value_where,$form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus');document.location.href='$kehalaman'</script>";
	}else{
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

if(isset($_GET['arsip_data'])){

	$result = $a_data_hak_akses->arsip_data("Id_Hak_Akses",$Get_Id_Primary);

	if($result['Status'] == "Sukses"){
		
		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Hak_Akses");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_hak_akses->update_data($form_field,$form_value,$form_field_where,$form_criteria_where,$form_value_where,$form_connector_where);

		echo "<script>alert('Data Berhasil Dipindahkan Ke Arsip');document.location.href='$kehalaman'</script>";
	}else{
		echo "<script>alert('Terjadi Kesalahan Saat Memindahkan Data Ke Arsip');document.location.href='$kehalaman'</script>";
	}

}

if(isset($_GET['restore_data_dari_arsip'])){

	$result = $a_data_hak_akses->restore_data_dari_arsip("Id_Hak_Akses",$Get_Id_Primary);

	if($result['Status'] == "Sukses"){
		
		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Hak_Akses");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_hak_akses->update_data($form_field,$form_value,$form_field_where,$form_criteria_where,$form_value_where,$form_connector_where);

		echo "<script>alert('Data Berhasil Berhasil Di Keluarkan Dari Arsip');document.location.href='$kehalaman'</script>";
	}else{
		echo "<script>alert('Terjadi Kesalahan Saat Mengeluarkan Data Dari Arsip');document.location.href='$kehalaman'</script>";
	}

}

if(isset($_GET['restore_data_dari_tong_sampah'])){

	$result = $a_data_hak_akses->restore_data_dari_tong_sampah("Id_Hak_Akses",$Get_Id_Primary);

	if($result['Status'] == "Sukses"){
		
		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Hak_Akses");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_hak_akses->update_data($form_field,$form_value,$form_field_where,$form_criteria_where,$form_value_where,$form_connector_where);

		echo "<script>alert('Data Berhasil Di Restore Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	}else{
		echo "<script>alert('Terjadi Kesalahan Saat Restore Data Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	}

}

if(isset($_GET['hapus_data_permanen'])){

	$result = $a_data_hak_akses->hapus_data_permanen("Id_Hak_Akses",$Get_Id_Primary);
	if($result['Status'] == "Sukses"){
		
		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Hak_Akses");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_hak_akses->update_data($form_field,$form_value,$form_field_where,$form_criteria_where,$form_value_where,$form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus Permanen');document.location.href='$kehalaman'</script>";
	}else{
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI HITUNG DATA (COUNT)

$count_field_where = array("Status");

$count_criteria_where = array("=");

$count_connector_where = array("");

//DATA AKTIF
$count_value_where = array("Aktif");
$hitung_Aktif = $a_data_hak_akses->hitung_data_dengan_filter($count_field_where,$count_criteria_where,$count_value_where,$count_connector_where);
$hitung_Aktif = $hitung_Aktif['Hasil'];
//DATA TERARSIP
$count_value_where = array("Terarsip");
$hitung_Terarsip = $a_data_hak_akses->hitung_data_dengan_filter($count_field_where,$count_criteria_where,$count_value_where,$count_connector_where);
$hitung_Terarsip = $hitung_Terarsip['Hasil'];
//DATA TERHAPUS (SAMPAH)
$count_value_where = array("Terhapus");
$hitung_Terhapus = $a_data_hak_akses->hitung_data_dengan_filter($count_field_where,$count_criteria_where,$count_value_where,$count_connector_where);
$hitung_Terhapus = $hitung_Terhapus['Hasil'];
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if(!((isset($_GET["tambah"])) OR (isset($_GET["edit"])))){
	if(isset($_GET['filter_status'])){
		$filter_status = $_GET['filter_status'];
	}else{
		$filter_status = "Aktif";
	}

	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("$filter_status");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);

	if($result['Status'] == "Sukses"){
		$list_datatable_master = $result['Hasil'];
	}
}

?>