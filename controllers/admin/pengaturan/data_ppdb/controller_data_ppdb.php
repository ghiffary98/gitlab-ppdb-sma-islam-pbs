<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_ppdb = new a_data_ppdb();
$a_data_ppdb_syarat_dan_ketentuan = new a_data_ppdb_syarat_dan_ketentuan();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI HAK AKSES
$Kode_Halaman = "Data_PPDB";
$Nama_Halaman = "Data PPDB";

$Link_Redirect_Tidak_Diberi_Akses_Baca_Data = "?menu=home";
$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data = $kehalaman;
$Link_Redirect_Tidak_Diberi_Akses_Arsip_Restore_Hapus_Data = $kehalaman;

$search_field_where = array("Status", "Id_Hak_Akses");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Hak_Akses]");
$search_connector_where = array("AND", "");

$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$data_hak_akses = $result['Hasil'][0];
}

if (isset($data_hak_akses)) {
	if ($data_hak_akses['Hak_Akses'] == "Super Administrator") {

	} else {
		$search_field_where = array("Id_Hak_Akses", "Kode_Halaman", "Nama_Halaman");
		$search_criteria_where = array("=", "=", "=");
		$search_value_where = array("$u_data_user[Id_Hak_Akses]", "$Kode_Halaman", "$Nama_Halaman");
		$search_connector_where = array("AND", "AND", "");

		$result = $a_data_hak_akses_detail->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

		if ($result['Status'] == "Sukses") {
			$data_hak_akses_detail = $result['Hasil'][0];
		}

		if (isset($data_hak_akses_detail)) {
			//CEK HAK AKSES BACA DATA
			if ($data_hak_akses_detail['Baca_Data'] <> "Iya") {
				echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
				exit();
			}

			//CEK HAK AKSES SIMPAN DATA
			if (isset($_POST['submit_simpan'])) {
				if ($data_hak_akses_detail['Simpan_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menambahkan data');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES UPDATE DATA
			if (isset($_POST['submit_update'])) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			if ((isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah']))) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES HAPUS DATA
			if ((isset($_GET['hapus_data_ke_tong_sampah'])) or (isset($_GET['hapus_data_permanen']))) {
				if ($data_hak_akses_detail['Hapus_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menghapus data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Hapus_Data'</script>";
					exit();
				}
			}
		} else {
			echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
			exit();
		}
	}
} else {
	echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
	exit();
}

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
$folder_penyimpanan_file_foto_alur_pendaftaran = "media/foto_alur_pendaftaran/";
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI SIMPAN DATA (CREATE)
if (isset($_POST['submit_simpan'])) {

	$form_field = array("Id_PPDB", "Tahun_Ajaran", "Judul", "Deskripsi", "Tanggal_Mulai_Pendaftaran", "Tanggal_Akhir_Pendaftaran", "No_Rekening_Utama", "No_Rekening_Cadangan", "Biaya_Pendaftaran_Formulir", "Biaya_PPDB", "Biaya_Admin_Xendit", "Masa_Waktu_Pembayaran_Pembelian_Formulir", "Masa_Waktu_Pengisian_Berkas", "Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi", "Umur_Maksimal_Pendaftar", "Total_Target_Siswa", "Informasi_Tambahan", "Status_PPDB", "Waktu_Simpan_Data", "Status");

	$form_value = array(NULL, "$_POST[Tahun_Ajaran]", "$_POST[Judul]", "$_POST[Deskripsi]", "$_POST[Tanggal_Mulai_Pendaftaran]", "$_POST[Tanggal_Akhir_Pendaftaran]", "$_POST[No_Rekening_Utama]", "$_POST[No_Rekening_Cadangan]", "$_POST[Biaya_Pendaftaran_Formulir]", "$_POST[Biaya_PPDB]", "$_POST[Biaya_Admin_Xendit]", "$_POST[Masa_Waktu_Pembayaran_Pembelian_Formulir]", "$_POST[Masa_Waktu_Pengisian_Berkas]", "$_POST[Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi]", "$_POST[Umur_Maksimal_Pendaftar]", "$_POST[Total_Target_Siswa]", "$_POST[Informasi_Tambahan]", "$_POST[Status_PPDB]", "$Waktu_Sekarang", "Aktif");

	$result = $a_data_ppdb->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		$Id_PPDB_Ini = $result['Id'];

		//FUNGSI UPLOAD FILE Foto_Alur_Pendaftaran
		if ($_FILES['Foto_Alur_Pendaftaran']['size'] <> 0 && $_FILES['Foto_Alur_Pendaftaran']['error'] == 0) {
			$post_file_upload = $_FILES['Foto_Alur_Pendaftaran'];
			$path_file_upload = $_FILES['Foto_Alur_Pendaftaran']['name'];
			$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
			$ukuran_file_upload = $_FILES['Foto_Alur_Pendaftaran']['size'];
			$nama_file_upload = "foto_alur_pendaftaran_" . $Id_PPDB_Ini;
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_foto_alur_pendaftaran;
			$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
			$maksimum_ukuran_file_upload = 100000000;

			$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

			if ($result_upload_file['Status'] == "Sukses") {
				$form_field = array("Foto_Alur_Pendaftaran");
				$form_value = array("$nama_file_upload.$ext_file_upload");
				$form_field_where = array("Id_PPDB");
				$form_criteria_where = array("=");
				$form_value_where = array("$Id_PPDB_Ini");
				$form_connector_where = array("");

				$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			} else {
			}
		}
		//FUNGSI UPLOAD FILE Foto_Alur_Pendaftaran

		echo "<script>alert('Data Tersimpan');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menyimpan Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------
#FUNGSI SIMPAN DATA (CREATE)
if (isset($_POST['submit_simpan_update_syarat_dan_ketentuan'])) {
	if (isset($_POST['Syarat_Dan_Ketentuan_Data_Ketentuan_Umum'])) {

		$result = $a_data_ppdb_syarat_dan_ketentuan->hapus_data_permanen("Id_PPDB", "$Get_Id_Primary", "Iya");


		foreach ($_POST['Syarat_Dan_Ketentuan_Data_Ketentuan_Umum'] as $Syarat_Dan_Ketentuan_Data_Ketentuan_Umum) {
			$form_field = array("Id_Syarat_Dan_Ketentuan", "Id_PPDB", "Syarat_Dan_Ketentuan", "Posisi", "Waktu_Update_Data", "Waktu_Simpan_Data", "Status");
			$form_value = array(NULL, "$Get_Id_Primary", "$Syarat_Dan_Ketentuan_Data_Ketentuan_Umum", "Ketentuan Umum", "$Waktu_Sekarang", "$Waktu_Sekarang", "Aktif");
			$result = $a_data_ppdb_syarat_dan_ketentuan->tambah_data($form_field, $form_value);
		}
		foreach ($_POST['Syarat_Dan_Ketentuan_Data_Catatan'] as $Syarat_Dan_Ketentuan_Data_Catatan) {
			$form_field = array("Id_Syarat_Dan_Ketentuan", "Id_PPDB", "Syarat_Dan_Ketentuan", "Posisi", "Waktu_Update_Data", "Waktu_Simpan_Data", "Status");
			$form_value = array(NULL, "$Get_Id_Primary", "$Syarat_Dan_Ketentuan_Data_Catatan", "Catatan", "$Waktu_Sekarang", "$Waktu_Sekarang", "Aktif");
			$result = $a_data_ppdb_syarat_dan_ketentuan->tambah_data($form_field, $form_value);
		}


		if ($result['Status'] == "Sukses") {
			echo "<script>alert('Data Tersimpan');document.location.href='$Link_Sekarang'</script>";
		} else {
			echo "<script>alert('Terjadi Kesalahan Saat Menyimpan Data');document.location.href='$Link_Sekarang'</script>";
		}
	} else {
		echo "<script>alert('Data Tersimpan');document.location.href='$Link_Sekarang'</script>";
	}
}


#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
if (isset($_GET['edit'])) {

	$result = $a_data_ppdb->baca_data_id("Id_PPDB", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit = $result['Hasil'];
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	$form_field = array("Tahun_Ajaran", "Judul", "Deskripsi", "Tanggal_Mulai_Pendaftaran", "Tanggal_Akhir_Pendaftaran", "No_Rekening_Utama", "No_Rekening_Cadangan", "Biaya_Pendaftaran_Formulir", "Biaya_PPDB", "Biaya_Admin_Xendit", "Masa_Waktu_Pembayaran_Pembelian_Formulir", "Masa_Waktu_Pengisian_Berkas", "Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi", "Umur_Maksimal_Pendaftar", "Total_Target_Siswa", "Informasi_Tambahan", "Status_PPDB", "Waktu_Update_Data");

	$form_value = array("$_POST[Tahun_Ajaran]", "$_POST[Judul]", "$_POST[Deskripsi]", "$_POST[Tanggal_Mulai_Pendaftaran]", "$_POST[Tanggal_Akhir_Pendaftaran]", "$_POST[No_Rekening_Utama]", "$_POST[No_Rekening_Cadangan]", "$_POST[Biaya_Pendaftaran_Formulir]", "$_POST[Biaya_PPDB]", "$_POST[Biaya_Admin_Xendit]", "$_POST[Masa_Waktu_Pembayaran_Pembelian_Formulir]", "$_POST[Masa_Waktu_Pengisian_Berkas]", "$_POST[Masa_Waktu_Pembayaran_PPDB_Setelah_Semua_Terverifikasi]", "$_POST[Umur_Maksimal_Pendaftar]", "$_POST[Total_Target_Siswa]", "$_POST[Informasi_Tambahan]", "$_POST[Status_PPDB]", "$Waktu_Sekarang");

	$form_field_where = array("Id_PPDB");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		//FUNGSI UPLOAD FILE Foto_Alur_Pendaftaran
		if ($_FILES['Foto_Alur_Pendaftaran']['size'] <> 0 && $_FILES['Foto_Alur_Pendaftaran']['error'] == 0) {
			$post_file_upload = $_FILES['Foto_Alur_Pendaftaran'];
			$path_file_upload = $_FILES['Foto_Alur_Pendaftaran']['name'];
			$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
			$ukuran_file_upload = $_FILES['Foto_Alur_Pendaftaran']['size'];
			$nama_file_upload = "foto_alur_pendaftaran_" . $Get_Id_Primary;
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_foto_alur_pendaftaran;
			$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
			$maksimum_ukuran_file_upload = 100000000;

			$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

			if ($result_upload_file['Status'] == "Sukses") {
				$form_field = array("Foto_Alur_Pendaftaran");
				$form_value = array("$nama_file_upload.$ext_file_upload");
				$form_field_where = array("Id_PPDB");
				$form_criteria_where = array("=");
				$form_value_where = array("$Get_Id_Primary");
				$form_connector_where = array("");

				$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			} else {
			}
		}
		//FUNGSI UPLOAD FILE Foto_Alur_Pendaftaran

		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI DELETE DATA (DELETE)
if (isset($_GET['hapus_data_ke_tong_sampah'])) {

	$result = $a_data_ppdb->hapus_data_ke_tong_sampah("Id_PPDB", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_PPDB");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['arsip_data'])) {

	$result = $a_data_ppdb->arsip_data("Id_PPDB", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_PPDB");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Dipindahkan Ke Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Memindahkan Data Ke Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_arsip'])) {

	$result = $a_data_ppdb->restore_data_dari_arsip("Id_PPDB", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_PPDB");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Berhasil Di Keluarkan Dari Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengeluarkan Data Dari Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_tong_sampah'])) {

	$result = $a_data_ppdb->restore_data_dari_tong_sampah("Id_PPDB", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_PPDB");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Di Restore Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Restore Data Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['hapus_data_permanen'])) {

	$result = $a_data_ppdb->hapus_data_permanen("Id_PPDB", $Get_Id_Primary);
	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_PPDB");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_ppdb->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus Permanen');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI HITUNG DATA (COUNT)

$count_field_where = array("Status");

$count_criteria_where = array("=");

$count_connector_where = array("");

//DATA AKTIF
$count_value_where = array("Aktif");
$hitung_Aktif = $a_data_ppdb->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Aktif = $hitung_Aktif['Hasil'];
//DATA TERARSIP
$count_value_where = array("Terarsip");
$hitung_Terarsip = $a_data_ppdb->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terarsip = $hitung_Terarsip['Hasil'];
//DATA TERHAPUS (SAMPAH)
$count_value_where = array("Terhapus");
$hitung_Terhapus = $a_data_ppdb->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terhapus = $hitung_Terhapus['Hasil'];
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	if (isset($_GET['filter_status'])) {
		$filter_status = $_GET['filter_status'];
	} else {
		$filter_status = "Aktif";
	}

	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("$filter_status");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_datatable_master = $result['Hasil'];
	}
}
#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK SYARAT DAN KETENTUAN
if (isset($_GET["edit"])) {
	$search_field_where = array("Status", "Id_PPDB", "Posisi");
	$search_criteria_where = array("=", "=", "=");
	$search_value_where = array("Aktif", "$Get_Id_Primary", "Ketentuan Umum");
	$search_connector_where = array("AND", "AND", "");

	$nomor = 0;

	$result = $a_data_ppdb_syarat_dan_ketentuan->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_syarat_dan_ketentuan_data_ketentuan = $result['Hasil'];
	}
}

if (isset($_GET["edit"])) {
	$search_field_where = array("Status", "Id_PPDB", "Posisi");
	$search_criteria_where = array("=", "=", "=");
	$search_value_where = array("Aktif", "$Get_Id_Primary", "Catatan");
	$search_connector_where = array("AND", "AND", "");

	$nomor = 0;

	$result = $a_data_ppdb_syarat_dan_ketentuan->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_syarat_dan_ketentuan_data_catatan = $result['Hasil'];
	}
}
?>