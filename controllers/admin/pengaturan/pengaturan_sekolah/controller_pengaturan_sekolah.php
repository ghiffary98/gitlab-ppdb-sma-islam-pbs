<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_pengaturan_sekolah = new a_pengaturan_sekolah();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI HAK AKSES
$Kode_Halaman = "Pengaturan_Sekolah";
$Nama_Halaman = "Pengaturan Sekolah";

$Link_Redirect_Tidak_Diberi_Akses_Baca_Data = "?menu=home";
$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data = $kehalaman;
$Link_Redirect_Tidak_Diberi_Akses_Arsip_Restore_Hapus_Data = $kehalaman;

$search_field_where = array("Status","Id_Hak_Akses");
$search_criteria_where = array("=","=");
$search_value_where = array("Aktif","$u_data_user[Id_Hak_Akses]");
$search_connector_where = array("AND","");

$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$data_hak_akses = $result['Hasil'][0];
}

if(isset($data_hak_akses)){
	if($data_hak_akses['Hak_Akses'] == "Super Administrator"){
		
	}else{
		$search_field_where = array("Id_Hak_Akses","Kode_Halaman","Nama_Halaman");
		$search_criteria_where = array("=","=","=");
		$search_value_where = array("$u_data_user[Id_Hak_Akses]","$Kode_Halaman","$Nama_Halaman");
		$search_connector_where = array("AND","AND","");

		$result = $a_data_hak_akses_detail->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

		if ($result['Status'] == "Sukses") {
			$data_hak_akses_detail = $result['Hasil'][0];
		}

		if(isset($data_hak_akses_detail)){
			//CEK HAK AKSES BACA DATA
			if($data_hak_akses_detail['Baca_Data'] <> "Iya"){
				echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
				exit();
			}

			//CEK HAK AKSES SIMPAN DATA
			if(isset($_POST['submit_simpan'])){
				if($data_hak_akses_detail['Simpan_Data'] <> "Iya"){
					echo "<script>alert('Anda tidak diberikan akses untuk menambahkan data');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES UPDATE DATA
			if(isset($_POST['submit_update'])){
				if($data_hak_akses_detail['Update_Data'] <> "Iya"){
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			if((isset($_GET['restore_data_dari_tong_sampah'])) OR (isset($_GET['restore_data_dari_tong_sampah'])) OR (isset($_GET['restore_data_dari_tong_sampah']))){
				if($data_hak_akses_detail['Update_Data'] <> "Iya"){
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES HAPUS DATA
			if((isset($_GET['hapus_data_ke_tong_sampah'])) OR (isset($_GET['hapus_data_permanen']))){
				if($data_hak_akses_detail['Hapus_Data'] <> "Iya"){
					echo "<script>alert('Anda tidak diberikan akses untuk menghapus data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Hapus_Data'</script>";
					exit();
				}
			}
		}else{
			echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
			exit();
		}
	}
}else{
	echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
	exit();
}

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI SIMPAN DATA (CREATE)
if (isset($_POST['submit_simpan'])) {

	$form_field = array("Id_Pengaturan_Sekolah", "Nama_Sekolah", "Deskripsi_Singkat", "Deksripsi_Lengkap", "Alamat_Lengkap", "Email_Admin", "Email_Customer_Service", "Email_Support", "Nomor_Telpon", "Nomor_Handphone", "Nama_Facebook", "Url_Facebook", "Nama_Instagram", "Url_Instagram", "Nama_Twitter", "Url_Twitter", "Nama_Linkedin", "Url_Linkedin", "Nama_Youtube", "Url_Youtube", "Embed_Google_Maps", "Google_Maps_Url", "Waktu_Simpan_Data");

	$form_value = array(NULL, "$_POST[Nama_Sekolah]", "$_POST[Deskripsi_Singkat]", "$_POST[Deksripsi_Lengkap]", "$_POST[Alamat_Lengkap]", "$_POST[Email_Admin]", "$_POST[Email_Customer_Service]", "$_POST[Email_Support]", "$_POST[Nomor_Telpon]", "$_POST[Nomor_Handphone]", "$_POST[Nama_Facebook]", "$_POST[Url_Facebook]", "$_POST[Nama_Instagram]", "$_POST[Url_Instagram]", "$_POST[Nama_Twitter]", "$_POST[Url_Twitter]", "$_POST[Nama_Linkedin]", "$_POST[Url_Linkedin]", "$_POST[Nama_Youtube]", "$_POST[Url_Youtube]", "$_POST[Embed_Google_Maps]", "$_POST[Google_Maps_Url]", "$Waktu_Sekarang");

	$result = $a_pengaturan_sekolah->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		echo "<script>alert('Data Tersimpan');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menyimpan Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
if (isset($_GET['edit'])) {

	$result = $a_pengaturan_sekolah->baca_data_id("Id_Pengaturan_Sekolah", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit = $result['Hasil'];
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	$form_field = array("Nama_Sekolah","Deskripsi_Singkat","Deksripsi_Lengkap","Alamat_Lengkap","Email_Admin","Email_Customer_Service","Email_Support","Nomor_Telpon","Nomor_Handphone","Nama_Facebook","Url_Facebook","Nama_Instagram","Url_Instagram","Nama_Twitter","Url_Twitter","Nama_Linkedin","Url_Linkedin","Nama_Youtube","Url_Youtube","Embed_Google_Maps","Google_Maps_Url","Waktu_Update_Data");

	$form_value = array("$_POST[Nama_Sekolah]","$_POST[Deskripsi_Singkat]","$_POST[Deksripsi_Lengkap]","$_POST[Alamat_Lengkap]","$_POST[Email_Admin]","$_POST[Email_Customer_Service]","$_POST[Email_Support]","$_POST[Nomor_Telpon]","$_POST[Nomor_Handphone]","$_POST[Nama_Facebook]","$_POST[Url_Facebook]","$_POST[Nama_Instagram]","$_POST[Url_Instagram]","$_POST[Nama_Twitter]","$_POST[Url_Twitter]","$_POST[Nama_Linkedin]","$_POST[Url_Linkedin]","$_POST[Nama_Youtube]","$_POST[Url_Youtube]","$_POST[Embed_Google_Maps]","$_POST[Google_Maps_Url]","$Waktu_Sekarang");

	$form_field_where = array("Id_Pengaturan_Sekolah");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_pengaturan_sekolah->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}

#-----------------------------------------------------------------------------------
#FUNGSI CEK APAKAH SUDAH ADA SATU DATA ATAU BELUM UNTUK PENGATURAN SEKOLAH
$count_field_where = array("Id_Pengaturan_Sekolah");
$count_criteria_where = array("<>");
$count_connector_where = array("");
$count_value_where = array("");
$hitung_data = $a_pengaturan_sekolah->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_data = $hitung_data['Hasil'];

if ($hitung_data > 0) {
	$search_field_where = array("Id_Pengaturan_Sekolah");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("");
	$result = $a_pengaturan_sekolah->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$data = $result['Hasil'][0];
	}
	if (!isset($_GET['edit'])) {
		echo "<script>document.location.href='$Link_Sekarang&edit&id=" . $a_hash->encode($data["Id_Pengaturan_Sekolah"], $_GET['menu']) . "'</script>";
	}
} else {
	if (!isset($_GET['tambah'])) {
		echo "<script>document.location.href='$Link_Sekarang&tambah'</script>";
	}
}
?>