<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if(isset($_GET['menu'])){
	if (isset($_GET['url_kembali'])) {
		$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
		$kehalaman = "$url_kembali";
	} else {
		if (isset($_GET['filter_status'])) {
			$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
		} else {
			$kehalaman = "?menu=" . $_GET['menu'];
		}
	}
}else{
	$kehalaman = "?menu=home";
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI HAK AKSES
	$Kode_Halaman = "Laporan_Rekap_Pembelian_Formulir";
	$Nama_Halaman = "Laporan Rekap Pembelian Formulir";

	$Link_Redirect_Tidak_Diberi_Akses_Baca_Data = "?menu=home";
	$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data = $kehalaman;
	$Link_Redirect_Tidak_Diberi_Akses_Arsip_Restore_Hapus_Data = $kehalaman;
	
	$search_field_where = array("Status","Id_Hak_Akses");
	$search_criteria_where = array("=","=");
	$search_value_where = array("Aktif","$u_data_user[Id_Hak_Akses]");
	$search_connector_where = array("AND","");

	$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$data_hak_akses = $result['Hasil'][0];
	}

	if(isset($data_hak_akses)){
		if($data_hak_akses['Hak_Akses'] == "Super Administrator"){
			
		}else{
			$search_field_where = array("Id_Hak_Akses","Kode_Halaman","Nama_Halaman");
			$search_criteria_where = array("=","=","=");
			$search_value_where = array("$u_data_user[Id_Hak_Akses]","$Kode_Halaman","$Nama_Halaman");
			$search_connector_where = array("AND","AND","");

			$result = $a_data_hak_akses_detail->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$data_hak_akses_detail = $result['Hasil'][0];
			}

			if(isset($data_hak_akses_detail)){
				//CEK HAK AKSES BACA DATA
				if($data_hak_akses_detail['Baca_Data'] <> "Iya"){
					echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
					exit();
				}

				//CEK HAK AKSES SIMPAN DATA
				if(isset($_POST['submit_simpan'])){
					if($data_hak_akses_detail['Simpan_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk menambahkan data');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
						exit();
					}
				}

				//CEK HAK AKSES UPDATE DATA
				if(isset($_POST['submit_update'])){
					if($data_hak_akses_detail['Update_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
						exit();
					}
				}

				if((isset($_GET['restore_data_dari_tong_sampah'])) OR (isset($_GET['restore_data_dari_tong_sampah'])) OR (isset($_GET['restore_data_dari_tong_sampah']))){
					if($data_hak_akses_detail['Update_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
						exit();
					}
				}

				if(isset($_GET['update_status_verifikasi'])){
					if($data_hak_akses_detail['Update_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
						exit();
					}
				}

				//CEK HAK AKSES HAPUS DATA
				if((isset($_GET['hapus_data_ke_tong_sampah'])) OR (isset($_GET['hapus_data_permanen']))){
					if($data_hak_akses_detail['Hapus_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk menghapus data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Hapus_Data'</script>";
						exit();
					}
				}
			}else{
				echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
				exit();
			}
		}
	}else{
		echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
		exit();
	}

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
$folder_penyimpanan_file_bukti_pembelian_formulir = "media/bukti_pembayaran_pembelian_formulir/";
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI HITUNG DATA (COUNT)

$count_field_where = array("Status","Status_Verifikasi_Pembelian_Formulir","(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_pembelian_formulir.Id_Pendaftar)");

$count_criteria_where = array("=","=","=");

$count_connector_where = array("AND","AND","");

//DATA AKTIF
$count_value_where = array("Aktif","Sudah Diverifikasi","$Id_PPDB_Saat_Ini");
$hitung_Aktif = $a_data_pendaftar_pembelian_formulir->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Aktif = $hitung_Aktif['Hasil'];
//DATA TERARSIP
$count_value_where = array("Terarsip","Sudah Diverifikasi","$Id_PPDB_Saat_Ini");
$hitung_Terarsip = $a_data_pendaftar_pembelian_formulir->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terarsip = $hitung_Terarsip['Hasil'];
//DATA TERHAPUS (SAMPAH)
$count_value_where = array("Terhapus","Sudah Diverifikasi","$Id_PPDB_Saat_Ini");
$hitung_Terhapus = $a_data_pendaftar_pembelian_formulir->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terhapus = $hitung_Terhapus['Hasil'];
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	if (isset($_GET['filter_status'])) {
		$filter_status = $_GET['filter_status'];
	} else {
		$filter_status = "Aktif";
	}

	

	$search_field_where = array("Status","Status_Verifikasi_Pembelian_Formulir","(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_pembelian_formulir.Id_Pendaftar)");
	$search_criteria_where = array("=","=","=");
	$search_value_where = array("$filter_status","Sudah Diverifikasi","$Id_PPDB_Saat_Ini");
	$search_connector_where = array("AND","AND","");

	$nomor = 0;

	$result = $a_data_pendaftar_pembelian_formulir->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_datatable_master = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PPDB
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_ppdb = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENDAFTAR
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_data_pendaftar->baca_data_dengan_filter($search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar;
		foreach ($array_result_relasi_data_pendaftar as $relasi_data_pendaftar) {
			$id_relasi_data_pendaftar = strval($relasi_data_pendaftar['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$id_relasi_data_pendaftar] = $relasi_data_pendaftar;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar" ID_PRIMARY "Id_Pendaftar"
}
?>