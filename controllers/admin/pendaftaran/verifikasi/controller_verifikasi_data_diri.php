<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_pengaturan_pekerjaan_orang_tua = new a_pengaturan_pekerjaan_orang_tua();
$a_pengaturan_penghasilan_orang_tua = new a_pengaturan_penghasilan_orang_tua();
$a_pengaturan_kendaraan_yang_dipakai_kesekolah = new a_pengaturan_kendaraan_yang_dipakai_kesekolah();
$a_pengaturan_asal_sekolah = new a_pengaturan_asal_sekolah();
$a_pengumuman_pendaftar = new a_pengumuman_pendaftar();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI HAK AKSES
$Kode_Halaman = "Verifikasi_Data_Diri";
$Nama_Halaman = "Verifikasi Data Diri";

$Link_Redirect_Tidak_Diberi_Akses_Baca_Data = "?menu=home";
$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data = $kehalaman;
$Link_Redirect_Tidak_Diberi_Akses_Arsip_Restore_Hapus_Data = $kehalaman;

$search_field_where = array("Status", "Id_Hak_Akses");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Hak_Akses]");
$search_connector_where = array("AND", "");

$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$data_hak_akses = $result['Hasil'][0];
}

if (isset($data_hak_akses)) {
	if ($data_hak_akses['Hak_Akses'] == "Super Administrator") {
	} else {
		$search_field_where = array("Id_Hak_Akses", "Kode_Halaman", "Nama_Halaman");
		$search_criteria_where = array("=", "=", "=");
		$search_value_where = array("$u_data_user[Id_Hak_Akses]", "$Kode_Halaman", "$Nama_Halaman");
		$search_connector_where = array("AND", "AND", "");

		$result = $a_data_hak_akses_detail->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

		if ($result['Status'] == "Sukses") {
			$data_hak_akses_detail = $result['Hasil'][0];
		}

		if (isset($data_hak_akses_detail)) {
			//CEK HAK AKSES BACA DATA
			if ($data_hak_akses_detail['Baca_Data'] <> "Iya") {
				echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
				exit();
			}

			//CEK HAK AKSES SIMPAN DATA
			if (isset($_POST['submit_simpan'])) {
				if ($data_hak_akses_detail['Simpan_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menambahkan data');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES UPDATE DATA
			if (isset($_POST['submit_update'])) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			if ((isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah']))) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES HAPUS DATA
			if ((isset($_GET['hapus_data_ke_tong_sampah'])) or (isset($_GET['hapus_data_permanen']))) {
				if ($data_hak_akses_detail['Hapus_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menghapus data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Hapus_Data'</script>";
					exit();
				}
			}
		} else {
			echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
			exit();
		}
	}
} else {
	echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
	exit();
}

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN

//HAPUS DUPLIKAT DATA PADA SELECT OPTION
function Hapus_Duplikat_Data_Pada_Select_Option($array_object, $Hapus_Berdasarkan_Nama_Field)
{
	$data = $array_object;
	$uniqueValues = array();
	$filteredData = array();

	foreach ($data as $item) {
		$hapus_berdasarkan_field = $item[$Hapus_Berdasarkan_Nama_Field];

		if (!in_array($hapus_berdasarkan_field, $uniqueValues)) {
			$uniqueValues[] = $hapus_berdasarkan_field;
			$filteredData[] = $item;
		}
	}

	$filteredJsonData = $filteredData;
	return $filteredJsonData;
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
if (isset($_GET['edit'])) {

	$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit = $result['Hasil'];
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	if (!isset($_POST['Jenis_Kelamin'])) {
		$_POST['Jenis_Kelamin'] = "";
	}

	if (!isset($_POST['Penerima_KPS'])) {
		$_POST['Penerima_KPS'] = "";
	}

	if (!isset($_POST['Penerima_KIP'])) {
		$_POST['Penerima_KIP'] = "";
	}

	if (!isset($_POST['Penerima_KJP_DKI'])) {
		$_POST['Penerima_KJP_DKI'] = "";
	}

	if (!isset($_POST['Status_Perwalian'])) {
		$_POST['Status_Perwalian'] = "";
	}

	$form_field = array("NIK", "NIS", "NISN", "NIK", "NIPD", "Nama_Lengkap", "Jenis_Kelamin", "Tempat_Lahir", "Tanggal_Lahir", "Agama", "Status_Dalam_Keluarga", "Jalan", "Kelurahan", "Kecamatan", "Kota", "Provinsi", "Id_Kelurahan", "Id_Kecamatan", "Id_Kota", "Id_Provinsi", "Kode_Pos", "Titik_Lintang_Alamat", "Titik_Bujur_Alamat", "Nomor_Handphone", "Nomor_Telepon", "Email", "Golongan_Darah", "Kebutuhan_Khusus", "Tinggi_Badan", "Berat_Badan", "Lingkar_Kepala", "Asal_Sekolah", "Alamat_Sekolah", "Kelas", "Nomor_Peserta_Ujian_Nasional", "Nomor_Seri_Ijazah", "Penerima_KPS", "Penerima_KIP", "Nomor_KIP", "Penerima_KJP_DKI", "Nomor_KJP_DKI", "Nomor_KPS", "Nomor_KKS", "Nomor_KK", "Nomor_Registrasi_Akta_Lahir", "Anak_Ke", "Jumlah_Saudara", "Status_Perwalian", "NIK_Ayah", "Nama_Ayah", "Tempat_Lahir_Ayah", "Tanggal_Lahir_Ayah", "Jenjang_Pendidikan_Ayah", "Pekerjaan_Ayah", "Penghasilan_Ayah", "No_HP_Ayah", "NIK_Ibu", "Nama_Ibu", "Tempat_Lahir_Ibu", "Tanggal_Lahir_Ibu", "Jenjang_Pendidikan_Ibu", "Pekerjaan_Ibu", "Penghasilan_Ibu", "No_HP_Ibu", "NIK_Wali", "Nama_Wali", "Tempat_Lahir_Wali", "Tanggal_Lahir_Wali", "Jenjang_Pendidikan_Wali", "Pekerjaan_Wali", "Penghasilan_Wali", "No_HP_Wali", "Kendaraan_Yang_Dipakai_Kesekolah", "Jarak_Rumah_Ke_Sekolah", "Ukuran_Seragam", "Status_Verifikasi_Data_Diri", "Waktu_Update_Data");

	$form_value = array("$_POST[NIK]", "$_POST[NIS]", "$_POST[NISN]", "$_POST[NIK]", "$_POST[NIPD]", "$_POST[Nama_Lengkap]", "$_POST[Jenis_Kelamin]", "$_POST[Tempat_Lahir]", "$_POST[Tanggal_Lahir]", "$_POST[Agama]", "$_POST[Status_Dalam_Keluarga]", "$_POST[Jalan]", "$_POST[Kelurahan]", "$_POST[Kecamatan]", "$_POST[Kota]", "$_POST[Provinsi]", "$_POST[Id_Kelurahan]", "$_POST[Id_Kecamatan]", "$_POST[Id_Kota]", "$_POST[Id_Provinsi]", "$_POST[Kode_Pos]", "$_POST[Titik_Lintang_Alamat]", "$_POST[Titik_Bujur_Alamat]", "$_POST[Nomor_Handphone]", "$_POST[Nomor_Telepon]", "$_POST[Email]", "$_POST[Golongan_Darah]", "$_POST[Kebutuhan_Khusus]", "$_POST[Tinggi_Badan]", "$_POST[Berat_Badan]", "$_POST[Lingkar_Kepala]", "$_POST[Asal_Sekolah]", "$_POST[Alamat_Sekolah]", "$_POST[Kelas]", "$_POST[Nomor_Peserta_Ujian_Nasional]", "$_POST[Nomor_Seri_Ijazah]", "$_POST[Penerima_KPS]", "$_POST[Penerima_KIP]", "$_POST[Nomor_KIP]", "$_POST[Penerima_KJP_DKI]", "$_POST[Nomor_KJP_DKI]", "$_POST[Nomor_KPS]", "$_POST[Nomor_KKS]", "$_POST[Nomor_KK]", "$_POST[Nomor_Registrasi_Akta_Lahir]", "$_POST[Anak_Ke]", "$_POST[Jumlah_Saudara]", "$_POST[Status_Perwalian]", "$_POST[NIK_Ayah]", "$_POST[Nama_Ayah]", "$_POST[Tempat_Lahir_Ayah]", "$_POST[Tanggal_Lahir_Ayah]", "$_POST[Jenjang_Pendidikan_Ayah]", "$_POST[Pekerjaan_Ayah]", "$_POST[Penghasilan_Ayah]", "$_POST[No_HP_Ayah]", "$_POST[NIK_Ibu]", "$_POST[Nama_Ibu]", "$_POST[Tempat_Lahir_Ibu]", "$_POST[Tanggal_Lahir_Ibu]", "$_POST[Jenjang_Pendidikan_Ibu]", "$_POST[Pekerjaan_Ibu]", "$_POST[Penghasilan_Ibu]", "$_POST[No_HP_Ibu]", "$_POST[NIK_Wali]", "$_POST[Nama_Wali]", "$_POST[Tempat_Lahir_Wali]", "$_POST[Tanggal_Lahir_Wali]", "$_POST[Jenjang_Pendidikan_Wali]", "$_POST[Pekerjaan_Wali]", "$_POST[Penghasilan_Wali]", "$_POST[No_HP_Wali]", "$_POST[Kendaraan_Yang_Dipakai_Kesekolah]", "$_POST[Jarak_Rumah_Ke_Sekolah]", "$_POST[Ukuran_Seragam]", "$_POST[Status_Verifikasi_Data_Diri]", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where, "Iya");

	if ($result['Status'] == "Sukses") {
		if($edit['Status_Verifikasi_Data_Diri'] <> $_POST['Status_Verifikasi_Data_Diri']){
			if($_POST['Status_Verifikasi_Data_Diri'] == "Sudah Diverifikasi"){
				$Judul_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri";
				$Isi_Pengumuman_Untuk_Pendaftar = "Selamat, Data Diri Anda 'Sudah Diverifikasi' Oleh Admin";

				Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);

				$result = $a_data_pendaftar->update_waktu_terakhir_semua_status_sudah_diverifikasi("$edit[Id_Pendaftar]");
				
			}

			if($_POST['Status_Verifikasi_Data_Diri'] == "Belum Diverifikasi"){
				$Judul_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri";
				$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri Anda telah di ubah statusnya menjadi 'Belum Diverifikasi' Oleh Admin, pastikan Data Diri Anda telah di isi dengan benar";

				Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
			}

			if($_POST['Status_Verifikasi_Data_Diri'] == "Verifikasi Ditolak"){
				$Judul_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri";
				$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri Anda telah di ubah statusnya menjadi 'Verifikasi Ditolak' Oleh Admin, pastikan Data Diri Anda telah di isi dengan benar";

				Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
			}
		}
		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI UPDATE STATUS VERIFIKASI
if (isset($_GET['update_status_verifikasi'])) {
	$form_field = array("Status_Verifikasi_Data_Diri", "Waktu_Update_Data");
	$form_value = array("$_GET[update_status_verifikasi]", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", $Get_Id_Primary);
		if ($result['Status'] == "Sukses") {
			$edit = $result['Hasil'];
		}else{
			$edit = null;
		}

		if($_GET['update_status_verifikasi'] == "Sudah Diverifikasi"){
			$Judul_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri";
			$Isi_Pengumuman_Untuk_Pendaftar = "Selamat, Data Diri Anda 'Sudah Diverifikasi' Oleh Admin";

			Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
			$result = $a_data_pendaftar->update_waktu_terakhir_semua_status_sudah_diverifikasi("$edit[Id_Pendaftar]");
		}

		if($_GET['update_status_verifikasi'] == "Belum Diverifikasi"){
			$Judul_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri";
			$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri Anda telah di ubah statusnya menjadi 'Belum Diverifikasi' Oleh Admin, pastikan Data Diri Anda telah di isi dengan benar";

			Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
		}

		if($_GET['update_status_verifikasi'] == "Verifikasi Ditolak"){
			$Judul_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri";
			$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Data Diri Anda telah di ubah statusnya menjadi 'Verifikasi Ditolak' Oleh Admin, pastikan Data Diri Anda telah di isi dengan benar";

			Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
		}

		if($_GET['update_status_verifikasi'] == "Menunggu Verifikasi"){
			$search_field_where = array("Id_Pendaftar","Judul_Pengumuman");
			$search_criteria_where = array("=","=");
			$search_value_where = array("$edit[Id_Pendaftar]","Verifikasi Data Diri");
			$search_connector_where = array("AND","");
			$result = $a_pengumuman_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$data_pengumuman_sebelum = $result['Hasil'];
				foreach($data_pengumuman_sebelum as $data_pengumuman_sebelumnya){
					$form_field = array("Waktu_Update_Data","Status");
					$form_value = array("$Waktu_Sekarang","Terhapus");
					$form_field_where = array("Id_Pengumuman_Pendaftar");
					$form_criteria_where = array("=");
					$form_value_where = array("$data_pengumuman_sebelumnya[Id_Pengumuman_Pendaftar]");
					$form_connector_where = array("");

					$result = $a_pengumuman_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				}
			}
		}

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI DELETE DATA (DELETE)
if (isset($_GET['hapus_data_ke_tong_sampah'])) {

	$result = $a_data_pendaftar->hapus_data_ke_tong_sampah("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}
}

if (isset($_GET['arsip_data'])) {

	$result = $a_data_pendaftar->arsip_data("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Dipindahkan Ke Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Memindahkan Data Ke Arsip');document.location.href='$kehalaman'</script>";
	}
}

if (isset($_GET['restore_data_dari_arsip'])) {

	$result = $a_data_pendaftar->restore_data_dari_arsip("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Berhasil Di Keluarkan Dari Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengeluarkan Data Dari Arsip');document.location.href='$kehalaman'</script>";
	}
}

if (isset($_GET['restore_data_dari_tong_sampah'])) {

	$result = $a_data_pendaftar->restore_data_dari_tong_sampah("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Di Restore Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Restore Data Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	}
}

if (isset($_GET['hapus_data_permanen'])) {

	$result = $a_data_pendaftar->hapus_data_permanen("Id_Pendaftar", $Get_Id_Primary);
	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus Permanen');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}
}

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI HITUNG DATA (COUNT)

$count_field_where = array("Status", "Id_PPDB");

$count_criteria_where = array("=", "=");

$count_connector_where = array("AND", "");

//DATA AKTIF
$count_value_where = array("Aktif", "$Id_PPDB_Saat_Ini");
$hitung_Aktif = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Aktif = $hitung_Aktif['Hasil'];
//DATA TERARSIP
$count_value_where = array("Terarsip", "$Id_PPDB_Saat_Ini");
$hitung_Terarsip = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terarsip = $hitung_Terarsip['Hasil'];
//DATA TERHAPUS (SAMPAH)
$count_value_where = array("Terhapus", "$Id_PPDB_Saat_Ini");
$hitung_Terhapus = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terhapus = $hitung_Terhapus['Hasil'];
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	if (isset($_GET['filter_status'])) {
		$filter_status = $_GET['filter_status'];
	} else {
		$filter_status = "Aktif";
	}

	$search_field_where = array("Status", "Id_PPDB");
	$search_criteria_where = array("=", "=");
	$search_value_where = array("$filter_status", "$Id_PPDB_Saat_Ini");
	$search_connector_where = array("AND", "");

	$nomor = 0;

	$result = $a_data_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_datatable_master = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PPDB
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_ppdb = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN PEKERJAAN ORANG TUA
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_pekerjaan_orang_tua->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_pekerjaan_orang_tua = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN PENGHASILAN ORANG TUA
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_penghasilan_orang_tua->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_penghasilan_orang_tua = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN KENDARAAN YANG DIPAKAI KESEKOLAH
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_kendaraan_yang_dipakai_kesekolah->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_kendaraan_yang_dipakai_kesekolah = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN ASAL SEKOLAH
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_asal_sekolah->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_asal_sekolah = $result['Hasil'];
	}
}
