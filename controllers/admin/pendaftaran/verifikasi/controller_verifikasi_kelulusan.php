<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
$a_data_pendaftar_verifikasi_berkas = new a_data_pendaftar_verifikasi_berkas();
$a_data_pendaftar_pembayaran_ppdb = new a_data_pendaftar_pembayaran_ppdb();
$a_data_pendaftar_nilai_rapor = new a_data_pendaftar_nilai_rapor();
$a_data_pendaftar_program_layanan = new a_data_pendaftar_program_layanan();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_data_siswa = new a_data_siswa();
$a_data_siswa_nilai_rapor = new a_data_siswa_nilai_rapor();
$a_pengumuman_pendaftar = new a_pengumuman_pendaftar();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI HAK AKSES
	$Kode_Halaman = "Verifikasi_Kelulusan";
	$Nama_Halaman = "Verifikasi Kelulusan";

	$Link_Redirect_Tidak_Diberi_Akses_Baca_Data = "?menu=home";
	$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data = $kehalaman;
	$Link_Redirect_Tidak_Diberi_Akses_Arsip_Restore_Hapus_Data = $kehalaman;
	
	$search_field_where = array("Status","Id_Hak_Akses");
	$search_criteria_where = array("=","=");
	$search_value_where = array("Aktif","$u_data_user[Id_Hak_Akses]");
	$search_connector_where = array("AND","");

	$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$data_hak_akses = $result['Hasil'][0];
	}

	if(isset($data_hak_akses)){
		if($data_hak_akses['Hak_Akses'] == "Super Administrator"){
			
		}else{
			$search_field_where = array("Id_Hak_Akses","Kode_Halaman","Nama_Halaman");
			$search_criteria_where = array("=","=","=");
			$search_value_where = array("$u_data_user[Id_Hak_Akses]","$Kode_Halaman","$Nama_Halaman");
			$search_connector_where = array("AND","AND","");

			$result = $a_data_hak_akses_detail->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$data_hak_akses_detail = $result['Hasil'][0];
			}

			if(isset($data_hak_akses_detail)){
				//CEK HAK AKSES BACA DATA
				if($data_hak_akses_detail['Baca_Data'] <> "Iya"){
					echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
					exit();
				}

				//CEK HAK AKSES SIMPAN DATA
				if(isset($_POST['submit_simpan'])){
					if($data_hak_akses_detail['Simpan_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk menambahkan data');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
						exit();
					}
				}

				//CEK HAK AKSES UPDATE DATA
				if(isset($_POST['submit_update'])){
					if($data_hak_akses_detail['Update_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
						exit();
					}
				}

				if((isset($_GET['restore_data_dari_tong_sampah'])) OR (isset($_GET['restore_data_dari_tong_sampah'])) OR (isset($_GET['restore_data_dari_tong_sampah']))){
					if($data_hak_akses_detail['Update_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
						exit();
					}
				}

				if(isset($_GET['update_status_verifikasi'])){
					if($data_hak_akses_detail['Update_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
						exit();
					}
				}

				//CEK HAK AKSES HAPUS DATA
				if((isset($_GET['hapus_data_ke_tong_sampah'])) OR (isset($_GET['hapus_data_permanen']))){
					if($data_hak_akses_detail['Hapus_Data'] <> "Iya"){
						echo "<script>alert('Anda tidak diberikan akses untuk menghapus data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Hapus_Data'</script>";
						exit();
					}
				}
			}else{
				echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
				exit();
			}
		}
	}else{
		echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
		exit();
	}

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
$folder_penyimpanan_file_bukti_pembayaran_ppdb = "media/bukti_pembayaran_ppdb/";
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE STATUS VERIFIKASI
if (isset($_GET['update_status_verifikasi'])) {
	$form_field = array("Status_Kelulusan","Waktu_Update_Data");
	$form_value = array("$_GET[update_status_verifikasi]","$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		if($_GET['update_status_verifikasi'] == "Lulus"){
			$search_field_where = array("Id_Pendaftar","Judul_Pengumuman");
			$search_criteria_where = array("=","=");
			$search_value_where = array("$Get_Id_Primary","Kelulusan");
			$search_connector_where = array("AND","");
			$result = $a_pengumuman_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$data_pengumuman_sebelum = $result['Hasil'];
				foreach($data_pengumuman_sebelum as $data_pengumuman_sebelumnya){
					$form_field = array("Waktu_Update_Data","Status");
					$form_value = array("$Waktu_Sekarang","Terhapus");
					$form_field_where = array("Id_Pengumuman_Pendaftar");
					$form_criteria_where = array("=");
					$form_value_where = array("$data_pengumuman_sebelumnya[Id_Pengumuman_Pendaftar]");
					$form_connector_where = array("");

					$result = $a_pengumuman_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				}
			}

			$Judul_Pengumuman_Untuk_Pendaftar = "Kelulusan";
			$Isi_Pengumuman_Untuk_Pendaftar = "Selamat Anda Lulus dalam PPDB ini";

			Simpan_Pengumuman_Pendaftar("$Get_Id_Primary","Id_Pendaftar","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
		}

		if($_GET['update_status_verifikasi'] == "Tidak Lulus"){
			$search_field_where = array("Id_Pendaftar","Judul_Pengumuman");
			$search_criteria_where = array("=","=");
			$search_value_where = array("$Get_Id_Primary","Kelulusan");
			$search_connector_where = array("AND","");
			$result = $a_pengumuman_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$data_pengumuman_sebelum = $result['Hasil'];
				foreach($data_pengumuman_sebelum as $data_pengumuman_sebelumnya){
					$form_field = array("Waktu_Update_Data","Status");
					$form_value = array("$Waktu_Sekarang","Terhapus");
					$form_field_where = array("Id_Pengumuman_Pendaftar");
					$form_criteria_where = array("=");
					$form_value_where = array("$data_pengumuman_sebelumnya[Id_Pengumuman_Pendaftar]");
					$form_connector_where = array("");

					$result = $a_pengumuman_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				}
			}

			$Judul_Pengumuman_Untuk_Pendaftar = "Kelulusan";
			$Isi_Pengumuman_Untuk_Pendaftar = "Mohon Maaf, Anda Tidak Lulus dalam PPDB ini";

			Simpan_Pengumuman_Pendaftar("$Get_Id_Primary","Id_Pendaftar","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
		}

		if($_GET['update_status_verifikasi'] == "Menunggu Hasil"){
			$search_field_where = array("Id_Pendaftar","Judul_Pengumuman");
			$search_criteria_where = array("=","=");
			$search_value_where = array("$Get_Id_Primary","Kelulusan");
			$search_connector_where = array("AND","");
			$result = $a_pengumuman_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$data_pengumuman_sebelum = $result['Hasil'];
				foreach($data_pengumuman_sebelum as $data_pengumuman_sebelumnya){
					$form_field = array("Waktu_Update_Data","Status");
					$form_value = array("$Waktu_Sekarang","Terhapus");
					$form_field_where = array("Id_Pengumuman_Pendaftar");
					$form_criteria_where = array("=");
					$form_value_where = array("$data_pengumuman_sebelumnya[Id_Pengumuman_Pendaftar]");
					$form_connector_where = array("");

					$result = $a_pengumuman_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				}
			}
		}

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI UPDATE STATUS VERIFIKASI
if (isset($_GET['salin_ke_data_siswa_master'])) {

	$count_field_where = array("Id_Pendaftar","Status");
	$count_criteria_where = array("=","=");
	$count_value_where = array($Get_Id_Primary,"Aktif");
	$count_connector_where = array("AND","");
	
	$hitung_data = $a_data_siswa->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
	$hitung_data = $hitung_data['Hasil'];

	if($hitung_data > 0){
		echo "<script>alert('Data Ini Sudah Pernah di Salin !');document.location.href='$kehalaman'</script>";
		exit();
	}

	$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit = $result['Hasil'];
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data Pendaftar');document.location.href='$kehalaman'</script>";
		exit();
	}

	$result = $a_data_pendaftar_nilai_rapor->baca_data_id("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit_nilai_rapor = $result['Hasil'];
	} else {
		$edit_nilai_rapor = null;
	}

	

	$form_field = array("Id_Siswa","Id_Pendaftar","Id_PPDB","NIS_Saat_Ini","NIK","NIS","NISN","NIPD","Nama_Lengkap","Jenis_Kelamin","Tempat_Lahir","Tanggal_Lahir","Agama","Status_Dalam_Keluarga","Jalan","Kelurahan","Kecamatan","Kota","Provinsi","Id_Kelurahan","Id_Kecamatan","Id_Kota","Id_Provinsi","Kode_Pos","Titik_Lintang_Alamat","Titik_Bujur_Alamat","Nomor_Handphone","Nomor_Telepon","Email","Golongan_Darah","Kebutuhan_Khusus","Tinggi_Badan","Berat_Badan","Lingkar_Kepala","Asal_Sekolah","Alamat_Sekolah","Kelas","Nomor_Peserta_Ujian_Nasional","Nomor_Seri_Ijazah","Penerima_KPS","Penerima_KIP","Nomor_KIP","Penerima_KJP_DKI","Nomor_KJP_DKI","Nomor_KPS","Nomor_KKS","Nomor_KK","Nomor_Registrasi_Akta_Lahir","Anak_Ke","Jumlah_Saudara","Status_Perwalian","NIK_Ayah","Nama_Ayah","Tempat_Lahir_Ayah","Tanggal_Lahir_Ayah","Jenjang_Pendidikan_Ayah","Pekerjaan_Ayah","Penghasilan_Ayah","No_HP_Ayah","NIK_Ibu","Nama_Ibu","Tempat_Lahir_Ibu","Tanggal_Lahir_Ibu","Jenjang_Pendidikan_Ibu","Pekerjaan_Ibu","Penghasilan_Ibu","No_HP_Ibu","NIK_Wali","Nama_Wali","Tempat_Lahir_Wali","Tanggal_Lahir_Wali","Jenjang_Pendidikan_Wali","Pekerjaan_Wali","Penghasilan_Wali","No_HP_Wali","Kendaraan_Yang_Dipakai_Kesekolah","Jarak_Rumah_Ke_Sekolah","Ukuran_Seragam","Waktu_Simpan_Data","Status");

	$form_value = array(NULL,"$edit[Id_Pendaftar]","$edit[Id_PPDB]","","$edit[NIK]","$edit[NIS]","$edit[NISN]","$edit[NIPD]","$edit[Nama_Lengkap]","$edit[Jenis_Kelamin]","$edit[Tempat_Lahir]","$edit[Tanggal_Lahir]","$edit[Agama]","$edit[Status_Dalam_Keluarga]","$edit[Jalan]","$edit[Kelurahan]","$edit[Kecamatan]","$edit[Kota]","$edit[Provinsi]","$edit[Id_Kelurahan]","$edit[Id_Kecamatan]","$edit[Id_Kota]","$edit[Id_Provinsi]","$edit[Kode_Pos]","$edit[Titik_Lintang_Alamat]","$edit[Titik_Bujur_Alamat]","$edit[Nomor_Handphone]","$edit[Nomor_Telepon]","$edit[Email]","$edit[Golongan_Darah]","$edit[Kebutuhan_Khusus]","$edit[Tinggi_Badan]","$edit[Berat_Badan]","$edit[Lingkar_Kepala]","$edit[Asal_Sekolah]","$edit[Alamat_Sekolah]","$edit[Kelas]","$edit[Nomor_Peserta_Ujian_Nasional]","$edit[Nomor_Seri_Ijazah]","$edit[Penerima_KPS]","$edit[Penerima_KIP]","$edit[Nomor_KIP]","$edit[Penerima_KJP_DKI]","$edit[Nomor_KJP_DKI]","$edit[Nomor_KPS]","$edit[Nomor_KKS]","$edit[Nomor_KK]","$edit[Nomor_Registrasi_Akta_Lahir]","$edit[Anak_Ke]","$edit[Jumlah_Saudara]","$edit[Status_Perwalian]","$edit[NIK_Ayah]","$edit[Nama_Ayah]","$edit[Tempat_Lahir_Ayah]","$edit[Tanggal_Lahir_Ayah]","$edit[Jenjang_Pendidikan_Ayah]","$edit[Pekerjaan_Ayah]","$edit[Penghasilan_Ayah]","$edit[No_HP_Ayah]","$edit[NIK_Ibu]","$edit[Nama_Ibu]","$edit[Tempat_Lahir_Ibu]","$edit[Tanggal_Lahir_Ibu]","$edit[Jenjang_Pendidikan_Ibu]","$edit[Pekerjaan_Ibu]","$edit[Penghasilan_Ibu]","$edit[No_HP_Ibu]","$edit[NIK_Wali]","$edit[Nama_Wali]","$edit[Tempat_Lahir_Wali]","$edit[Tanggal_Lahir_Wali]","$edit[Jenjang_Pendidikan_Wali]","$edit[Pekerjaan_Wali]","$edit[Penghasilan_Wali]","$edit[No_HP_Wali]","$edit[Kendaraan_Yang_Dipakai_Kesekolah]","$edit[Jarak_Rumah_Ke_Sekolah]","$edit[Ukuran_Seragam]","$Waktu_Sekarang","Aktif");

	$result = $a_data_siswa->tambah_data($form_field, $form_value);
	
	if ($result['Status'] == "Sukses") {
		$Id_Siswa_Terbaru = $result['Id'];

		if(isset($edit_nilai_rapor)){
			$form_field = array("Id_Siswa_Nilai_Rapor","Id_Siswa","Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti","Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan","Nilai_Semester_1_Bahasa_Indonesia","Nilai_Semester_1_Bahasa_Inggris","Nilai_Semester_1_Matematika","Nilai_Semester_1_IPA","Nilai_Semester_1_IPS","Nilai_Semester_1_Seni_Dan_Budaya","Nilai_Semester_1_PJOK","Nilai_Semester_1_Prakarya","Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti","Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan","Nilai_Semester_2_Bahasa_Indonesia","Nilai_Semester_2_Bahasa_Inggris","Nilai_Semester_2_Matematika","Nilai_Semester_2_IPA","Nilai_Semester_2_IPS","Nilai_Semester_2_Seni_Dan_Budaya","Nilai_Semester_2_PJOK","Nilai_Semester_2_Prakarya","Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti","Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan","Nilai_Semester_3_Bahasa_Indonesia","Nilai_Semester_3_Bahasa_Inggris","Nilai_Semester_3_Matematika","Nilai_Semester_3_IPA","Nilai_Semester_3_IPS","Nilai_Semester_3_Seni_Dan_Budaya","Nilai_Semester_3_PJOK","Nilai_Semester_3_Prakarya","Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti","Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan","Nilai_Semester_4_Bahasa_Indonesia","Nilai_Semester_4_Bahasa_Inggris","Nilai_Semester_4_Matematika","Nilai_Semester_4_IPA","Nilai_Semester_4_IPS","Nilai_Semester_4_Seni_Dan_Budaya","Nilai_Semester_4_PJOK","Nilai_Semester_4_Prakarya","Nilai_SKL_Ijazah","Waktu_Simpan_Data","Status");
			$form_value = array(NULL,"$Id_Siswa_Terbaru","$edit_nilai_rapor[Nilai_Semester_1_Pendidikan_Agama_Islam_Dan_Budi_Pekerti]","$edit_nilai_rapor[Nilai_Semester_1_Pendidikan_Pancasila_Dan_Kewarganegaraan]","$edit_nilai_rapor[Nilai_Semester_1_Bahasa_Indonesia]","$edit_nilai_rapor[Nilai_Semester_1_Bahasa_Inggris]","$edit_nilai_rapor[Nilai_Semester_1_Matematika]","$edit_nilai_rapor[Nilai_Semester_1_IPA]","$edit_nilai_rapor[Nilai_Semester_1_IPS]","$edit_nilai_rapor[Nilai_Semester_1_Seni_Dan_Budaya]","$edit_nilai_rapor[Nilai_Semester_1_PJOK]","$edit_nilai_rapor[Nilai_Semester_1_Prakarya]","$edit_nilai_rapor[Nilai_Semester_2_Pendidikan_Agama_Islam_Dan_Budi_Pekerti]","$edit_nilai_rapor[Nilai_Semester_2_Pendidikan_Pancasila_Dan_Kewarganegaraan]","$edit_nilai_rapor[Nilai_Semester_2_Bahasa_Indonesia]","$edit_nilai_rapor[Nilai_Semester_2_Bahasa_Inggris]","$edit_nilai_rapor[Nilai_Semester_2_Matematika]","$edit_nilai_rapor[Nilai_Semester_2_IPA]","$edit_nilai_rapor[Nilai_Semester_2_IPS]","$edit_nilai_rapor[Nilai_Semester_2_Seni_Dan_Budaya]","$edit_nilai_rapor[Nilai_Semester_2_PJOK]","$edit_nilai_rapor[Nilai_Semester_2_Prakarya]","$edit_nilai_rapor[Nilai_Semester_3_Pendidikan_Agama_Islam_Dan_Budi_Pekerti]","$edit_nilai_rapor[Nilai_Semester_3_Pendidikan_Pancasila_Dan_Kewarganegaraan]","$edit_nilai_rapor[Nilai_Semester_3_Bahasa_Indonesia]","$edit_nilai_rapor[Nilai_Semester_3_Bahasa_Inggris]","$edit_nilai_rapor[Nilai_Semester_3_Matematika]","$edit_nilai_rapor[Nilai_Semester_3_IPA]","$edit_nilai_rapor[Nilai_Semester_3_IPS]","$edit_nilai_rapor[Nilai_Semester_3_Seni_Dan_Budaya]","$edit_nilai_rapor[Nilai_Semester_3_PJOK]","$edit_nilai_rapor[Nilai_Semester_3_Prakarya]","$edit_nilai_rapor[Nilai_Semester_4_Pendidikan_Agama_Islam_Dan_Budi_Pekerti]","$edit_nilai_rapor[Nilai_Semester_4_Pendidikan_Pancasila_Dan_Kewarganegaraan]","$edit_nilai_rapor[Nilai_Semester_4_Bahasa_Indonesia]","$edit_nilai_rapor[Nilai_Semester_4_Bahasa_Inggris]","$edit_nilai_rapor[Nilai_Semester_4_Matematika]","$edit_nilai_rapor[Nilai_Semester_4_IPA]","$edit_nilai_rapor[Nilai_Semester_4_IPS]","$edit_nilai_rapor[Nilai_Semester_4_Seni_Dan_Budaya]","$edit_nilai_rapor[Nilai_Semester_4_PJOK]","$edit_nilai_rapor[Nilai_Semester_4_Prakarya]","$edit_nilai_rapor[Nilai_SKL_Ijazah]","$Waktu_Sekarang","Aktif");
			$result = $a_data_siswa_nilai_rapor->tambah_data($form_field, $form_value);
		}
		
		echo "<script>alert('Data Berhasil Disalin Ke Data Siswa Master');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menyalin Data');document.location.href='$kehalaman'</script>";
	}
	

}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	if (isset($_GET['filter_status'])) {
		$filter_status = $_GET['filter_status'];
	} else {
		$filter_status = "Aktif";
	}

	$search_field_where = array("Status","Id_PPDB");
	$search_criteria_where = array("=","=");
	$search_value_where = array("$filter_status","$Id_PPDB_Saat_Ini");
	$search_connector_where = array("AND","");

	$nomor = 0;

	$result = $a_data_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_datatable_master = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PPDB
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_ppdb = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
// a_data_pendaftar_pembelian_formulir
// a_data_pendaftar_verifikasi_berkas
// a_data_pendaftar_pembayaran_ppdb

#FUNGSI AMBIL LIST DATA PENDAFTAR
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_pembelian_formulir" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_data_pendaftar_pembelian_formulir->baca_data_dengan_filter($search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_pembelian_formulir = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_pembelian_formulir;
		foreach ($array_result_relasi_data_pendaftar_pembelian_formulir as $relasi_data_pendaftar_pembelian_formulir) {
			$id_relasi_data_pendaftar_pembelian_formulir = strval($relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_pembelian_formulir['Id_Pendaftar'][$id_relasi_data_pendaftar_pembelian_formulir] = $relasi_data_pendaftar_pembelian_formulir;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_pembelian_formulir = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_pembelian_formulir" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_verifikasi_berkas" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_data_pendaftar_verifikasi_berkas->baca_data_dengan_filter($search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_verifikasi_berkas = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_verifikasi_berkas;
		foreach ($array_result_relasi_data_pendaftar_verifikasi_berkas as $relasi_data_pendaftar_verifikasi_berkas) {
			$id_relasi_data_pendaftar_verifikasi_berkas = strval($relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_verifikasi_berkas['Id_Pendaftar'][$id_relasi_data_pendaftar_verifikasi_berkas] = $relasi_data_pendaftar_verifikasi_berkas;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_verifikasi_berkas = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_verifikasi_berkas" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_pembayaran_ppdb" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_data_pendaftar_pembayaran_ppdb->baca_data_dengan_filter($search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_pembayaran_ppdb = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_pembayaran_ppdb;
		foreach ($array_result_relasi_data_pendaftar_pembayaran_ppdb as $relasi_data_pendaftar_pembayaran_ppdb) {
			$id_relasi_data_pendaftar_pembayaran_ppdb = strval($relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_pembayaran_ppdb['Id_Pendaftar'][$id_relasi_data_pendaftar_pembayaran_ppdb] = $relasi_data_pendaftar_pembayaran_ppdb;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_pembayaran_ppdb = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_pembayaran_ppdb" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_siswa" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_data_siswa->baca_data_dengan_filter($search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_siswa = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_siswa;
		foreach ($array_result_relasi_data_siswa as $relasi_data_siswa) {
			$id_relasi_data_siswa = strval($relasi_data_siswa['Id_Pendaftar']);
			$array_hasil_relasi_data_siswa['Id_Pendaftar'][$id_relasi_data_siswa] = $relasi_data_siswa;
		}
	
	
	}else{
		$array_result_relasi_data_siswa = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_siswa" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_nilai_rapor" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_data_pendaftar_nilai_rapor->baca_data_dengan_filter($search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_nilai_rapor = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_nilai_rapor;
		foreach ($array_result_relasi_data_pendaftar_nilai_rapor as $relasi_data_pendaftar_nilai_rapor) {
			$id_relasi_data_pendaftar_nilai_rapor = strval($relasi_data_pendaftar_nilai_rapor['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_nilai_rapor['Id_Pendaftar'][$id_relasi_data_pendaftar_nilai_rapor] = $relasi_data_pendaftar_nilai_rapor;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_nilai_rapor = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_nilai_rapor" ID_PRIMARY "Id_Pendaftar"

	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar_program_layanan" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");
	
	$result_data_relasi = $a_data_pendaftar_program_layanan->baca_data_dengan_filter($search_field_where,$search_criteria_where,$search_value_where,$search_connector_where);
	
	if($result_data_relasi['Status'] == "Sukses"){
		$array_result_relasi_data_pendaftar_program_layanan = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar_program_layanan;
		foreach ($array_result_relasi_data_pendaftar_program_layanan as $relasi_data_pendaftar_program_layanan) {
			$id_relasi_data_pendaftar_program_layanan = strval($relasi_data_pendaftar_program_layanan['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar_program_layanan['Id_Pendaftar'][$id_relasi_data_pendaftar_program_layanan] = $relasi_data_pendaftar_program_layanan;
		}
	
	
	}else{
		$array_result_relasi_data_pendaftar_program_layanan = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar_program_layanan" ID_PRIMARY "Id_Pendaftar"
}

?>