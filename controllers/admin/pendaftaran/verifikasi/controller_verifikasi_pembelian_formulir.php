<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar_pembelian_formulir = new a_data_pendaftar_pembelian_formulir();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_pengumuman_pendaftar = new a_pengumuman_pendaftar();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI HAK AKSES
$Kode_Halaman = "Verifikasi_Pembelian_Formulir";
$Nama_Halaman = "Verifikasi Pembelian Formulir";

$Link_Redirect_Tidak_Diberi_Akses_Baca_Data = "?menu=home";
$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data = $kehalaman;
$Link_Redirect_Tidak_Diberi_Akses_Arsip_Restore_Hapus_Data = $kehalaman;

$search_field_where = array("Status", "Id_Hak_Akses");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Hak_Akses]");
$search_connector_where = array("AND", "");

$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$data_hak_akses = $result['Hasil'][0];
}

if (isset($data_hak_akses)) {
	if ($data_hak_akses['Hak_Akses'] == "Super Administrator") {

	} else {
		$search_field_where = array("Id_Hak_Akses", "Kode_Halaman", "Nama_Halaman");
		$search_criteria_where = array("=", "=", "=");
		$search_value_where = array("$u_data_user[Id_Hak_Akses]", "$Kode_Halaman", "$Nama_Halaman");
		$search_connector_where = array("AND", "AND", "");

		$result = $a_data_hak_akses_detail->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

		if ($result['Status'] == "Sukses") {
			$data_hak_akses_detail = $result['Hasil'][0];
		}

		if (isset($data_hak_akses_detail)) {
			//CEK HAK AKSES BACA DATA
			if ($data_hak_akses_detail['Baca_Data'] <> "Iya") {
				echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
				exit();
			}

			//CEK HAK AKSES SIMPAN DATA
			if (isset($_POST['submit_simpan'])) {
				if ($data_hak_akses_detail['Simpan_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menambahkan data');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES UPDATE DATA
			if (isset($_POST['submit_update'])) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			if ((isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah']))) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			if (isset($_GET['update_status_verifikasi'])) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES HAPUS DATA
			if ((isset($_GET['hapus_data_ke_tong_sampah'])) or (isset($_GET['hapus_data_permanen']))) {
				if ($data_hak_akses_detail['Hapus_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menghapus data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Hapus_Data'</script>";
					exit();
				}
			}
		} else {
			echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
			exit();
		}
	}
} else {
	echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
	exit();
}

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
$folder_penyimpanan_file_bukti_pembayaran_pembelian_formulir = "media/bukti_pembayaran_pembelian_formulir/";
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI SIMPAN DATA (CREATE)
if (isset($_POST['submit_simpan'])) {

	$form_field = array("Id_Pendaftar_Pembelian_Formulir", "Id_Pendaftar", "Status_Verifikasi_Pembelian_Formulir", "Waktu_Simpan_Data", "Status");

	$form_value = array(NULL, "$_POST[Id_Pendaftar]", "$_POST[Status_Verifikasi_Pembelian_Formulir]", "$Waktu_Sekarang", "Aktif");

	$result = $a_data_pendaftar_pembelian_formulir->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		$Id_Pendaftar_Pembelian_Formulir_Ini = $result['Id'];

		//FUNGSI AMBIL DATA PENDAFTAR
		$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$_POST[Id_Pendaftar]");

		if ($result['Status'] == "Sukses") {
			$edit_data_pendaftar = $result['Hasil'];
		} else {
			$edit_data_pendaftar = null;
		}

		//FUNGSI AMBIL DATA PPDB
		$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

		if ($result['Status'] == "Sukses") {
			$edit_data_ppdb = $result['Hasil'];
		} else {
			$edit_data_ppdb = null;
		}

		//FUNGSI UPLOAD FILE Bukti_Pembayaran_Pembelian_Formulir
		if ($_FILES['Bukti_Pembayaran_Pembelian_Formulir']['size'] <> 0 && $_FILES['Bukti_Pembayaran_Pembelian_Formulir']['error'] == 0) {
			$post_file_upload = $_FILES['Bukti_Pembayaran_Pembelian_Formulir'];
			$path_file_upload = $_FILES['Bukti_Pembayaran_Pembelian_Formulir']['name'];
			$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
			$ukuran_file_upload = $_FILES['Bukti_Pembayaran_Pembelian_Formulir']['size'];
			$nama_file_upload = "bukti_pembayaran_pembelian_formulir_" . $_POST['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $Id_Pendaftar_Pembelian_Formulir_Ini;
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_bukti_pembayaran_pembelian_formulir;
			$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
			$maksimum_ukuran_file_upload = 100000000;

			$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

			if ($result_upload_file['Status'] == "Sukses") {
				$form_field = array("Bukti_Pembayaran_Pembelian_Formulir");
				$form_value = array("$nama_file_upload.$ext_file_upload");
				$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
				$form_criteria_where = array("=");
				$form_value_where = array("$Get_Id_Primary");
				$form_connector_where = array("");

				$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			} else {
			}
		}
		//FUNGSI UPLOAD FILE Bukti_Pembayaran_Pembelian_Formulir

		echo "<script>alert('Data Tersimpan');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menyimpan Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
if (isset($_GET['edit'])) {

	$result = $a_data_pendaftar_pembelian_formulir->baca_data_id("Id_Pendaftar_Pembelian_Formulir", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit = $result['Hasil'];

		//FUNGSI AMBIL DATA PENDAFTAR
		$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$edit[Id_Pendaftar]");

		if ($result['Status'] == "Sukses") {
			$edit_data_pendaftar = $result['Hasil'];
		} else {
			$edit_data_pendaftar = null;
		}

		//FUNGSI AMBIL DATA PPDB
		$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

		if ($result['Status'] == "Sukses") {
			$edit_data_ppdb = $result['Hasil'];
		} else {
			$edit_data_ppdb = null;
		}
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	$form_field = array("Status_Verifikasi_Pembelian_Formulir", "Waktu_Update_Data");
	$form_value = array("$_POST[Status_Verifikasi_Pembelian_Formulir]", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		//FUNGSI HAPUS FILE Bukti_Pembayaran_Pembelian_Formulir
		if ((isset($_POST['hapus_file_bukti_pembayaran'])) AND ($_POST['hapus_file_bukti_pembayaran'] == "Iya")){
				$nama_file_upload = $edit['Bukti_Pembayaran_Pembelian_Formulir'];
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_bukti_pembayaran_pembelian_formulir;

				$result_hapus_file = $a_hapus_file->hapus_file($nama_file_upload, $folder_penyimpanan_file_upload);
				// if ($result_hapus_file['Status'] == "Sukses") {
					$form_field = array("Bukti_Pembayaran_Pembelian_Formulir");
					$form_value = array("");
					$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				// }
		}

		//FUNGSI UPLOAD FILE Bukti_Pembayaran_Pembelian_Formulir
		if ($_FILES['Bukti_Pembayaran_Pembelian_Formulir']['size'] <> 0 && $_FILES['Bukti_Pembayaran_Pembelian_Formulir']['error'] == 0) {
			$post_file_upload = $_FILES['Bukti_Pembayaran_Pembelian_Formulir'];
			$path_file_upload = $_FILES['Bukti_Pembayaran_Pembelian_Formulir']['name'];
			$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
			$ukuran_file_upload = $_FILES['Bukti_Pembayaran_Pembelian_Formulir']['size'];
			$nama_file_upload = "bukti_pembayaran_pembelian_formulir_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit['Id_Pendaftar_Pembelian_Formulir'];
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_bukti_pembayaran_pembelian_formulir;
			$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
			$maksimum_ukuran_file_upload = 100000000;

			$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

			if ($result_upload_file['Status'] == "Sukses") {
				$form_field = array("Bukti_Pembayaran_Pembelian_Formulir");
				$form_value = array("$nama_file_upload.$ext_file_upload");
				$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
				$form_criteria_where = array("=");
				$form_value_where = array("$Get_Id_Primary");
				$form_connector_where = array("");

				$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			} else {
			}
		}
		//FUNGSI UPLOAD FILE Bukti_Pembayaran_Pembelian_Formulir

		if($edit['Status_Verifikasi_Pembelian_Formulir'] <> $_POST['Status_Verifikasi_Pembelian_Formulir']){
			
			$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", $edit['Id_Pendaftar']);
			if ($result['Status'] == "Sukses") {
				$data_pendaftar = $result['Hasil'];
			}else{
				$data_pendaftar = null;
			}

			$result = $a_data_ppdb->baca_data_id("Id_PPDB", $data_pendaftar['Id_PPDB']);
			if ($result['Status'] == "Sukses") {
				$data_ppdb = $result['Hasil'];
			}else{
				$data_ppdb = null;
			}

			if($_POST['Status_Verifikasi_Pembelian_Formulir'] == "Sudah Diverifikasi"){
				$Judul_Pengumuman_Untuk_Pendaftar = "Pembelian Formulir";

				if((isset($data_ppdb)) AND ($data_ppdb['Masa_Waktu_Pembayaran_Pembelian_Formulir'] <> 0)){
					$Isi_Pengumuman_Untuk_Pendaftar = "Selamat, Pembelian Formulir Anda 'Sudah Diverifikasi' Oleh Admin. Harap selesaikan Pengisian Berkas maksimal ".$data_ppdb['Masa_Waktu_Pengisian_Berkas']." Hari dari Admin memverifikasi Pembayaran Ini. Jika Tidak, maka otomatis Status PPDB Anda yakni 'Gugur'";
				}else{
					$Isi_Pengumuman_Untuk_Pendaftar = "Selamat, Pembelian Formulir Anda 'Sudah Diverifikasi' Oleh Admin";
				}

				Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Pembelian_Formulir","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);

				$result = $a_data_pendaftar->update_waktu_terakhir_semua_status_sudah_diverifikasi("$edit[Id_Pendaftar]");

				$form_field = array("Waktu_Sudah_Diverifikasi_Pembayaran_Pembelian_Formulir");
				$form_value = array("$Waktu_Sekarang");

				$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
				$form_criteria_where = array("=");
				$form_value_where = array("$Get_Id_Primary");
				$form_connector_where = array("");

				$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			}

			if($_POST['Status_Verifikasi_Pembelian_Formulir'] == "Belum Diverifikasi"){
				$Judul_Pengumuman_Untuk_Pendaftar = "Pembelian Formulir";
				$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Pembelian Formulir Anda telah di ubah statusnya menjadi 'Belum Diverifikasi' Oleh Admin, pastikan Anda sudah membayar Pembelian Formulir ini";

				Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Pembelian_Formulir","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
			}

			if($_POST['Status_Verifikasi_Pembelian_Formulir'] == "Verifikasi Ditolak"){
				$Judul_Pengumuman_Untuk_Pendaftar = "Pembelian Formulir";
				$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Pembelian Formulir Anda telah di ubah statusnya menjadi 'Verifikasi Ditolak' Oleh Admin, pastikan Anda sudah membayar Pembelian Formulir ini";

				Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Pembelian_Formulir","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
			}
		}


		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI UPDATE STATUS VERIFIKASI
if (isset($_GET['update_status_verifikasi'])) {
	$form_field = array("Status_Verifikasi_Pembelian_Formulir", "Waktu_Update_Data");
	$form_value = array("$_GET[update_status_verifikasi]", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		$result = $a_data_pendaftar_pembelian_formulir->baca_data_id("Id_Pendaftar_Pembelian_Formulir", $Get_Id_Primary);
		if ($result['Status'] == "Sukses") {
			$edit = $result['Hasil'];
		}else{
			$edit = null;
		}

		$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", $edit['Id_Pendaftar']);
		if ($result['Status'] == "Sukses") {
			$data_pendaftar = $result['Hasil'];
		}else{
			$data_pendaftar = null;
		}

		$result = $a_data_ppdb->baca_data_id("Id_PPDB", $data_pendaftar['Id_PPDB']);
		if ($result['Status'] == "Sukses") {
			$data_ppdb = $result['Hasil'];
		}else{
			$data_ppdb = null;
		}

		if($_GET['update_status_verifikasi'] == "Sudah Diverifikasi"){
			$Judul_Pengumuman_Untuk_Pendaftar = "Pembelian Formulir";
			if((isset($data_ppdb)) AND ($data_ppdb['Masa_Waktu_Pembayaran_Pembelian_Formulir'] <> 0)){
				$Isi_Pengumuman_Untuk_Pendaftar = "Selamat, Pembelian Formulir Anda 'Sudah Diverifikasi' Oleh Admin. Harap selesaikan Pengisian Berkas maksimal ".$data_ppdb['Masa_Waktu_Pengisian_Berkas']." Hari dari Admin memverifikasi Pembayaran Ini. Jika Tidak, maka otomatis Status PPDB Anda yakni 'Gugur'";
			}else{
				$Isi_Pengumuman_Untuk_Pendaftar = "Selamat, Pembelian Formulir Anda 'Sudah Diverifikasi' Oleh Admin";
			}

			Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Pembelian_Formulir","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);

			$result = $a_data_pendaftar->update_waktu_terakhir_semua_status_sudah_diverifikasi("$edit[Id_Pendaftar]");

			$form_field = array("Waktu_Sudah_Diverifikasi_Pembayaran_Pembelian_Formulir");
			$form_value = array("$Waktu_Sekarang");

			$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
			$form_criteria_where = array("=");
			$form_value_where = array("$Get_Id_Primary");
			$form_connector_where = array("");

			$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
		}

		if($_GET['update_status_verifikasi'] == "Belum Diverifikasi"){
			$Judul_Pengumuman_Untuk_Pendaftar = "Pembelian Formulir";
			$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Pembelian Formulir Anda telah di ubah statusnya menjadi 'Belum Diverifikasi' Oleh Admin, pastikan Anda sudah membayar Pembelian Formulir ini";

			Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Pembelian_Formulir","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
		}

		if($_GET['update_status_verifikasi'] == "Verifikasi Ditolak"){
			$Judul_Pengumuman_Untuk_Pendaftar = "Pembelian Formulir";
			$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Pembelian Formulir Anda telah di ubah statusnya menjadi 'Verifikasi Ditolak' Oleh Admin, pastikan Anda sudah membayar Pembelian Formulir ini";

			Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Pembelian_Formulir","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
		}

		if($_GET['update_status_verifikasi'] == "Menunggu Verifikasi"){
			$search_field_where = array("Id_Pendaftar","Judul_Pengumuman");
			$search_criteria_where = array("=","=");
			$search_value_where = array("$edit[Id_Pendaftar]","Pembelian Formulir");
			$search_connector_where = array("AND","");
			$result = $a_pengumuman_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$data_pengumuman_sebelum = $result['Hasil'];
				foreach($data_pengumuman_sebelum as $data_pengumuman_sebelumnya){
					$form_field = array("Waktu_Update_Data","Status");
					$form_value = array("$Waktu_Sekarang","Terhapus");
					$form_field_where = array("Id_Pengumuman_Pendaftar");
					$form_criteria_where = array("=");
					$form_value_where = array("$data_pengumuman_sebelumnya[Id_Pengumuman_Pendaftar]");
					$form_connector_where = array("");

					$result = $a_pengumuman_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				}
			}
		}

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI DELETE DATA (DELETE)
if (isset($_GET['hapus_data_ke_tong_sampah'])) {

	$result = $a_data_pendaftar_pembelian_formulir->hapus_data_ke_tong_sampah("Id_Pendaftar_Pembelian_Formulir", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['arsip_data'])) {

	$result = $a_data_pendaftar_pembelian_formulir->arsip_data("Id_Pendaftar_Pembelian_Formulir", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Dipindahkan Ke Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Memindahkan Data Ke Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_arsip'])) {

	$result = $a_data_pendaftar_pembelian_formulir->restore_data_dari_arsip("Id_Pendaftar_Pembelian_Formulir", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Berhasil Di Keluarkan Dari Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengeluarkan Data Dari Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_tong_sampah'])) {

	$result = $a_data_pendaftar_pembelian_formulir->restore_data_dari_tong_sampah("Id_Pendaftar_Pembelian_Formulir", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Di Restore Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Restore Data Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['hapus_data_permanen'])) {

	$result = $a_data_pendaftar_pembelian_formulir->hapus_data_permanen("Id_Pendaftar_Pembelian_Formulir", $Get_Id_Primary);
	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Pembelian_Formulir");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_pembelian_formulir->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus Permanen');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI HITUNG DATA (COUNT)

$count_field_where = array("Status","(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_pembelian_formulir.Id_Pendaftar)");

$count_criteria_where = array("=","=");

$count_connector_where = array("AND","");

//DATA AKTIF
$count_value_where = array("Aktif","$Id_PPDB_Saat_Ini");
$hitung_Aktif = $a_data_pendaftar_pembelian_formulir->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Aktif = $hitung_Aktif['Hasil'];
//DATA TERARSIP
$count_value_where = array("Terarsip","$Id_PPDB_Saat_Ini");
$hitung_Terarsip = $a_data_pendaftar_pembelian_formulir->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terarsip = $hitung_Terarsip['Hasil'];
//DATA TERHAPUS (SAMPAH)
$count_value_where = array("Terhapus","$Id_PPDB_Saat_Ini");
$hitung_Terhapus = $a_data_pendaftar_pembelian_formulir->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terhapus = $hitung_Terhapus['Hasil'];
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	if (isset($_GET['filter_status'])) {
		$filter_status = $_GET['filter_status'];
	} else {
		$filter_status = "Aktif";
	}

	$search_field_where = array("Status","(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_pembelian_formulir.Id_Pendaftar)");
	$search_criteria_where = array("=","=");
	$search_value_where = array("$filter_status","$Id_PPDB_Saat_Ini");
	$search_connector_where = array("AND","");

	$nomor = 0;

	$result = $a_data_pendaftar_pembelian_formulir->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_datatable_master = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PPDB
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_ppdb = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENDAFTAR
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");

	$result_data_relasi = $a_data_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result_data_relasi['Status'] == "Sukses") {
		$array_result_relasi_data_pendaftar = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar;
		foreach ($array_result_relasi_data_pendaftar as $relasi_data_pendaftar) {
			$id_relasi_data_pendaftar = strval($relasi_data_pendaftar['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$id_relasi_data_pendaftar] = $relasi_data_pendaftar;
		}


	} else {
		$array_result_relasi_data_pendaftar = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar" ID_PRIMARY "Id_Pendaftar"
}
?>