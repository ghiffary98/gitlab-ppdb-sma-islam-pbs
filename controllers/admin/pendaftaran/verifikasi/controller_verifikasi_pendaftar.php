<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_pengaturan_pekerjaan_orang_tua = new a_pengaturan_pekerjaan_orang_tua();
$a_pengaturan_penghasilan_orang_tua = new a_pengaturan_penghasilan_orang_tua();
$a_pengaturan_kendaraan_yang_dipakai_kesekolah = new a_pengaturan_kendaraan_yang_dipakai_kesekolah();
$a_pengaturan_asal_sekolah = new a_pengaturan_asal_sekolah();
$a_pengumuman_pendaftar = new a_pengumuman_pendaftar();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI HAK AKSES
$Kode_Halaman = "Verifikasi_Pendaftar";
$Nama_Halaman = "Verifikasi Pendaftar";

$Link_Redirect_Tidak_Diberi_Akses_Baca_Data = "?menu=home";
$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data = $kehalaman;
$Link_Redirect_Tidak_Diberi_Akses_Arsip_Restore_Hapus_Data = $kehalaman;

$search_field_where = array("Status", "Id_Hak_Akses");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Hak_Akses]");
$search_connector_where = array("AND", "");

$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$data_hak_akses = $result['Hasil'][0];
}

if (isset($data_hak_akses)) {
	if ($data_hak_akses['Hak_Akses'] == "Super Administrator") {

	} else {
		$search_field_where = array("Id_Hak_Akses", "Kode_Halaman", "Nama_Halaman");
		$search_criteria_where = array("=", "=", "=");
		$search_value_where = array("$u_data_user[Id_Hak_Akses]", "$Kode_Halaman", "$Nama_Halaman");
		$search_connector_where = array("AND", "AND", "");

		$result = $a_data_hak_akses_detail->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

		if ($result['Status'] == "Sukses") {
			$data_hak_akses_detail = $result['Hasil'][0];
		}

		if (isset($data_hak_akses_detail)) {
			//CEK HAK AKSES BACA DATA
			if ($data_hak_akses_detail['Baca_Data'] <> "Iya") {
				echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
				exit();
			}

			//CEK HAK AKSES SIMPAN DATA
			if (isset($_POST['submit_simpan'])) {
				if ($data_hak_akses_detail['Simpan_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menambahkan data');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES UPDATE DATA
			if (isset($_POST['submit_update'])) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			if ((isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah']))) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES HAPUS DATA
			if ((isset($_GET['hapus_data_ke_tong_sampah'])) or (isset($_GET['hapus_data_permanen']))) {
				if ($data_hak_akses_detail['Hapus_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menghapus data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Hapus_Data'</script>";
					exit();
				}
			}
		} else {
			echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
			exit();
		}
	}
} else {
	echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
	exit();
}

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN

//HAPUS DUPLIKAT DATA PADA SELECT OPTION
function Hapus_Duplikat_Data_Pada_Select_Option($array_object, $Hapus_Berdasarkan_Nama_Field) {
    $data = $array_object;
    $uniqueValues = array();
    $filteredData = array();
	
    foreach ($data as $item) {
        $hapus_berdasarkan_field = $item[$Hapus_Berdasarkan_Nama_Field];

        if (!in_array($hapus_berdasarkan_field, $uniqueValues)) {
            $uniqueValues[] = $hapus_berdasarkan_field;
            $filteredData[] = $item;
        }
    }

    $filteredJsonData = $filteredData;
    return $filteredJsonData;
}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
if (isset($_GET['edit'])) {

	$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit = $result['Hasil'];
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	if(isset($_POST['Password'])){
		$_POST['Password_Pendaftar'] = $a_hash->encode($_POST["Password"]);
		$_POST['Password'] = $a_hash_password->hash_password($_POST['Password']);

		$form_field = array("Password", "Password_Pendaftar", "Waktu_Update_Data");

		$form_value = array("$_POST[Password]", "$_POST[Password_Pendaftar]", "$Waktu_Sekarang");

		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
	}else{
		$result['Status'] = "Sukses";
	}
	

	if ($result['Status'] == "Sukses") {

		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI DELETE DATA (DELETE)
if (isset($_GET['hapus_data_ke_tong_sampah'])) {

	$result = $a_data_pendaftar->hapus_data_ke_tong_sampah("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['arsip_data'])) {

	$result = $a_data_pendaftar->arsip_data("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Dipindahkan Ke Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Memindahkan Data Ke Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_arsip'])) {

	$result = $a_data_pendaftar->restore_data_dari_arsip("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Berhasil Di Keluarkan Dari Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengeluarkan Data Dari Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_tong_sampah'])) {

	$result = $a_data_pendaftar->restore_data_dari_tong_sampah("Id_Pendaftar", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Di Restore Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Restore Data Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['hapus_data_permanen'])) {

	$result = $a_data_pendaftar->hapus_data_permanen("Id_Pendaftar", $Get_Id_Primary);
	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus Permanen');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI HITUNG DATA (COUNT)

$count_field_where = array("Status","Id_PPDB");

$count_criteria_where = array("=","=");

$count_connector_where = array("AND","");

//DATA AKTIF
$count_value_where = array("Aktif","$Id_PPDB_Saat_Ini");
$hitung_Aktif = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Aktif = $hitung_Aktif['Hasil'];
//DATA TERARSIP
$count_value_where = array("Terarsip","$Id_PPDB_Saat_Ini");
$hitung_Terarsip = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terarsip = $hitung_Terarsip['Hasil'];
//DATA TERHAPUS (SAMPAH)
$count_value_where = array("Terhapus","$Id_PPDB_Saat_Ini");
$hitung_Terhapus = $a_data_pendaftar->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terhapus = $hitung_Terhapus['Hasil'];
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	if (isset($_GET['filter_status'])) {
		$filter_status = $_GET['filter_status'];
	} else {
		$filter_status = "Aktif";
	}

	$search_field_where = array("Status","Id_PPDB");
	$search_criteria_where = array("=","=");
	$search_value_where = array("$filter_status","$Id_PPDB_Saat_Ini");
	$search_connector_where = array("AND","");

	$nomor = 0;

	$result = $a_data_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_datatable_master = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PPDB
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_ppdb = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN PEKERJAAN ORANG TUA
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_pekerjaan_orang_tua->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_pekerjaan_orang_tua = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN PENGHASILAN ORANG TUA
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_penghasilan_orang_tua->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_penghasilan_orang_tua = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN KENDARAAN YANG DIPAKAI KESEKOLAH
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_kendaraan_yang_dipakai_kesekolah->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_kendaraan_yang_dipakai_kesekolah = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENGATURAN ASAL SEKOLAH
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_pengaturan_asal_sekolah->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_pengaturan_asal_sekolah = $result['Hasil'];
	}
}
?>