<?php
#-----------------------------------------------------------------------------------
#MEMANGGIL CLASS DARI MODEL
$a_data_pendaftar_verifikasi_berkas = new a_data_pendaftar_verifikasi_berkas();
$a_data_pendaftar = new a_data_pendaftar();
$a_data_ppdb = new a_data_ppdb();
$a_pengumuman_pendaftar = new a_pengumuman_pendaftar();
#-----------------------------------------------------------------------------------

//UNTUK REDIRECT
if (isset($_GET['url_kembali'])) {
	$url_kembali = $a_hash->decode_link_kembali($_GET['url_kembali']);
	$kehalaman = "$url_kembali";
} else {
	if (isset($_GET['filter_status'])) {
		$kehalaman = "?menu=" . $_GET['menu'] . "&filter_status=" . $_GET['filter_status'];
	} else {
		$kehalaman = "?menu=" . $_GET['menu'];
	}
}

//UNTUK MENGAMBIL GET ID SEBAGAI VARIABLE ID PRIMARY
if (isset($_GET['id'])) {
	$Get_Id_Primary = $a_hash->decode($_GET['id'], $_GET['menu']);
}

#-----------------------------------------------------------------------------------
#FUNGSI HAK AKSES
$Kode_Halaman = "Verifikasi_Verifikasi_Berkas";
$Nama_Halaman = "Verifikasi Verifikasi Berkas";

$Link_Redirect_Tidak_Diberi_Akses_Baca_Data = "?menu=home";
$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data = $kehalaman;
$Link_Redirect_Tidak_Diberi_Akses_Arsip_Restore_Hapus_Data = $kehalaman;

$search_field_where = array("Status", "Id_Hak_Akses");
$search_criteria_where = array("=", "=");
$search_value_where = array("Aktif", "$u_data_user[Id_Hak_Akses]");
$search_connector_where = array("AND", "");

$result = $a_data_hak_akses->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if ($result['Status'] == "Sukses") {
	$data_hak_akses = $result['Hasil'][0];
}

if (isset($data_hak_akses)) {
	if ($data_hak_akses['Hak_Akses'] == "Super Administrator") {

	} else {
		$search_field_where = array("Id_Hak_Akses", "Kode_Halaman", "Nama_Halaman");
		$search_criteria_where = array("=", "=", "=");
		$search_value_where = array("$u_data_user[Id_Hak_Akses]", "$Kode_Halaman", "$Nama_Halaman");
		$search_connector_where = array("AND", "AND", "");

		$result = $a_data_hak_akses_detail->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

		if ($result['Status'] == "Sukses") {
			$data_hak_akses_detail = $result['Hasil'][0];
		}

		if (isset($data_hak_akses_detail)) {
			//CEK HAK AKSES BACA DATA
			if ($data_hak_akses_detail['Baca_Data'] <> "Iya") {
				echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
				exit();
			}

			//CEK HAK AKSES SIMPAN DATA
			if (isset($_POST['submit_simpan'])) {
				if ($data_hak_akses_detail['Simpan_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menambahkan data');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES UPDATE DATA
			if (isset($_POST['submit_update'])) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			if ((isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah'])) or (isset($_GET['restore_data_dari_tong_sampah']))) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			if (isset($_GET['update_status_verifikasi'])) {
				if ($data_hak_akses_detail['Update_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk mengupdate data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Simpan_Update_Data'</script>";
					exit();
				}
			}

			//CEK HAK AKSES HAPUS DATA
			if ((isset($_GET['hapus_data_ke_tong_sampah'])) or (isset($_GET['hapus_data_permanen']))) {
				if ($data_hak_akses_detail['Hapus_Data'] <> "Iya") {
					echo "<script>alert('Anda tidak diberikan akses untuk menghapus data ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Hapus_Data'</script>";
					exit();
				}
			}
		} else {
			echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
			exit();
		}
	}
} else {
	echo "<script>alert('Anda tidak diberikan akses untuk melihat halaman ini');document.location.href='$Link_Redirect_Tidak_Diberi_Akses_Baca_Data'</script>";
	exit();
}

#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI TAMBAHAN
$folder_penyimpanan_file_berkas_akte_kelahiran = "media/berkas/akte_kelahiran/";
$folder_penyimpanan_file_berkas_kartu_keluarga = "media/berkas/kartu_keluarga/";
$folder_penyimpanan_file_berkas_rapor = "media/berkas/rapor/";
$folder_penyimpanan_file_berkas_kartu_nisn = "media/berkas/kartu_nisn/";
$folder_penyimpanan_file_berkas_skl_ijazah = "media/berkas/skl_ijazah/";
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI SIMPAN DATA (CREATE)
if (isset($_POST['submit_simpan'])) {

	$form_field = array("Id_Pendaftar_Verifikasi_Berkas", "Id_Pendaftar", "Status_Verifikasi_Berkas", "Waktu_Simpan_Data", "Status");

	$form_value = array(NULL, "$_POST[Id_Pendaftar]", "$_POST[Status_Verifikasi_Berkas]", "$Waktu_Sekarang", "Aktif");

	$result = $a_data_pendaftar_verifikasi_berkas->tambah_data($form_field, $form_value);

	if ($result['Status'] == "Sukses") {
		$Id_Pendaftar_Verifikasi_Berkas_Ini = $result['Id'];

		//FUNGSI AMBIL DATA PENDAFTAR
		$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$_POST[Id_Pendaftar]");

		if ($result['Status'] == "Sukses") {
			$edit_data_pendaftar = $result['Hasil'];
		} else {
			$edit_data_pendaftar = null;
		}

		//FUNGSI AMBIL DATA PPDB
		$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

		if ($result['Status'] == "Sukses") {
			$edit_data_ppdb = $result['Hasil'];
		} else {
			$edit_data_ppdb = null;
		}

		//FUNGSI UPLOAD FILE Akta_Kelahiran
		if(isset($_FILES['Akta_Kelahiran'])){
			if ($_FILES['Akta_Kelahiran']['size'] <> 0 && $_FILES['Akta_Kelahiran']['error'] == 0) {
				$post_file_upload = $_FILES['Akta_Kelahiran'];
				$path_file_upload = $_FILES['Akta_Kelahiran']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['Akta_Kelahiran']['size'];
				$nama_file_upload = "berkas_akte_kelahiran_" . $_POST['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $Id_Pendaftar_Verifikasi_Berkas_Ini;
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_akte_kelahiran;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("Akta_Kelahiran");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE Akta_Kelahiran

		//FUNGSI UPLOAD FILE Kartu_Keluarga
		if(isset($_FILES['Kartu_Keluarga'])){
			if ($_FILES['Kartu_Keluarga']['size'] <> 0 && $_FILES['Kartu_Keluarga']['error'] == 0) {
				$post_file_upload = $_FILES['Kartu_Keluarga'];
				$path_file_upload = $_FILES['Kartu_Keluarga']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['Kartu_Keluarga']['size'];
				$nama_file_upload = "berkas_kartu_keluarga_" . $_POST['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $Id_Pendaftar_Verifikasi_Berkas_Ini;
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_kartu_keluarga;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("Kartu_Keluarga");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE Kartu_Keluarga

		//FUNGSI UPLOAD FILE Rapor
		if(isset($_FILES['Rapor'])){
			if ($_FILES['Rapor']['size'] <> 0 && $_FILES['Rapor']['error'] == 0) {
				$post_file_upload = $_FILES['Rapor'];
				$path_file_upload = $_FILES['Rapor']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['Rapor']['size'];
				$nama_file_upload = "berkas_rapor_" . $_POST['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $Id_Pendaftar_Verifikasi_Berkas_Ini;
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_rapor;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("Rapor");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE Rapor

		//FUNGSI UPLOAD FILE Kartu_NISN
		if(isset($_FILES['Kartu_NISN'])){
			if ($_FILES['Kartu_NISN']['size'] <> 0 && $_FILES['Kartu_NISN']['error'] == 0) {
				$post_file_upload = $_FILES['Kartu_NISN'];
				$path_file_upload = $_FILES['Kartu_NISN']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['Kartu_NISN']['size'];
				$nama_file_upload = "berkas_kartu_nisn_" . $_POST['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $Id_Pendaftar_Verifikasi_Berkas_Ini;
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_kartu_nisn;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("Kartu_NISN");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE Kartu_NISN

		//FUNGSI UPLOAD FILE SKL_Ijazah
		if(isset($_FILES['SKL_Ijazah'])){
			if ($_FILES['SKL_Ijazah']['size'] <> 0 && $_FILES['SKL_Ijazah']['error'] == 0) {
				$post_file_upload = $_FILES['SKL_Ijazah'];
				$path_file_upload = $_FILES['SKL_Ijazah']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['SKL_Ijazah']['size'];
				$nama_file_upload = "berkas_skl_ijazah_" . $_POST['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $Id_Pendaftar_Verifikasi_Berkas_Ini;
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_skl_ijazah;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("SKL_Ijazah");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE SKL_Ijazah

		echo "<script>alert('Data Tersimpan');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menyimpan Data');document.location.href='$kehalaman'</script>";
	}
}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI EDIT DATA (READ)
if (isset($_GET['edit'])) {

	$result = $a_data_pendaftar_verifikasi_berkas->baca_data_id("Id_Pendaftar_Verifikasi_Berkas", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {
		$edit = $result['Hasil'];

		//FUNGSI AMBIL DATA PENDAFTAR
		$result = $a_data_pendaftar->baca_data_id("Id_Pendaftar", "$edit[Id_Pendaftar]");

		if ($result['Status'] == "Sukses") {
			$edit_data_pendaftar = $result['Hasil'];
		} else {
			$edit_data_pendaftar = null;
		}

		//FUNGSI AMBIL DATA PPDB
		$result = $a_data_ppdb->baca_data_id("Id_PPDB", "$edit_data_pendaftar[Id_PPDB]");

		if ($result['Status'] == "Sukses") {
			$edit_data_ppdb = $result['Hasil'];
		} else {
			$edit_data_ppdb = null;
		}
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Membaca Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI UPDATE DATA (UPDATE)
if (isset($_POST['submit_update'])) {
	$form_field = array("Status_Verifikasi_Berkas", "Waktu_Update_Data");
	$form_value = array("$_POST[Status_Verifikasi_Berkas]", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {
		//FUNGSI HAPUS FILE Akta_Kelahiran
		if ((isset($_POST['hapus_file_akta_kelahiran'])) AND ($_POST['hapus_file_akta_kelahiran'] == "Iya")){
			$nama_file_upload = $edit['Akta_Kelahiran'];
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_akte_kelahiran;

			$result_hapus_file = $a_hapus_file->hapus_file($nama_file_upload, $folder_penyimpanan_file_upload);
			// if ($result_hapus_file['Status'] == "Sukses") {
				$form_field = array("Akta_Kelahiran");
				$form_value = array("");
				$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
				$form_criteria_where = array("=");
				$form_value_where = array("$Get_Id_Primary");
				$form_connector_where = array("");

				$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			// }
		}

		//FUNGSI HAPUS FILE Kartu_Keluarga
		if ((isset($_POST['hapus_file_kartu_keluarga'])) AND ($_POST['hapus_file_kartu_keluarga'] == "Iya")){
			$nama_file_upload = $edit['Kartu_Keluarga'];
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_kartu_keluarga;

			$result_hapus_file = $a_hapus_file->hapus_file($nama_file_upload, $folder_penyimpanan_file_upload);
			// if ($result_hapus_file['Status'] == "Sukses") {
				$form_field = array("Kartu_Keluarga");
				$form_value = array("");
				$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
				$form_criteria_where = array("=");
				$form_value_where = array("$Get_Id_Primary");
				$form_connector_where = array("");

				$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			// }
		}

		//FUNGSI HAPUS FILE Rapor
		if ((isset($_POST['hapus_file_rapor'])) AND ($_POST['hapus_file_rapor'] == "Iya")){
			$nama_file_upload = $edit['Rapor'];
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_rapor;

			$result_hapus_file = $a_hapus_file->hapus_file($nama_file_upload, $folder_penyimpanan_file_upload);
			// if ($result_hapus_file['Status'] == "Sukses") {
				$form_field = array("Rapor");
				$form_value = array("");
				$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
				$form_criteria_where = array("=");
				$form_value_where = array("$Get_Id_Primary");
				$form_connector_where = array("");

				$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			// }
		}

		//FUNGSI HAPUS FILE Kartu_NISN
		if ((isset($_POST['hapus_file_kartu_nisn'])) AND ($_POST['hapus_file_kartu_nisn'] == "Iya")){
			$nama_file_upload = $edit['Kartu_NISN'];
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_kartu_nisn;

			$result_hapus_file = $a_hapus_file->hapus_file($nama_file_upload, $folder_penyimpanan_file_upload);
			// if ($result_hapus_file['Status'] == "Sukses") {
				$form_field = array("Kartu_NISN");
				$form_value = array("");
				$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
				$form_criteria_where = array("=");
				$form_value_where = array("$Get_Id_Primary");
				$form_connector_where = array("");

				$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			// }
		}

		//FUNGSI HAPUS FILE SKL_Ijazah
		if ((isset($_POST['hapus_file_skl_ijazah'])) AND ($_POST['hapus_file_skl_ijazah'] == "Iya")){
			$nama_file_upload = $edit['SKL_Ijazah'];
			$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_skl_ijazah;

			$result_hapus_file = $a_hapus_file->hapus_file($nama_file_upload, $folder_penyimpanan_file_upload);
			// if ($result_hapus_file['Status'] == "Sukses") {
				$form_field = array("SKL_Ijazah");
				$form_value = array("");
				$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
				$form_criteria_where = array("=");
				$form_value_where = array("$Get_Id_Primary");
				$form_connector_where = array("");

				$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
			// }
		}

		//FUNGSI UPLOAD FILE Akta_Kelahiran
		if(isset($_FILES['Akta_Kelahiran'])){
			if ($_FILES['Akta_Kelahiran']['size'] <> 0 && $_FILES['Akta_Kelahiran']['error'] == 0) {
				$post_file_upload = $_FILES['Akta_Kelahiran'];
				$path_file_upload = $_FILES['Akta_Kelahiran']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['Akta_Kelahiran']['size'];
				$nama_file_upload = "berkas_akte_kelahiran_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit['Id_Pendaftar_Verifikasi_Berkas'];
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_akte_kelahiran;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("Akta_Kelahiran");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE Akta_Kelahiran

		//FUNGSI UPLOAD FILE Kartu_Keluarga
		if(isset($_FILES['Kartu_Keluarga'])){
			if ($_FILES['Kartu_Keluarga']['size'] <> 0 && $_FILES['Kartu_Keluarga']['error'] == 0) {
				$post_file_upload = $_FILES['Kartu_Keluarga'];
				$path_file_upload = $_FILES['Kartu_Keluarga']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['Kartu_Keluarga']['size'];
				$nama_file_upload = "berkas_kartu_keluarga_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit['Id_Pendaftar_Verifikasi_Berkas'];
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_kartu_keluarga;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("Kartu_Keluarga");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE Kartu_Keluarga

		//FUNGSI UPLOAD FILE Rapor
		if(isset($_FILES['Rapor'])){
			if ($_FILES['Rapor']['size'] <> 0 && $_FILES['Rapor']['error'] == 0) {
				$post_file_upload = $_FILES['Rapor'];
				$path_file_upload = $_FILES['Rapor']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['Rapor']['size'];
				$nama_file_upload = "berkas_rapor_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit['Id_Pendaftar_Verifikasi_Berkas'];
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_rapor;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("Rapor");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE Rapor

		//FUNGSI UPLOAD FILE Kartu_NISN
		if(isset($_FILES['Kartu_NISN'])){
			if ($_FILES['Kartu_NISN']['size'] <> 0 && $_FILES['Kartu_NISN']['error'] == 0) {
				$post_file_upload = $_FILES['Kartu_NISN'];
				$path_file_upload = $_FILES['Kartu_NISN']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['Kartu_NISN']['size'];
				$nama_file_upload = "berkas_kartu_nisn_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit['Id_Pendaftar_Verifikasi_Berkas'];
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_kartu_nisn;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("Kartu_NISN");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE Kartu_NISN

		//FUNGSI UPLOAD FILE SKL_Ijazah
		if(isset($_FILES['SKL_Ijazah'])){
			if ($_FILES['SKL_Ijazah']['size'] <> 0 && $_FILES['SKL_Ijazah']['error'] == 0) {
				$post_file_upload = $_FILES['SKL_Ijazah'];
				$path_file_upload = $_FILES['SKL_Ijazah']['name'];
				$ext_file_upload = pathinfo($path_file_upload, PATHINFO_EXTENSION);
				$ukuran_file_upload = $_FILES['SKL_Ijazah']['size'];
				$nama_file_upload = "berkas_skl_ijazah_" . $edit_data_pendaftar['Id_Pendaftar'] . "_" . $edit_data_ppdb['Id_PPDB'] . "_" . $edit['Id_Pendaftar_Verifikasi_Berkas'];
				$folder_penyimpanan_file_upload = $folder_penyimpanan_file_berkas_skl_ijazah;
				$tipe_file_yang_diizikan_file_upload = array("png", "gif", "jpg", "jpeg", "pdf");
				$maksimum_ukuran_file_upload = 100000000;

				$result_upload_file = $a_upload_file->upload_file($post_file_upload, $nama_file_upload, $folder_penyimpanan_file_upload, $tipe_file_yang_diizikan_file_upload, $maksimum_ukuran_file_upload);

				if ($result_upload_file['Status'] == "Sukses") {
					$form_field = array("SKL_Ijazah");
					$form_value = array("$nama_file_upload.$ext_file_upload");
					$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
					$form_criteria_where = array("=");
					$form_value_where = array("$Get_Id_Primary");
					$form_connector_where = array("");

					$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				} else {
				}
			}
		}
		//FUNGSI UPLOAD FILE SKL_Ijazah		
		
		if($edit['Status_Verifikasi_Berkas'] <> $_POST['Status_Verifikasi_Berkas']){
			if($_POST['Status_Verifikasi_Berkas'] == "Sudah Diverifikasi"){
				$Judul_Pengumuman_Untuk_Pendaftar = "Berkas";
				$Isi_Pengumuman_Untuk_Pendaftar = "Selamat, Berkas Anda 'Sudah Diverifikasi' Oleh Admin";

				Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Verifikasi_Berkas","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);

				$result = $a_data_pendaftar->update_waktu_terakhir_semua_status_sudah_diverifikasi("$edit[Id_Pendaftar]");
			}

			if($_POST['Status_Verifikasi_Berkas'] == "Belum Diverifikasi"){
				$Judul_Pengumuman_Untuk_Pendaftar = "Berkas";
				$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Berkas Anda telah di ubah statusnya menjadi 'Belum Diverifikasi' Oleh Admin, pastikan berkas-berkas yang Anda upload sesuai";

				Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Verifikasi_Berkas","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
			}

			if($_POST['Status_Verifikasi_Berkas'] == "Verifikasi Ditolak"){
				$Judul_Pengumuman_Untuk_Pendaftar = "Berkas";
				$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Berkas Anda telah di ubah statusnya menjadi 'Verifikasi Ditolak' Oleh Admin, pastikan berkas-berkas yang Anda upload sesuai";

				Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Verifikasi_Berkas","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
			}
		}
		
		echo "<script>alert('Data Terupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI UPDATE STATUS VERIFIKASI
if (isset($_GET['update_status_verifikasi'])) {
	$form_field = array("Status_Verifikasi_Berkas", "Waktu_Update_Data");
	$form_value = array("$_GET[update_status_verifikasi]", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		$result = $a_data_pendaftar_verifikasi_berkas->baca_data_id("Id_Pendaftar_Verifikasi_Berkas", $Get_Id_Primary);
		if ($result['Status'] == "Sukses") {
			$edit = $result['Hasil'];
		}else{
			$edit = null;
		}

		if($_GET['update_status_verifikasi'] == "Sudah Diverifikasi"){
			$Judul_Pengumuman_Untuk_Pendaftar = "Berkas";
			$Isi_Pengumuman_Untuk_Pendaftar = "Selamat, Berkas Anda 'Sudah Diverifikasi' Oleh Admin";

			Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Verifikasi_Berkas","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);

			$result = $a_data_pendaftar->update_waktu_terakhir_semua_status_sudah_diverifikasi("$edit[Id_Pendaftar]");
		}

		if($_GET['update_status_verifikasi'] == "Belum Diverifikasi"){
			$Judul_Pengumuman_Untuk_Pendaftar = "Berkas";
			$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Berkas Anda telah di ubah statusnya menjadi 'Belum Diverifikasi' Oleh Admin, pastikan berkas-berkas yang Anda upload sesuai";

			Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Verifikasi_Berkas","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
		}

		if($_GET['update_status_verifikasi'] == "Verifikasi Ditolak"){
			$Judul_Pengumuman_Untuk_Pendaftar = "Berkas";
			$Isi_Pengumuman_Untuk_Pendaftar = "Verifikasi Berkas Anda telah di ubah statusnya menjadi 'Verifikasi Ditolak' Oleh Admin, pastikan berkas-berkas yang Anda upload sesuai";

			Simpan_Pengumuman_Pendaftar("$edit[Id_Pendaftar]","Id_Pendaftar_Verifikasi_Berkas","$Get_Id_Primary",$Judul_Pengumuman_Untuk_Pendaftar,$Isi_Pengumuman_Untuk_Pendaftar);
		}

		if($_GET['update_status_verifikasi'] == "Menunggu Verifikasi"){
			$search_field_where = array("Id_Pendaftar","Judul_Pengumuman");
			$search_criteria_where = array("=","=");
			$search_value_where = array("$edit[Id_Pendaftar]","Berkas");
			$search_connector_where = array("AND","");
			$result = $a_pengumuman_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

			if ($result['Status'] == "Sukses") {
				$data_pengumuman_sebelum = $result['Hasil'];
				foreach($data_pengumuman_sebelum as $data_pengumuman_sebelumnya){
					$form_field = array("Waktu_Update_Data","Status");
					$form_value = array("$Waktu_Sekarang","Terhapus");
					$form_field_where = array("Id_Pengumuman_Pendaftar");
					$form_criteria_where = array("=");
					$form_value_where = array("$data_pengumuman_sebelumnya[Id_Pengumuman_Pendaftar]");
					$form_connector_where = array("");

					$result = $a_pengumuman_pendaftar->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);
				}
			}
		}

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$kehalaman'</script>";
	}

}

#FUNGSI UPDATE STATUS VERIFIKASI MASING-MASING BERKAS
#Status_Verifikasi_Akta_Kelahiran
if (isset($_POST['Status_Verifikasi_Akta_Kelahiran_Terima'])) {
	$form_field = array("Status_Verifikasi_Akta_Kelahiran", "Waktu_Update_Data");
	$form_value = array("Sudah Diverifikasi", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}

if (isset($_POST['Status_Verifikasi_Akta_Kelahiran_Tolak'])) {
	$form_field = array("Status_Verifikasi_Akta_Kelahiran", "Waktu_Update_Data");
	$form_value = array("Verifikasi Ditolak", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}
#Status_Verifikasi_Akta_Kelahiran

#Status_Verifikasi_Kartu_Keluarga
if (isset($_POST['Status_Verifikasi_Kartu_Keluarga_Terima'])) {
	$form_field = array("Status_Verifikasi_Kartu_Keluarga", "Waktu_Update_Data");
	$form_value = array("Sudah Diverifikasi", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}

if (isset($_POST['Status_Verifikasi_Kartu_Keluarga_Tolak'])) {
	$form_field = array("Status_Verifikasi_Kartu_Keluarga", "Waktu_Update_Data");
	$form_value = array("Verifikasi Ditolak", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}
#Status_Verifikasi_Kartu_Keluarga

#Status_Verifikasi_Rapor
if (isset($_POST['Status_Verifikasi_Rapor_Terima'])) {
	$form_field = array("Status_Verifikasi_Rapor", "Waktu_Update_Data");
	$form_value = array("Sudah Diverifikasi", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}

if (isset($_POST['Status_Verifikasi_Rapor_Tolak'])) {
	$form_field = array("Status_Verifikasi_Rapor", "Waktu_Update_Data");
	$form_value = array("Verifikasi Ditolak", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}
#Status_Verifikasi_Rapor

#Status_Verifikasi_Kartu_NISN
if (isset($_POST['Status_Verifikasi_Kartu_NISN_Terima'])) {
	$form_field = array("Status_Verifikasi_Kartu_NISN", "Waktu_Update_Data");
	$form_value = array("Sudah Diverifikasi", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}

if (isset($_POST['Status_Verifikasi_Kartu_NISN_Tolak'])) {
	$form_field = array("Status_Verifikasi_Kartu_NISN", "Waktu_Update_Data");
	$form_value = array("Verifikasi Ditolak", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}
#Status_Verifikasi_Kartu_NISN

#Status_Verifikasi_SKL_Ijazah
if (isset($_POST['Status_Verifikasi_SKL_Ijazah_Terima'])) {
	$form_field = array("Status_Verifikasi_SKL_Ijazah", "Waktu_Update_Data");
	$form_value = array("Sudah Diverifikasi", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}

if (isset($_POST['Status_Verifikasi_SKL_Ijazah_Tolak'])) {
	$form_field = array("Status_Verifikasi_SKL_Ijazah", "Waktu_Update_Data");
	$form_value = array("Verifikasi Ditolak", "$Waktu_Sekarang");

	$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
	$form_criteria_where = array("=");
	$form_value_where = array("$Get_Id_Primary");
	$form_connector_where = array("");

	$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Diupdate');document.location.href='$Link_Sekarang'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengupdate Data');document.location.href='$Link_Sekarang'</script>";
	}

}
#Status_Verifikasi_SKL_Ijazah
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI DELETE DATA (DELETE)
if (isset($_GET['hapus_data_ke_tong_sampah'])) {

	$result = $a_data_pendaftar_verifikasi_berkas->hapus_data_ke_tong_sampah("Id_Pendaftar_Verifikasi_Berkas", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['arsip_data'])) {

	$result = $a_data_pendaftar_verifikasi_berkas->arsip_data("Id_Pendaftar_Verifikasi_Berkas", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Dipindahkan Ke Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Memindahkan Data Ke Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_arsip'])) {

	$result = $a_data_pendaftar_verifikasi_berkas->restore_data_dari_arsip("Id_Pendaftar_Verifikasi_Berkas", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Berhasil Di Keluarkan Dari Arsip');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Mengeluarkan Data Dari Arsip');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['restore_data_dari_tong_sampah'])) {

	$result = $a_data_pendaftar_verifikasi_berkas->restore_data_dari_tong_sampah("Id_Pendaftar_Verifikasi_Berkas", $Get_Id_Primary);

	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Di Restore Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Restore Data Dari Tong Sampah');document.location.href='$kehalaman'</script>";
	}

}

if (isset($_GET['hapus_data_permanen'])) {

	$result = $a_data_pendaftar_verifikasi_berkas->hapus_data_permanen("Id_Pendaftar_Verifikasi_Berkas", $Get_Id_Primary);
	if ($result['Status'] == "Sukses") {

		$form_field = array("Waktu_Update_Data");
		$form_value = array("$Waktu_Sekarang");
		$form_field_where = array("Id_Pendaftar_Verifikasi_Berkas");
		$form_criteria_where = array("=");
		$form_value_where = array("$Get_Id_Primary");
		$form_connector_where = array("");

		$result = $a_data_pendaftar_verifikasi_berkas->update_data($form_field, $form_value, $form_field_where, $form_criteria_where, $form_value_where, $form_connector_where);

		echo "<script>alert('Data Berhasil Terhapus Permanen');document.location.href='$kehalaman'</script>";
	} else {
		echo "<script>alert('Terjadi Kesalahan Saat Menghapus Data');document.location.href='$kehalaman'</script>";
	}

}

#-----------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------
#FUNGSI HITUNG DATA (COUNT)

$count_field_where = array("Status","(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_verifikasi_berkas.Id_Pendaftar)");

$count_criteria_where = array("=","=");

$count_connector_where = array("AND","");

//DATA AKTIF
$count_value_where = array("Aktif","$Id_PPDB_Saat_Ini");
$hitung_Aktif = $a_data_pendaftar_verifikasi_berkas->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Aktif = $hitung_Aktif['Hasil'];
//DATA TERARSIP
$count_value_where = array("Terarsip","$Id_PPDB_Saat_Ini");
$hitung_Terarsip = $a_data_pendaftar_verifikasi_berkas->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terarsip = $hitung_Terarsip['Hasil'];
//DATA TERHAPUS (SAMPAH)
$count_value_where = array("Terhapus","$Id_PPDB_Saat_Ini");
$hitung_Terhapus = $a_data_pendaftar_verifikasi_berkas->hitung_data_dengan_filter($count_field_where, $count_criteria_where, $count_value_where, $count_connector_where);
$hitung_Terhapus = $hitung_Terhapus['Hasil'];
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA UNTUK DATATABLE
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	if (isset($_GET['filter_status'])) {
		$filter_status = $_GET['filter_status'];
	} else {
		$filter_status = "Aktif";
	}

	$search_field_where = array("Status","(SELECT Id_PPDB FROM tb_data_pendaftar WHERE Id_Pendaftar = tb_data_pendaftar_verifikasi_berkas.Id_Pendaftar)");
	$search_criteria_where = array("=","=");
	$search_value_where = array("$filter_status","$Id_PPDB_Saat_Ini");
	$search_connector_where = array("AND","");

	$nomor = 0;

	$result = $a_data_pendaftar_verifikasi_berkas->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_datatable_master = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PPDB
if (((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	$search_field_where = array("Status");
	$search_criteria_where = array("=");
	$search_value_where = array("Aktif");
	$search_connector_where = array("");

	$nomor = 0;

	$result = $a_data_ppdb->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result['Status'] == "Sukses") {
		$list_data_ppdb = $result['Hasil'];
	}
}

#-----------------------------------------------------------------------------------
#FUNGSI AMBIL LIST DATA PENDAFTAR
if (!((isset($_GET["tambah"])) or (isset($_GET["edit"])))) {
	//AMBIL DATA RELASI MENJADI ARRAY TABLE tb_"data_pendaftar" ID_PRIMARY "Id_Pendaftar"
	$search_field_where = array("Status");
	$search_criteria_where = array("<>");
	$search_value_where = array("");
	$search_connector_where = array("ORDER BY Waktu_Simpan_Data DESC");

	$result_data_relasi = $a_data_pendaftar->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

	if ($result_data_relasi['Status'] == "Sukses") {
		$array_result_relasi_data_pendaftar = $result_data_relasi['Hasil'];
		$array_hasil_relasi_data_pendaftar;
		foreach ($array_result_relasi_data_pendaftar as $relasi_data_pendaftar) {
			$id_relasi_data_pendaftar = strval($relasi_data_pendaftar['Id_Pendaftar']);
			$array_hasil_relasi_data_pendaftar['Id_Pendaftar'][$id_relasi_data_pendaftar] = $relasi_data_pendaftar;
		}


	} else {
		$array_result_relasi_data_pendaftar = NULL;
	}
	//AMBIL DATA RELASI MENJADI ARRAY TABLE "data_pendaftar" ID_PRIMARY "Id_Pendaftar"
}
?>