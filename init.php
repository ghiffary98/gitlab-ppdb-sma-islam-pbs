<?php
session_start();

if(isset($_GET['tampilkan_error999'])){
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

include "config/config.php";
include "config/database.php";

include "models/core/format_tanggal/format_tanggal.php";
include "models/core/format_angka/format_angka.php";
include "models/core/hash/hash.php";
include "models/core/hash_password/hash_password.php";
include "models/core/ubah_ukuran_gambar/ubah_ukuran_gambar.php";
include "models/core/upload_file/upload_file.php";
include "models/core/hapus_file/hapus_file.php";
include "models/admin/data_hak_akses/model_data_hak_akses.php";
include "models/global/data_ppdb/model_data_ppdb.php";
include "models/global/pengaturan_sekolah/model_pengaturan_sekolah.php";


$a_hash_password = new a_hash_password();
$a_hash = new a_hash();
$a_ubah_ukuran_gambar = new a_ubah_ukuran_gambar();
$a_upload_file = new a_upload_file();
$a_hapus_file = new a_hapus_file();
$a_data_hak_akses = new a_data_hak_akses();
$a_data_hak_akses_detail = new a_data_hak_akses_detail();
$a_data_ppdb = new a_data_ppdb();
$a_pengaturan_sekolah = new a_pengaturan_sekolah();

//MEMBACA PENGATURAN SEKOLAH SAAT INI
$search_field_where = array("Id_Pengaturan_Sekolah");
$search_criteria_where = array("<>");
$search_value_where = array("");
$search_connector_where = array("ORDER BY Id_Pengaturan_Sekolah DESC");
$result = $a_pengaturan_sekolah->baca_data_dengan_filter($search_field_where, $search_criteria_where, $search_value_where, $search_connector_where);

if($result["Status"] == "Sukses"){
    $data_sekolah_saat_ini = $result['Hasil'][0];
}else{
    $data_sekolah_saat_ini = null;
}

//MEMBACA PPDB SAAT INI
if (isset($_COOKIE['Id_PPDB'])) {
    $Id_PPDB_Saat_Ini = $_COOKIE['Id_PPDB'];

    $result = $a_data_ppdb->baca_data_id("Id_PPDB", $Id_PPDB_Saat_Ini);

	if ($result['Status'] == "Sukses") {
		$data_ppdb_saat_ini = $result['Hasil'];
	} else {
		$data_ppdb_saat_ini = null;
	}
}else{
    $Id_PPDB_Saat_Ini = "0";
    $data_ppdb_saat_ini['Judul'] = "";
    $data_ppdb_saat_ini['Tahun_Ajaran'] = "";
}
?>