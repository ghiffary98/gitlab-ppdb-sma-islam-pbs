<div class="sidebar sidebar-style-2">
	<div class="sidebar-wrapper scrollbar scrollbar-inner">
		<div class="sidebar-content">
			<ul class="nav nav-primary">
				<li class="nav-item <?php if (!(isset($_GET['menu'])) or ($_GET['menu'] == "") or ($_GET['menu'] == "dashboard")) {
										echo "active";
									} ?>">
					<a href="dashboard_admin.php" aria-expanded="false">
						<i class="fas fa-home"></i>
						<p>Dashboard</p>
					</a>
				</li>
				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Pendaftar</h4>
				</li>

				<!-- PENDAFTARAN -->
				<li class="nav-item <?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_pendaftar") or ($_GET['menu'] == "pendaftar_verifikasi_pembelian_formulir") or ($_GET['menu'] == "pendaftar_verifikasi_raport") or ($_GET['menu'] == "pendaftar_verifikasi_layanan") or ($_GET['menu'] == "pendaftar_verifikasi_pembayaran_ppdb") or ($_GET['menu'] == "pendaftar_verifikasi_kelulusan"))) {
										echo "active";
									} ?>">
					<a data-toggle="collapse" href="#pendaftaran">
						<i class="fas fa-clipboard-check"></i>
						<p>Verifikasi</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_pendaftar") or ($_GET['menu'] == "pendaftar_verifikasi_pembelian_formulir") or ($_GET['menu'] == "pendaftar_verifikasi_raport") or ($_GET['menu'] == "pendaftar_verifikasi_layanan") or ($_GET['menu'] == "pendaftar_verifikasi_pembayaran_ppdb") or ($_GET['menu'] == "pendaftar_verifikasi_kelulusan"))) {
												echo "show";
											} else {
												echo "show";
											} ?>" id="pendaftaran">
						<ul class="nav nav-collapse">
							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_pendaftar"))) {
											echo "active";
										} ?>">
								<a href="?menu=pendaftar_verifikasi_pendaftar">
									<span class="sub-item">Pendaftar</span>
								</a>
							</li>

							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_pembelian_formulir"))) {
											echo "active";
										} ?>">
								<a href="?menu=pendaftar_verifikasi_pembelian_formulir">
									<span class="sub-item">Pembelian Formulir</span>
								</a>
							</li>

							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_data_diri"))) {
											echo "active";
										} ?>">
								<a href="?menu=pendaftar_verifikasi_data_diri">
									<span class="sub-item">Data Diri</span>
								</a>
							</li>

							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_raport"))) {
											echo "active";
										} ?>">
								<a href="?menu=pendaftar_verifikasi_raport">
									<span class="sub-item">Nilai Raport</span>
								</a>
							</li>

							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_layanan"))) {
											echo "active";
										} ?>">
								<a href="?menu=pendaftar_verifikasi_layanan">
									<span class="sub-item">Program Layanan</span>
								</a>
							</li>

							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_berkas"))) {
											echo "active";
										} ?>">
								<a href="?menu=pendaftar_verifikasi_berkas">
									<span class="sub-item">Berkas</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_pembayaran_ppdb"))) {
											echo "active";
										} ?>">
								<a href="?menu=pendaftar_verifikasi_pembayaran_ppdb">
									<span class="sub-item">Pembayaran PPDB</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "pendaftar_verifikasi_kelulusan"))) {
											echo "active";
										} ?>">
								<a href="?menu=pendaftar_verifikasi_kelulusan">
									<span class="sub-item">Kelulusan</span>
								</a>
							</li>
						</ul>
					</div>
				</li>

				<!-- LAPORAN -->
				<li class="nav-item <?php if (isset($_GET['menu']) and (($_GET['menu'] == "laporan_pendaftar") or ($_GET['menu'] == "laporan_pembelian_formulir") or ($_GET['menu'] == "laporan_lolos_berkas") or ($_GET['menu'] == "laporan_lunas_ppdb") or ($_GET['menu'] == "laporan_pendaftar_diterima"))) {
										echo "active";
									} ?>">
					<a data-toggle="collapse" href="#laporan">
						<i class="fas fa-clipboard-list"></i>
						<p>Laporan</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php if (isset($_GET['menu']) and (($_GET['menu'] == "laporan_pendaftar") or ($_GET['menu'] == "laporan_pembelian_formulir") or ($_GET['menu'] == "laporan_lolos_berkas") or ($_GET['menu'] == "laporan_lunas_ppdb") or ($_GET['menu'] == "laporan_pendaftar_diterima"))) {
												echo "show";
											} ?>" id="laporan">
						<ul class="nav nav-collapse">
							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "laporan_pendaftar"))) {
											echo "active";
										} ?>">
								<a href="?menu=laporan_pendaftar">
									<span class="sub-item">Rekap Pendaftar</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "laporan_pembelian_formulir"))) {
											echo "active";
										} ?>">
								<a href="?menu=laporan_pembelian_formulir">
									<span class="sub-item">Rekap Pembelian Formulir</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "laporan_lolos_berkas"))) {
											echo "active";
										} ?>">
								<a href="?menu=laporan_lolos_berkas">
									<span class="sub-item">Rekap Lolos Berkas</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "laporan_lunas_ppdb"))) {
											echo "active";
										} ?>">
								<a href="?menu=laporan_lunas_ppdb">
									<span class="sub-item">Rekap Lunas PPDB</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and (($_GET['menu'] == "laporan_pendaftar_diterima"))) {
											echo "active";
										} ?>">
								<a href="?menu=laporan_pendaftar_diterima">
									<span class="sub-item">Rekap Pendaftar Diterima</span>
								</a>
							</li>
						</ul>
					</div>
				</li>


				<!-- DATA SISWA -->

				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Data Siswa</h4>
				</li>

				<li class="nav-item <?php if (isset($_GET['menu']) and (($_GET['menu'] == "data_siswa_master") or ($_GET['menu'] == "export_data_siswa_format_8355") or ($_GET['menu'] == "export_data_siswa_format_raport") or ($_GET['menu'] == "export_data_siswa_format_all"))) {
										echo "active";
									} ?>">
					<a data-toggle="collapse" href="#datasiswa">
						<i class="fas fa-id-card"></i>
						<p>Data Siswa</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php if (isset($_GET['menu']) and (($_GET['menu'] == "data_siswa_master") or ($_GET['menu'] == "export_data_siswa_format_8355") or ($_GET['menu'] == "export_data_siswa_format_raport") or ($_GET['menu'] == "export_data_siswa_format_all"))) {
												echo "show";
											} ?>" id="datasiswa">
						<ul class="nav nav-collapse">
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "data_siswa_master")) {
											echo "active";
										} ?>">
								<a href="?menu=data_siswa_master">
									<span class="sub-item">Data Siswa (Master)</span>
								</a>
							</li>

							<li>
								<a data-toggle="collapse" href="#subnav1">
									<span class="sub-item">Export to Excel</span>
									<span class="caret"></span>
								</a>
								<div class="collapse <?php if (isset($_GET['menu']) and (($_GET['menu'] == "data_siswa_master") or ($_GET['menu'] == "export_data_siswa_format_8355") or ($_GET['menu'] == "export_data_siswa_format_raport") or ($_GET['menu'] == "export_data_siswa_format_all"))) {
															echo "show";
														} ?>" id="subnav1">
									<ul class="nav nav-collapse subnav">
										<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "export_data_siswa_format_8355")) {
														echo "active";
													} ?>">
											<a href="?menu=export_data_siswa_format_8355">
												<span class="sub-item">Format 8355</span>
											</a>
										</li>
										<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "export_data_siswa_format_all")) {
														echo "active";
													} ?>">
											<a href="?menu=export_data_siswa_format_all">
												<span class="sub-item">Format Asli</span>
											</a>
										</li>
										<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "export_data_siswa_format_raport")) {
														echo "active";
													} ?>">
											<a href="?menu=export_data_siswa_format_raport">
												<span class="sub-item">Format Raport</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</li>

				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Pengumuman</h4>
				</li>

				<li class="nav-item <?php if (isset($_GET['menu']) and (($_GET['menu'] == "pengumuman_umum") or ($_GET['menu'] == "pengumuman_siswa"))) {
										echo "active";
									} ?>">
					<a data-toggle="collapse" href="#pengumuman">
						<i class="fas fa-bullhorn"></i>
						<p>Pengumuman</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php if (isset($_GET['menu']) and (($_GET['menu'] == "pengumuman_umum") or ($_GET['menu'] == "pengumuman_siswa"))) {
												echo "show";
											} ?>" id="pengumuman">
						<ul class="nav nav-collapse">
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pengumuman_umum")) {
											echo "active";
										} ?>">
								<a href="?menu=pengumuman_umum">
									<span class="sub-item">Umum</span>
								</a>
							</li>
						</ul>
					</div>
				</li>

				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Pengaturan</h4>
				</li>

				<!-- PENGATURAN -->
				<li class="nav-item <?php if (isset($_GET['menu']) and (($_GET['menu'] == "data_pengguna") or ($_GET['menu'] == "data_hak_akses") or ($_GET['menu'] == "data_ppdb") or ($_GET['menu'] == "pengaturan_sekolah") or ($_GET['menu'] == "informasi_ppdb") or ($_GET['menu'] == "pengaturan_data_pekerjaan_orang_tua") or ($_GET['menu'] == "pengaturan_data_penghasilan_orang_tua") or ($_GET['menu'] == "pengaturan_data_asal_sekolah") or ($_GET['menu'] == "pengaturan_data_kendaraan_yang_dipakai_kesekolah")  or ($_GET['menu'] == "backup_and_restore"))) {
										echo "active";
									} ?>">
					<a data-toggle="collapse" href="#pengaturan">
						<i class="fas fa-cogs"></i>
						<p>Pengaturan</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php if (isset($_GET['menu']) and (($_GET['menu'] == "data_pengguna") or ($_GET['menu'] == "data_hak_akses") or ($_GET['menu'] == "data_ppdb") or ($_GET['menu'] == "pengaturan_sekolah") or ($_GET['menu'] == "informasi_ppdb") or ($_GET['menu'] == "pengaturan_data_pekerjaan_orang_tua") or ($_GET['menu'] == "pengaturan_data_penghasilan_orang_tua") or ($_GET['menu'] == "pengaturan_data_asal_sekolah") or ($_GET['menu'] == "pengaturan_data_kendaraan_yang_dipakai_kesekolah")  or ($_GET['menu'] == "backup_and_restore"))) {
												echo "show";
											} ?>" id="pengaturan">
						<ul class="nav nav-collapse">
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "data_pengguna")) {
											echo "active";
										} ?>">
								<a href="?menu=data_pengguna">
									<span class="sub-item">List Admin</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "data_hak_akses")) {
											echo "active";
										} ?>">
								<a href="?menu=data_hak_akses">
									<span class="sub-item">Hak Akses</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "data_ppdb")) {
											echo "active";
										} ?>">
								<a href="?menu=data_ppdb">
									<span class="sub-item">Data PPDB</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pengaturan_sekolah")) {
											echo "active";
										} ?>">
								<a href="?menu=pengaturan_sekolah">
									<span class="sub-item">Informasi SMA Islam PBS</span>
								</a>
							</li>
							<hr>

							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pengaturan_data_asal_sekolah")) {
											echo "active";
										} ?>">
								<a href="?menu=pengaturan_data_asal_sekolah">
									<span class="sub-item">Pengaturan List Asal Sekolah</span>
								</a>
							</li>

							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pengaturan_data_pekerjaan_orang_tua")) {
											echo "active";
										} ?>">
								<a href="?menu=pengaturan_data_pekerjaan_orang_tua">
									<span class="sub-item">Pengaturan Pekerjaan Orang Tua</span>
								</a>
							</li>

							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pengaturan_data_penghasilan_orang_tua")) {
											echo "active";
										} ?>">
								<a href="?menu=pengaturan_data_penghasilan_orang_tua">
									<span class="sub-item">Pengaturan Penghasilan Orang Tua</span>
								</a>
							</li>

							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pengaturan_data_kendaraan_yang_dipakai_kesekolah")) {
											echo "active";
										} ?>">
								<a href="?menu=pengaturan_data_kendaraan_yang_dipakai_kesekolah">
									<span class="sub-item">Pengaturan Kendaraan Yang Dipakai Kesekolah</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>