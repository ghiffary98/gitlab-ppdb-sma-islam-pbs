<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <?php echo $data_sekolah_saat_ini['Nama_Sekolah'] ?>
                    </a>
                </li>
                <li class="nav-item d-none">
                    <a class="nav-link" href="#">
                        Help
                    </a>
                </li>
                <li class="nav-item d-none">
                    <a class="nav-link" href="#">
                        Licenses
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright ml-auto">
            Copyright &copy; 2023. All Right Reserved</a>
        </div>
    </div>
</footer>