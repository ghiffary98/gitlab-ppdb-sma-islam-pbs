<div class="sidebar sidebar-style-2">
	<div class="sidebar-wrapper scrollbar scrollbar-inner">


		<div class="sidebar-content">

			<div class="user">
				<div class="avatar-sm float-left mr-2">
					<span class="px-2 py-1 avatar-title rounded-circle bg-warning"> <?php echo $initials ?> </span>
				</div>
				<div class="info">

					<span class="text-dark">
						<b><?php echo $userFullName ?></b>
						<h6><?php echo $u_data_user['Nomor_Pendaftaran'] ?></h6>
					</span>

					<div class="clearfix"></div>
				</div>
			</div>

			<ul class="nav nav-primary">
				<li class="nav-item <?php if (!(isset($_GET['menu'])) or ($_GET['menu'] == "") or ($_GET['menu'] == "home")) {
										echo "active";
									} ?>">
					<a href="?menu=home" class="collapsed" aria-expanded="false">
						<i class="fas fa-home"></i>
						<p>Dashboard</p>

					</a>
				</li>
				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Menu Utama</h4>
				</li>

				<!-- PENDAFTARAN -->
				<li class="nav-item <?php if (isset($_GET['menu']) and (($_GET['menu'] == "pembelian_formulir") or ($_GET['menu'] == "verifikasi_data_diri") or ($_GET['menu'] == "verifikasi_raport") or ($_GET['menu'] == "pilih_layanan") or ($_GET['menu'] == "verifikasi_berkas") or ($_GET['menu'] == "pembayaran_ppdb"))) {
										echo "active";
									} ?>">
					<a data-toggle="collapse" href="#pendaftaran">
						<i class="fas fa-clipboard-check"></i>
						<p>Pendaftar</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php if (isset($_GET['menu']) and (($_GET['menu'] == "pembelian_formulir") or ($_GET['menu'] == "verifikasi_data_diri") or ($_GET['menu'] == "verifikasi_raport") or ($_GET['menu'] == "pilih_layanan") or ($_GET['menu'] == "verifikasi_berkas") or ($_GET['menu'] == "pembayaran_ppdb"))) {
												echo "show";
											} else {
												echo "show";
											} ?>" id="pendaftaran">
						<ul class="nav nav-collapse">
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pembelian_formulir")) {
											echo "active";
										} ?>">
								<a href="?menu=pembelian_formulir">
									<span class="sub-item">Pembelian Formulir</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "verifikasi_data_diri")) {
											echo "active";
										} ?>">
								<a href="?menu=verifikasi_data_diri">
									<span class="sub-item">Data Diri</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "verifikasi_raport")) {
											echo "active";
										} ?>">
								<a href="?menu=verifikasi_raport">
									<span class="sub-item">Nilai Raport</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pilih_layanan")) {
											echo "active";
										} ?>">
								<a href="?menu=pilih_layanan">
									<span class="sub-item">Pilih Layanan</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "verifikasi_berkas")) {
											echo "active";
										} ?>">
								<a href="?menu=verifikasi_berkas">
									<span class="sub-item">Berkas</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pembayaran_ppdb")) {
											echo "active";
										} ?>">
								<a href="?menu=pembayaran_ppdb">
									<span class="sub-item">Pembayaran PPDB</span>
								</a>
							</li>
						</ul>
					</div>
				</li>

				<!-- LAPORAN -->
				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Pengumuman</h4>
				</li>

				<li class="nav-item <?php if (isset($_GET['menu']) and (($_GET['menu'] == "pengumuman_kelulusan"))) {
										echo "active";
									} ?>">
					<a data-toggle="collapse" href="#pengumuman">
						<i class="fas fa-volume-up"></i>
						<p>Pengumuman</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php if ((isset($_GET['menu']) and (($_GET['menu'] == "pengumuman_kelulusan"))) or (isset($_GET['menu']) and (($_GET['menu'] == "pengumuman_pendaftar")))) {
												echo "show";
											} ?>" id="pengumuman">
						<ul class="nav nav-collapse">
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pengumuman_kelulusan")) {
											echo "active";
										} ?>">
								<a href="?menu=pengumuman_kelulusan">
									<span class="sub-item">Kelulusan</span>
								</a>
							</li>
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "pengumuman_pendaftar")) {
											echo "active";
										} ?>">
								<a href="?menu=pengumuman_pendaftar">
									<span class="sub-item">Pendaftar</span>
								</a>
							</li>
						</ul>
					</div>
				</li>

				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Pengaturan</h4>
				</li>

				<!-- PENGATURAN -->
				<li class="nav-item <?php if (isset($_GET['menu']) and (($_GET['menu'] == "reset_password"))) {
										echo "active";
									} ?>">
					<a data-toggle="collapse" href="#pengaturan">
						<i class="fas fa-cog"></i>
						<p>Pengaturan</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php if (isset($_GET['menu']) and (($_GET['menu'] == "reset_password"))) {
												echo "show";
											} ?>" id="pengaturan">
						<ul class="nav nav-collapse">
							<li class="<?php if (isset($_GET['menu']) and ($_GET['menu'] == "reset_password")) {
											echo "active";
										} ?>">
								<a href="?menu=reset_password">
									<span class="sub-item">Ubah Password</span>
								</a>
							</li>
						</ul>
					</div>
				</li>

			</ul>
		</div>
	</div>
</div>