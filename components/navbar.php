<div class="main-header">
	<!-- Logo Header -->
	<div class="logo-header" data-background-color="blue">
		<?php if (strpos(parse_url($Link_Sekarang, PHP_URL_PATH), "dashboard_admin.php") !== false) { ?>
			<a href="#" class="logo text-white" data-toggle="modal" data-target="#Modal_Ubah_PPDB">
				<b><?php echo $data_ppdb_saat_ini['Judul'] ?> &nbsp; <i class="fas fa-arrow-circle-down"></i> </b>
			</a>
		<?php } else { ?>
			<a href="dashboard.php" class="logo text-white">
				<b><?php echo $data_ppdb_saat_ini['Judul'] ?></b>
			</a>
		<?php } ?>

		<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon">
				<i class="icon-menu"></i>
			</span>
		</button>
		<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
		<div class="nav-toggle">
			<button class="btn btn-toggle toggle-sidebar">
				<i class="icon-menu"></i>
			</button>
		</div>
	</div>
	<!-- End Logo Header -->

	<!-- Navbar Header -->
	<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

		<div class="container-fluid">
			<img src="assets/images/logo/logo-pb-soedirman-light2.png" alt="navbar brand" class="navbar-brand" style="height: 50px;">
			<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
				<li class="nav-item">
					<font class="text-white"><b><?php echo $u_data_user['Nama_Lengkap'] ?></b></font> &nbsp;&nbsp;
				</li>
				<li class="nav-item dropdown hidden-caret">
					<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="true">
						<div class="flex-1">
							<div class="pt-1 pb-1 pl-2 pr-2 avatar-title rounded-circle bg-warning">
								<?php echo substr($initials, 0, 1) ?>
							</div>
						</div>
					</a>
					<ul class="dropdown-menu dropdown-user animated fadeIn">
						<div class="dropdown-user-scroll scrollbar-outer">
							<li>
								<div class="user-box">
									<div class="avatar-lg">
										<span class="px-2 py-1 avatar-title rounded-circle bg-warning"> <?php echo $initials ?> </span>
									</div>
									<div class="u-text">
										<h4><?php echo $u_data_user['Nama_Lengkap'] ?></h4>
										<p class="text-muted"><?php echo $u_data_user['Email'] ?></p>
										<p class="text-muted"><?php if (isset($u_data_user['Nomor_Pendaftaran'])) {
																	echo $u_data_user['Nomor_Pendaftaran'];
																} ?></p>
										<div class="d-none ">
											<a href="?menu=verifikasi_data_diri" class="btn btn-xs btn-primary btn-xs">View Profile</a>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="?menu=reset_password"><i class="fas fa-key"></i> &nbsp; Ubah Password</a>
							</li>
							<li>
								<div class="dropdown-divider"></div>
								<?php if (strpos(parse_url($Link_Sekarang, PHP_URL_PATH), "dashboard_admin.php") !== false) { ?>
									<a class="dropdown-item text-danger" href="logout_admin.php" onclick="return confirm('Anda yakin ingin keluar dari halaman ini?');"><i class="fas fa-power-off"></i> &nbsp; Logout</a>
								<?php } else { ?>
									<a class="dropdown-item text-danger" href="logout.php" onclick="return confirm('Anda yakin ingin keluar dari halaman ini?');"><i class="fas fa-power-off"></i> &nbsp; Logout</a>
								<?php } ?>
							</li>
						</div>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
	<!-- End Navbar -->
</div>
<?php

// Set CORS headers
header("Access-Control-Allow-Origin: http://127.0.0.1/");
header("Access-Control-Allow-Methods: GET, OPTIONS");

// Check if it's a preflight request (OPTIONS request)
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	header("Access-Control-Allow-Headers: Content-Type");
	exit();
}

?>
<?php if (strpos(parse_url($Link_Sekarang, PHP_URL_PATH), "dashboard_admin.php") !== false) { ?>

	<div class="modal fade" id="Modal_Ubah_PPDB" tabindex="-1" role="dialog" aria-labelledby="Modal_Ubah_PPDBLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="Modal_Ubah_PPDBLabel">Pilih PPDB</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>
									PPDB
									</td>
								<th align="center">
									<center>
										Aksi
									</center>
									</td>
							</tr>
						</thead>
						<tbody>
							<?php
							if ((isset($list_data_ppdb))) {
								foreach ($list_data_ppdb as $data) {
									$nomor++; ?>
									<form method="POST">
										<tr>
											<td>
												<b><?php echo $data['Judul'] ?></b>
												-
												<?php echo $data['Tahun_Ajaran'] ?>
												<br>
												<?php if ($data['Status_PPDB'] == "Aktif") { ?>
													<span class="badge badge-success"><?php echo $data['Status_PPDB'] ?></span>
												<?php } else { ?>
													<span class="badge badge-danger"><?php echo $data['Status_PPDB'] ?></span>
												<?php } ?>
											</td>
											<td align="center">
												<?php if ($Id_PPDB_Saat_Ini == $data['Id_PPDB']) { ?>
													<i class="fas fa-check"></i>
												<?php } else { ?>
													<input type="hidden" readonly name="Id_PPDB" value="<?php echo $data['Id_PPDB'] ?>">
													<button type="submit" name="Pindah_PPDB" class="btn btn-primary btn-xs">Pilih</button>
												<?php } ?>
											</td>
										</tr>
									</form>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php } ?>